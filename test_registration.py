"""
Test for the registration algorithm
"""
import platform
from os.path import exists
import time

from scipy.ndimage import rotate

from SimpleITK import (
    GetArrayFromImage,
    GetImageFromArray,
    ReadImage,
    WriteImage
    )

from zbi.handling import DataHandling
from zbi import processing

from zbi.scripts.registration import registration, get_affine_matrix

def test_registration(
        pathin: str,
        pathout: str
        ):
    """
    Test registration algorithm.

    Parameters
    ----------
    pathin: str
        Path to the image to register

    pathout: str
        Folder used to save registration result

    Returns
    -------
    None
    """

    dye = DataHandling(pathin, pathout)

    dye.write_step()
    affine_matrix, centroid = get_affine_matrix(
        dye.get_original(),
        pigmented_eyes = False,
        debug=True
        )
    registered = registration(
        array=dye.get_original(),
        matrix_affine= affine_matrix,
        centroid= centroid,
        debug=True)
    if isinstance(type(registered), str):
        print(registered)
        return False
    dye.set_current(registered)

    dye.write_step("affin_etransform_dev")

if platform.system() == "Linux":
    PATHINBASE = "/data/data_for_bio_paper/raw/Chosen_ones/"
    PATHOUT = "/data/hcsProcessing/reg_test/"
elif platform.system() == "Windows":
    PATHINBASE = "D:/work/SL-Data-Test/WT/raw/Chosen_ones/"  
    PATHOUT = "D:/work/SL-Data-Test/WT/reg_test/"
else:
    raise ValueError("The current operating system is not planned to be used yet.")

PATHINS = [
    PATHINBASE + "200108Fa_2144a_5dpf_SL-193-DR_512_Nh_merge_C00.mha",
    PATHINBASE + "200108Fa_2144d_5dpf_SL-193-DR_512_Nh_merge_C00.mha",
    PATHINBASE + "200108Fa_2145g_5dpf_SL-193-DR_512_Nh_merge_C00.mha",
    PATHINBASE + "200108Fa_2145j_5dpf_SL-193-DR_512_Nh_merge_C00.mha",
    PATHINBASE + "200108Fa_2146a_5dpf_SL-193-DR_512_Nh_merge_C00.mha",
    # PATHINBASE + "200108Fa_2146h_5dpf_SL-193-DR_512_Nh_merge_C00.mha",
    # PATHINBASE + "200108Fa_2147j_5dpf_SL-193-DR_512_Nh_merge_C00.mha",
    # PATHINBASE + "200108Fa_2147l_5dpf_SL-193-DR_512_Nh_merge_C00.mha",
    # PATHINBASE + "200117Fa_2148c_5dpf_SL-193-DR_512_Nh_merge_C00.mha",
    # PATHINBASE + "200117Fa_2148j_5dpf_SL-193-DR_512_Nh_merge_C00.mha",
    # PATHINBASE + "200117Fa_2149b_5dpf_SL-193-DR_512_Nh_merge_C00.mha",
    # PATHINBASE + "200117Fa_2149l_5dpf_SL-193-DR_512_Nh_merge_C00.mha",
    ]

for pathin in PATHINS:
    print(pathin)
    test_registration(
        pathin,
        PATHOUT
        )
