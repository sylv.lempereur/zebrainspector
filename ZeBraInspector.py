print("launch ZeBraInspector")

print("import functions from time")
from time import time

start = time()
   
print(time())	

print("import functions from PySide2")
from  PySide2.QtWidgets import QApplication

print(time())

print("import functions from APP")
from APP.WindowMain import MainWindow

print(time())

if __name__ == '__main__':
    print("Import sys")
    from sys import argv, exit
    print(time())

    print("Creation of QApplication")
    app = QApplication(argv)
    print(time())

    print("Creation Of MainWindow")
    mainWin = MainWindow()
    print(time())

    print("Display MainWindow")
    print("total: " +str(time() - start))
    mainWin.show()
    exit(app.exec_())

