from PySide2 import QtCore
from PySide2 import QtGui

from PySide2.QtWidgets import (QLabel, QPushButton, QProgressBar, QLineEdit, QSizePolicy, QWidget, QFileDialog, QTableView, QVBoxLayout,QHBoxLayout,QApplication)
from PySide2.QtCore import QBasicTimer
from GlobVar import GlobVar
from APP.VolReader import VolReader
from SylvainWrapper import SylvainVolumetrie
from pathlib import Path
from time import time
from time import strftime
from time import localtime
from os.path import basename
from os import listdir
from os.path import isfile, join


from APP.Event import Event
from APP import MODEL
from APP.WMessageBox import MessagesIHM
from APP.XLSXexport import addinfo
import os
from openpyxl import load_workbook

############################################################################
#
#
#       THREAD used to load Data // GUI Thread not used
#


class LoaderThread(QtCore.QThread):

    signal_progressbar    = QtCore.Signal(int)
    signal_changeRowColor = QtCore.Signal(int,int)
    signal_Terminated     = QtCore.Signal()
    signal_SendData       = QtCore.Signal(int,int,int,float)

    def __init__(self, SeqFilesToLoad, CallingWindow):
        QtCore.QThread.__init__(self)
        self.SeqFilesToLoad = SeqFilesToLoad.copy()
        self.AskToStop = False
        CallingWindow.signal_AskToStop.connect(self.AAskToStop)

    @QtCore.Slot()
    def AAskToStop(self):
        self.AskToStop = True



    def run(self):
        #MODEL.LoadSequences = copy.deepcopy(self.SeqChannelOrder)   # conserve l'ordre de chargement
        JobList = list(self.SeqFilesToLoad)
        rowInTable = 0
        self.Results = {}

        while rowInTable < len(JobList) and not self.AskToStop:
            try:
                self.signal_changeRowColor.emit(rowInTable,1)
                filename = JobList[rowInTable]

                # lecture
                self.signal_changeRowColor.emit(rowInTable, 2)
                R = VolReader(filename)
                # on extrait le premier volume présent dans le fichier
                rep = R.LoadData(0)
                # on récupère les données 3D
                a, b, Vol3D = rep[0]


                # compute volume & others
                self.signal_changeRowColor.emit(rowInTable, 3)

                values = SylvainVolumetrie(Vol3D)

                self.Results[filename] = values
                self.signal_SendData.emit(rowInTable,values[0],values[1],values[2])
            except:
                self.signal_changeRowColor.emit(rowInTable, 6)

            # affichage interactif

            pcent = ((rowInTable+1) * 100) / len(JobList)
            self.signal_progressbar.emit(pcent)
            self.signal_changeRowColor.emit(rowInTable, 4)

            # next file
            rowInTable += 1

        print(self.Results)
        self.signal_Terminated.emit()

############################################################################
#
#
#       GUI to select and to launch loading process
#
#

class WidgetVolum(QWidget):


    def FitWindowToTable(self):
        self.table.resizeColumnsToContents()
        w = 0
        for j in range(6):
            w += self.table.columnWidth(j)
        self.setMinimumWidth(w + 50)
        self.setMinimumHeight(200)


    def StoreInfoInTable(self,row,INFOS):
        for x in range(len(INFOS)) :
            T = str(INFOS[x])

            self.model.setItem(row, x, QtGui.QStandardItem(T))
            item = self.model.item(row, x)
            item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)


    def ChangeFolder(self):
        dir = QFileDialog.getExistingDirectory(self, "Select  Directory","", QFileDialog.ShowDirsOnly)
        if len(dir) > 0 :
           self.CurrentFolder.setText(dir)


    def AddFilesToList(self):
        # charge les fichiers
        rep = self.CurrentFolder.text()
        if MessagesIHM.CheckFolderProblem(rep) : return
        LFiles = listdir(rep)
        onlyfiles = [join(rep, f) for f in LFiles if isfile(join(rep, f))]


        # filtre les fichiers

        filter = self.MaskForChannel.text()
        self.LFileNames = [l for l in onlyfiles if l.find(filter) >= 0]
        self.InitTable()



    ####################################################################
    #
    #     LOAD DATA - GUI SIDE
    #
    #     mainly the view interactions : progressbar, message
    #     exchange signals with the loader thread

    ##################    SIGNALS

    signal_AskToStop = QtCore.Signal()

    @QtCore.Slot(int, int, int, int)
    def SendData(self, row:int , v1:int ,v2:int ,v3:float ):
        f = basename(self.LFileNames[row])

        self.StoreInfoInTable(row, [f, v1, v2, v3 ])
        self.FitWindowToTable()

        # save to file
        sheetname = "RESULTS"
        filename =  self.xls
        today = strftime("%d %m %Y")
        clock = strftime("%H:%M", localtime())

        HEADERS = ["date", "time", "filename", "larva volume", "white matter volume", "ratio"]

        addinfo(filename, sheetname, HEADERS, [today, clock, f, v1, v2, v3])


    @QtCore.Slot(int)
    def UpdateProgressBar(self, pcentpgrogress : int ):
        self.pbar.setValue(pcentpgrogress)
        self.NewChronoStart()


    @QtCore.Slot(int, int)
    def ChangeRowColor(self, rowk:int, colorid : int):
        if colorid == 0: color = QtCore.Qt.white
        if colorid == 1: color = QtCore.Qt.yellow   # channel 1
        if colorid == 2: color = QtCore.Qt.magenta  # channel 2
        if colorid == 3: color = QtCore.Qt.cyan     # channel 3
        if colorid == 4: color = QtCore.Qt.green
        if colorid == 6: color = QtCore.Qt.red

        for x in range(6):
            item = self.model.item(rowk, x)
            if item is not None:
                item.setBackground(color)
                self.model.setItem(rowk, x, item)
                index = self.model.index(rowk, x)
                self.table.update(index)
                self.table.scrollTo(index)
                self.table.repaint()

    @QtCore.Slot()
    def ProcessFinished(self):
        self.LoadRunning = False
        self.buttonSTOP.setText("STOP")


    # SEND SIGNAL
    def StopLoading(self):
        self.buttonSTOP.setText("Asking to stop")
        self.signal_AskToStop.emit()


    ################   run the loader thread

    def IsFileLocked(self,fname):
        if not os.path.isfile(fname) :
            return False
        try:
            wb = load_workbook(fname)
            wb.save(fname)
        except Exception as info:
            return True
        wb.close()
        return False


    def StartLoad(self):

        if len(self.LFileNames) == 0 :
            MessagesIHM.Critical("Warning", "Please select Files")
            return

        # selectionne fichier xlsx

        QF = QFileDialog(self)
        QF.setFileMode(QFileDialog.AnyFile)
        QF.setNameFilter("excel (*.xlsx)")

        if not QF.exec(): return
        self.xls = QF.selectedFiles()[0]
        if not self.xls.endswith(".xlsx") :
            self.xls += ".xlsx"


        if self.IsFileLocked(self.xls) :
            MessagesIHM.Critical('Message', "Can not open xlsx file. Please close this file in excel")
            return


        if not self.LoadRunning:
            self.LoadRunning = True
            self.NewChronoStart()
            self.pbar.setValue(0)


            self.ThreadLoad = LoaderThread(self.LFileNames, self)
            self.ThreadLoad.signal_progressbar.connect(self.UpdateProgressBar)
            self.ThreadLoad.signal_changeRowColor.connect(self.ChangeRowColor)
            self.ThreadLoad.signal_Terminated.connect(self.ProcessFinished)
            self.ThreadLoad.signal_SendData.connect(self.SendData)

            self.ThreadLoad.start()

    def NewChronoStart(self):
        self.T0 = time()

    def timerEvent(self, e):
        msg = "Time spent on last action"

        if self.LoadRunning :
           dt  = str(int(time() - self.T0))
           msg = msg + " - " + dt + " seconds"
           self.TimeSpent.setText(msg)
           self.TimeSpent.repaint()
           self.TimeSpent.update()

        self.TimeSpent.setText( msg )

    ####################################################################
    #
    #     CONSTRUCT


    def InitTable(self):
        for k in range(self.model.columnCount()):
            self.ChangeRowColor(k, 0)
        self.model.clear()
        self.model.setHorizontalHeaderLabels(['Filename             ', 'white matter', 'larva', 'ratio'])

        for i in range(len(self.LFileNames)):
            f = basename(self.LFileNames[i])
            self.StoreInfoInTable(i, [f, "", "", "" ])
        self.FitWindowToTable()



    def InitAll(self):
        self.LoadRunning = False                 # load data thread running
        self.buttonSTOP.setText("STOP")
        self.LFileNames = []
        self.InitTable()



    def ChangeChannel(self,newCHA):
        filter = self.MaskForChannel.text()
        if filter.startswith("C00") or filter.startswith("C01") or filter.startswith("C02") :
            filter = filter[3:]
        filter = newCHA + filter
        self.MaskForChannel.setText(filter)


    def CreateSeparator(self):
        Separator = QWidget(self)
        Separator.setFixedHeight(2)
        Separator.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        Separator.setStyleSheet("background-color: #303030;")
        return Separator


    def __init__(self, *args):
        QWidget.__init__(self, *args)
        self.setWindowIcon(GlobVar.AppIcon)
        self.setWindowTitle("Process Volumetry")

        layout = QVBoxLayout(self)

        ##################################################

        button = QPushButton("Change Folder", self)
        button.clicked.connect(self.ChangeFolder)
        layout.addWidget(button)

        layout3 = QHBoxLayout();
        label3 = QLabel("Current Folder ")
        self.CurrentFolder = QLineEdit(GlobVar.SegFolder)
        layout3.addWidget(label3)
        layout3.addWidget(self.CurrentFolder)
        layout.addLayout(layout3)

        layout.addWidget(self.CreateSeparator())

        ###################################################        FILTERS


        TT = QHBoxLayout()

        TT.addWidget(QLabel("Change Channel Filter ", self))

        button = QPushButton("CHA 0", self)
        button.clicked.connect( lambda: self.ChangeChannel("C00") )
        TT.addWidget(button)

        button = QPushButton("CHA 1", self)
        button.clicked.connect( lambda: self.ChangeChannel("C01") )
        TT.addWidget(button)

        button = QPushButton("CHA 2", self)
        button.clicked.connect(  lambda: self.ChangeChannel("C02") )
        TT.addWidget(button)

        layout.addLayout(TT)

        #######################################################################

        queryLayout2 = QHBoxLayout();
        label2 = QLabel("Filter      ")
        self.MaskForChannel = QLineEdit("C00_segmentation.mha")
        queryLayout2.addWidget(label2)
        queryLayout2.addWidget(self.MaskForChannel)
        layout.addLayout(queryLayout2)

        self.FilterButton = QPushButton("SELECT FILES FROM FILTER", self)
        self.FilterButton.clicked.connect(self.AddFilesToList)
        layout.addWidget(self.FilterButton)

        layout.addWidget(self.CreateSeparator())


        ########################################################################



        self.LoadButton = QPushButton("COMPUTE VOLUMETRY", self)
        self.LoadButton.clicked.connect(self.StartLoad)
        layout.addWidget(self.LoadButton)

        self.pbar = QProgressBar(self)
        self.pbar.setValue(0)
        layout.addWidget(self.pbar)

        self.TimeSpent = QLabel()
        self.TimeSpent.setStyleSheet("QLabel { background-color : white; }")
        self.TimeSpent.setAlignment(QtCore.Qt.AlignCenter);
        layout.addWidget(self.TimeSpent)

        self.buttonSTOP = QPushButton("STOP", self)
        self.buttonSTOP.clicked.connect(self.StopLoading)
        layout.addWidget(self.buttonSTOP)

        layout.addWidget(self.CreateSeparator())


        # Create the QTableView widget and associate to its model

        self.model = QtGui.QStandardItemModel(self)
        self.table = QTableView(self)
        self.table.setModel(self.model)
        self.table.verticalHeader().hide()
        self.table.setMinimumHeight(100)
        self.table.height = 500
        layout.addWidget(self.table)

        self.setLayout(layout)

        self.InitAll()

        self.NewChronoStart()
        self.timer = QBasicTimer()
        self.timer.start(300, self)







