from pathlib import Path
from re import findall


def SlotName(idFish):
    idFish += 1
    name = str(idFish)
    if len(name) < 2 : name = "0" + name
    return name


# filename xxxx_ID009_C002_registration.mha => xxx 9 et 2

def SplitFileName(filename, channelMask, IDMask):
    def RemoveAtEnd(string, end):
        return string[:-len(end)]

    # retire l'extension
    extension = Path(filename).suffix
    filename = RemoveAtEnd(filename,extension)

    # retire segmentation/registration
    type = None
    if filename.endswith("_segmentation") :
        type = "segmentation"
        filename = RemoveAtEnd(filename, "_segmentation")
    if filename.endswith("_registration"):
        type = "registration"
        filename = RemoveAtEnd(filename, "_registration")

    # recherche info sur le channel
    channel = None
    regex = channelMask + "[0-9]+$"
    result = findall(regex, filename)
    if len(result) > 0:
        found = result[-1]
        filename = RemoveAtEnd(filename, found)
        number = found[len(channelMask):]
        channel = int(number)

    # recherche info sur le idFish
    id = None
    regex = IDMask + "[0-9]+$"
    result = findall(regex, filename)
    if len(result) > 0:
        found = result[-1]
        filename = RemoveAtEnd(filename, found)
        number = found[len(IDMask):]
        id = int(number)

    basename = filename

    return (basename, id, channel)




