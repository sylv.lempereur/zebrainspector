from PySide2.QtWidgets import (QLabel, QPushButton, QWidget,  QVBoxLayout,  QHBoxLayout, QApplication)
from PySide2.QtGui import QPixmap
from GlobVar import GlobVar
from APP.img import LIMG
from PySide2 import QtCore

from base64 import b64decode

MMM = ""  # message info pour le user
_survive = []  # pour eviter un GC, il faut conserver le np array support du QImage, sinon, boum


####################################
#
#   INTERFACE

LQPixmap = []
LLL = []
maxS = (10,10)
CurImg = 0


class WidgetHelp(QWidget):


    ###############################################
    #
    #             CTR

    def LoadAllImg(self):
        if len(LQPixmap) > 0 : return

        for i in range(len(LIMG)):
            uuencoded = LIMG[i]
            pm = QPixmap()
            buffer = b64decode(uuencoded)
            LLL.append(buffer)
            pm.loadFromData(buffer)
            LQPixmap.append(pm)

        LW = []
        LH = []

        for qpix in LQPixmap :
            LW.append(qpix.width())
            LH.append(qpix.height())

        global maxS
        maxS = (max(LW),max(LH))



    def LoadIMG(self,id):
        self.IMG1.setPixmap(LQPixmap[id])

        #self.IMG1.setMinimumWidth(self.pm.width())
        #self.IMG1.setMinimumHeight(self.pm.height())
        #self.adjustSize()


    def ShowImage(self,o):
        global CurImg
        CurImg = (CurImg + o) % len(LIMG)
        self.LoadIMG(CurImg)


    def __init__(self):
        QWidget.__init__(self)

        self.setWindowIcon(GlobVar.AppIcon)
        self.setWindowTitle("Help Window")

        self.Mainlayout = QVBoxLayout(self)
        self.setLayout(self.Mainlayout)

        # Orange: R 214   V 124   B 28   D67C1C

        self.LoadAllImg()



        self.B1 = QPushButton("<<<<", self)
        self.B1.clicked.connect(lambda: self.ShowImage(-1))
        self.B1.setMinimumHeight(40)
        self.B1.setStyleSheet("background-color: #D67C1C;")

        self.B2 = QPushButton(">>>>", self)
        self.B2.clicked.connect(lambda: self.ShowImage(1))
        self.B2.setMinimumHeight(40)
        self.B2.setStyleSheet("background-color: #D67C1C;")

        self.B3 = QPushButton("CLOSE", self)
        self.B3.clicked.connect(lambda: self.close())
        self.B3.setMinimumHeight(40)
        self.B3.setStyleSheet("background-color: #D67C1C;")


        HL = QHBoxLayout()
        HL.addWidget(self.B1)
        HL.addWidget(self.B2)
        HL.addWidget(self.B3)

        self.Mainlayout.addLayout(HL)

        self.IMG1 = QLabel()
        self.IMG1.setMinimumWidth(maxS[0])
        self.IMG1.setMinimumHeight(maxS[1])
        self.IMG1.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)

        self.Mainlayout.addWidget(self.IMG1)

        # dimensionne la fenetre de tavail par rapport à la taille de l'écran
        r = QApplication.desktop().screenGeometry()
        w, h = r.width(), r.height()
        Rx = 0.6
        ratio = (1 - Rx) / 2
        self.setGeometry(w * ratio, h * ratio, w * Rx, h * Rx)

        global CurImg

        self.LoadIMG(CurImg)



