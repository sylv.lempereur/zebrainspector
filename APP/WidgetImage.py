from PySide2.QtWidgets import QWidget
from PySide2.QtGui import QPainter,  QColor
from PySide2.QtCore import QPoint
from PySide2.QtCore import QRect
import PySide2 as PQt

from APP import MODEL
from APP.Event import Event

from APP.WButtonState import BoutonState


from APP.EventDispatcher import AssociateDispatcher
AssociateDispatcher()



class WidgetImage(QWidget):
    """ Représente le Widget d'affichage de la zone de dessin d'une modalité <br>
    """

    def __init__(self, idChannel, SlotPos, parent=None):
        QWidget.__init__(self, parent=parent)
        self.setMouseTracking(True)
        # https://doc.qt.io/qt-5/qwidget.html#autoFillBackground-prop

        # evite l'effacement du background lors de l'invalidate (preserve la surface)

        self.setAutoFillBackground(False)
        self.setAttribute(PQt.QtCore.Qt.WA_OpaquePaintEvent, True)

        self.idChannel = idChannel
        self.SlotPos = SlotPos

    def SetChannel(self,idChannel):
        self.idChannel = idChannel

    #########################################################################
    #
    #                                EVENT
    #
    #########################################################################

    def mouseDoubleClickEvent(self, event):
        if event.button() == PQt.QtCore.Qt.RightButton:
            pass


    def mousePressEvent(self, event):
        xy = (event.x(), event.y())
        Event.Create.MouseDown( xy[0], xy[1], BoutonState(event), self.SlotPos, self.idChannel ).Dispatch()


    def mouseMoveEvent(self, event):
        # mis a jour de la coord courante
        xy = (event.x(), event.y())
        Event.Create.MouseMove( xy[0], xy[1], BoutonState(event), self.SlotPos, self.idChannel ).Dispatch()



    def mouseReleaseEvent(self,event):
        # mis a jour de la coord courante
        xy = (event.x(), event.y())
        Event.Create.MouseUp( xy[0], xy[1], BoutonState(event), self.SlotPos, self.idChannel ).Dispatch()



    def enterEvent(self,event):
        #QApplication.setOverrideCursor(QCursor(PQt.QtCore.Qt.BlankCursor))
        #self.setCursor(PQt.QtCore.Qt.BlankCursor)
        #Event.Create.OnShow().Dispatch()
        pass


    def leaveEvent(self,event):
        #  self.setCursor(PQt.QtCore.Qt.ArrowCursor)
        pass


    def resizeEvent(self, event=None):
        """  Méthode exécutée à chaque redimensionnement
        """
        Event.Create.ChangeScreenSize(  ).Dispatch()

    def wheelEvent(self, event):
        pass



    def paintEvent(self, T):

        #print("WIMG" + str(self.SlotPos) + "  C" + str(self.idChannel))

        qp = QPainter()
        qp.begin(self)
        #Coul = [PQt.QtCore.Qt.red, PQt.QtCore.Qt.green, PQt.QtCore.Qt.blue]
        #coul = self.palette().color(QWidget.backgroundRole(self))

        qp.fillRect(0, 0, 20000, 20000, QColor(128,128,128))



        #dessine la vignette précalculée au centre
        WScreen = self.width()
        HScreen = self.height()

        idFish = MODEL.GetIdFishFromSlotPos(self.SlotPos)
        Qimg = MODEL.GetImage(idFish, self.idChannel)

        # il y a assez de place pour dessiner l'image
        ratio = 1
        if WScreen >= Qimg.width() and HScreen >= Qimg.height():

           posX = WScreen // 2 - Qimg.width()   // 2
           posY = HScreen // 2 - Qimg.height()  // 2

           qp.drawImage( QPoint(posX,posY), Qimg )

        else :

            if Qimg.width() > 0 and Qimg.height() > 0 and WScreen > 0 and HScreen > 0 :
                rx = WScreen / Qimg.width()
                ry = HScreen / Qimg.height()
                # ratio used to reduce image
                ratio = min(rx, ry)
                Lx = int(Qimg.width() * ratio)
                Ly = int(Qimg.height() * ratio)
                minX = WScreen // 2 - Lx // 2
                minY = HScreen // 2 - Ly // 2
                QR = QRect(minX, minY, Lx, Ly)
                qp.drawImage(QR, Qimg)




        # tool (zoom)

        MODEL.GetCurrentTool().DessinneTool(qp, self.SlotPos, self.idChannel, (WScreen,HScreen,ratio))

        qp.end()


