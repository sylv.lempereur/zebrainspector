from PySide2.QtWidgets import QWidget
from PySide2.QtGui import QPainter, QColor
from psutil import Process, virtual_memory
from os import getpid

class WidgetMemory(QWidget) :


    def __init__(self, height):
        QWidget.__init__(self)
        self.setFixedWidth(height)

        self.setMinimumWidth(300)

    def MiseAJour(self):
        self.repaint()

    def paintEvent(self, T):
        T = dict(virtual_memory()._asdict())
        total = (T["total"] // 100000000) / 10
        usedBySystem = (T["used"] // 100000000) / 10
        if total < 1: total = 1

        process = Process(getpid())
        usedByPython = process.memory_percent()

        qp = QPainter()
        qp.begin(self)

        largX = int( (usedBySystem * self.width()) / total )
        lar2X = int( (usedByPython * self.width()) / total )
        qp.fillRect(0, 0, 8000, 50, QColor(128, 128, 128))
        qp.fillRect(0,0,largX,50,QColor(230,230,230))
        qp.fillRect(0,0,lar2X,50,QColor(200,200,200))