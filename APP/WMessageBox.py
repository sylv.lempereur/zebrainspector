"""
pour eviter des imports QT un peu partout dans le programme
"""
import os.path
from PySide2.QtWidgets import QMessageBox

from GlobVar import GlobVar

from PySide2.QtWidgets import QApplication
import PySide2

class MessagesIHM:

    Yes = "Yes"
    No  = "No"

    @staticmethod
    def WaitCursor():
        QApplication.setOverrideCursor(PySide2.QtGui.QCursor(PySide2.QtCore.Qt.WaitCursor))

    @staticmethod
    def NormalCursor():
       QApplication.setOverrideCursor(PySide2.QtGui.QCursor(PySide2.QtCore.Qt.ArrowCursor))

    @staticmethod
    def Question(msg1,msg2):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Question)
        msg.setWindowTitle(msg1)
        msg.setText(msg2)
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        msg.setWindowIcon(GlobVar.AppIcon)
        r = msg.exec_()
        if r == QMessageBox.Yes : return MessagesIHM.Yes
        return MessagesIHM.No

    @staticmethod
    def Critical(msg1,msg2):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Critical)
        msg.setWindowTitle(msg1)
        msg.setText(msg2)
        msg.setStandardButtons(QMessageBox.Ok)
        msg.setWindowIcon(GlobVar.AppIcon)
        msg.exec_()

    @staticmethod
    def CheckFolderProblem(foldername):
        if os.path.exists(foldername) : return False
        MessagesIHM.Critical("Warning", "Invalid Folder")
        return True




