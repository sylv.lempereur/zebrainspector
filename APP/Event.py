from GlobVar import GlobVar

class Event:

    Dispatcher = None

    ############### Zones devant être réaffichées

    def AskToRepaintAllSlots(self):
        self.LRepaintOrder.append( (-1,-1,-1) )

    def AskToRepaintCHA(self, idCHA):
        self.LRepaintOrder.append( (-1, -1, idCHA) )

    def AskToRepaintSlot(self, x, y):
        self.LRepaintOrder.append( (x,y,-1) )

    def AskToRepaintView(self, x, y, idCHA):
        self.LRepaintOrder.append((x, y, idCHA))

    def LPosToRepaint(self, nbCols, nbLignes, NbActiveCHA):
        L = {}

        for x,y,c in self.LRepaintOrder :
            if x >= nbCols   : continue
            if y >= nbLignes : continue

            if x < 0 : X = [ i for i in range(nbCols)]
            else :     X = [x]

            if y < 0 : Y = [ i for i in range(nbLignes)]
            else :     Y = [ y ]

            if c < 0 : C = [i for i in NbActiveCHA]
            else :     C = [ c ]

            for x in X :
                for y in Y :
                    for c in C :
                       L[(x,y,c)] = True

        return L.keys()

    #######################################

    ForMediateur        =   2   # cet élément concerne le médiateur, inutile de le propager sur TOOLS/SELECTION...
    EndPropagation      =  16   # indique que le traitement de l'event est terminé


    def __init__(self, EventType, Name1=None, Value1=None, Name2=None, Value2=None, Name3=None, Value3=None, Name4=None, Value4=None, Name5=None, Value5=None, Name6=None, Value6=None, ForMediateur = False):
        self.type = EventType
        if Name1 is not None: self.__setattr__(Name1, Value1)
        if Name2 is not None: self.__setattr__(Name2, Value2)
        if Name3 is not None: self.__setattr__(Name3, Value3)
        if Name4 is not None: self.__setattr__(Name4, Value4)
        if Name5 is not None: self.__setattr__(Name5, Value5)
        if Name6 is not None: self.__setattr__(Name6, Value6)

        self.__Flag = 0   # indique les différentes états de traitements du message
        if ForMediateur : self.SetFlag(Event.ForMediateur)
        self.LRepaintOrder = []


    def SetFlag(self,flag):                     self.__Flag |= flag
    def StopDispatch(self):                     self.SetFlag(Event.EndPropagation)
    def HasToBeProcessed(self):                 return not (self.__Flag & Event.EndPropagation)
    def HasToBeSentToMediateur(self)->bool:     return self.__Flag & Event.ForMediateur


    def IsType(self,type : 'Event.Type'):
        return self.type == type

    def IsMouseEvent(self): return self.IsType(Event.Type.MouseMove) or self.IsType(Event.Type.MouseUp) or self.IsType(Event.Type.MouseDown)

    def Dispatch(self):
        if Event.Dispatcher is not None :
           Event.Dispatcher(self)


    class Type:
        ChangePage      = "ChangePage"
        ClearData       = "ClearData"
        LoadData        = "LoadData"
        ChangeCha       = "ChangeCha"
        ChangeRowCol    = "ChangeRowCol"
        Undo            = "Undo"
        Redo            = "Redo"


        ChangeZoom      = "ChangeZoom"
        ChangeTool      = "ChangeTool"
        ChangeScroll    = "ChangeScroll"
        ChangeProj      = "ChangeProj"
        ChangeGamma     = "ChangeGamma"
        ChangeThick     = "ChangeThick"
        ChangeZoomMode  = "ChangeZoomMode"
        ChangeSelected  = "ChangeSelected"
        ChangeRot       = "ChangeRot"
        ChangeRotQual   = "ChangeRotQual"


        MouseDown       = "MouseDown"
        MouseMove       = "MouseMove"
        MouseUp         = "MouseUp"


        ChangeScreenSize = "ChangeScreenSize"
        OnShow           = "OnShow"



    # Qt.NoButton	    0x00000000	The button state does not refer to any button (see QMouseEvent.button()).
    # Qt.LeftButton	    0x00000001	The left button is pressed,
    # Qt.RightButton	0x00000002	The right button.
    # Qt.MidButton	    0x00000004	The middle button.




    class EventCreator:
        # ZEBRE
        def ChangeCha(self,ChaMixer):               return Event(Event.Type.ChangeCha,"ChaMixer",ChaMixer,ForMediateur=True)
        def ChangeRowCol(self,nbRow,nbCol):         return Event(Event.Type.ChangeRowCol,"nbRow",nbRow,"nbCol",nbCol,ForMediateur=True)
        def ChangePage(self,idPage):                return Event(Event.Type.ChangePage,"idPage",idPage,ForMediateur=True)
        def ClearData(self,AskQuestion=True):       return Event(Event.Type.ClearData, "AskQuestion",AskQuestion   , ForMediateur=True)
        def OnShow(self):                           return Event(Event.Type.OnShow, ForMediateur=True)
        def ChangeTool(self,ToolClass,v=None,ChangeButtonState=False):  return Event(Event.Type.ChangeTool,"ToolClass",ToolClass,"val", v,"ChangeButtonState",ChangeButtonState, ForMediateur=True)
        def LoadData(self,idFish,idCha,Data3D):     return Event(Event.Type.LoadData, "idFish", idFish, "idCha", idCha, "Data3D",Data3D ,  ForMediateur=True)
        def ChangeScroll(self,slotPos,value):       return Event(Event.Type.ChangeScroll, "slotPos", slotPos, "value", value ,ForMediateur=True)
        def ChangeProj(self,ProjName):              return Event(Event.Type.ChangeProj, "ProjName", ProjName, ForMediateur=True)
        def ChangeGamma(self,idCha,black,white,Gamma):   return Event(Event.Type.ChangeGamma, "idCha", idCha, "black", black, "white", white, "Gamma", Gamma, ForMediateur=True)
        def ChangeThick(self,thickness):            return Event(Event.Type.ChangeThick, "thickness", thickness)
        def ChangeZoomMode(self,mode):              return Event(Event.Type.ChangeZoomMode, "mode", mode)
        def ChangeSelected(self,slotPos,state):     return Event(Event.Type.ChangeSelected, "slotPos", slotPos, "state", state ,ForMediateur=True)
        def ChangeRot(self,RotAngle):               return Event(Event.Type.ChangeRot, "RotAngle",RotAngle,ForMediateur=True)
        def ChangeRotQual(self, RotQual):           return Event(Event.Type.ChangeRotQual, "RotQual", RotQual,  ForMediateur=True)
        def Undo(self, slotPos):                    return Event(Event.Type.Undo, "slotPos", slotPos, ForMediateur=True)
        def Redo(self, slotPos):                    return Event(Event.Type.Redo, "slotPos", slotPos, ForMediateur=True)

        # MOUSE
        def ChangeScreenSize(self):                         return  Event(Event.Type.ChangeScreenSize, ForMediateur=True)
        def MouseDown(self,x,y,BtState,slotPos,idCha):      return  Event(Event.Type.MouseDown, "x",x,"y",y,"ButtonState",BtState, "slotPos", slotPos, "idCha", idCha)
        def MouseMove(self,x,y,BtState,slotPos,idCha):      return  Event(Event.Type.MouseMove, "x",x,"y",y,"ButtonState",BtState, "slotPos", slotPos, "idCha", idCha)
        def MouseUp(self,x,y,BtState,slotPos,idCha):        return  Event(Event.Type.MouseUp, "x",x,"y",y,"ButtonState",  BtState, "slotPos", slotPos, "idCha", idCha)

    Create = EventCreator()