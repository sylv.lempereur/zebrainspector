from PySide2 import QtCore as Core
from PySide2.QtWidgets import ( QLabel, QPushButton, QHBoxLayout, QScrollBar, QCheckBox, QSizePolicy, QWidget, QVBoxLayout)

from GlobVar import GlobVar

from APP import MODEL
from APP.WidgetImage import WidgetImage
from APP.WidgetSnapshot import WidgetSnapshot
from APP.Event import Event


#############################################################################################################
"""

- L'interface est composée de plusieurs <<Slot>> répartis sur une grille à l'écran.
- Pour répérer un <<Slot>> dans l'interface, on utilise
    - un <<SlotPos>> : coord x,y du slot dans la grille
    - un <<idSlot>>  : itérateur de 0 à nb-1 slots sur la page
- les datas de l'interface sont stockées dans <<WidgetFishes>> et non dans MODEL
- <<<WidgetFishes>> se comporte comme un layout manager
- <<WidgetFishes>> crée 6x3 <<Slots>> et en masque certains lorqu'il y a moins de 18 poissons à l'écran
- Chaque <<Slot>> crée 3 <<WidgeImage>> (vues) et en masque certaines si moins de 3 channels affichés
- les conversions SlotPos <=> idSlot <=> Slot appartiennent au WidgetFishes
- les conversions SlotPos/idSlot <=> idFish appartiennent au MODEL

"""
################################################################################################################

class Slot(QWidget) :

    def paintEvent(self, event):
        QWidget.paintEvent(self, event)

    def RedessineChannel(self,idCHA):
        if idCHA < 0 :
            self.ImgView[0].update()
            self.ImgView[1].update()
            self.ImgView[2].update()
        else :
          if idCHA in self.channelMixer :
            view = self.channelMixer.index(idCHA)
            self.ImgView[view].update()


    def __init__(self, SlotPos):
        QWidget.__init__(self)
        self.SlotPos = SlotPos
        self.channelMixer = [0, 1, 2]   # channel 0 sur vue 0, channel 1 sur vue 1...
        self.ImgView = [None, None, None]

        self.LayoutV = QVBoxLayout(self)

        ##  Création des trois vues

        self.L1 = QHBoxLayout()


        for i in range(3):
            Im = WidgetImage(i, SlotPos )
            Im.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)
            self.L1.addWidget(Im)
            self.ImgView[i] = Im

        self.LayoutV.addLayout(self.L1)

        ##  Barre des menus du slot
        self.L2 = QHBoxLayout()
        self.LabelA1B1  = QLabel("XX", self)
        self.LabelA1B1.setFixedWidth(30)
        self.L2.addWidget(self.LabelA1B1)



        self.ScrollBar = QScrollBar(Core.Qt.Horizontal, self)
        self.ScrollBar.setStyleSheet("QScrollBar:horizontal   {  background-color: #BBBBBB; }")
        Pol_SC = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        Pol_SC.setHorizontalStretch(2)
        self.ScrollBar.setSizePolicy(Pol_SC)
        self.ScrollBar.setMaximumHeight(20)
        self.L2.addWidget(self.ScrollBar)

        self.LabelCoord = QLabel("0", self)
        self.LabelCoord.setFixedWidth(30)
        self.L2.addWidget(self.LabelCoord)

        BB = QPushButton("Snapshot")
        BB.clicked.connect(lambda: self.CreateSnapshotDialog(SlotPos))
        self.L2.addWidget(BB)

        self.CheckBox = QCheckBox("Selected", self)
        Pol_CB = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        Pol_CB.setHorizontalStretch(1)
        self.CheckBox.setSizePolicy(Pol_CB)
        self.CheckBox.setMinimumHeight(20)
        self.L2.addWidget(self.CheckBox)

        self.ScrollBar.valueChanged.connect(lambda: Event.Create.ChangeScroll(SlotPos, self.ScrollBar.value()).Dispatch())
        self.CheckBox.toggled.connect(lambda: Event.Create.ChangeSelected(SlotPos, self.CheckBox.isChecked()).Dispatch())

        BB = QPushButton('Undo')
        BB.clicked.connect(lambda: Event.Create.Undo(SlotPos).Dispatch() )
        BB.setFixedWidth(45)
        self.L2.addWidget(BB)
        BB = QPushButton('Redo')  # \u2192
        BB.clicked.connect(lambda: Event.Create.Redo(SlotPos).Dispatch() )
        BB.setFixedWidth(45)
        self.L2.addWidget(BB)

        self.LayoutV.addLayout(self.L2)

    def SetChannelMixer(self, LVisibleChannels):  # [ 0, 2] => 2 views, channel 0 et 2
        self.channelMixer = LVisibleChannels
        nb = len(self.channelMixer)

        for i in range(3):
            self.ImgView[i].setVisible(i< nb)

        for i in range(nb):
            self.ImgView[i].SetChannel(LVisibleChannels[i])






    def SetPosInfo(self,val):
        self.LabelCoord.setText(str(val))

    def SetLabel(self,info):
        self.LabelA1B1.setText(info)

    def SetScrollRange(self,maxval):
        S = self.ScrollBar
        S.blockSignals(True)
        S.setRange(0,maxval)
        S.blockSignals(False)

    def SetScrollValue(self, val):
        S = self.ScrollBar
        S.blockSignals(True)
        S.setValue(val)
        S.blockSignals(False)

    def SetCheckBox(self,isSelected):
        C = self.CheckBox
        C.blockSignals(True)
        C.setChecked(isSelected)
        C.blockSignals(False)

    def CreateSnapshotDialog(self,SlotPos):
        idSlot = MODEL.FishWindow.SlotPosToSlotId(SlotPos)
        idFish = MODEL.GetIdFishFromSlotPos(SlotPos)
        idPage = MODEL.GetCurrentPage()
        self.WW = WidgetSnapshot(idSlot,idFish,idPage)
        self.WW.show()

############################################################


class WidgetFishes(QWidget) :

    def GetNbCols(self):
        return self.nbCols

    def GetNbLignes(self):
        return self.nbLignes

    def GetChannelMixer(self): # [ 0, 2] => 2 images, channel 0 et 2
        return self.ChannelMixer

    def GetNbChannels(self):
        return len(self.ChannelMixer)

    def GetNbFishPerPage(self):
        return  self.nbCols * self.nbLignes

    def showEvent(self, event):
        Event.Create.OnShow().Dispatch()

    def _isVisible(self, SlotPos):
        x, y = SlotPos
        if x >= self.nbCols : return False
        if y >= self.nbLignes : return False
        return True

    def _ChangeLayout(self):
        for x in range(GlobVar.MaxCol):
            for y in range(GlobVar.MaxRow):
                self.PosToSlot[(x,y)].setVisible(self._isVisible((x,y)))

        self.WidgetRows[1].setVisible(self.nbCols > 1 )
        self.WidgetRows[2].setVisible(self.nbCols > 2 )

    def SetNbRowCol(self,nbRows,nbCols):
        self.nbCols   = nbCols
        self.nbLignes = nbRows
        self._ChangeLayout()


    def SetChannelMixer(self, LVisibleChannels): # [ 1 , 2 ]
        self.ChannelMixer = LVisibleChannels.copy()
        for Slot in self.PosToSlot.values():
            Slot.SetChannelMixer(LVisibleChannels)


    def IdSlotToSlotPos(self, IdSlot):
        x = IdSlot  // self.nbLignes
        y = IdSlot  % self.nbLignes
        return (x,y)

    def SlotPosToSlotId(self, Pos):
        x,y = Pos
        return y + x * self.nbLignes

    def GetSlotByidSlot(self, idSlot):
        P = self.IdSlotToSlotPos(idSlot)
        return self.GetSlotBySlotPos(P)

    def GetSlotBySlotPos(self, P):
        if P not in self.PosToSlot:
            return None
        return self.PosToSlot[P]



    ######  CTR

    def __init__(self, *args):
        QWidget.__init__(self, *args)
        MODEL.FishWindow = self

        self.PosToSlot = {}

        self.WidgetRows = [ None, None, None]

        self.Mainlayout = QHBoxLayout(self)
        self.setLayout(self.Mainlayout)

        # on prépare toutes les vues :

        self.nbCols       = 3
        self.nbLignes     = 6
        self.ChannelMixer = [ 0, 1, 2]


        for x in range(self.nbCols):
           Col = QWidget(self)
           self.WidgetRows[x] = Col
           layout = QVBoxLayout(self)
           Col.setLayout(layout)
           for y in range(self.nbLignes) :
               key = (x,y)
               slot = Slot(key)
               self.PosToSlot[key] = slot
               layout.addWidget(slot)
           self.Mainlayout.addWidget(Col)







