from APP import MODEL
from APP.MEDIATEUR import MEDIATEUR
from APP.Event import Event
from GlobVar import GlobVar


# reste ici car MEDIATEUR et EVENT sont en dépendances cyclique




def Dispatch(event: Event):
    """ Gère la propagation des events dans le logiciel<br>
    NE  doit pas etre appelé directement, faire ===>>> Event.Create.NomEvent().Dispatch() <<<===
    ce module NE DOIT PAS ETRE IMPORTE
    Code mis a part de la classe Event car cette fonction est très importante <br>
    """


    # print(event.type)

    # Les TOOLS sont prioritaires sur le traitement  des events

    if event.HasToBeProcessed() and not event.HasToBeSentToMediateur():
        MODEL.GetCurrentTool().ManageEvent(event)

    if event.HasToBeProcessed():
        MEDIATEUR.Dispatch(event)

    ###############    RE - AFFICHAGE OPTIMISE

    nbCols    = MODEL.FishWindow.GetNbCols()
    nbLignes  = MODEL.FishWindow.GetNbLignes()
    ActiveCHA = MODEL.FishWindow.GetChannelMixer()

    LPosCha = event.LPosToRepaint(nbCols, nbLignes, ActiveCHA)


    for t in LPosCha :
        x, y, c = t
        Slot = MODEL.FishWindow.GetSlotBySlotPos((x, y))
        if Slot is not None :
            # print("DIS ", x,y,c)
            Slot.RedessineChannel(c)



########################################################################################################

# gestion dépendance cyclique

def AssociateDispatcher():
   Event.Dispatcher = Dispatch

########################################################################################################






