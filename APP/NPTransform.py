from time import time
#start = time()
#print("\t\t\t\tImport numpy")
from numpy import (
    empty,
    moveaxis,
    ndarray,
    uint8
    )
#print("\t\t\t\t" + str(time() - start))

#start = time()
#print("\t\t\t\tImport numba")
from numba import jit
#print("\t\t\t\t" + str(time() - start))


# transforme tous les volumes3D


class Transform :

    @staticmethod
    def ReduceForGUI(Vol3D, BinLevel, BitDepth):
        for k in range(BinLevel):
            Vol3D = Transform.ReduceX2(Vol3D)
        Transform.To8bits(Vol3D, BitDepth)
        return Vol3D

    # divise la taille par 2 et passe en 8 bits
    @staticmethod
    def ReduceX2(img: ndarray) :  # (512,2200,400) => (256,1100,200)
        img = _ReduceX2(img)
        return img

    @staticmethod
    def To8bits(img: ndarray,  bitdepth):
        decBit = bitdepth - 8
        img >>= decBit
        img.astype(uint8,copy = False)



    @staticmethod
    def LonguestAxisToIndex(Vol3d,index):
        S = Vol3d.shape
        LonguestAxis = S.index(max(S))
        if LonguestAxis == index :
            return Vol3d
        T = [0, 1, 2]
        T[LonguestAxis] = index        # permute
        T[index]        = LonguestAxis

        return moveaxis(Vol3d, [0, 1, 2], T )

########################################################################################



@jit
def _To8bits(array3D,dec):
    S = array3D.shape
    R = empty(S, dtype=uint8)
    mx = array3D.max()
    if mx == 0 : mx = 1

    for i0 in range(S[0]):
        for i1 in range(S[1]):
            for i2 in range(S[2]):
                t = array3D[i0,i1,i2]
                t = ((t * 255) // mx)
                if t < 0    : t = 0
                if t > 255  : t = 255
                R[i0,i1,i2] = t
    return R
@jit
def _ReduceX2(array3D):
    a,b,c = array3D.shape
    aa = a // 2
    bb = b // 2
    cc = c // 2

    R = empty( (aa,bb,cc), dtype = array3D.dtype)

    for i0 in range(aa):
        for i1 in range(bb):
            for i2 in range(cc):
                p0 = i0 * 2
                p1 = i1 * 2
                p2 = i2 * 2
                v = 0
                v += array3D[p0 + 0, p1 + 0, p2 + 0]
                v += array3D[p0 + 0, p1 + 0, p2 + 1]
                v += array3D[p0 + 0, p1 + 1, p2 + 0]
                v += array3D[p0 + 0, p1 + 1, p2 + 1]
                v += array3D[p0 + 1, p1 + 0, p2 + 0]
                v += array3D[p0 + 1, p1 + 0, p2 + 1]
                v += array3D[p0 + 1, p1 + 1, p2 + 0]
                v += array3D[p0 + 1, p1 + 1, p2 + 1]
                R[i0,i1,i2] = v // 8
    return R






