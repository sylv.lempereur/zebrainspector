from numba import jit
from math import (
    cos as mathcos,
    pow as mathpow,
    radians,
    sin as mathsin
    )

from typing import (
    NewType,
    Tuple
    )
from numpy import (
    empty,
    float32,
    int32,
    maximum,
    ndarray,
    uint8,
    zeros
    )
QUAD= NewType('QUAD', Tuple[int,int,int,int])


from PySide2.QtGui import QImage
from GlobVar import GlobVar

def ForceInside(val,maxval) :
    if val < 0 : val = 0
    if val >= maxval : val = maxval-1
    return val

@jit
def ApplyGamma(Img2D8bits, Black,White,Gamma) :

    S = Img2D8bits.shape
    R = empty(S,dtype=uint8)
    Delta = White - Black

    invGamma = 1 / Gamma

    if Delta == 0 :
        for x in range(S[0]):
            for y in range(S[1]):
                R[x, y] = Black
    else :
        for x in range(S[0]):
            for y in range(S[1]):
                v = Img2D8bits[x,y]
                v = (v - Black) / Delta
                v = mathpow(v,invGamma)
                v = int(v*255)
                if v < 0    : v = 0
                if v > 255  : v = 255
                R[x,y] = v
    return R


@jit
def GenerateSamples(samplesPerDir,cos,sin):

    NbSamples = samplesPerDir * samplesPerDir
    SamplesCoord = zeros((NbSamples,2),dtype=float32)
    p=0
    for ddz in range(samplesPerDir):
        for ddy in range(samplesPerDir):
            dz = ddz / samplesPerDir
            dy = ddy / samplesPerDir
            SamplesCoord[p,0] = dz * cos - dy * sin
            SamplesCoord[p,1] = dz * sin + dy * cos
            p += 1
    return SamplesCoord

@jit
def SuperSample(xx,yy,zz,Vol3D,SamplesCoord,samplesPerDir ):
    NbSamples = samplesPerDir * samplesPerDir
    Lz,Ly,Lx = Vol3D.shape

    sigma = 0
    for k in range(NbSamples):
        zzz = zz + SamplesCoord[k,0]
        yyy = yy + SamplesCoord[k,1]
        if zzz >=0 and zzz < Lz and yyy >= 0 and yyy < Ly:
            sigma += Vol3D[int(zzz),int(yyy),xx]
    sigma = int(sigma/NbSamples)
    return sigma

@jit
def Constant(Vol3D,angle):  # rot constants
    angle = radians(angle)
    Lz, Ly, Lx = Vol3D.shape
    oz = Lz / 2
    oy = Ly / 2
    cos = mathcos(angle)
    sin = mathsin(angle)
    ooz =  oz * (1 - cos) + oy * sin
    ooy = -oz * sin       + oy * (1 - cos)

    return angle,Lz,Ly,Lx,oz,oy,cos,sin,ooz,ooy

@jit
def ProjTransverse(Vol3D,angle,xcoupe, samplesPerDir ):

    angle, Lz, Ly, Lx, oz, oy, cos, sin, ooz, ooy = Constant(Vol3D,angle)

    # generates sampling strategy
    SamplesCoord = GenerateSamples(samplesPerDir,cos,sin)

    #compute rotation
    Img = zeros((Lz,Ly),dtype=uint8)
    x  = xcoupe
    for z in range(Lz):
      for y in range(Ly):
        zz = z * cos - y * sin + ooz
        yy = z * sin + y * cos + ooy

        Img[z, y] = SuperSample(x, yy, zz, Vol3D, SamplesCoord, samplesPerDir )

    return Img

@jit
def ProjSagittale(Vol3D,angle,ycoupe, samplesPerDir):

    angle, Lz, Ly, Lx, oz, oy, cos, sin, ooz, ooy = Constant(Vol3D, angle)

    # generates sampling strategy
    SamplesCoord = GenerateSamples(samplesPerDir, cos, sin)

    #compute rotation
    Img = zeros((Lz,Lx),dtype=uint8)
    y  = ycoupe
    for z in range(Lz):
      for x in range(Lx):
        zz = z * cos - y * sin + ooz
        yy = z * sin + y * cos + ooy

        Img[z,x] = SuperSample(x, yy, zz, Vol3D, SamplesCoord, samplesPerDir )

    return Img

@jit
def ProjDorsale(Vol3D,angle,zcoupe,samplesPerDir):

    angle, Lz, Ly, Lx, oz, oy, cos, sin, ooz, ooy = Constant(Vol3D,angle)

    # generates sampling strategy
    SamplesCoord = GenerateSamples(samplesPerDir,cos,sin)

    #compute rotation
    Img = zeros((Ly,Lx),dtype=uint8)
    z  = zcoupe
    for y in range(Ly):
      for x in range(Lx):
        zz = z * cos - y * sin + ooz
        yy = z * sin + y * cos + ooy

        Img[y,x] = SuperSample(x, yy, zz ,Vol3D, SamplesCoord, samplesPerDir)

    return Img


def VolToSlice(NameProj, angle, pos3D, Data3D, NbLayersStacked, qual) :

    # [z,y,x] avec
    # x axe principal (2202)  celui de la rotation aussi (rotissoire)
    # y : vers le user
    # z : vers le haut


    Lz, Ly, Lx = Data3D.shape
    R = [] # une coupe par thickness

    if  NameProj == GlobVar.VueDorsale :
        for n in range(NbLayersStacked):
            zcoupe = ForceInside(pos3D[0] + n, Lz)
            R.append( ProjDorsale(Data3D, angle, zcoupe, samplesPerDir=qual) )

    if  NameProj == GlobVar.VueSagittale :
        for n in range(NbLayersStacked):
            ycoupe = ForceInside(pos3D[1]+n, Ly)
            R.append( ProjSagittale(Data3D, angle, ycoupe,  samplesPerDir=qual) )

    if NameProj == GlobVar.VueTransverse:
        for n in range(NbLayersStacked):
            xcoupe = ForceInside(pos3D[2]+n, Lx)

            R.append( ProjTransverse(Data3D, angle, xcoupe,  samplesPerDir=qual) )


    Img = maximum.reduce(R)

    return Img

def SliceToImage(Data2D, ParamGamma):
    black, white, Gamma = ParamGamma
    ooo = ApplyGamma(Data2D, black, white, Gamma)
    QImg, Data = RVBToQImage(ooo, ooo, ooo)

    return QImg,Data



def CreateEmptyImage():
    Z = zeros((40,40),dtype=int32)
    for i in range(40):
        Z[i, i] = 255
        Z[i, 39-i] = 255
    return RVBToQImage(Z,Z,Z)

def EmptyVol():
    Z = zeros((40, 40, 40), dtype=uint8)
    for i in range(40):
        for k in range(40):
            Z[i, i, k     ] = 255
            Z[i, 39 - i, k] = 255
            Z[i,k, i] = 255
            Z[i,k, 39 - i] = 255
            Z[k, i, i] = 255
            Z[k, i, 39 - i] = 255

    return Z



@jit(nopython=True)
def __RVBToQImage(IRt, IGt, IBt):  # L1 = L1+L2
    S=IRt.shape
    Z = empty(shape=S, dtype=int32)

    dim1, dim2 = Z.shape
    for y in range(dim1):
        for x in range(dim2):
            Z[y][x] = int(IBt[y][x]) | (int(IGt[y][x]) << 8) | (int(IRt[y][x]) << 16)

    return Z

def RVBToQImage(ImageR : ndarray, ImageG : ndarray, ImageB : ndarray, )->QImage:  # L1 = L1+L2
    """ creation d'un QImage a partir de 3 NPImage """

    NParray32bits = __RVBToQImage(ImageR,ImageG,ImageB)
    dim1,dim2 = ImageB.shape
    QIm = QImage(NParray32bits.data, dim2, dim1, QImage.Format_RGB32)
    return QIm, NParray32bits   # retourne les data pour stocker le pointeur avec l'image afin d'eviter un GC
