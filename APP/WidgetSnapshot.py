from PySide2.QtWidgets import (QLabel, QPushButton, QWidget, QLineEdit , QComboBox, QVBoxLayout, QRadioButton, QFileDialog, QHBoxLayout, QScrollArea, QApplication)
from PySide2.QtGui import QPixmap
from APP.VolReader import VolReader
from APP.NPTransform import Transform
from GlobVar import GlobVar
from datetime import date
from APP import MODEL
from PySide2 import QtCore
from PySide2.QtCore import QBasicTimer

from APP.NPToImage import VolToSlice, SliceToImage
from os.path import join as ospathjoin
from time import time
import APP.GlobFnt as GlobFnt


###########################################################
#
#        COMPUTE  THREAD

MMM = ""    # message info pour le user
_survive = []  # pour eviter un GC, il faut conserver le np array support du QImage, sinon, boum

class LoaderThread(QtCore.QThread):

    signal_SendData       = QtCore.Signal(object,object,object)

    def __init__(self, seq, idFish,quality):
        self.quality = quality
        QtCore.QThread.__init__(self)
        self.seq = seq
        self.idFish = idFish

    def run(self):
        global MMM
        MMM = "Loading in progress - Duration :  "
        R = self.ReloadDataHD8bits()
        MMM = "Load terminated - Computing images - Duration : "
        LIMG = self.TransfromToImage(R)
        
        self.signal_SendData.emit(LIMG[0], LIMG[1], LIMG[2])

    def ReloadDataHD8bits(self):
        SeqChannelOrder = self.seq

        RESULT = [ None ] * GlobVar.MaxChannels

        # on charge les différentes modalités du poisson

        for channelid in range(3) :
            if SeqChannelOrder[channelid] is not None :
               filen, PosInFile = SeqChannelOrder[channelid]
               R = VolReader(filen)
               rep = R.LoadData(PosInFile)
               if len(rep) > 0 and rep[0] is not None :
                   a, b, Data = rep[0]
                   Transform.To8bits(Data, MODEL.GetBitDepth())
                   RESULT[channelid] = Transform.LonguestAxisToIndex(Data, 2)

        return RESULT

    def TransfromToImage(self,ListVol):
        LIMG = [ None, None, None ]
        for i in range(GlobVar.MaxChannels):
            if ListVol[i] is not None:
                P3D = MODEL.GetPosition(self.idFish)
                P3D = [P3D[0] * 4, P3D[1] * 4, P3D[2] * 4]


                T = VolToSlice(MODEL.GetProjName(),MODEL.GetRotAngle(), P3D, ListVol[i] , MODEL.GetThickness() , qual = self.quality)
                Img, Data = SliceToImage(T, MODEL.GetGammaParam(i))
                _survive.append( Data )   # survie tant que la fenetre est ouverte
                LIMG[i] = Img.copy()
        return LIMG


##################################################################################################################
#
#
#              MAIN FRAME
#
#
##################################################################################################################


class WidgetSnapshot(QWidget) :


    @QtCore.Slot(object,object,object )
    def SendData(self,IMG1,IMG2,IMG3):
        self.LIMG = [ IMG1, IMG2, IMG3 ]
  
        self.B1.setEnabled(self.LIMG[0] is not None)
        self.B2.setEnabled(self.LIMG[1] is not None)
        self.B3.setEnabled(self.LIMG[2] is not None)

        if self.LIMG[0] is not None:
            self.ShowImage(0)
        else:
            if self.LIMG[1] is not None:
                self.ShowImage(1)
            else:
                if self.LIMG[2] is not None:
                   self.ShowImage(2)


        self.timer.stop()
        self.button3.setText("Extract Images")
        self.button3.setStyleSheet("")


    def CreateFileNames(self):
        # data
        orientation = GlobVar.VueShortName[MODEL.GetProjName()]
        epaiss = "T" + str(MODEL.GetThickness())
        posAxes = "P" + str(MODEL.GetAxisPos(self.idFish) * 4)
        Rota = "R" + str(MODEL.GetRotAngle())
        MODGamma = []
        for i in range(3):
            BWG = MODEL.GetGammaParam(i)
            txt = "G" + str(BWG[0]) + "_" + str(BWG[1]) + "_" + str(BWG[2])
            MODGamma.append(txt)


        today = date.today()
        day = today.strftime("%Y.%m.%d")
        sample = GlobFnt.SlotName(self.idFish)

        #	la date // numero échantillon C3 D8 // orientation  // ep coupes // n° coupe // M123 modalites // WBGamma
        sep = "_"
        base = day + sep + sample + sep + orientation + sep + posAxes + sep + epaiss + sep + Rota

        prop1 = base + sep + "M1" + sep + MODGamma[0]
        prop2 = base + sep + "M2" + sep + MODGamma[1]
        prop3 = base + sep + "M3" + sep + MODGamma[2]

        self.Edit1.setText(prop1)
        self.Edit2.setText(prop2)
        self.Edit3.setText(prop3)

        infos = []
        infos.append("Date\t\t: " + day)
        infos.append("Sample\t\t: " + sample)
        infos.append("Orientation\t: " + orientation)
        infos.append("Thickness\t\t: " + epaiss)
        infos.append("Axis pos\t\t: " + posAxes)
        infos.append("Rotation\t\t: " + Rota)
        infos.append("Channel\t\t: M1 / M2 / M3")
        infos.append("Gamma\t\t: Black Level / White Level / Gamma value")
        infos.append("")
        infos.append("Filename ")

        for i in range(10)  :
            self.ListMSG[i].setText(infos[i])

    ###############################################
    #
    #             CTR

    def ChangeQual(self, val):
        self.quality   =   val


    def __init__(self, idSlot, idFish, idPage) :
        QWidget.__init__(self)

        self.LIMG = [None, None, None]

        self.setWindowIcon(GlobVar.AppIcon)
        self.setWindowTitle("Taking Snapshot")
        self.idFish = idFish
        self.idSlot = idSlot
        self.idPage = idPage

        self.Mainlayout = QVBoxLayout(self)
        self.setLayout(self.Mainlayout)

        self.ListMSG = []
        for i in range(10):
            w = QLabel("", self)
            self.Mainlayout.addWidget(w)
            self.ListMSG.append(w)

        self.Edit1 = QLineEdit(self)
        self.Edit2 = QLineEdit(self)
        self.Edit3 = QLineEdit(self)

        self.Mainlayout.addWidget(self.Edit1)
        self.Mainlayout.addWidget(self.Edit2)
        self.Mainlayout.addWidget(self.Edit3)

        self.CreateFileNames()

        #####################################################

        self.quality = 5

        '''
        self.llayout = QHBoxLayout()

        self.b1 = QRadioButton("  Fast     -    Low Quality")
        self.b1.toggled.connect(lambda:  self.ChangeQual(3) )
        self.llayout.addWidget(self.b1)

        self.b2 = QRadioButton("Normal     -    Medium Quality")
        self.b2.toggled.connect(lambda: self.ChangeQual(5))
        self.llayout.addWidget(self.b2)

        self.b3 = QRadioButton("  Slow     -    High Quality")
        self.b3.setChecked(True)
        self.b3.toggled.connect(lambda: self.ChangeQual(7))
        self.llayout.addWidget(self.b3)

        self.Mainlayout.addLayout(self.llayout)
        '''

        self.button3 = QPushButton("Extract Images", self)
        self.button3.clicked.connect(self.ComputeImages)
        self.Mainlayout.addWidget(self.button3)

        self.button = QPushButton("Save Images", self)
        self.button.clicked.connect(self.Save)
        self.Mainlayout.addWidget(self.button)



        self.button2 = QPushButton("Close", self)
        self.button2.clicked.connect(lambda: self.close())
        self.Mainlayout.addWidget(self.button2)

        self.setMinimumHeight(200)
        self.setMinimumWidth(400)

        self.B1 = QPushButton(GlobVar.ChannelShortName + " 0", self)
        self.B1.clicked.connect(lambda : self.ShowImage(0))
        self.B2 = QPushButton(GlobVar.ChannelShortName + " 1", self)
        self.B2.clicked.connect(lambda: self.ShowImage(1))
        self.B3 = QPushButton(GlobVar.ChannelShortName + " 2", self)
        self.B3.clicked.connect(lambda: self.ShowImage(2))

        HL = QHBoxLayout()
        HL.addWidget(self.B1)
        HL.addWidget(self.B2)
        HL.addWidget(self.B3)
        self.Mainlayout.addLayout(HL)

        self.IMG1 = QLabel()
        self.scroll = QScrollArea()
        self.scroll.setWidget(self.IMG1)
        self.scroll.setWidgetResizable(True)

        self.Mainlayout.addWidget(self.scroll)

        # dimensionne la fenetre de tavail par rapport à la taille de l'écran
        r = QApplication.desktop().screenGeometry()
        w, h = r.width(), r.height()
        Rx = 0.6
        ratio = (1 - Rx) / 2
        self.setGeometry(w * ratio, h * ratio, w * Rx, h * Rx)

    ###############################################
    #
    #             RUN THREAD  ==>> doit etre lancé en premier

    def ComputeImages(self):
        self.CreateFileNames()
        self.button3.setText("Work in progress, please wait...")
        self.button3.setStyleSheet("background-color: red")
        self.button3.repaint()

        FileInfo = MODEL.HistoryGetFilesInAllSlot()[self.idFish]

        self.ThreadLoad = LoaderThread(FileInfo, self.idFish, self.quality)
        self.ThreadLoad.signal_SendData.connect(self.SendData)
        self.ThreadLoad.start()

        self.t0 = time()
        self.timer = QBasicTimer()
        self.timer.start(1000, self)

    def timerEvent(self, e):
        dt = str(int(time() - self.t0))
        msg = MMM + dt + " seconds"
        self.button3.setText( msg )
        self.button3.repaint()
        self.button3.update()


    ###############################################
    #
    #             AFfichate l'image

    survivePixmap = []
    def ShowImage(self,id):

        if self.LIMG[id] is None : return
        Q = QPixmap.fromImage(self.LIMG[id])
        WidgetSnapshot.survivePixmap.append(Q)    # to avoid GC
        self.IMG1.setPixmap(Q)
        self.IMG1.setScaledContents(False)



    ###############################################
    #
    #             Save img to disk

    def Save(self):
        OutputFolder = QFileDialog.getExistingDirectory(None, "Select Output Folder")
        if len(OutputFolder) == 0 : return

        for i in range(GlobVar.MaxChannels):
            if self.LIMG[i] is not None:
                LEdit = [ self.Edit1, self.Edit2, self.Edit3 ]
                fname = LEdit[i].text() + ".png"
                fullname = ospathjoin(OutputFolder, fname)
                self.LIMG[i].save(fullname, "PNG")









