import PySide2 as PQt

class BoutonState:


    # Qt.NoButton	    0x00000000	The button state does not refer to any button (see QMouseEvent.button()).
    # Qt.LeftButton	    0x00000001	The left button is pressed,
    # Qt.RightButton	0x00000002	The right button.
    # Qt.MidButton	    0x00000004	The middle button.

    Left = 1
    Right = 2
    Middle = 4

    def __init__(self, QtEvent):
        code = 0
        if QtEvent is not None:
            if QtEvent.button() == PQt.QtCore.Qt.LeftButton:
                code |= BoutonState.Left
            if QtEvent.button() == PQt.QtCore.Qt.RightButton:
                code |= BoutonState.Right
            if QtEvent.button() == PQt.QtCore.Qt.MidButton:
                code |= BoutonState.Middle

        self.code = code

    def LeftButtonPressed(self):
        return self.code & BoutonState.Left

    def SetButtonLeft(self):
        self.code = BoutonState.Left

    def RightButtonPressed(self):
        return self.code & BoutonState.Right

    def MiddleButtonPressed(self):
        return self.code & BoutonState.Middle