from APP.WMessageBox import MessagesIHM


class ToolAbstract:

    def ToolName(self):
        return ""

    def __init__(self):
        self.state = 0

    def ManageEvent(self,event:'Event'):
        pass

    def IsToolSelection(self):
        return False

    def DessinneTool(self, qp , SlotPos, idCha, ratio):
        pass

    def IsActive(self)->bool:
        return self.state != 0

    def IsInactive(self) -> bool:
        return self.state == 0

    def Terminate(self):            # appelé pour cloturer l'outil (changement d'outil)
        pass

    def ReStart(self):
        pass


    @staticmethod
    def MessageToolEnCours():
        MessagesIHM.Critical('Message', "Current tool seems to be active, please terminate current action")
        return
