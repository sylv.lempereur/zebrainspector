from typing import Tuple



def ADD(I2A: Tuple[int, int], I2B: Tuple[int, int]) -> Tuple[int, int]:
    """ Retourne A+B """
    return I2A[0] + I2B[0], I2A[1] + I2B[1]


def SUB(I2A: Tuple[int, int], I2B: Tuple[int, int]) -> Tuple[int, int]:
    """ Retourne A-B """
    return I2A[0] - I2B[0], I2A[1] - I2B[1]