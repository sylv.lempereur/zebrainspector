from APP import MODEL
from APP.Event import Event


from typing import NewType,Tuple
QUAD = NewType('QUAD', Tuple[int,int,int,int])

from APP.Tool import ToolAbstract
from GlobVar import GlobVar
from APP.ToolZoom import ToolZoom
from APP.NPTransform import Transform
from APP.WMessageBox import MessagesIHM
from APP.VolReader import VolReader
import APP.GlobFnt as GlobFnt


class MEDIATEUR:
    """ cette classe gère les scénarios associés à des user event
        le médiateur gère les interactions entre la GUI et le MODELE
        Le mediateur doit déclarer les zones invalidées dans l'affichage écran.
        les données invalidées par le scénario sont gérées au niveau du modele
    """

    @staticmethod
    def _UpdateSlotIHM(SlotPos):
        idSlot = MODEL.FishWindow.SlotPosToSlotId(SlotPos)
        Slot = MODEL.FishWindow.GetSlotBySlotPos(SlotPos)
        if Slot is None : return
        idFish = MODEL.GetIdFishFromSlotPos(SlotPos)

        laxis = MODEL.GetAxisLength(idFish)
        Slot.SetScrollRange( laxis)

        paxis = MODEL.GetAxisPos(idFish)
        Slot.SetScrollValue( paxis)

        paxisBeforeBinning = paxis *  MODEL.GetBinFactor()  # donne le numéro du plan dans l'échelle originale
        Slot.SetPosInfo(paxisBeforeBinning)

        isSelected = MODEL.GetSelected(idFish)
        Slot.SetCheckBox(isSelected)

        #slotName = GlobVar.Slot Name(idSlot, MODEL.GetCurrentPage())
        slotName = GlobFnt.SlotName(idFish)
        Slot.SetLabel(slotName)

    @staticmethod
    def _UpdateAllIHM():
        for idSlot in range(MODEL.FishWindow.GetNbFishPerPage()):
            SlotPos = MODEL.FishWindow.IdSlotToSlotPos(idSlot)
            MEDIATEUR._UpdateSlotIHM(SlotPos)

    @staticmethod
    def CheckToolActif():   # verifie qu'un outil n'est pas en cours
        CurTOOL = MODEL.GetCurrentTool()
        #print(CurTOOL)
        if CurTOOL.IsActive():
            ToolAbstract.MessageToolEnCours()
            return True
        return False

    @staticmethod
    def Dispatch(event : Event):

        ###############################   IHM

        if event.HasToBeProcessed() and event.IsType(Event.Type.ChangeRowCol):
            nbRow = event.nbRow
            nbCol = event.nbCol
            if MODEL.FishWindow is not None :
                MODEL.FishWindow.SetNbRowCol(nbRow,nbCol)
                MODEL.ChangePage(0)
                event.AskToRepaintAllSlots()
                event.StopDispatch()

        if event.HasToBeProcessed() and event.IsType(Event.Type.ChangeCha):
            MODEL.FishWindow.SetChannelMixer(event.ChaMixer)
            event.AskToRepaintAllSlots()
            event.StopDispatch()



        if event.HasToBeProcessed() and event.IsType(Event.Type.ChangeRotQual):
            quality = event.RotQual
            MODEL.SetRotQuality(quality)
            event.AskToRepaintAllSlots()
            event.StopDispatch()


        if event.HasToBeProcessed() and event.IsType(Event.Type.ChangeRot) :
            rotangle = event.RotAngle
            MODEL.SetRotAngle(rotangle)
            event.AskToRepaintAllSlots()
            event.StopDispatch()


        if event.HasToBeProcessed() and event.IsType(Event.Type.ChangeSelected ):
            idFish = MODEL.GetIdFishFromSlotPos(event.slotPos)
            MODEL.SetSelected(idFish, event.state)
            MEDIATEUR._UpdateSlotIHM(event.slotPos)
            event.StopDispatch()

        if event.HasToBeProcessed() and event.IsType(Event.Type.ChangeZoomMode):
            MODEL.SetZoomMode(event.mode)
            event.AskToRepaintAllSlots()
            event.StopDispatch()

        if event.HasToBeProcessed() and event.IsType(Event.Type.ChangeThick):
            binFactor = MODEL.GetBinFactor()
            if event.thickness < binFactor :
                MessagesIHM.Critical('Message', "New thickness less than Downsampling factor, PLEASE CHOOSE ANOTHER THICKNESS")
                return
            MODEL.SetThickness(event.thickness)
            event.AskToRepaintAllSlots()
            event.StopDispatch()

        if event.HasToBeProcessed() and event.IsType(Event.Type.ChangePage):
            MODEL.ChangePage(event.idPage)
            MEDIATEUR._UpdateAllIHM()
            event.AskToRepaintAllSlots()
            event.StopDispatch()


        if event.HasToBeProcessed() and event.IsType(Event.Type.ChangeGamma):
            Params = [ event.black, event.white, event.Gamma ]
            MODEL.SetGammaParam(event.idCha, Params)
            event.AskToRepaintCHA(event.idCha)
            event.StopDispatch()


        if event.HasToBeProcessed() and event.IsType(Event.Type.ChangeProj):
            MODEL.ChangeProj(event.ProjName)
            MEDIATEUR._UpdateAllIHM()
            event.AskToRepaintAllSlots()
            event.StopDispatch()

        if event.HasToBeProcessed() and event.IsType(Event.Type.Undo):
            MEDIATEUR.Undo(event.slotPos)
            MEDIATEUR._UpdateSlotIHM(event.slotPos)
            event.AskToRepaintSlot(*event.slotPos)

        if event.HasToBeProcessed() and event.IsType(Event.Type.Redo):
            MEDIATEUR.Redo(event.slotPos)
            MEDIATEUR._UpdateSlotIHM(event.slotPos)
            event.AskToRepaintSlot(*event.slotPos)


        if event.HasToBeProcessed() and event.IsType(Event.Type.ChangeScroll):
            if MODEL.AreScrollBarsLinked() :
                for i in range(MODEL.FishWindow.GetNbFishPerPage()):
                    slotpos = MODEL.FishWindow.IdSlotToSlotPos(i)
                    MEDIATEUR.ChangeScroll(slotpos, event.value)
                    MEDIATEUR._UpdateSlotIHM(slotpos)
                    event.AskToRepaintAllSlots()
            else :
                MEDIATEUR.ChangeScroll(event.slotPos,event.value)
                MEDIATEUR._UpdateSlotIHM(event.slotPos)
                event.AskToRepaintSlot(*event.slotPos)
            event.StopDispatch()

        ###############################  LOAD /   / CLEAR

        if event.HasToBeProcessed() and event.IsType(Event.Type.LoadData):
            MEDIATEUR.LoadVolume(event)
            event.AskToRepaintAllSlots()
            event.StopDispatch()


        if event.HasToBeProcessed() and event.IsType(Event.Type.ClearData) :
            if MEDIATEUR.CheckToolActif(): return
            MEDIATEUR.ClearData(event.AskQuestion)
            MODEL.ChangePage(0)
            MEDIATEUR._UpdateAllIHM()
            event.AskToRepaintAllSlots()
            event.StopDispatch()


        ###############################  TOOLS

        if  event.HasToBeProcessed() and event.IsType(Event.Type.ChangeTool) :
            if MEDIATEUR.CheckToolActif(): return
            ToolClassName = event.ToolClass
            if ToolClassName == "ToolZoom":     MODEL.ChangeTool(ToolZoom())
            event.StopDispatch()

        ############################   ZOOM / POS

        if event.IsType(Event.Type.ChangeScreenSize):
            #event.AskToRepaintAllSlots()
            if MODEL.MainWindow is not None :
                MODEL.MainWindow.update()
            event.StopDispatch()

        if event.IsType(Event.Type.OnShow):
            MEDIATEUR._UpdateAllIHM()
            event.AskToRepaintAllSlots()
            event.StopDispatch()






    ##############################################################################################################################
    #
    #                               CHANGE

    @staticmethod
    def ChangeScroll(SlotPos,value):
        idFish = MODEL.GetIdFishFromSlotPos(SlotPos)
        P3D = MODEL.GetPosition(idFish)
        N3D = [ P3D[0], P3D[1], P3D[2] ]
        name = MODEL.GetProjName()

        if name == GlobVar.VueDorsale : N3D[0] = value
        else:
            if name == GlobVar.VueTransverse : N3D[2] = value
            else: N3D[1] = value


        MODEL.SetPosition(idFish,N3D)

    @staticmethod
    def Undo(SlotPos):
        idFish = MODEL.GetIdFishFromSlotPos(SlotPos)
        rep = MODEL.HistoryUndo(idFish)
        if rep is not None :
            oldHisto, newHisto = rep
            MEDIATEUR._ChannelToReload(idFish, oldHisto, newHisto)


    @staticmethod
    def Redo(SlotPos):
        idFish = MODEL.GetIdFishFromSlotPos(SlotPos)
        rep = MODEL.HistoryRedo(idFish)
        if rep is not None :
            oldHisto, newHisto = rep
            MEDIATEUR._ChannelToReload(idFish, oldHisto, newHisto)


    def _ChannelToReload(idFish, HistoOld, HistoNew):
        for idCha in range(3):
            CHA_OUPUT = HistoNew["CHA_OUTPUTS"][idCha]
            CHA_INPUT = HistoOld["CHA_OUTPUTS"][idCha]
            if CHA_INPUT != CHA_OUPUT:
                if CHA_OUPUT is None:
                    MODEL.SetVolume(idFish, idCha, MODEL.EmptyVolume)
                else:
                    filename, idVol = CHA_OUPUT
                    try:
                        MessagesIHM.WaitCursor()
                        R = VolReader(filename)
                        a, b, Vol3D = R.LoadData(idVol)[0]
                        Vol3D = Transform.ReduceForGUI(Vol3D, MODEL.GetBinLevel(), MODEL.GetBitDepth())
                        MODEL.SetVolume(idFish, idCha, Vol3D)
                        MessagesIHM.NormalCursor()
                    except:
                        pass

    ##############################################################################################################################
    #
    #                               LOAD   SAVE    CLEAR

    @staticmethod
    def LoadVolume(event):
        idPoisson    = event.idFish
        idCha        = event.idCha
        FileName, IdVolInFile, Vol3D   = event.Data3D   # Main_Vol3D : reduction x2 et 8 bits

        Vol3D = Transform.ReduceForGUI(Vol3D,MODEL.GetBinLevel(),MODEL.GetBitDepth())

        MODEL.SetVolume(idPoisson,idCha,Vol3D)
        SMX4 = Vol3D.shape

        PosCenter = [ SMX4[0] // 2, SMX4[1] // 2, SMX4[2] // 2 ]

        # change position of view / on choisit le centre du nouveau volume
        MODEL.SetPosition(idPoisson, PosCenter)

        # met en place le repaint si poisson visible à l'écran
        if MODEL.IsFishVisible(idPoisson):
            idSlot = MODEL.GetFishidSlot(idPoisson)
            if idSlot is not None:
                SlotPos = MODEL.FishWindow.IdSlotToSlotPos(idSlot)
                x, y = SlotPos
                event.AskToRepaintView (x, y, idCha) # indique que la view   doit passer en repaint
                MEDIATEUR._UpdateSlotIHM(SlotPos)

        # créé l'historique de chargement
        MODEL.HistoryNewStep(idPoisson,"LOAD_VOLUME",{ "FileName": FileName, "IdVolInFile" : IdVolInFile })
        MODEL.HistoryChangeVolInChannel(idPoisson ,idCha,(FileName,IdVolInFile))




    @staticmethod
    def ClearData(AskQuestion):

        if AskQuestion  :

            reply = MessagesIHM.Question('WARNING !!!', "All data will be cleared, are you sure ?")
            if reply != MessagesIHM.Yes: return False

            reply = MessagesIHM.Question('WARNING !!!', "Everything will be deleted, please confirm !")
            if reply != MessagesIHM.Yes: return False

        MODEL.InitModel()
        MEDIATEUR._UpdateAllIHM()
        return True

