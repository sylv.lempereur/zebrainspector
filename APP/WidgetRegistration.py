from PySide2.QtWidgets import (QPushButton, QProgressBar, QLabel, QHBoxLayout, QCheckBox, QComboBox, QWidget, QFileDialog, QVBoxLayout, QTableView)
from PySide2 import QtCore
from PySide2 import QtGui
from GlobVar import GlobVar
from APP.VolReader import VolReader
from PySide2.QtCore import QBasicTimer

from APP import MODEL
from time import time
from os import makedirs
from os.path import (
    exists,
    join as ospathjoin,
    splitext)
from numpy import (
    clip,
    ndarray
    )
from SimpleITK import (GetImageFromArray, WriteImage)
from APP.Event import Event
from pathlib import Path
from SylvainWrapper import (
    get_affine_matrix,
    SylvainRecalage,
    SylvainSegmentation
    )
import os.path
import APP.GlobFnt as GlobFnt

############################################################################
#
#
#       Thread used to load Data // GUI Thread not used for this
#
#


class LoaderThread(QtCore.QThread):

    signal_progressbar    = QtCore.Signal(int)
    signal_changeRowColor = QtCore.Signal(int,int,str)
    signal_Terminated     = QtCore.Signal()
    signal_SendData       = QtCore.Signal(int,int,object)
    signal_AddInfoInHistory    = QtCore.Signal(int, str, object)

    def __init__(self, SeqChannelOrder, NbVolToLoad, outputFolder, CallingWindow, ReferenceChannel, OutChannel, RegistrationMethod, UseTiff):
        QtCore.QThread.__init__(self)
        self.SeqChannelOrder = SeqChannelOrder.copy()
        self.AskToStop = False
        self.NbVolToLoad = NbVolToLoad
        self.outputFolder = outputFolder
        if not exists(outputFolder):
            makedirs(outputFolder)

        self.OutChannel = OutChannel
        self.ReferenceChannel = ReferenceChannel
        # self.IsPigmented = IsPigmented
        self.RegistrationMethod = RegistrationMethod
        self.tiff = UseTiff
        CallingWindow.signal_AskToStop.connect(self.AAskToStop)

    @QtCore.Slot()
    def AAskToStop(self):
        self.AskToStop = True

    def BuildFileName(self,filename, idFish, channel):
        def len3(s):
            s= str(s)
            if len(s) >= 3 : return s
            else:  return len3("0"+s)

        newfilename, a ,b = GlobFnt.SplitFileName(filename,"_C","_ID")

        format = ".mha"
        if self.tiff : format = ".tiff"

        code = "_ID" + len3(idFish) + "_C" + len3(channel)

        newfilename += code + "_registration"   + format

        fullfilename = ospathjoin(self.outputFolder, newfilename)

        return fullfilename

    def LoadFileDataForProcessing(self, channel, filen, idInFile ):


        # first pass create MHA
        R = VolReader(filen)
        rep = R.LoadData(idInFile)[0]
        Vol3D = rep[2]
        spacing   = R.GetSpacingAfterLoad()


        return [  Vol3D, spacing ]


    def run(self):

        # comptage des jobs
        nbJobDone = 0
        nbJobs = 0
        for idFish in range(0, GlobVar.MaxFishes):
            if MODEL.GetSelected(idFish):
                for channelid in range(3):
                    if self.SeqChannelOrder[idFish][channelid] != None:
                        nbJobs += 1

        # lancement du traitement

        for idFish in range(0, GlobVar.MaxFishes):
            if not MODEL.GetSelected(idFish): continue  # le user n'a pas sélectionné ce poisson

            FileInSlot = self.SeqChannelOrder[idFish]
            if FileInSlot == [None, None, None] : continue
            # structure de FileInSlot [ [ (filename, idInFile, row) x 3 ] x idFish ]

            ToDoListFish = [ ]

            try:

                # etape 1 : recherche tous les fichiers concernant cette ligne idFish



                for channelid in range(3) :
                    if self.AskToStop: continue
                    if FileInSlot[channelid] == None : continue

                    nbJobDone += 1  # ce fichier doit etre traité
                    filename, idInFile, rowInTable = FileInSlot[channelid]

                    #  chargement des données

                    self.signal_changeRowColor.emit(rowInTable, 1, "Loading Data")
                    Vol3D, spacing  = self.LoadFileDataForProcessing( channelid, filename, idInFile )
                    basename = os.path.basename(filename)
                    newfilename = ospathjoin(self.outputFolder, basename)
                    fullfilename = self.BuildFileName(newfilename,idFish,channelid)

                    # le channel de reference en premier
                    info = (channelid, fullfilename, Vol3D, spacing, rowInTable)
                    if channelid == self.ReferenceChannel :
                        ToDoListFish.insert(0, info)
                    else :
                        ToDoListFish.append(info)




                # etape2 : recalage + save + reload  de toutes les modalités de la ligne


                for t in range(len(ToDoListFish)) :
                    if self.AskToStop: continue
                    Job = ToDoListFish[t]
                    channel, fullfilename, Vol3D, spacing, rowInTable = Job


                    # traitement

                    if channel == self.ReferenceChannel:
                        self.signal_changeRowColor.emit(rowInTable, 2, "Compute transformation matrix")
                        affine_matrix, centroid = get_affine_matrix( Vol3D, RegistrationMethod = self.RegistrationMethod )


                    self.signal_changeRowColor.emit(rowInTable, 2, "Registration in progress - Channel " + str(channel)  )
                    Vol3D = SylvainRecalage(Vol3D=Vol3D, matrix_affine=affine_matrix, centroid=centroid)

                    # au cas ou les algos retourne des valeurs un peu supérieure 4095
                    clip(Vol3D, 0, MODEL.GetBitDepthMaxValue(), out=Vol3D)

                    ToDoListFish[t] = None   # pour écraser le vol3d en mémoire (gc)
                    # sav du mha recalé
                    self.signal_changeRowColor.emit(rowInTable, 3, "Writing file to disk - Channel " + str(channel)  )
                    T = GetImageFromArray(Vol3D)
                    T.SetSpacing((spacing, spacing, spacing) )
                    WriteImage(T, fullfilename)

                    # send data to GUI thead
                    self.signal_changeRowColor.emit(rowInTable, 4, "Updating view - Channel " + str(channel)  )
                    self.signal_SendData.emit(idFish, channel, (fullfilename,0,Vol3D))
                    self.signal_AddInfoInHistory.emit(idFish,"RECALAGE_SYLVAIN", {"REF_CHANNEL": self.ReferenceChannel,
                                                                              "RegistrationMethod": self.RegistrationMethod,
                                                                              "affine_matrix": affine_matrix,
                                                                              "centroid": centroid})

                    Vol3D = None   # gc

                    # terminé
                    self.signal_changeRowColor.emit(rowInTable, 5, "")


            except :
                for channelid in range(3) :
                    if FileInSlot[channelid] == None : continue
                    filename, idInFile, rowInTable = FileInSlot[channelid]
                    self.signal_changeRowColor.emit(rowInTable, 6, "ERROR")


            # affichage progression
            pcent = ((nbJobDone) * 100) / nbJobs
            self.signal_progressbar.emit(pcent)




        self.signal_Terminated.emit()



############################################################################
#
#
#       GUI to select and to launch loading process
#
#



# A window class
class WidgetRegistration(QWidget):

    ####################################################################
    #
    #     LOAD INFORMATION OF SELECTED FILES


    def ModelInit(self):
        self.model.clear()
        self.model.setHorizontalHeaderLabels(['Fish', 'Channel', 'Filename & Id' ])

    def FitWindowToTable(self):
        self.table.resizeColumnsToContents()
        w = 0
        for j in range(3):
            w += self.table.columnWidth(j);
        self.setFixedWidth(w + 50)


    def StoreInfoInTable(self,row,INFOS):
        for x in range(len(INFOS)) :
           self.model.setItem(row, x, QtGui.QStandardItem(str(INFOS[x])))
           item = self.model.item(row, x)
           item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
           item.setTextAlignment( QtCore.Qt.AlignCenter )

    def FillTable(self,seq):

        row = 0
        for idFish in range(GlobVar.MaxFishes):
            for ChannelId in range(3):
                FileInChannel = seq[idFish][ChannelId]
                if FileInChannel is None : continue
                filen, idInFile = FileInChannel

                fileshortname = Path(filen).name + "  (" + str(idInFile) +")"
                idFis = MODEL.GetSlotNameFromidFish( idFish )

                self.StoreInfoInTable(row,[idFis, ChannelId, fileshortname ])

                infos = (filen , idInFile , row)
                seq[idFish][ChannelId] = infos    # insere le n° de ligne dans le tableau

                self.NbVolToLoad += 1
                row += 1

        self.FitWindowToTable()



    ####################################################################
    #
    #     LOAD DATA
    #     mainly the view interactions : progressbar, message
    #     exchange signals with the loader thread

    signal_AskToStop = QtCore.Signal()

    @QtCore.Slot(int,str,object)
    def AddInfoInHistory(self, idFish, action, LPARAMS):
        MODEL.HistoryAddKeyToCurrentHistory(idFish,action,LPARAMS)


    @QtCore.Slot(int, int, ndarray)
    def SendData(self, idFish, idCol, Info3D):
        Event.Create.LoadData(idFish,idCol,Info3D).Dispatch()  # send data to model


    @QtCore.Slot(int)
    def UpdateProgressBar(self, pcentpgrogress : int  ):
        self.pbar.setValue(pcentpgrogress)



    @QtCore.Slot(int, int,str)
    def ChangeRowColor(self, rowk:int, colorid : int, msg : str):
        self.MMM = msg
        self.t0 = time()

        if colorid == 0: color = QtCore.Qt.white
        if colorid == 1: color = QtCore.Qt.yellow
        if colorid == 2: color = QtCore.Qt.magenta
        if colorid == 3: color = QtCore.Qt.cyan
        if colorid == 4: color = QtCore.Qt.lightGray
        if colorid == 5: color = QtCore.Qt.green
        if colorid == 6: color = QtCore.Qt.red

        for x in range(3):
            item = self.model.item(rowk, x)
            if item is not None:
                item.setBackground(color)
                self.model.setItem(rowk, x, item)
                index = self.model.index(rowk, x)
                self.table.update(index)
                self.table.scrollTo(index)
                self.table.repaint()

    # run the loader thread
    def StartLoad(self):
        if not self.LoadRunning :
           OutputFolder = QFileDialog.getExistingDirectory(None, "Select Output Folder")
           if len(OutputFolder) == 0: return


           self.LoadRunning = True
           self.pbar.setValue(0)
           self.MMM = "Loading in progress..."

           for k in range(self.model.columnCount()) :
             self.ChangeRowColor(k,0,"")

           #isPigmented = self.checkPigmented.isChecked()
           RegistrationMethod = self.comboEYES.currentIndex()
           UseTiff = self.checkTiff.isChecked()

           self.ThreadLoad = LoaderThread(self.SeqChannelOrder, self.NbVolToLoad, OutputFolder, self,  self.ReferenceCHA, self.OutCHA, RegistrationMethod,UseTiff)
           self.ThreadLoad.signal_progressbar.connect(self.UpdateProgressBar)
           self.ThreadLoad.signal_changeRowColor.connect(self.ChangeRowColor)
           self.ThreadLoad.signal_Terminated.connect(self.ProcessFinished)
           self.ThreadLoad.signal_SendData.connect(self.SendData)
           self.ThreadLoad.signal_AddInfoInHistory.connect(self.AddInfoInHistory)


           self.ThreadLoad.start()

           self.t0 = time()
           self.timer = QBasicTimer()
           self.timer.start(1000, self)

    def timerEvent(self, e):
        dt = str(int(time() - self.t0))
        msg = self.MMM + " - " + dt + " seconds"
        self.LoadButton.setText(msg)
        self.LoadButton.repaint()
        self.LoadButton.update()

    @QtCore.Slot()
    def ProcessFinished(self):
        self.timer.stop()
        self.buttonSTOP.setText("STOP")
        self.LoadButton.setText(self.msgLoadButton)
        self.LoadRunning = False

    def StopLoading(self):
        self.buttonSTOP.setText("Asking to stop")
        self.signal_AskToStop.emit()



    ####################################################################
    #
    #     CTR

    def ChangeReferenceCHA(self):
        self.ReferenceCHA = self.combo.currentIndex()


    def __init__(self, seq  ):
        QWidget.__init__(self)
        self.ReferenceCHA = 0
        self.OutCHA = 1 #  CHA de sortie de l'algo de segmentation


        self.setWindowIcon(GlobVar.AppIcon)
        self.LoadRunning = False
        self.SeqChannelOrder = seq
        self.NbVolToLoad = 0                  # compte la quantité de volume à charger, utile pour la progressbar



        self.setWindowTitle("Process Registration")


        # Create the QTableView widget and associate to its model

        self.model = QtGui.QStandardItemModel(self)
        #self.ModelInit()
        self.table = QTableView(self)
        self.table.setModel(self.model)
        self.table.verticalHeader().hide()
        self.table.setMinimumHeight(100)
        self.table.height = 500

        # Place the table widget into a layout
        layout = QVBoxLayout(self)

        layout.addWidget(QLabel("Important: please check all required marks"))





        lay2 = QHBoxLayout(self)
        lay2.addWidget(QLabel("Reference/Dye Channel  :  "))
        self.combo = QComboBox(self)
        self.combo.addItem("CHA 0")
        self.combo.addItem("CHA 1")
        self.combo.addItem("CHA 2")
        self.combo.setCurrentIndex(self.ReferenceCHA)
        self.combo.activated[str].connect(self.ChangeReferenceCHA)
        lay2.addWidget(self.combo)
        layout.addLayout(lay2)




        lay3 = QHBoxLayout(self)
        '''
        self.checkPigmented = QCheckBox("", self) #GLOBAL
        self.checkPigmented.setChecked(False)
        self.checkPigmented.setVisible(False)
        lay3.addWidget(QLabel("Pigmented eyes:         "), 0, QtCore.Qt.AlignLeft)
        lay3.addWidget(self.checkPigmented, 1, QtCore.Qt.AlignLeft)
        self.checkPigmented.setVisible(True)
        lay3.addWidget(QLabel("(no H2O2 treatment)"), 2, QtCore.Qt.AlignLeft)
        '''
        self.comboEYES = QComboBox(self)
        self.comboEYES.addItem("Registration without eyes")
        self.comboEYES.addItem("Registration using eyes")
        self.comboEYES.addItem("Registratiokn with pigmented eyes")
        self.comboEYES.setCurrentIndex(0)
        lay3.addWidget(self.comboEYES)
        layout.addLayout(lay3)



        lay4 = QHBoxLayout(self)
        lay4.addWidget( QLabel("Save in tiff format:     "),  0, QtCore.Qt.AlignLeft)
        self.checkTiff = QCheckBox("", self)
        self.checkTiff.setChecked(False)
        lay4.addWidget(self.checkTiff, 1, QtCore.Qt.AlignLeft)

        layout.addLayout(lay4)

        self.msgLoadButton = "Start Process"

        self.LoadButton = QPushButton(self.msgLoadButton, self)
        self.LoadButton.clicked.connect(self.StartLoad)
        layout.addWidget(self.LoadButton)

        self.pbar = QProgressBar(self)
        self.pbar.setValue(0)
        layout.addWidget(self.pbar)

        self.buttonSTOP = QPushButton("STOP", self)
        self.buttonSTOP.clicked.connect(self.StopLoading)
        layout.addWidget(self.buttonSTOP)

        layout.addWidget(self.table)
        self.setLayout(layout)

        self.ModelInit()
        self.FillTable(self.SeqChannelOrder)
        self.setMinimumWidth(300)







