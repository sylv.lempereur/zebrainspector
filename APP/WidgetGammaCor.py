from PySide2 import QtCore as Core
from PySide2.QtWidgets import ( QLabel, QSlider, QPushButton,  QWidget,  QVBoxLayout)
from GlobVar import GlobVar

"""
Fenetre permettant de modifier les contrastes pour une modalité
Créer depuis le MainWindow grâce aux bouttons MOD1/2/3
"""

from APP.Event import Event


class WidgetGammaCorrection(QWidget) :


    def __init__(self, black,white,Gamma,idCHA):
        QWidget.__init__(self)

        self.setWindowIcon(GlobVar.AppIcon)

        self.setWindowModality(Core.Qt.ApplicationModal)

        self.idCha  = idCHA
        self.undo   = [ black, white, Gamma]

        self.Mainlayout = QVBoxLayout(self)
        self.setLayout(self.Mainlayout)

        self.LabelBlack = QLabel("Black level: " + str(black),self)
        self.Mainlayout.addWidget(self.LabelBlack)
        self.Slider1 = QSlider(Core.Qt.Horizontal, self)
        self.Slider1.setRange(0, 255)
        self.Slider1.setValue(black)
        self.Slider1.valueChanged.connect(lambda: self.ValChanged())
        self.Mainlayout.addWidget(self.Slider1)

        self.LabelWhite = QLabel("White level: " + str(white),self)
        self.Mainlayout.addWidget(self.LabelWhite)
        self.Slider2 = QSlider(Core.Qt.Horizontal, self)
        self.Slider2.setRange(0, 255)
        self.Slider2.setValue(white)
        self.Slider2.valueChanged.connect(lambda: self.ValChanged())
        self.Mainlayout.addWidget(self.Slider2)

        self.LabelGamma = QLabel("Gamma correction : " + str(Gamma),self)
        self.Mainlayout.addWidget(self.LabelGamma)
        self.Slider3 = QSlider(Core.Qt.Horizontal, self)
        self.Slider3.setRange(1, 1000)
        self.Slider3.setValue(Gamma * 100)
        self.Slider3.valueChanged.connect(lambda: self.ValChanged())
        self.Mainlayout.addWidget(self.Slider3)



        self.button3 = QPushButton("Undo", self)
        self.button3.clicked.connect(lambda: self.Undo())
        self.Mainlayout.addWidget(self.button3)

        self.button = QPushButton("Reset", self)
        self.button.clicked.connect(self.ResetVal)
        self.Mainlayout.addWidget(self.button)

        self.button2 = QPushButton("Validate", self)
        self.button2.clicked.connect(lambda: self.close())
        self.Mainlayout.addWidget(self.button2)

        self.setWindowTitle("Contrast - MOD" + str(self.idCha+1))
        self.setMinimumHeight(200)
        self.setMinimumWidth(400)

    def Undo(self):
        self.Slider1.setValue(self.undo[0])
        self.Slider2.setValue(self.undo[1])
        self.Slider3.setValue(self.undo[2]*1000)

    def ResetVal(self):
        self.Slider1.setValue(0)
        self.Slider2.setValue(255)
        self.Slider3.setValue(100)

    def ValChanged(self):
        black = self.Slider1.value()
        white = self.Slider2.value()
        if black > white :
            white = black
            self.Slider2.blockSignals(True)
            self.Slider2.setValue(white)
            self.Slider2.blockSignals(False)
        Gamma = self.Slider3.value() / 100

        self.LabelGamma.setText( "Gamma correction: " + str(Gamma) )
        self.LabelBlack.setText( "Black level: " + str(black) )
        self.LabelWhite.setText( "White level: " + str(white) )

        Event.Create.ChangeGamma(self.idCha, black, white, Gamma).Dispatch()




