from time import time

from pathlib import Path
from numpy import ndarray
from PySide2 import QtCore
from PySide2 import QtGui
from PySide2.QtWidgets import (QLabel, QPushButton, QProgressBar, QLineEdit, QWidget, QFileDialog, QTableView, QVBoxLayout,QHBoxLayout,QApplication)
from PySide2.QtCore import QBasicTimer

from GlobVar import GlobVar
from APP.VolReader import VolReader
from APP.Event import Event
from APP import MODEL
from APP.WMessageBox import MessagesIHM
import APP.GlobFnt as GlobFnt

############################################################################
#
#
#       THREAD used to load Data // GUI Thread not used
#


class LoaderThread(QtCore.QThread):

    signal_progressbar    = QtCore.Signal(int)
    signal_changeRowColor = QtCore.Signal(int,int)
    signal_Terminated     = QtCore.Signal()
    signal_SendData       = QtCore.Signal(int,int,object)

    def __init__(self, SeqFilesToLoad, CallingWindow):
        QtCore.QThread.__init__(self)
        self.SeqFilesToLoad = SeqFilesToLoad.copy()
        self.AskToStop = False
        CallingWindow.signal_AskToStop.connect(self.AAskToStop)

    @QtCore.Slot()
    def AAskToStop(self):
        self.AskToStop = True

    def LoadFileData(self, rowInTable, seqid, channelid, filename):
        R = VolReader(filename)
        Data = R.LoadData()

        for i in range(len(Data)):                  # file with multiple channel
            Vol3D = Data[i]
            self.signal_SendData.emit(seqid, channelid + i, Vol3D)  # send data to GUI thread


    def run(self):
        #MODEL.LoadSequences = copy.deepcopy(self.SeqChannelOrder)   # conserve l'ordre de chargement
        JobList = list(self.SeqFilesToLoad.keys())
        rowInTable = 0

        while rowInTable < len(JobList) and not self.AskToStop:
            try:
                self.signal_changeRowColor.emit(rowInTable,1)
                key = JobList[rowInTable]
                seqid, channelid = key
                filename = self.SeqFilesToLoad[key]

                self.LoadFileData(rowInTable, seqid, channelid, filename)    # charge les fichiers


            except:
                self.signal_changeRowColor.emit(rowInTable, 5) #si erreur ligne rouge
            else :
                self.signal_changeRowColor.emit(rowInTable, 4)

            # affichage interactif

            pcent = ((rowInTable+1) * 100) / len(JobList)
            self.signal_progressbar.emit(pcent)


            # next file
            rowInTable += 1

        self.signal_Terminated.emit()

############################################################################
#
#
#       GUI to select and to launch loading process
#
#



class WinLoadInfo(QWidget):



    # A file toto_C0002.mha =>  dict((i,2)) => fname
    # where i  increments for each file with a new filename prefix

    def OrderFilesAndSeq(self, FileList , next_seqid):
        result = {}

        seq_keys = {}
        for fname in FileList :

            # on essaye : tag idfish + tag idchannel
            IDMask = self.MaskForID.text()
            ChannelMask = self.MaskForChannel.text()

            FilesBeginning, idFish, idChannel =  GlobFnt.SplitFileName( fname, ChannelMask, IDMask )

            # recherche un idFish
            if idFish is None:
                if FilesBeginning in seq_keys:
                    idFish = seq_keys[FilesBeginning]  # on prend le n° slot d'un fichier déjà chargé avec un autre channel
                else:
                    idFish = next_seqid
                    next_seqid += 1
                    seq_keys[FilesBeginning] = idFish

            # pas de tag channel
            if idChannel is None :
                idChannel = 0


            result[(idFish, idChannel)] = fname
        return result



    def FitWindowToTable(self):
        self.table.resizeColumnsToContents()

        w = 0
        for j in range(6):
            w += self.table.columnWidth(j);
        self.setFixedWidth(w + 50)
        self.setMinimumSize(200, 200)

    def StoreInfoInTable(self,row,INFOS):
        for x in range(len(INFOS)) :
            T = str(INFOS[x])
            if x == 0 :   T =  MODEL.GetSlotNameFromidFish( INFOS[x] )
            self.model.setItem(row, x, QtGui.QStandardItem(T))
            item = self.model.item(row, x)
            item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            item.setTextAlignment(QtCore.Qt.AlignCenter)

    def AddFilesToList(self):
        QF = QFileDialog(self)
        QF.setFileMode(QFileDialog.ExistingFiles)

        QF.setNameFilter("IMG3D (*.mha *.nd2 *.tif *.tiff *.nrrd *.hdf5)")

        if QF.exec():
            row = 0
            fileNames = QF.selectedFiles()

            # met à jour self.SeqChannelOrder contenant la liste des fichiers à charger
            newSeq = self.OrderFilesAndSeq(fileNames, self.FirstFreeIDFish)
            for k in newSeq.keys():
               self.FilesToLoadAtGivenSlotChannel[k] = newSeq[k]


            self.LoadRunning = True   # declenche le compteur de temps et bloque le load data
            self.NewChronoStart()
            nbread = 0
            nbtot = len(self.FilesToLoadAtGivenSlotChannel.keys())
            for k in self.FilesToLoadAtGivenSlotChannel.keys() :

                seqid, channelid = k
                filen = self.FilesToLoadAtGivenSlotChannel[k]
                R = VolReader(filen)
                Size, LChannelsName, prof, NbChannels = R.LoadInfo()
                fileshortname = Path(filen).name
                self.StoreInfoInTable(row,[seqid,channelid,fileshortname,Size,LChannelsName,prof])
                row += 1
                self.FitWindowToTable()
                self.table.scrollToBottom()
                nbread += 1
                self.pbar.setValue( (nbread*100) // nbtot )
                QApplication.processEvents()       # met à jour l'interface'

            # move FirstFreeIDFish just after the last loaded
            for k in self.FilesToLoadAtGivenSlotChannel.keys():
                seqid, channelid = k
                if seqid+1 > self.FirstFreeIDFish :
                    self.SetFirstSlotToUse(seqid + 1)

            self.LoadRunning = False



    ####################################################################
    #
    #     LOAD DATA - GUI SIDE
    #
    #     mainly the view interactions : progressbar, message
    #     exchange signals with the loader thread

    ##################    SIGNALS

    signal_AskToStop = QtCore.Signal()

    @QtCore.Slot(int, int, ndarray)
    def SendData(self, idFish, idCha, Info3D: object):
        Event.Create.LoadData(idFish, idCha, Info3D).Dispatch()  # send data to model


    @QtCore.Slot(int)
    def UpdateProgressBar(self, pcentpgrogress : int ):
        self.pbar.setValue(pcentpgrogress)
        self.NewChronoStart()


    @QtCore.Slot(int, int)
    def ChangeRowColor(self, rowk:int, colorid : int):
        if colorid == 0: color = QtCore.Qt.white
        if colorid == 1: color = QtCore.Qt.yellow   # channel 1
        if colorid == 2: color = QtCore.Qt.magenta  # channel 2
        if colorid == 3: color = QtCore.Qt.cyant    # channel 3
        if colorid == 4: color = QtCore.Qt.green
        if colorid == 5: color = QtCore.Qt.red

        for x in range(6):
            item = self.model.item(rowk, x)
            if item is not None:
                item.setBackground(color)
                self.model.setItem(rowk, x, item)
                index = self.model.index(rowk, x)
                self.table.update(index)
                self.table.scrollTo(index)
                self.table.repaint()

    @QtCore.Slot()
    def ProcessFinished(self):
        self.InitProcess()


    # SEND SIGNAL
    def StopLoading(self):
        if self.LoadRunning:
          self.buttonSTOP.setText("Asking to stop")
          self.signal_AskToStop.emit()


    ################   run the loader thread


    def StartLoad(self):
        if len(self.FilesToLoadAtGivenSlotChannel) == 0 :
            MessagesIHM.Critical("Warning", "Please Add Files")
            return

        if not self.LoadRunning:
            self.LoadRunning = True
            self.NewChronoStart()
            self.pbar.setValue(0)
            self.LoadButton.setText("Loading in progress...")

            self.ThreadLoad = LoaderThread(self.FilesToLoadAtGivenSlotChannel, self)
            self.ThreadLoad.signal_progressbar.connect(self.UpdateProgressBar)
            self.ThreadLoad.signal_changeRowColor.connect(self.ChangeRowColor)
            self.ThreadLoad.signal_Terminated.connect(self.ProcessFinished)
            self.ThreadLoad.signal_SendData.connect(self.SendData)

            self.ThreadLoad.start()

    def NewChronoStart(self):
        self.T0 = time()

    def timerEvent(self, e):
        msg = "Time spent on last action"

        if self.LoadRunning :
           dt  = str(int(time() - self.T0))
           msg = msg + " - " + dt + " seconds"
           self.TimeSpent.setText(msg)
           self.TimeSpent.repaint()
           self.TimeSpent.update()

        self.TimeSpent.setText( msg )

    ####################################################################
    #
    #     CONSTRUCT

    def InitProcess(self):  # appelé lorsque process de traitement est fini
        self.LoadRunning = False  # load data thread running
        self.buttonSTOP.setText("STOP")
        self.LoadButton.setText("Load data")

    def InitAll(self):
        self.InitProcess()
        self.FilesToLoadAtGivenSlotChannel = {}  # store files to load, key = (n°slot,n°channel)
        for k in range(self.model.columnCount()):
            self.ChangeRowColor(k, 0)
        self.model.clear()
        self.model.setHorizontalHeaderLabels(['Slot', 'Channel', 'Filename', 'Size', 'Channels', 'Type'])


    def SetFirstSlotToUse(self,idFish):
        if idFish is None : return
        self.FirstFreeIDFish = idFish
        slotName = MODEL.GetSlotNameFromidFish(idFish)
        self.LabPosInsertion.setText( "  " + slotName + "  "  )


    def __init__(self, *args):
        QWidget.__init__(self, *args)
        self.setWindowIcon(GlobVar.AppIcon)

        TT = QHBoxLayout()
        TT.addWidget(QLabel("Start Position of Loaded Images: ",self))
        self.LabPosInsertion = QLabel(self)
        TT.addWidget(self.LabPosInsertion)
        self.LabPosInsertion.setStyleSheet("QLabel { background-color : white; }")


        layout = QVBoxLayout(self)
        self.SetFirstSlotToUse(MODEL.GetFirstFreeIdFish())

        button = QPushButton("<<<", self)
        button.clicked.connect( lambda: self.SetFirstSlotToUse(self.FirstFreeIDFish - 1) if self.FirstFreeIDFish > 0 else None)
        TT.addWidget(button)

        button = QPushButton(">>>", self)
        button.clicked.connect(lambda: self.SetFirstSlotToUse( self.FirstFreeIDFish + 1) if self.FirstFreeIDFish < GlobVar.MaxFishes - 1 else None)
        TT.addWidget(button)

        layout.addLayout(TT)

        # Create the QTableView widget and associate to its model

        self.model = QtGui.QStandardItemModel(self)
        self.table = QTableView(self)
        self.table.setModel(self.model)
        self.table.verticalHeader().hide()
        self.table.setMinimumHeight(100)
        self.table.height = 500
        # Place the table widget into a layout

        queryLayout2 = QHBoxLayout();
        label2 = QLabel("Tag for channel      ")
        self.MaskForChannel = QLineEdit("_C")
        queryLayout2.addWidget(label2)
        queryLayout2.addWidget(self.MaskForChannel)
        layout.addLayout(queryLayout2)

        queryLayout3 = QHBoxLayout();
        label3 = QLabel("Tag for fish ID        ")
        self.MaskForID = QLineEdit("_ID")
        queryLayout3.addWidget(label3)
        queryLayout3.addWidget(self.MaskForID)
        layout.addLayout(queryLayout3)



        button = QPushButton("Add files",self)
        button.clicked.connect(self.AddFilesToList)
        layout.addWidget(button)

        self.LoadButton = QPushButton("Load data", self)
        self.LoadButton.clicked.connect(self.StartLoad)
        layout.addWidget(self.LoadButton)

        self.ClearButton = QPushButton("Clear List", self)
        self.ClearButton.clicked.connect(self.InitAll)
        layout.addWidget(self.ClearButton)



        self.pbar = QProgressBar(self)
        self.pbar.setValue(0)
        layout.addWidget(self.pbar)

        self.TimeSpent = QLabel()
        self.TimeSpent.setStyleSheet("QLabel { background-color : white; }")
        self.TimeSpent.setAlignment(QtCore.Qt.AlignCenter);
        layout.addWidget(self.TimeSpent)

        self.buttonSTOP = QPushButton("STOP", self)
        self.buttonSTOP.clicked.connect(self.StopLoading)
        layout.addWidget(self.buttonSTOP)

        layout.addWidget(self.table)
        self.setLayout(layout)

        self.InitAll()

        self.NewChronoStart()
        self.timer = QBasicTimer()
        self.timer.start(300, self)







