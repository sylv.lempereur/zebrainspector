from time import time
#start = time()
#print("\t\t\tImport numpy")
from numpy import (
    array as nparray,
    int8,
    int16,
    take,
    uint8,
    uint16,
    zeros
    )
#print("\t\t\tdone in " + str(time() - start))
#start = time()

#print("\t\t\tImport SimpleITK")
from SimpleITK import (
    GetArrayFromImage,
    ImageFileReader,
    ReadImage,
    sitkInt8,
    sitkInt16,
    sitkUInt8,
    sitkUInt16,
    sitkVectorInt8,
    sitkVectorUInt8,
    sitkVectorUInt16,
    sitkVectorInt16
    )
#print("\t\t\tdone in " + str(time() - start))
#start = time()

#print("\t\t\tImport Number")
from numbers import Number
#print("\t\t\tdone in " + str(time() - start))
#start = time()

#print("\t\t\tImport ND2Reader")
from nd2reader import ND2Reader  # requires scikit image : pip install scikit-image
from nd2reader.parser import Parser
#print("\t\t\tdone in " + str(time() - start))
#start = time()

#print("\t\t\tImport APP.NPTransform")
from APP.NPTransform import Transform
#print("\t\t\tdone in " + str(time() - start))
#start = time()

###########################################################
#
#    Permet de switcher vers le bon reader


class VolReader:

    def __init__(self, fname):
        self.filename = fname

        def EndWith(string, List):
            for end in List:
                if string.endswith(end): return True
            return False

        if fname.endswith(".nd2"):
            self.Reader = ReaderND2()

        if EndWith(fname, [".mha", ".tiff", ".tiff", ".tif", ".hdf5", ".nrrd"]):
            self.Reader = ReaderMHA()

    def LoadInfo(self):
        return self.Reader.LoadInfo(self.filename)

    def LoadData(self, idVolume = -1):
        LInfo3D = self.Reader.LoadData(self.filename, idVolume)

        # force longuest axis at index 0
        for i in range(len(LInfo3D)):
            filename, idVol, V3D = LInfo3D[i]
            LastCol = 2
            OrderAxisVol = Transform.LonguestAxisToIndex(V3D,LastCol)
            LInfo3D[i] = (filename, idVol, OrderAxisVol)

        return LInfo3D

    def GetSpacingAfterLoad(self):
        print("Spacing : ", self.Reader.GetSpacingAfterLoading() )
        return self.Reader.GetSpacingAfterLoading()


#####################################################################################
##
##          ND2 READER


class ReaderND2:
    def __init__(self):
        self.spacing = 1

    def LoadInfo(self, fname):
        #ND.LilianData = fromfile(fname, dtype=uint8)
        fh = open(fname, "rb")
        parser = Parser(fh)
        metadata = parser.metadata
        Lx = metadata["width"]
        Ly = metadata["height"]
        Lz = metadata["z_levels"].stop
        ChannelsName = metadata["channels"]
        prof = "Unknown"

        try:
            Plan = parser.get_image_by_attributes(0, 0, 0, 0, Ly, Lx)
            if Plan.dtype == int8 or Plan.dtype == uint8: prof = " 8 bits"
            if Plan.dtype == int16 or Plan.dtype == uint16: prof = "16 bits"
        except:
            pass

        fh.close()
        NbChannels = len(ChannelsName)

        return [ "  [" + str(Ly)+","+str(Lx) + "]" + " x " + str(Lz)+ "  " , ChannelsName, prof, NbChannels]

    # autres approches
    def LoadDataII(self, file, idVolume):
        image = ND2Reader(file)
        image.iter_axes = 'c'
        image.bundle_axes = 'xyz'     # when 'z' is available, this will be default
        image.default_coords['c']     # 0 is the default setting

        self._TryToGetSpacing(image)

        Result = []
        for i in range(image.sizes['c']):
            if idVolume == -1 or i == idVolume:  # permet de charger juste une image
                R = nparray(image[i])
                R = R.astype(uint16)   # retourne des float64
                Result.append((file, i, R))
        image.close()
        return Result

    def _TryToGetSpacing(self,image):
        self.spacing = 1
        try:
            voxel_size_side = image.metadata['pixel_microns']
            if isinstance(voxel_size_side, Number):
                if voxel_size_side > 0 and voxel_size_side < 10000000:
                    self.spacing = voxel_size_side
        except:
            pass

    def LoadData(self, file,  idVolume):

        try:
            with ND2Reader(file) as image:
                if "c" in image.sizes:
                    n_channels = image.sizes["c"]
                else:
                    n_channels = 1
                n_rows = image.sizes["x"]
                n_columns = image.sizes["y"]
                n_slices = image.sizes["z"]

                self._TryToGetSpacing(image)

                Result = []
                for channel in range(0, n_channels):
                    if idVolume == -1 or channel == idVolume :  # permet de charger juste une image
                        array = zeros( ( n_slices, n_columns, n_rows ), dtype = uint16 )
                        for z in range(0, image.sizes["z"]):
                            T = image.get_frame_2D(c = channel, z=z)
                            array[image.sizes["z"] - z - 1, :, :] = T
                        Result.append( (file, channel, array ))
        except:
            return self.LoadDataII(file, idVolume)

        return Result

    def GetSpacingAfterLoading(self):
        return self.spacing



#####################################################################################
##
##          MHA READER

class ReaderMHA:
    def __init__(self):
        self.spacing = 1

    def LoadInfo(self, fname):
        reader = ImageFileReader()
        reader.SetFileName(fname)
        reader.ReadImageInformation()

        ChannelsName = str(reader.GetNumberOfComponents())
        size = reader.GetSize()
        size = size[2], size[1], size[0]
        PixelType = reader.GetPixelID()
        prof = "Unknown"

        if PixelType == sitkUInt8 or PixelType == sitkInt8: prof = " 8 bits"
        if PixelType == sitkUInt16 or PixelType == sitkInt16: prof = "16 bits"
        if PixelType == sitkVectorUInt8 or PixelType == sitkVectorInt8: prof = " x8 bits"
        if PixelType == sitkVectorUInt16 or PixelType == sitkVectorInt16: prof = "x16 bits"
        NbChannels = 1
        return [size, ChannelsName, prof, NbChannels]

    def _TryToGetSpacing(self, v):
        self.spacing = 1
        try:
            s = v.GetSpacing()[0]
            if isinstance(s, Number):
                if 0 < s < 10000000:
                    self.spacing = s
        except:
            pass

    def LoadData(self, fname, idVolume):

        v = ReadImage(fname)
        Data = GetArrayFromImage(v)

        self._TryToGetSpacing(v)

        S = Data.shape
        if len(S) == 3:   # un seul volume
            return [(fname, 0, Data) ]
        else:
            Result = []
            idaxes = S.index(min(S))  # au cas où l'indice sur les vol soit pas le dernier
            nb_components = S[idaxes]
            for i in range(nb_components):
                Vol = take(Data, i, axis=idaxes)
                if Vol.max() != 0:  # retire les pages vides
                    if idVolume == -1 or i == idVolume:  # permet de charger juste une image
                      Result.append( (fname,i,Vol) )
            return Result

    def GetSpacingAfterLoading(self):
        return self.spacing
