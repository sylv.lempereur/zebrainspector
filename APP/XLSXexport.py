

import os.path


from openpyxl import load_workbook
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from openpyxl.styles import Alignment


def _resize_column(sheet):
    delta = 2
    column = 1
    while sheet[get_column_letter(column) + str(1)].value:
        row = 1
        maxdim = 0
        while sheet[get_column_letter(column) + str(row)].value:
            cell = get_column_letter(column) + str(row)
            if not str(sheet[cell].value)[0] == "=":
                maxdim = max(maxdim,len(str(sheet[cell].value)))
            row += 1

        if sheet.column_dimensions[get_column_letter(column)].width <  maxdim + delta:
            sheet.column_dimensions[get_column_letter(column)].width = maxdim + delta
        column += 1



def _AddSheet(wb,sheetname,HEADERS):

        wb.create_sheet(sheetname)
        sheet = wb[sheetname]



        for index, header in enumerate(HEADERS):
            column_letter = get_column_letter(index +1 )
            cell = column_letter + "1"
            sheet[cell] = header
            sheet.column_dimensions[column_letter].width = len(header) + 1
            sheet[cell].alignment  = Alignment(horizontal="center")

def _openXLS(fname, sheetname,HEADERS):

    # create file
    if not os.path.isfile(fname) :
        wb = Workbook()
        wb.remove(wb["Sheet"])

        if sheetname not in wb.sheetnames:
            _AddSheet(wb,sheetname,HEADERS)
        wb.save(fname)
        wb.close()

    wb = load_workbook(fname)
    return wb


def addinfo(fname,sheetname,HEADERS,values):

    wb = _openXLS(fname,sheetname,HEADERS)

    #  verifie si la sheet "RESULTS" existe
    #  cas où on ouvre un fichier existant
    if sheetname not in wb.sheetnames:
        _AddSheet(wb,sheetname,HEADERS)
    sheet = wb[sheetname]


    # recherche première ligne vide
    row = 2
    while sheet["A" + str(row)].value:
        row += 1

    # ajoute les infos  :

    for x in range(len(values)) :
        letter = get_column_letter(x+1)
        cell = letter + str(row)

        sheet[cell] = values[x]
        sheet[cell].alignment  = Alignment(horizontal="center")

    # redim colonnes auto

    _resize_column(sheet)

    # end

    wb.save(fname)
    wb.close()




##########################################################################
"""
sheetname = "RESULTS"
filename = r"c:\temp\test.xlsx"

today = time.strftime("%d %m %Y")

clock = time.strftime("%H:%M", time.localtime())

HEADERS = ["date","time","filename", "larva volume", "white matter volume", "ratio"]

mhafile = "c:\\blala\\toto.mha"

addinfo(filename,sheetname,HEADERS,[today,clock,os.path.basename(mhafile),1,2,3])

"""