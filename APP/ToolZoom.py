from PySide2.QtCore import QRect
from PySide2.QtGui import QPainter, QColor

from APP import MODEL
from APP.Event import Event
from APP.Tool import ToolAbstract
from typing import NewType, Tuple
QUAD= NewType('QUAD', Tuple[int,int,int,int])



###########################################################################################
#
#           Rectangular Selection
#

class ToolZoom(ToolAbstract):


    def __init__(self):
        super(ToolAbstract, self).__init__()
        self.state = 0
        self.MousePos = (0, 0)
        self.SlotPos = (0,0)
        self.idCha  = 0



    def DessinneTool(self, qp: QPainter, SlotPos, idCha, SizeInfo):

        WidgetWidth, WidgetHeight, ratio = SizeInfo

        if self.state != 0:
            """ appelé par le repaint du widget -"""
            Z = MODEL.GetZoomMode()
            sameSlot =    SlotPos == self.slotPos
            sameCHA  =    idCha   == self.idCha
            if Z == 0 and (not sameSlot or not sameCHA) : return
            if Z == 1 and (not sameSlot) : return
            if Z == 2 and (not sameCHA)  : return
            # Z: 3 : tout le monde


            qp.fillRect(0, 0, 20000, 20000, QColor(128, 128, 128))

            idFish = MODEL.GetIdFishFromSlotPos(SlotPos)
            Qimg = MODEL.GetImage(idFish, idCha)
            ImgWidth  = Qimg.width() * ratio
            ImgHeight = Qimg.height() * ratio

            # coin haut gauche de l'image affichée echelle 1
            CHGx = WidgetWidth // 2 - ImgWidth  // 2
            CHGy = WidgetHeight // 2 - ImgHeight // 2

            # ecart avec le point cliqué
            dx = int((self.MousePos[0] - CHGx) / ratio )
            dy = int((self.MousePos[1] - CHGy) / ratio )
            zoom = 4

            # coin haut gauche de l'image zoom
            CHGZx = self.MousePos[0] - zoom * dx
            CHGZy = self.MousePos[1] - zoom * dy

            qp.drawImage( QRect(CHGZx,CHGZy,Qimg.width()*zoom, Qimg.height()*zoom), Qimg)




    def ThrowRepaint(self,e:Event):
        Z = MODEL.GetZoomMode()
        if Z == 0 :  e.AskToRepaintView(* self.slotPos,self.idCha)
        if Z == 1 :  e.AskToRepaintSlot(* self.slotPos)
        if Z == 2 :  e.AskToRepaintCHA(self.idCha)
        if Z == 3 :  e.AskToRepaintAllSlots()



    def ManageEvent(self, e:Event):

        if e.IsType(Event.Type.MouseDown) and e.ButtonState.LeftButtonPressed() and self.state == 0:
            self.MousePos = (e.x, e.y)
            self.state     = 1
            self.slotPos   = e.slotPos
            self.idCha     = e.idCha
            self.ThrowRepaint(e)
            e.StopDispatch()



        if e.IsType(Event.Type.MouseUp) and self.state == 1:
            self.state = 0
            self.ThrowRepaint(e)
            e.StopDispatch()



        if e.IsType(Event.Type.MouseDown)  or e.IsType(Event.Type.MouseMove):
            if self.state == 1:
                self.MousePos = (e.x, e.y)
                self.ThrowRepaint(e)
                e.StopDispatch()

