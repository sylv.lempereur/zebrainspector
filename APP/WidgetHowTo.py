from PySide2.QtWidgets import (QLabel, QPushButton, QWidget,  QVBoxLayout,  QHBoxLayout, QApplication)
from PySide2.QtGui import QPixmap
from GlobVar import GlobVar
from APP.img import LIMG
from PySide2 import QtCore
from PySide2 import QtCore as Core
from PySide2.QtGui import QFont


str  = "<P><b>Requests at:</b>  zbi@tefor.net  "

str += "<P><b>Download software and test images:</b> at <a href = \"https://tefor.net/portfolio/zebrainspector\" >  https://tefor.net/portfolio/zebrainspector </a>"

str += "<P><b>Screen resolution:</b> best for 1920x1080 screens. Use large screens."

str += "<P><b>Image format and depth:</b> nd2, mha and tif. Priviledged depth: 12 bit."

str += "<P><b>Image specifications:</b> whole fry needed. 2500X1000X500 voxel images (voxel size around two microns, voxels must be isotropic)."

str += "<P><b>Configure interface:</b> define numbers of lines and columns. On the right of the top menu bar, select zoom mode. Zoom is performed by clicking on an image."

str += "<P><b>Downsampling factor:</b> For rendering, file downsampled four times by default. Better renderings with no ,downsampling. Adapt for best compromise between sample numbers and sizes and rendering quality."

str += "<P><b>Optical sections:</b> H, S and T buttons in top menu bar display respectively horizontal, sagittal and transverse views. Moreover, a rotation around the anteroposterior axis can be performed."

str += "<P><b>Snapshot capture:</b> Optionnally, tune image parameters by clicking on CH O 1 2 in the bottom menu bar. Click on the snapshot button below each image. Extraction of image may take one minute."

str += "<P><b>Segmentation:</b> Segmentation must be performed before registration. Select Ref/Dye channel and output channel. Default output: mha. Click tiff if needed."

str += "<P><b>Registration:</b> Select Ref/Dye channel. Default: samples with depigmented eyes (using H2O2 ). For pigmentation mutants with native fluorescence, the “pigmented eyes (no H2O2 treatment)” checkbox should be selected."

str += "<P><b>Volumetric analysis results in an excel table:</b> Select images with correct segmentations by checking the “selected” checkbox below each image. Click on the volumetry button in the bottom menu bar. During the volumetric analysis, numbers of voxels that are found in each segmentation are counted.  Values are stored in a spreadsheet to allow further statistical analysis."

str += "<P><b>Image format and depth:</b> nd2, mha and tif. For tif, use splitted channels and add _C00, _C01 or_C02 at the end of the filenames to specify channels, for correct locations of images in columns."

class WidgetHowTo(QWidget):



    def __init__(self):
        QWidget.__init__(self)
        self.setWindowModality(Core.Qt.ApplicationModal)

        self.setWindowIcon(GlobVar.AppIcon)
        self.setWindowTitle("HowTo Window")

        self.Mainlayout = QVBoxLayout(self)
        self.setLayout(self.Mainlayout)

        self.lab = QLabel()
        self.lab.setText(str)
        self.lab.setFont( QFont('Arial', 13))
        self.Mainlayout.addWidget(self.lab)
        self.lab.setWordWrap(True)
        self.lab.setOpenExternalLinks(True)



        self.B3 = QPushButton("CLOSE", self)
        self.B3.clicked.connect(lambda: self.close())
        self.Mainlayout.addWidget(self.B3)
        self.B3.setMinimumHeight(50)



        # dimensionne la fenetre de tavail par rapport à la taille de l'écran
        r = QApplication.desktop().screenGeometry()
        w, h = r.width(), r.height()
        Rx = 0.6
        ratio = (1 - Rx) / 2
        self.setGeometry(w * ratio, h * ratio, w * Rx, h * Rx)



