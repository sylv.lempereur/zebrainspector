## VERIFIE

from typing import Tuple, NewType
QUAD = NewType('QUAD', Tuple[int,int,int,int])


"""
struct qui gére une zone rectangulaire : (Xmin,Ymin,Xmax,Ymax).
La zone est définie avec MIN et MAX COMPRIS >= et <=
"""

def Xmin(Q: QUAD) -> int :
    return Q[0]

def Ymin(Q: QUAD) -> int :
    return Q[1]

def Xmax(Q: QUAD) -> int:
    return Q[2]

def Ymax(Q: QUAD) -> int:
    return Q[3]


def UpperLeft(Q:QUAD) -> Tuple[int,int]  :
    Xmin,Ymin,Xmax,Ymax = Q
    return Xmin,Ymin

def LowerLeft(Q:QUAD) -> Tuple[int,int]  :
    Xmin,Ymin,Xmax,Ymax = Q
    return Xmin,Ymax


def LowerRight(Q:QUAD) -> Tuple[int,int]  :
    Xmin, Ymin, Xmax, Ymax = Q
    return Xmax, Ymax


def FromP1P2(P1:Tuple[int,int],P2:Tuple[int,int])->QUAD:
    """ Crée un Quad à partir de deux points. Les points sont QUELCONQUES - ils n'ont pas besoin d'être le hautgauche et le basdroit. """
    x1, y1 = P1
    x2, y2 = P2
    return min(x1, x2), min(y1, y2), max(x1, x2), max(y1, y2)



def FromPWH(XY_UL:Tuple[int,int],W:int,H:int)->QUAD :
    """ create quad from Position / Width / Height """
    x,y = XY_UL
    if W < 1 : return None
    if H < 1 : return None
    return x,y,x+W-1,y+H-1






