from APP.Tool import ToolAbstract
from numpy import maximum


from typing import NewType,Tuple
QUAD= NewType('QUAD', Tuple[int,int,int,int])
import copy

from APP.VolReader import VolReader
from APP.NPToImage import EmptyVol
from APP.NPToImage import VolToSlice, SliceToImage
from GlobVar import GlobVar
import APP.GlobFnt as GlobFnt

######################################################################################################################
######################################################################################################################

"""
idFish : numero du poisson  
idCol  : numéro de la colonne, correspond à la modalité/channel
idSlot : numéro de la ligne à l'écran, va de 0 à 5
"""

LinkAllScrollbars = False

EmptyVolume = EmptyVol()  # cube avec diagonales utilisé pour remplir les zones vides

_MainTool        = ToolAbstract()  # outil en cours, on a qu'un outil : zoom

MainWindow       = None   # fenetre principale (menu,docks...)
FishWindow       = None   # fenetre gérant l'affichage des poissons
#_ImageWidget     = [ [ None, None, None ]   for i in range(GlobVar.MaxFishPerPage) ]  # les 6x3 zones d'affichage

class ViewInfo :                        # informations associées à UN affichage écran = 1poisson/1channel
    def __init__(self):
        self._VolumeBin        = EmptyVolume    # le volume3D original en bin4 8 bits
        self._Sprite           = None           # vignette précalculée - utilisée pour affichage à l'écran
        self._SpriteData       = None           # pointeur vers le tbl de data du sprite pour eviter un GC
        self._DirtySprite      = True           # la vignette n'est plus à jour, doit être recalculée si besoin
        self._Loaded           = False          # no data Loader


class FishInfo :                     # information relative à un poisson et commune à toutes ses channels
    def __init__(self):
        self._PosXYZ     = [0,0,0]   # position 3D dans plans de coupe
        self._Selected   = True      # indique si ce poisson est sélectionné
        self._View       = [ ViewInfo() for i in range(GlobVar.MaxChannels) ]  # les trois modalités
        # stocke la liste des actions appliquées à ce poisson
        self._History    = [ { "ACTION": "START", "CHA_INPUTS" : [None,None,None], "CHA_OUTPUTS" : [None,None,None]} ]
        self._CurrentHistory = 0;


class DataModel:
    def __init__(self):
        # Global
        self._CurrentProj  = GlobVar.VueDorsale     # Projection des vues
        self._AngleRot     = 0                      # angle de rotation sur l'axe principal (rotissoire)
        self._CurrentPage  = 0                      # page active
        self._CurrentThick = GlobVar.LThickness[0]  # epaisseur de proj = nombre de couches empilées (en absolue = avant le bin4)
        self._ZoomMode     = 1                      # indique comment se comporte l'outil zoom : col/ligne/tous
        self._RotQuality   = 2                      # 0 1 2 3   qualité de l'algo de rotation

        self._ParamGamma = [[0, 255, 1] for i in range(GlobVar.MaxChannels)]  # black/white/Gamma pour chaque modalité
        self._Fish         = [ FishInfo() for i in range(GlobVar.MaxFishes+50)]  # les infos des 48 poissons potentiels



_BinFactor = 4    # commun à toutes les images chargées  x1  x2  x4  x8
_BitDepth  = 12

_Data = DataModel()  # et voila le pointeur vers toute la structure du model


######################################################################################################################
######################################################################################################################


#########################      BASICS

def LinkScrollBars(TrueFalse):
    global LinkAllScrollbars
    LinkAllScrollbars = TrueFalse

def AreScrollBarsLinked():
    return LinkAllScrollbars

def GetBitDepthMaxValue():
    return 2**_BitDepth - 1

def GetBitDepth():
    return _BitDepth

def SetBitDepth(val):
    global  _BitDepth
    _BitDepth = val

def GetBinLevel():
    L = { 1:0, 2:1, 4:2, 8:3, 16:4, 32:5}
    return L[_BinFactor]

def GetBinFactor():           # ratio entre l'image/vol en mémoire et la résolution d'origine des data
    return _BinFactor

def SetBinFactor(val):
    global _BinFactor
    _BinFactor = val


def InitModel():
    global _Data
    _Data = DataModel()
    if MainWindow is not None :
        MainWindow.ResetIHM()

def GetFirstFreeIdFish():  # determine first idFish not used (no data loaded)
    for idFish in range(GlobVar.MaxFishes):
        V0 = _Data._Fish[idFish]._View[0]
        V1 = _Data._Fish[idFish]._View[1]
        V2 = _Data._Fish[idFish]._View[2]

        if   V0._Loaded == False and  V1._Loaded == False  and  V2._Loaded == False :
            return idFish

    return 0

#######################     Selected

def SetSelected(idFish,val):
    Fish = _Data._Fish[idFish]
    Fish._Selected = val

def GetSelected(idFish):
    Fish = _Data._Fish[idFish]
    return Fish._Selected

#######################     Rotation

def SetRotAngle(angleRotDeg):


    _Data._AngleRot = angleRotDeg
    if MainWindow is not None :
        MainWindow.ModifyRotValueIHM(angleRotDeg)
    InvalidateAllSprites()


def GetRotAngle():
    return _Data._AngleRot

def GetRotQuality() :
    return _Data._RotQuality

def SetRotQuality(id):
    _Data._RotQuality = id
    InvalidateAllSprites()

#######################     HISTORY


def HistoryUndo(idFish):
    Fish = _Data._Fish[idFish]
    if Fish._CurrentHistory > 0 :
        oldHistory = Fish._CurrentHistory
        newHistory = oldHistory - 1
        Fish._CurrentHistory = newHistory
        return Fish._History[oldHistory], Fish._History[newHistory]


def HistoryRedo(idFish):
    Fish = _Data._Fish[idFish]
    if Fish._CurrentHistory < len(Fish._History) - 1:
        oldHistory = Fish._CurrentHistory
        newHistory = oldHistory + 1
        Fish._CurrentHistory = newHistory
        return Fish._History[oldHistory], Fish._History[newHistory]


def _CancelForward(idFish):
    Fish = _Data._Fish[idFish]
    Histo = Fish._History
    Fish._History = Fish._History[0:Fish._CurrentHistory+1]


def HistoryNewStep(idFish,ACTION_TAG,LPARAMS):
    Fish = _Data._Fish[idFish]
    CurrentHisto = Fish._History[-1]

    _CancelForward(idFish) # en cas de backward
    newStep = {}
    newStep["ACTION"]  = ACTION_TAG
    newStep["LPARAMS"] = LPARAMS
    newStep["CHA_INPUTS"]  = copy.deepcopy(CurrentHisto["CHA_OUTPUTS"])
    newStep["CHA_OUTPUTS"] = copy.deepcopy(CurrentHisto["CHA_OUTPUTS"])

    Fish._History.append( newStep )

    Fish._CurrentHistory = len(Fish._History) - 1;

def HistoryChangeVolInChannel(idFish,idCha,FileNIdVol) :
    Fish = _Data._Fish[idFish]
    CurrentHisto = Fish._History[-1]
    CurrentHisto["CHA_OUTPUTS"][idCha] = FileNIdVol


def HistoryAddKeyToCurrentHistory(idFish,KEY,LPARAMS):
    _CancelForward(idFish)  # en cas de backward
    Fish = _Data._Fish[idFish]
    CurrentHistory = Fish._History[-1]
    CurrentHistory[KEY] = LPARAMS


def HistoryGetCopy(idFish):
    Fish = _Data._Fish[idFish]
    H = Fish._History[0:Fish._CurrentHistory+1]
    H2 = copy.deepcopy(H)
    return H2

def HistoryGetFilesInAllSlot():
    L = []
    for idFish in range(0, GlobVar.MaxFishes):

        H = HistoryGetCopy(idFish)[-1]  # prend la dernière historique
        L.append(  H["CHA_OUTPUTS"]  )
    return L


#######################     POSITION


def SetPosition(idFish,Pos3D):
    Fish = _Data._Fish[idFish]
    Fish._PosXYZ = Pos3D
    InvalidateFishSprites(idFish)


def GetPosition(idFish):
    Fish = _Data._Fish[idFish]
    return Fish._PosXYZ

########################    ZOOM Mode

def GetZoomMode():
    return _Data._ZoomMode

def SetZoomMode(mode):
    _Data._ZoomMode = mode

#########################    THICKNESS

def SetThickness(val):
    _Data._CurrentThick = val
    InvalidateAllSprites()

def GetThickness():
    return _Data._CurrentThick

#########################    PAGE



def GetMaxNumberOfPages():
    return (GlobVar.MaxFishes // FishWindow.GetNbFishPerPage()) + 1


def ChangePage(idPage):
    if idPage < 0 : idPage = 0
    if idPage >= GetMaxNumberOfPages() : idPage = GetMaxNumberOfPages() - 1
    _Data._CurrentPage = idPage
    if MainWindow is not None :
        MainWindow.SetPage(idPage)

def GetCurrentPage():
    return _Data._CurrentPage

#########################    ParamGamma

def GetGammaParam(idCol) :
    return  _Data._ParamGamma[idCol]

def SetGammaParam(idCol,Params):
    _Data._ParamGamma[idCol] = Params
    InvalidateOneChannelSprites(idCol)

#########################     Axis & Proj


def GetidAxe():  # retourne le numéro de l'axe utilsée par les scrollbars
    NameProj = _Data._CurrentProj
    if NameProj == GlobVar.VueTransverse  :    return 2
    if NameProj == GlobVar.VueDorsale     :    return 0
    return 1

# si proj dorsale, on recherche la longueur la plus importante sur les trois volumes dans l'axe correspondant
def GetAxisLength(idFish) :

    idAxe  = GetidAxe()
    Vol3D0 = _GetVolume(idFish, 0).shape
    Vol3D1 = _GetVolume(idFish, 1).shape
    Vol3D2 = _GetVolume(idFish, 2).shape
    Vmax   = maximum(maximum(Vol3D0, Vol3D1), Vol3D2 )
    length = Vmax[idAxe]
    return length

# pour une proj donnée, retourne la position sur l'axe en question
def GetAxisPos(idFish) :
    Fish = _Data._Fish[idFish]
    idAxe = GetidAxe()
    pos3D = Fish._PosXYZ
    return pos3D[idAxe]


def ChangeProj(projName):
    _Data._CurrentProj = projName
    InvalidateAllSprites()



def GetProjName():
    return _Data._CurrentProj

##################################       Volumes


def SetVolume(idFish,idCha,Vol3D):
    if idFish > GlobVar.MaxFishes : return   # check
    View = _Data._Fish[idFish]._View[idCha]
    View._VolumeBin = Vol3D
    View._Loaded = True
    InvalidateSprite(idFish, idCha)

def _GetVolume(idFish,idCha):
    View = _Data._Fish[idFish]._View[idCha]
    return View._VolumeBin





####################################################################################################################
#
#       Get current

# idSlot : n° du slot sur la page d'affichage

def GetFishidSlot(idFish:int):
    if idFish < 0 or idFish >= GlobVar.MaxFishes : return None
    firstFishInPage = _Data._CurrentPage * FishWindow.GetNbFishPerPage()
    idslot = idFish - firstFishInPage
    return idslot

def GetFishPage(idFish:int):
    if idFish < 0 or idFish >= GlobVar.MaxFishes : return None
    idPage = idFish // FishWindow.GetNbFishPerPage()
    return  idPage

def IsFishVisible(idFish:int):
    idPage = GetFishPage(idFish)
    return idPage == GetCurrentPage()

def GetIdFishFromSlotPos(SlotPos):
    idSlot = FishWindow.SlotPosToSlotId(SlotPos)
    idFish = idSlot + GetCurrentPage() * FishWindow.GetNbFishPerPage()
    return idFish

def GetIdFishFromidSlot(idSlot):
    idFish = idSlot + GetCurrentPage() * FishWindow.GetNbFishPerPage()
    return idFish


def GetSlotNameFromidFish(idFish:int):
    #idSlot = GetFishidSlot(idFish)
    #idPage = GetFishPage(idFish)
    #slotName = "A1"
    #if (idSlot is not None) and (idPage is not None):
    slotName = GlobFnt.SlotName(idFish)
    return slotName

###################      Gestion des images/vignettes


def GetImage(idPoisson, idCha):               # Pattern Dirty
    Fish = _Data._Fish[idPoisson]
    View = Fish._View[idCha]

    if View._DirtySprite :                    # la vignette n'est plus valide, il faut recalculer l'image affichée
       Vol3D     = _GetVolume(idPoisson, idCha)
       pos3D     = Fish._PosXYZ
       Gamma     = _Data._ParamGamma[idCha]
       ThickBin = _Data._CurrentThick // GetBinFactor()   # bin4 lors du load, il faut diminuer l'ep par4
       NameProj  = _Data._CurrentProj
       angle     = GetRotAngle()
       qual      = GetRotQuality() * 2 + 1    # pour 0=> 1 sample, 1=>3² = 9 samples,  2=>(5x5)=25, pour 3=>(7x7)=49 samples et

       T                 = VolToSlice(NameProj,angle, pos3D, Vol3D,ThickBin,qual)
       Sprite,SpriteData = SliceToImage(T,Gamma)
       View._Sprite      = Sprite
       View._SpriteData  = SpriteData
       View._DirtySprite = False

    return View._Sprite

def InvalidateSprite(idFish, idChannel) :
    View = _Data._Fish[idFish]._View[idChannel]
    View._DirtySprite= True

def InvalidateFishSprites(idFish):
    for i in range(GlobVar.MaxChannels):
        InvalidateSprite(idFish, i)

def InvalidateOneChannelSprites(idChannel):
    for idFish in range(GlobVar.MaxFishes):
        InvalidateSprite(idFish, idChannel)

def InvalidateAllSprites():
    for i in range(GlobVar.MaxFishes) :
       InvalidateFishSprites(i)



###################      TOOL


def ChangeTool(ToolInstance):
    global _MainTool
    _MainTool.Terminate()
    _MainTool = ToolInstance


def GetCurrentTool():
    return _MainTool




