from PySide2.QtWidgets import (QPushButton, QProgressBar, QLabel, QHBoxLayout, QCheckBox, QComboBox, QWidget, QFileDialog, QVBoxLayout, QTableView)
from PySide2 import QtCore
from PySide2 import QtGui
from GlobVar import GlobVar
from APP.VolReader import VolReader
from PySide2.QtCore import QBasicTimer

from APP import MODEL
from time import time
from os import makedirs
import os.path
from os.path import (
    exists,
    join as ospathjoin,
    splitext)
from numpy import (
    clip,
    ndarray
    )
from SimpleITK import (GetImageFromArray, WriteImage)
from APP.Event import Event
from pathlib import Path
from SylvainWrapper import (
    get_affine_matrix,
    SylvainRecalage,
    SylvainSegmentation
    )

import APP.GlobFnt as GlobFnt

############################################################################
#
#
#       Thread used to load Data // GUI Thread not used for this
#
#


class LoaderThread(QtCore.QThread):

    signal_progressbar    = QtCore.Signal(int)
    signal_changeRowColor = QtCore.Signal(int,int,str)
    signal_Terminated     = QtCore.Signal()
    signal_SendData       = QtCore.Signal(int,int,object)
    signal_AddInfoInHistory    = QtCore.Signal(int, str, object)

    def __init__(self, SeqChannelOrder, outputFolder, CallingWindow,   ReferenceChannel, OutChannel,  UseTiff):
        QtCore.QThread.__init__(self)
        self.SeqChannelOrder = SeqChannelOrder.copy()
        self.AskToStop = False

        self.outputFolder = outputFolder
        if not exists(outputFolder):
            makedirs(outputFolder)

        self.ReferenceChannel = ReferenceChannel
        self.OutChannel = OutChannel
        self.tiff = UseTiff
        CallingWindow.signal_AskToStop.connect(self.AAskToStop)

    @QtCore.Slot()
    def AAskToStop(self):
        self.AskToStop = True

    def BuildFileName(self,filename, idFish, channel):
        def len3(s):
            s= str(s)
            if len(s) >= 3 : return s
            else:  return len3("0"+s)

        newfilename, a ,b = GlobFnt.SplitFileName(filename,"_C","_ID")

        format = ".mha"
        if self.tiff : format = ".tiff"

        code = "_ID" + len3(idFish) + "_C" + len3(channel)

        newfilename += code + "_segmentation"   + format

        fullfilename = ospathjoin(self.outputFolder, newfilename)

        return fullfilename

    def LoadFileDataForProcessing(self, filen, idInFile ):


        # first pass create MHA
        R = VolReader(filen)
        rep = R.LoadData(idInFile)[0]
        Vol3D = rep[2]
        spacing   = R.GetSpacingAfterLoad()


        return [  Vol3D, spacing ]


    def run(self):

        channelid = self.ReferenceChannel

        # comptage des jobs
        nbJobDone = 0
        nbJobs = 0
        for idFish in range(0, GlobVar.MaxFishes):
            if MODEL.GetSelected(idFish):
                if self.SeqChannelOrder[idFish][channelid] != None:
                    nbJobs += 1

        # lancement du traitement

        for idFish in range(0, GlobVar.MaxFishes):

            if not MODEL.GetSelected(idFish): continue  # le user n'a pas sélectionné ce poisson

            FileInSlot = self.SeqChannelOrder[idFish][channelid]
            if FileInSlot ==   None  : continue
            # structure de FileInSlot [ [ (filename, idInFile, row) x 3 ] x idFish ]

            try:

                # etape 1 : recherche tous les fichiers concernant cette ligne idFish

                if self.AskToStop: continue

                nbJobDone += 1  # ce fichier doit etre traité
                filename, idInFile, rowInTable = FileInSlot

                #  chargement des données

                self.signal_changeRowColor.emit(rowInTable, 1, "Loading Data")
                Vol3D, spacing  = self.LoadFileDataForProcessing( filename, idInFile )

                basename =  os.path.basename(filename)
                newfilename = ospathjoin(self.outputFolder,basename)
                fullfilename = self.BuildFileName(filename,idFish,self.OutChannel)


                # etape2 : recalage + save + reload  de toutes les modalités de la ligne

                self.signal_changeRowColor.emit(rowInTable, 2, "Segmentation in progress - Channel " + str(channelid)  )

                Vol3D = SylvainSegmentation(Vol3D)

                # au cas ou les algos retourne des valeurs un peu supérieure 4095
                clip(Vol3D, 0, MODEL.GetBitDepthMaxValue(), out=Vol3D)


                # sav du mha recalé
                self.signal_changeRowColor.emit(rowInTable, 3, "Writing file to disk - Channel " + str(channelid)  )
                T = GetImageFromArray(Vol3D)
                T.SetSpacing((spacing, spacing, spacing) )
                WriteImage(T, fullfilename)

                # send data to GUI thead
                self.signal_changeRowColor.emit(rowInTable, 4, "Updating view - Channel " + str(channelid)  )
                self.signal_SendData.emit(idFish, self.OutChannel, (fullfilename, 0 , Vol3D) )
                self.signal_AddInfoInHistory.emit(idFish, "SEGMENTATION_SYLVAIN",
                                                  {"REF_CHANNEL": self.ReferenceChannel,
                                                   "OUT_CHA": self.OutChannel})
                Vol3D = None   # gc

                # terminé
                self.signal_changeRowColor.emit(rowInTable, 5, "")

            except :

                if FileInSlot == None : continue
                filename, idInFile, rowInTable = FileInSlot
                self.signal_changeRowColor.emit(rowInTable, 6, "ERROR")


            # affichage progression
            pcent = ((nbJobDone) * 100) / nbJobs
            self.signal_progressbar.emit(pcent)



        self.signal_Terminated.emit()



############################################################################
#
#
#       GUI to select and to launch loading process
#
#



# A window class
class WidgetSegmentation(QWidget):

    ####################################################################
    #
    #     LOAD INFORMATION OF SELECTED FILES


    def ModelInit(self):
        self.model.clear()
        self.model.setHorizontalHeaderLabels(['Fish', 'Channel', 'Filename & Id' ])

    def FitWindowToTable(self):
        self.table.resizeColumnsToContents()
        w = 0
        for j in range(3):
            w += self.table.columnWidth(j);
        self.setFixedWidth(w + 50)


    def StoreInfoInTable(self,row,INFOS):
        for x in range(len(INFOS)) :
           self.model.setItem(row, x, QtGui.QStandardItem(str(INFOS[x])))
           item = self.model.item(row, x)
           item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
           item.setTextAlignment( QtCore.Qt.AlignCenter )

    def FillTable(self,seq):

        row = 0
        for idFish in range(GlobVar.MaxFishes):
            for ChannelId in range(3):
                FileInChannel = seq[idFish][ChannelId]
                if FileInChannel is None : continue
                filen, idInFile = FileInChannel

                fileshortname = Path(filen).name + "  (" + str(idInFile) +")"
                idFis = MODEL.GetSlotNameFromidFish( idFish )

                self.StoreInfoInTable(row,[idFis, ChannelId, fileshortname ])

                infos = (filen , idInFile , row)
                seq[idFish][ChannelId] = infos    # insere le n° de ligne dans le tableau

                row += 1

        self.FitWindowToTable()



    ####################################################################
    #
    #     LOAD DATA
    #     mainly the view interactions : progressbar, message
    #     exchange signals with the loader thread

    signal_AskToStop = QtCore.Signal()

    @QtCore.Slot(int,str,object)
    def AddInfoInHistory(self, idFish, action, LPARAMS):
        MODEL.HistoryAddKeyToCurrentHistory(idFish,action,LPARAMS)


    @QtCore.Slot(int, int, ndarray)
    def SendData(self, idFish, idCol, Info3D):
        Event.Create.LoadData(idFish,idCol,Info3D).Dispatch()  # send data to model


    @QtCore.Slot(int)
    def UpdateProgressBar(self, pcentpgrogress : int  ):
        self.pbar.setValue(pcentpgrogress)



    @QtCore.Slot(int, int,str)
    def ChangeRowColor(self, rowk:int, colorid : int, msg : str):
        self.MMM = msg
        self.t0 = time()

        if colorid == 0: color = QtCore.Qt.white
        if colorid == 1: color = QtCore.Qt.yellow
        if colorid == 2: color = QtCore.Qt.magenta
        if colorid == 3: color = QtCore.Qt.cyan
        if colorid == 4: color = QtCore.Qt.lightGray
        if colorid == 5: color = QtCore.Qt.green
        if colorid == 6: color = QtCore.Qt.red

        for x in range(3):
            item = self.model.item(rowk, x)
            if item is not None:
                item.setBackground(color)
                self.model.setItem(rowk, x, item)
                index = self.model.index(rowk, x)
                self.table.update(index)
                self.table.scrollTo(index)
                self.table.repaint()

    # run the loader thread
    def StartLoad(self):
        if not self.LoadRunning :
           OutputFolder = QFileDialog.getExistingDirectory(None, "Select Output Folder")
           if len(OutputFolder) == 0: return

           GlobVar.SegFolder = OutputFolder   # reused in volumetric analysis

           self.LoadRunning = True
           self.pbar.setValue(0)
           self.MMM = "Loading in progress..."

           for k in range(self.model.columnCount()) :
              self.ChangeRowColor(k,0,"")


           self.ThreadLoad = LoaderThread(self.SeqChannelOrder,  OutputFolder, self,  self.ReferenceCHA, self.OutCHA, self.checkTiff.isChecked())
           self.ThreadLoad.signal_progressbar.connect(self.UpdateProgressBar)
           self.ThreadLoad.signal_changeRowColor.connect(self.ChangeRowColor)
           self.ThreadLoad.signal_Terminated.connect(self.ProcessFinished)
           self.ThreadLoad.signal_SendData.connect(self.SendData)
           self.ThreadLoad.signal_AddInfoInHistory.connect(self.AddInfoInHistory)


           self.ThreadLoad.start()

           self.t0 = time()
           self.timer = QBasicTimer()
           self.timer.start(1000, self)

    def timerEvent(self, e):
        dt = str(int(time() - self.t0))
        msg = self.MMM + " - " + dt + " seconds"
        self.LoadButton.setText(msg)
        self.LoadButton.repaint()
        self.LoadButton.update()

    @QtCore.Slot()
    def ProcessFinished(self):
        self.timer.stop()
        self.buttonSTOP.setText("STOP")
        self.LoadButton.setText(self.msgLoadButton)
        self.LoadRunning = False

    def StopLoading(self):
        self.buttonSTOP.setText("Asking to stop")
        self.signal_AskToStop.emit()



    ####################################################################
    #
    #     CTR

    def ChangeReferenceCHA(self):

        L = [(0,1),(0,2),(1,0),(1,2),(2,0),(2,1)]
        self.ReferenceCHA, self.OutCHA = L[self.combo.currentIndex()]


    def __init__(self, seq ):
        QWidget.__init__(self)
        self.ReferenceCHA = 0
        self.OutCHA = 1 #  CHA de sortie de l'algo de segmentation


        self.setWindowIcon(GlobVar.AppIcon)
        self.LoadRunning = False
        self.SeqChannelOrder = seq



        self.setWindowTitle("Process Segmentation")

        # Create the QTableView widget and associate to its model

        self.model = QtGui.QStandardItemModel(self)
        #self.ModelInit()
        self.table = QTableView(self)
        self.table.setModel(self.model)
        self.table.verticalHeader().hide()
        self.table.setMinimumHeight(100)
        self.table.height = 500

        # Place the table widget into a layout
        layout = QVBoxLayout(self)

        layout.addWidget(QLabel("Important: please check all required marks"))

        self.combo = QComboBox(self)
        self.combo.addItem("Ref/Dye : CHA 0   -   Out : CHA 1")
        self.combo.addItem("Ref/Dye : CHA 0   -   Out : CHA 2")
        self.combo.addItem("Ref/Dye : CHA 1   -   Out : CHA 0")
        self.combo.addItem("Ref/Dye : CHA 1   -   Out : CHA 2")
        self.combo.addItem("Ref/Dye : CHA 2   -   Out : CHA 0")
        self.combo.addItem("Ref/Dye : CHA 2   -   Out : CHA 1")
        self.combo.setCurrentIndex(self.ReferenceCHA)
        self.combo.activated[str].connect(self.ChangeReferenceCHA)
        layout.addWidget(self.combo)

        lay4 = QHBoxLayout(self)
        lay4.addWidget( QLabel("Save in tiff format:     "),  0, QtCore.Qt.AlignLeft)
        self.checkTiff = QCheckBox("", self)
        self.checkTiff.setChecked(False)
        lay4.addWidget(self.checkTiff, 1, QtCore.Qt.AlignLeft)
        layout.addLayout(lay4)

        self.msgLoadButton = "Start Process"
        self.LoadButton = QPushButton(self.msgLoadButton, self)
        self.LoadButton.clicked.connect(self.StartLoad)
        layout.addWidget(self.LoadButton)

        self.pbar = QProgressBar(self)
        self.pbar.setValue(0)
        layout.addWidget(self.pbar)

        self.buttonSTOP = QPushButton("STOP", self)
        self.buttonSTOP.clicked.connect(self.StopLoading)
        layout.addWidget(self.buttonSTOP)

        layout.addWidget(self.table)
        self.setLayout(layout)

        self.ModelInit()
        self.FillTable(self.SeqChannelOrder)
        self.setMinimumWidth(300)







