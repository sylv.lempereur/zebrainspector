
from time import time

#print("\t import psutil")
from psutil import virtual_memory
#print("\t" + str(time()))
#print("\t import gc")
from gc import collect
#print("\t" + str(time()))

#print("\t import PySide2")
from PySide2 import QtCore

from PySide2.QtGui     import QIcon
from PySide2.QtWidgets import (QAction, QToolBar, QApplication, QStyle, QLabel, QComboBox, QPushButton, QSpinBox,
                               QButtonGroup, QMainWindow, QMessageBox, QMenuBar, QWidget, QFileDialog)

from PySide2.QtCore   import QBasicTimer
from PySide2.QtGui import QPixmap
from PySide2.QtGui import QIcon
from PySide2.QtCore import QRect
from PySide2.QtGui import QPainter, QColor
#print("\t" + str(time()))

from APP.WidgetHowTo import WidgetHowTo

#print("\t import APP.WidgetMemory")
from APP.WidgetMemory import WidgetMemory
#print("\t" + str(time()))


#print("\t import APP.WidgetLoadInfo")
from APP.WidgetLoadInfo  import  WinLoadInfo
#print("\t" + str(time()))

#print("\t import APP.WidgetGammaCor")
from APP.WidgetGammaCor  import WidgetGammaCorrection
#print("\t" + str(time()))

#print("\t import APP.WidgetHelp")
from APP.WidgetHelp      import WidgetHelp
#print("\t" + str(time()))

#print("\t import GlobVar")
from GlobVar import GlobVar
#print("\t" + str(time()))
#print("\t import APP.WidgetFishes")
from APP.WidgetFishes import WidgetFishes
#print("\t" + str(time()))
#print("\t import APP.MODEL")
from APP import MODEL
#print("\t" + str(time()))
#print("\t import APP.MEDIATEUR")
from APP.MEDIATEUR import MEDIATEUR
#print("\t" + str(time()))
#print("\t import APP.Event")
from APP.Event import Event
#print("\t" + str(time()))
#print("\t import APP.WidgetRecalSegment")
from APP.WidgetRegistration import WidgetRegistration
from APP.WidgetSegmentation import WidgetSegmentation
#print("\t" + str(time()))
#print("\t import APP.WidgetVolum")
from APP.WidgetVolum import WidgetVolum
#print("\t" + str(time()))

#########################################################################################################################################
#
#
#           creation fenetre


class MainWindow(QMainWindow):



    def __init__(self):
        super(MainWindow, self).__init__()
        MODEL.MainWindow = self


        self.setMenuBar(QMenuBar(self))
        self.setWindowTitle(GlobVar.TitleApp)
        self.statusBar().showMessage("Ready")

        # dimensionne la fenetre de tavail par rapport à la taille de l'écran
        r = QApplication.desktop().screenGeometry()
        w,h = r.width(), r.height()
        ratio = (1 - GlobVar.TailleFenetreDemarrage) / 2
        self.setGeometry(w *ratio, h*ratio, w*GlobVar.TailleFenetreDemarrage,h*GlobVar.TailleFenetreDemarrage)

        self.DefaultButtons = []
        self.createActions()
        self.createMenus()
        self.createTopToolBars()
        self.createBottomToolBars()
        self.createDockWindows()

        Event.Create.ChangeTool("ToolZoom",ChangeButtonState=True).Dispatch()

        self.ResetIHM()

        self.mymsg = WidgetHowTo()
        self.mymsg.show()


    #########################################################################################################################################
    #
    #
    #           ACTIONS et shortcuts

    def CreateICO(self):
        ico = QIcon()
        self.icolist = []  #to avoid garbage

        for s in [ 16, 20, 24, 32, 40, 48, 64, 96, 128, 256 ]:
            t = QPixmap(s, s)
            qp = QPainter(t)

            bleu   = QColor(59,75,129)
            qp.setBrush(bleu)
            qp.fillRect(0,0,1000,1000,bleu)

            orange = QColor(214, 124, 28, 255)
            qp.setBrush(orange)
            p13 = (s // 3) + 1
            p23 = (s * 2 // 3) -1
            p12 = (s // 2)
            qp.fillRect(p13, 0, 1000, p23, orange)
            qp.fillRect(0, p23, p13, 1000, orange)

            self.icolist.append(t)
            ico.addPixmap(t)

        return ico

    def createActions(self):
        def SetAction( icon = None, text = None,   triggered = None, shortcut = None, statusTip = None):
            if icon is not None :
              A = QAction(icon, text, self)
            else:
              A = QAction(text, self)
            if triggered is not None: A.triggered.connect(triggered)
            if statusTip is not None: A.setStatusTip(statusTip) # si different de text
            if shortcut is not None:  A.setShortcut(shortcut)
            return A

        # https://joekuan.wordpress.com/2015/09/23/list-of-qt-icons/
        desktop_icon = QIcon(QApplication.style().standardIcon(QStyle.SP_DesktopIcon))
        media_icon = QIcon(QApplication.style().standardIcon(QStyle.SP_MediaVolume))
        CDIcon = QIcon(QApplication.style().standardIcon(QStyle.SP_DriveCDIcon))
        # https://joekuan.wordpress.com/2015/09/23/list-of-qt-icons/
        IconLeft = QIcon(QApplication.style().standardIcon(QStyle.SP_ArrowLeft))
        IconRight = QIcon(QApplication.style().standardIcon(QStyle.SP_ArrowRight))
        IconUp = QIcon(QApplication.style().standardIcon(QStyle.SP_ArrowUp))
        IconDown = QIcon(QApplication.style().standardIcon(QStyle.SP_ArrowDown))

        PermutIcon = QIcon(QApplication.style().standardIcon(QStyle.SP_BrowserReload))
        BlankSheet = QIcon(QApplication.style().standardIcon(QStyle.SP_FileIcon))
        CroixRouge = QIcon(QApplication.style().standardIcon(QStyle.SP_DialogCancelButton))
        IconSave = QIcon(QApplication.style().standardIcon(QStyle.SP_DialogSaveButton))

        IconOpenFolder = QIcon(QApplication.style().standardIcon(QStyle.SP_DialogOpenButton))
        IconSaveSelected = QIcon(QApplication.style().standardIcon(QStyle.SP_DialogSaveButton))
        IconHelp = QIcon(QApplication.style().standardIcon(QStyle.SP_TitleBarContextHelpButton))

        # QT SEQUENCE
        # https://doc.qt.io/qt-5/qkeysequence.html
        GlobVar.AppIcon =  QIcon(QApplication.style().standardIcon(QStyle.SP_FileDialogListView))
        GlobVar.AppIcon  = self.CreateICO()
        self.setWindowIcon(GlobVar.AppIcon)


        self.ACTquitAct      = SetAction(QIcon(),"&Quit",  shortcut="Ctrl+Q", statusTip="Quit the application", triggered=self.CloseApp)
        self.ACTaboutAct     = SetAction(QIcon(),"&About",  shortcut=None, statusTip="Show the application's About box",  triggered=self.ActionAbout)
        self.ACTopenFiles    = SetAction(IconOpenFolder, "&Load Data", shortcut="Ctrl+L", statusTip="Load Data",   triggered=self.LoadData)
        self.ACTClearData    = SetAction(BlankSheet, "Clear Data",  statusTip="Clear Data",   triggered=self.ClearData)
        self.ACTSaveSelected = SetAction(IconSaveSelected, "Save Selected Fishes", statusTip="Save Selected Fishes", triggered=self.SaveSelected)
        self.ACTHelp = SetAction(IconHelp, "Help", statusTip="Help", triggered = self.Help)

        self.ACTBin1  = SetAction(None, "No Downsampling",  statusTip="No Downsampling",  triggered= lambda: self.SetBinning(1))
        self.ACTBin2  = SetAction(None, "Downsampling x2",  statusTip="Downsampling by a factor 2",  triggered=lambda: self.SetBinning(2))
        self.ACTBin4  = SetAction(None, "Downsampling x4",  statusTip="Downsampling by a factor 4",  triggered=lambda: self.SetBinning(4))
        self.ACTBin8  = SetAction(None, "Downsampling x8",  statusTip="Downsampling by a factor 8",  triggered=lambda: self.SetBinning(8))
        self.ACTBin1.setCheckable(True);
        self.ACTBin2.setCheckable(True);
        self.ACTBin4.setCheckable(True);
        self.ACTBin8.setCheckable(True);



        self.ACTBitDepth = []
        s = self
        s.A08 = SetAction(None, " 8 bits Bit Depth", statusTip="", triggered=lambda: self.SetBitDepth(8))
        s.A09 = SetAction(None, " 9 bits Bit Depth", statusTip="", triggered=lambda: self.SetBitDepth(9))
        s.A10 = SetAction(None, "10 bits Bit Depth", statusTip="", triggered=lambda: self.SetBitDepth(10))
        s.A11 = SetAction(None, "11 bits Bit Depth", statusTip="", triggered=lambda: self.SetBitDepth(11))
        s.A12 = SetAction(None, "12 bits Bit Depth", statusTip="", triggered=lambda: self.SetBitDepth(12))
        s.A13 = SetAction(None, "13 bits Bit Depth", statusTip="", triggered=lambda: self.SetBitDepth(13))
        s.A14 = SetAction(None, "14 bits Bit Depth", statusTip="", triggered=lambda: self.SetBitDepth(14))
        s.A15 = SetAction(None, "15 bits Bit Depth", statusTip="", triggered=lambda: self.SetBitDepth(15))
        s.A16 = SetAction(None, "16 bits Bit Depth", statusTip="", triggered=lambda: self.SetBitDepth(16))

        self.ACTBitDepth = [ s.A08, s.A09, s.A10, s.A11, s.A12, s.A13, s.A14, s.A15, s.A16 ]
        for T in self.ACTBitDepth :
            T.setCheckable(True)

    def SetBitDepth(self, val):
        reponse = MEDIATEUR.ClearData(True)
        if reponse == False: return  # abort

        pos = val-8
        for v in range(8):
            if v != pos:     self.ACTBitDepth[v].setChecked(False);
        self.ACTBitDepth[pos].setChecked(True);

        MODEL.SetBitDepth(val)


    def SetBinning(self,val):
        reponse = MEDIATEUR.ClearData(True)
        if reponse == False : return           # abort
        if val == 8 and MODEL.GetThickness() < 8 :
            self.ListButtonsChangeThick[8].animateClick()

        # change la selection dans le menu
        l = { 1: self.ACTBin1, 2: self.ACTBin2,   4:self.ACTBin4,  8: self.ACTBin8 }
        for v in l.keys():
            if v != val :       l[v].setChecked(False);
        l[val].setChecked(True);

        # modif var dans le modele
        MODEL.SetBinFactor(val)




    ############################################################################################################
    #
    #       MENU

    def Help(self):

        self.HelpW = WidgetHelp()
        self.HelpW.setWindowModality(QtCore.Qt.WindowModal)
        self.HelpW.setWindowTitle("Help Window")
        self.HelpW.show()


    def ClearData(self):
        Event.Create.ClearData(True).Dispatch()

    def LoadData(self, next):
        self.PopUpWinLoad = WinLoadInfo()
        self.PopUpWinLoad.setWindowModality(QtCore.Qt.WindowModal)
        self.PopUpWinLoad.setWindowTitle("Loading Images")
        self.PopUpWinLoad.show()



    def CloseApp(self):
        self.close()

    def SaveSelected(self):

        fileName = QFileDialog.getSaveFileName(self, 'Save File',"","TXT (*.txt)")
        if fileName[0] == ''  :  return

        FilesList = MODEL.HistoryGetFilesInAllSlot()

        f = open(fileName[0], "w")
        f.write("idFish idChannel FileName IdInFile\n")

        for idFish in range(0,GlobVar.MaxFishes):
            FILES = FilesList[idFish]

            for Channel in range(3):
                if FILES[Channel] is not None and MODEL.GetSelected(idFish):
                    SourceFile, IdVolInFile = FILES[Channel]
                    f.write(str(idFish)+"\n")
                    f.write(str(Channel)+"\n")
                    f.write(str(SourceFile)+"\n")
                    f.write(str(IdVolInFile)+"\n")

        f.close()



    def createMenus(self):

        ###############################################
        ##
        ##    FILE

        self.fileMenu = self.menuBar().addMenu("&File")

        self.fileMenu.addAction(self.ACTopenFiles)
        self.fileMenu.addAction(self.ACTClearData)
        self.fileMenu.addAction(self.ACTSaveSelected)
        self.fileMenu.addAction(self.ACTHelp)

        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.ACTquitAct)

        ################################################
        #
        #   OPTIONS

        self.OptMenu = self.menuBar().addMenu("&Options")
        self.OptMenu.addAction(self.ACTBin1)
        self.OptMenu.addAction(self.ACTBin2)
        self.OptMenu.addAction(self.ACTBin4)
        self.OptMenu.addAction(self.ACTBin8)
        self.ACTBin4.setChecked(True)


        self.OptMenu.addSeparator()
        for T in self.ACTBitDepth:
           self.OptMenu.addAction(T)
        self.A12.setChecked(True)
        MODEL.SetBitDepth(12)

        ##############################################
        ##
        ##  View

        self.viewMenu = self.menuBar().addMenu("&View")


        ##############################################
        ##
        ##   Help

        self.helpMenu = self.menuBar().addMenu("&Info")
        self.helpMenu.addAction(self.ACTaboutAct)



    ############################################################################################################
    #
    #       TOOLBAR



    def AddSpaceInQToolbar(self,qtbar,spacePix):
        placeholder = QWidget(self);
        qtbar.addWidget(placeholder)
        placeholder.setFixedWidth(spacePix)

    def timerEvent(self, e):
        # affiche la mémoire disponible et active le GC une fois par seconde
        collect()
        T = dict(virtual_memory()._asdict())
        free = (T["used"] // 100000000) / 10  # Go avec 1 chiffre après la virgule
        total = (T["total"] // 100000000) / 10
        used = (T["used"] // 100000000) / 10
        if total < 1: total = 1
        percent = int(used / total * 100)
        self.MemoryBar.MiseAJour()
        self.MemoryInfo.setText("    " + str(used) + "Go / " + str(total) + " Go")



    def ActiveButton(self,button):
        button.blockSignals(True)
        button.setChecked(True)
        button.blockSignals(False)



    ####################################################################################
    #
    #     Bottom Toolbar

    def valuePage(self):
        val = int(self.spinBoxPage.value())
        Event.Create.ChangePage(val-1).Dispatch()

    def SetPage(self,idPage):
        S = self.spinBoxPage
        S.blockSignals(True)
        S.setValue(idPage+1)
        S.blockSignals(False)


    def ChangeLinkView(self):
        MODEL.LinkScrollBars(self.LinkViewButton.isChecked())

    def createBottomToolBars(self):

        # changement des pages
        self.BottomToolBar = QToolBar("Page Change")
        self.BottomToolBar.setStyleSheet("QToolBar{spacing:10px;}");
        self.AddSpaceInQToolbar(self.BottomToolBar, 5)
        T = QLabel("Page select: ")
        self.BottomToolBar.addWidget(T)

        T = QPushButton("<<<" )
        T.clicked.connect(lambda: Event.Create.ChangePage(MODEL.GetCurrentPage() - 1).Dispatch())
        self.BottomToolBar.addWidget(T)

        self.spinBoxPage = QSpinBox(self)
        self.spinBoxPage.setRange(1, 1000);
        self.spinBoxPage.setSingleStep(1);
        self.spinBoxPage.setValue(1);
        self.spinBoxPage.setAccelerated(True)
        self.spinBoxPage.setMinimumWidth(40)
        self.spinBoxPage.setMinimumHeight(20)
        self.spinBoxPage.valueChanged.connect(self.valuePage)
        self.BottomToolBar.addWidget(self.spinBoxPage)

        T = QPushButton(">>>")
        T.clicked.connect(lambda: Event.Create.ChangePage(MODEL.GetCurrentPage() + 1).Dispatch())
        self.BottomToolBar.addWidget(T)

        self.AddSpaceInQToolbar(self.BottomToolBar, 5)

        self.addToolBar(QtCore.Qt.BottomToolBarArea, self.BottomToolBar)
        self.viewMenu.addAction(self.BottomToolBar.toggleViewAction())

        # Link Scroller
        self.ScrollLinkBar = QToolBar("Link Views")
        self.LinkViewButton = QPushButton("Link Views")
        self.LinkViewButton.setCheckable(True)
        self.ScrollLinkBar.addWidget(self.LinkViewButton)
        self.LinkViewButton.clicked.connect( self.ChangeLinkView )

        self.addToolBar(QtCore.Qt.BottomToolBarArea, self.ScrollLinkBar)
        self.viewMenu.addAction(self.ScrollLinkBar.toggleViewAction())

        # Gamma par modalité

        def CreateButtonForGroupGamma(numero):
            T = QPushButton(GlobVar.ChannelShortName + " " + str(numero))
            self.BottomGammaBar.addWidget(T)
            T.clicked.connect(lambda: self.CreateGammaDialog(numero) )


        self.BottomGammaBar = QToolBar("Constrast")
        self.BottomGammaBar.setStyleSheet("QToolBar{spacing:3px;}");
        self.addToolBar(QtCore.Qt.BottomToolBarArea, self.BottomGammaBar)

        self.AddSpaceInQToolbar(self.BottomGammaBar, 10)
        for i in range(3):
            CreateButtonForGroupGamma(i)
        self.AddSpaceInQToolbar(self.BottomGammaBar, 10)
        self.viewMenu.addAction(self.BottomGammaBar.toggleViewAction())

        # barre processing
        self.ProcessingBar = QToolBar("Processing")
        self.ProcessingBar.setStyleSheet("QToolBar{spacing:3px;}");
        self.addToolBar(QtCore.Qt.BottomToolBarArea, self.ProcessingBar)
        self.AddSpaceInQToolbar(self.ProcessingBar, 10)
        T = QPushButton(" >> REGISTRATION << ")
        T.clicked.connect(lambda: self.OpenRecalage(True))
        self.ProcessingBar.addWidget(T)

        T = QPushButton(" >> SEGMENTATION << ")
        T.clicked.connect(lambda: self.OpenSegmentation())
        self.ProcessingBar.addWidget(T)

        T = QPushButton(" >> VOLUMETRY  << ")
        T.clicked.connect(lambda: self.OpenVolumetry())
        self.ProcessingBar.addWidget(T)

        self.viewMenu.addAction(self.ProcessingBar.toggleViewAction())

        # information mémoire

        self.MemoryUsage = QToolBar("Memory usage")
        self.addToolBar(QtCore.Qt.BottomToolBarArea, self.MemoryUsage)
        self.MemoryBar = WidgetMemory(20)
        self.MemoryUsage.addWidget(self.MemoryBar)
        self.MemoryInfo = QLabel("",self)
        self.MemoryUsage.addWidget(self.MemoryInfo)
        self.viewMenu.addAction(self.MemoryUsage.toggleViewAction())
        self.timer = QBasicTimer()
        self.timer.start(1000, self)




    def OpenRecalage(self,IsRelacage):
        FileInSlot = MODEL.HistoryGetFilesInAllSlot()
        self.PopUpWinLoad = WidgetRegistration(FileInSlot)
        self.PopUpWinLoad.setWindowModality(QtCore.Qt.WindowModal)
        self.PopUpWinLoad.show()


    def OpenSegmentation(self):
        FileInSlot = MODEL.HistoryGetFilesInAllSlot()
        self.PopUpWinLoad = WidgetSegmentation(FileInSlot)
        self.PopUpWinLoad.setWindowModality(QtCore.Qt.WindowModal)
        self.PopUpWinLoad.show()



    def OpenVolumetry(self):
        self.PopUpWinLoad = WidgetVolum()
        self.PopUpWinLoad.setWindowModality(QtCore.Qt.WindowModal)
        self.PopUpWinLoad.show()


    ####################################################################################
    #
    #     TOP  Toolbar

    def createTopToolBars(self):



        ########################
        #
        #      Load


        self.MenuAction = self.addToolBar("Load")
        self.MenuAction.addAction(self.ACTClearData)
        self.MenuAction.addAction(self.ACTopenFiles)
        self.MenuAction.addAction(self.ACTSaveSelected)
        self.MenuAction.addAction(self.ACTHelp)

        self.viewMenu.addAction(self.MenuAction.toggleViewAction())

        ########################
        #
        #   Change Proj

        self.ChangeView = self.addToolBar("Change view")
        self.ChangeView.setStyleSheet("QToolBar{spacing:10px;}");
        self.GroupView  = QButtonGroup(self.ChangeView)
        info = [ "H", "S","T" ]
        projName = [ GlobVar.VueDorsale, GlobVar.VueSagittale, GlobVar.VueTransverse ]


        def CreateButtonForViews(id):
            T = QPushButton(info[id])
            self.GroupView.addButton(T)
            T.setCheckable(True)
            self.ChangeView.addWidget(T)
            if id == 0 : self.DefaultButtons.append(T)
            T.setFixedWidth(50)
            PName = projName[id]
            T.clicked.connect(lambda: Event.Create.ChangeProj(PName).Dispatch())

        self.AddSpaceInQToolbar(self.ChangeView, 3)
        for id in range(3):
            CreateButtonForViews(id)
        self.AddSpaceInQToolbar(self.ChangeView, 3)


        self.viewMenu.addAction(self.ChangeView.toggleViewAction())

        ########################
        #
        #  Change Layout
        #

        self.ChangeLayout= self.addToolBar("Change Layout")
        #self.ChangeLayout.setStyleSheet("QToolBar{spacing:5px;}");
        self.ChangeLayout.addWidget(QLabel(" Col "))
        self.spinBoxPCol = QSpinBox(self)
        self.spinBoxPRow = QSpinBox(self)

        def ChangeNbRowCol():

            nbRow = self.spinBoxPRow.value()
            nbCol = self.spinBoxPCol.value()
            Event.Create.ChangeRowCol(nbRow,nbCol).Dispatch()


        self.spinBoxPCol.setRange(1, GlobVar.MaxCol);
        self.spinBoxPCol.setSingleStep(1);
        self.spinBoxPCol.setValue(3);
        self.spinBoxPCol.setMinimumWidth(40)
        self.spinBoxPCol.setMinimumHeight(20)
        self.spinBoxPCol.valueChanged.connect(ChangeNbRowCol)
        self.ChangeLayout.addWidget(self.spinBoxPCol)

        self.ChangeLayout.addWidget(QLabel(" Row "))
        self.spinBoxPRow.setRange(1, GlobVar.MaxRow);
        self.spinBoxPRow.setSingleStep(1);
        self.spinBoxPRow.setValue(6);
        self.spinBoxPRow.setMinimumWidth(40)
        self.spinBoxPRow.setMinimumHeight(20)
        self.spinBoxPRow.valueChanged.connect(ChangeNbRowCol)
        self.ChangeLayout.addWidget(self.spinBoxPRow)

        def ChangeCHAConfig():
            ChannelMix = self.ChaListOptions[self.comboCHA.currentIndex()]
            print(ChannelMix)
            Event.Create.ChangeCha(ChannelMix).Dispatch()

        self.ChangeLayout.addWidget(QLabel(" Cha "))

        self.ChaListOptions = [ [0], [1], [2] , [0,1], [0,2], [1,2], [0,1,2] ]

        self.comboCHA = QComboBox(self)
        for opt in self.ChaListOptions :
            s = ""
            for v in opt : s = s + "  " + str(v)
            self.comboCHA.addItem(s)

        self.comboCHA.setCurrentIndex(6)
        self.comboCHA.activated[str].connect(ChangeCHAConfig)
        self.ChangeLayout.addWidget(self.comboCHA)

        self.viewMenu.addAction(self.ChangeLayout.toggleViewAction())

        ########################
        #
        #     Change Rot Angle
        """
        self.ChangeRot = self.addToolBar("Change Rotation")
        self.ChangeRot.setStyleSheet("QToolBar{spacing:10px;}");

        info = [ -45,-30,-15,0,15,30,45 ]

        def CreateButtonForRots(id):
            T = QPushButton(str(info[id]))
            T.setFixedWidth(50)
            self.ChangeRot.addWidget(T)
            T.clicked.connect(lambda: Event.Create.ChangeRot(info[id]).Dispatch())



        self.AddSpaceInQToolbar(self.ChangeRot, 10)
        for id in range(len(info)):
            CreateButtonForRots(id)

        

        self.AddSpaceInQToolbar(self.ChangeRot, 10)
        self.viewMenu.addAction(self.ChangeRot.toggleViewAction())
        """

        def ChangeRot(dec):
            Event.Create.ChangeRot(MODEL.GetRotAngle() + dec).Dispatch()
        ########################
        #
        #     Change Rot Angle 2

        self.ChangeRot2 = self.addToolBar("Change Rotation")
        self.ChangeRot2.setStyleSheet("QToolBar{spacing:2x;}");

        T = QPushButton("<<")
        T.setFixedWidth(30)
        self.ChangeRot2.addWidget(T)
        T.clicked.connect(lambda: ChangeRot(-15))

        T = QPushButton("<")
        T.setFixedWidth(30)
        self.ChangeRot2.addWidget(T)
        T.clicked.connect(lambda: ChangeRot(-5))

        self.spinBox = QSpinBox(self)
        self.spinBox.setRange(-361, 361);
        self.spinBox.setSingleStep(1);
        self.spinBox.setValue(0);
        self.spinBox.setAccelerated(True)
        self.spinBox.setMinimumWidth(40)
        self.spinBox.setMinimumHeight(20)
        self.spinBox.valueChanged.connect(self.valuecc)
        self.spinBox.setStyleSheet("QSpinBox::down-button{width: 20} QSpinBox::up-button{width: 20}")
        self.ChangeRot2.addWidget(self.spinBox)

        T = QPushButton(">")
        T.setFixedWidth(30)
        self.ChangeRot2.addWidget(T)
        T.clicked.connect(lambda: ChangeRot(5))

        T = QPushButton(">>")
        T.setFixedWidth(30)
        self.ChangeRot2.addWidget(T)
        T.clicked.connect(lambda: ChangeRot(15))


        self.combo = QComboBox(self)
        self.combo.addItem("Low")
        self.combo.addItem("Med")
        self.combo.addItem("High")
        self.combo.addItem("Extra")
        self.combo.setCurrentIndex(2)
        self.combo.activated[str].connect(self.onActivatedComboQualRot)

        self.ChangeRot2.addWidget(self.combo)

        self.AddSpaceInQToolbar(self.ChangeRot2, 10)
        self.viewMenu.addAction(self.ChangeRot2.toggleViewAction())

        ########################
        #
        #   Change Thick

        self.ChangeThick = self.addToolBar("Change thick")
        self.ChangeThick.setStyleSheet("QToolBar{spacing:3px;}");
        self.GroupThick = QButtonGroup(self.ChangeThick)

        self.ListButtonsChangeThick = {}
        def CreateButtonForThick(id):
            info = str(GlobVar.LThickness[id])
            T = QPushButton(info)
            self.ListButtonsChangeThick[GlobVar.LThickness[id]] = T
            self.GroupThick.addButton(T)
            T.setCheckable(True)
            T.setFixedWidth(50)
            if id == 0 :  self.DefaultButtons.append(T)
            self.ChangeThick.addWidget(T)
            T.clicked.connect(lambda: Event.Create.ChangeThick(GlobVar.LThickness[id]).Dispatch())

        self.ChangeThick.addWidget(QLabel("Thickness ",self))
        for id in range(5):
            CreateButtonForThick(id)
        self.AddSpaceInQToolbar(self.ChangeThick, 10)

        self.viewMenu.addAction(self.ChangeThick.toggleViewAction())

        ########################
        #
        #   Change ZoomMode  :  vertical / horizontal / all / one

        self.ChangeZoom = self.addToolBar("Change Zoom Mode")
        self.ChangeZoom.addWidget( QLabel("  Zoom  "))
        self.GroupZoom = QButtonGroup(self.ChangeThick)

        ListS = [ ".", "-","|", "#"]
        def CreateButtonForZoom(id):
            T = QPushButton(ListS[id])
            self.GroupZoom.addButton(T)
            T.setCheckable(True)
            if id == 1 :  self.DefaultButtons.append(T)
            self.ChangeZoom.addWidget(T)
            T.setFixedWidth(50)
            T.clicked.connect(lambda: Event.Create.ChangeZoomMode(id).Dispatch())

        self.AddSpaceInQToolbar(self.ChangeZoom, 10)
        for id in range(4):
            CreateButtonForZoom(id)
        self.AddSpaceInQToolbar(self.ChangeZoom, 10)

        self.viewMenu.addAction(self.ChangeZoom.toggleViewAction())



    def ResetIHM(self):
        for B in self.DefaultButtons :
            self.ActiveButton(B)
        self.ModifyRotValueIHM(0)


    def CreateGammaDialog(self,idCol):
        BWA = MODEL.GetGammaParam(idCol)
        self.wGamma = WidgetGammaCorrection(BWA[0], BWA[1], BWA[2], idCol)
        self.wGamma.show()

    def changeRotValue(self, value):
        Event.Create.ChangeRot(value).Dispatch()

    def valuecc(self):
        val = int(self.spinBox.value())
        if val < -360 : val = 0
        if val >  360 : val = 0
        self.changeRotValue(val)

    def ModifyRotValueIHM(self,rotAngle):
        self.spinBox.blockSignals(True)
        self.spinBox.setValue(rotAngle)
        self.spinBox.blockSignals(False)

    def onActivatedComboQualRot(self, text):
        id = self.combo.currentIndex()
        Event.Create.ChangeRotQual(id).Dispatch()


    ##########################################################################
    #
    #           CREATION DES FENETRES PRINCIPALES


    def createDockWindows(self):
        #  FENETRE PRINCIPALE

        MODEL.MyImageWidget = WidgetFishes(self)
        self.MyImageWidget = MODEL.MyImageWidget
        self.setCentralWidget(MODEL.MyImageWidget)


    def ActionAbout(self):
        QMessageBox.about(self, "Information", GlobVar.LicenceInfo)










