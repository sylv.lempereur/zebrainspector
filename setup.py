# Author:
#   Sylvain Lempereur <sylv.lempereur@gmail.com>
"""
Setup file of zbi package.
This file is used by pip to install this module.
It will automatically install needed packages.
"""

from setuptools import\
    setup,\
    find_packages

MAJOR = 0
MINOR = 1
PATCH = 0
VERSION = "{}.{}.{}".format(MAJOR, MINOR, PATCH)

CLASSIFIERS = [
    "Development Status :: 3 - Alpha",
    "Intended Audience :: Science/Research",
    "Intended Audience :: Developers",
    "Programming Language :: Python :: 3.7",
    "Topic :: Data handling",
    "Topic :: Scientific/Engineering",
    "Operating System :: Unix",
    "Operating System :: Microsoft :: Windows :: Windows 7"
]

# with open("core/version.py", "w") as f:
#     f.write("__version__ = '{}'\n".format(VERSION))

setup(
    name="ZeBraInspector",
    author="sylv.lempereur@gmail.com",
    description="Framework uses to compute segmentation and registration for the ZeBraInspector software",
    version=VERSION,
    license="BSD",
    classifiers=CLASSIFIERS,
    packages=find_packages(),
    install_requires=[
        "numpy",
        "numba==0.50.1",
        "scikit-image==0.16.2",
        "matplotlib==3.0.3",
        "scipy",
        "SimpleITK==1.2.4",
        "openpyxl==3.0.4",
        "PySide2",
        "psutil",
        "nd2reader",
        ],
    )
