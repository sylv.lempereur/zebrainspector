from zbi.scripts import (
	registration,
	segmentation
	)
import time
from zbi.scripts.registration import (
    get_affine_matrix as zbi_get_affine_matrix,
    registration
    )
from zbi.scripts import volumetry
import numpy as np


def test1(Vol3D):   # inversion intensité
    return 4096-Vol3D


def test2(Vol3D):   # descend le poissons pour qu'il soit coupé en deux
    a,b,c = Vol3D.shape
    d = b*c * a // 2
    return np.roll(Vol3D, d)


def SylvainRecalage(Vol3D, matrix_affine, centroid):
    # return test1(Vol3D)
    return registration(Vol3D, matrix_affine, centroid, debug=False)

def get_affine_matrix(Vol3D,RegistrationMethod, debug=False):
    return zbi_get_affine_matrix(Vol3D, RegistrationMethod)


def SylvainSegmentation(Vol3D):
    # return test2(Vol3D)
    return segmentation(Vol3D, debug=False)

def SylvainVolumetrie(Vol3D):
    #return (50000,665555,1.32)
    return volumetry(Vol3D)

