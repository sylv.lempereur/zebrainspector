# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This function computes a depth depedant contrast correction,
a segmentation of the whole larva and a segmentation of the white matter.
"""

# Standard import

from os import makedirs
from os.path import exists

# import sys

from time import time

# Non stadard imports

from openpyxl import load_workbook, Workbook
from openpyxl.utils import get_column_letter

from SimpleITK import ReadImage, GetArrayFromImage

# Local imports

from zbi.scripts.metric_computation import get_segmentation_metrics

from zbi.scripts.segmentations import (
    # grey_matter,
    white_matter,
    whole_larva,
)

from zbi.handling import DataHandling

from zbi import processing


def segmentation(
        path_input: str,
        path_output: str,
        sample: str,
        book: tuple,
        nomenclature: tuple = (
            "@",  # Delimitation between channels
            "C00",  # Dye Channel
            "C01",  # HuC channel
            ".mha",  # gile extension
        ),
    ):
    """
    Compute segmentation of white matter and whole larve
    of whole mounted 5dpf zebrafish.

    Returns
    -------
    Exit status
    """

    book[0].active["A" + str(book[1])].value = sample

    start_total = time()
    
    dye = DataHandling(
        (
            path_input
            + sample
            + nomenclature["delimitor"]
            + nomenclature["dye_channel"]
            + nomenclature["file_extension"]
        ),
        (path_output + "/" + sample + "/"),
        printable=True,
    )

    # huc = DataHandling(
    #     (
    #         path_input
    #         + sample
    #         + nomenclature["delimitor"]
    #         + nomenclature["huc_channel"]
    #         + nomenclature["file_extension"]
    #     ),
    #     (path_output + "/" + sample + "/"),
    #     printable=True,
    # )

    # dye.write_step()

    # huc.write_step()
    huc = None

    path_larva = (
        path_output
        + sample
        + "/"
        + sample
        + nomenclature["delimitor"]
        + nomenclature["dye_channel"]
        + "_larva"
        + nomenclature["file_extension"]
    )

    if exists(path_larva):
        larva = GetArrayFromImage(
                ReadImage(
                    path_larva
                    )
                )
        dye.set_larva(larva.copy())

        dye.set_bounding_box(
            processing.detection.bounding_box_positions(
                larva
                )
            )
        del larva
        if huc:
            huc.set_bounding_box(dye.get_bounding_box())
            huc.set_larva(dye.get_larva())
    else:
        start = time()

        whole_larva(dye, huc)

        book[0].active["B" +str(book[1])].value = int(time() - start)
        print(int(time()-start))

    # start = time()

    # depth_contrast_correction(dye)

    # book[0].active["C" +str(book[1])].value = int(time() - start)

    start = time()

    white_matter(dye)

    print(int(time() - start))

    book[0].active["D" + str(book[1])].value = int(time() - start)

    book[0].active["E" + str(book[1])].value = int(time() - start_total)

    return True


def metric(
        list_files: list,
        folder_specs,
        folder_auto: str,
        path_output: str
        ):
    """
    Compute Dice, general balance metrics and Matthew correlation coefficient.

    Parameters
    ----------
    list_files: list
        List of name of samples on which metrics will be computed

    folder_spec_1: str
        Path to the folder that contains manual segmentation
        of the first specialist

    folder_spec_2: str
        Path to the folder that contains manual segmentation
        of the second specialist

    folder_auto: str
        Path to the folder that contains
        automatic segmentation of the whole larva

    path_output: str
        Path used to save results on a xlsx format.

    Returns
    -------
    exit status
    """

    get_segmentation_metrics(
        list_files=list_files,
        folder_specs=folder_specs,
        folder_auto=folder_auto,
        path_output=path_output,
    )

    return True


def main(
    list_files: list,
    path_input: str,
    path_output: str,
    folder_specs: tuple = (None, None),
    nomenclature: tuple = (
        "@",  # Delimitation between channels
        "C01",  # Channel idneitifier
        ".mha",  # gile extension
    ),
):
    """
    """
    if exists(path_output + "times.xlsx"):
        book = load_workbook(path_output + "times.xlsx")
    else:
        book = Workbook()
    book.active.title = "computation time"
    book.active["A1"].value = "Sample"
    book.active["B1"].value = "Whole larva"
    book.active["C1"].value = "Contrast correction"
    book.active["D1"].value = "White matter"
    book.active["E1"].value = "Total"
    if path_input[-1] != "/":
        path_input += "/"
    if path_output[-1] != "/":
        path_output += "/"
    if not exists(path_input):
        raise ValueError(path_input + " does not exist.")
    if not exists(path_output):
        print(path_output + " does not exist.\n It will be created.")
        makedirs(path_output)
    pos = 2

    for file in list_files:
        print(file)
        segmentation(
            path_input=path_input,
            path_output=path_output,
            sample=file,
            nomenclature=nomenclature,
            book=(book, pos),
        )
        pos += 1
    book.active["A" + str(pos)] = "Total"
    for column in range(2, 6):
        book.active[get_column_letter(column) + str(pos)] = (
            "=sum("
            + get_column_letter(column)
            + "2:"
            + get_column_letter(column)
            + str(pos - 1)
            + ")"
        )

    book.save(path_output + "times.xlsx")

    if not None in folder_specs:
        get_segmentation_metrics(
            list_files=list_files,
            folder_specs=folder_specs,
            folder_auto=path_output,
            path_output=path_output + "metrics.xlsx",
        )


PATHIN = r"/data/data_for_bio_paper/raw/Chosen_ones/"

PATHOUT = r"/data/WT/85percent/"
SAMPLES = [
    "200108Fa_2144a_5dpf_SL-193-DR_512_Nh_merge",
    "200108Fa_2144d_5dpf_SL-193-DR_512_Nh_merge",
    "200108Fa_2145g_5dpf_SL-193-DR_512_Nh_merge",
    "200108Fa_2145j_5dpf_SL-193-DR_512_Nh_merge",
    "200108Fa_2146a_5dpf_SL-193-DR_512_Nh_merge",
    "200108Fa_2146h_5dpf_SL-193-DR_512_Nh_merge",
    "200108Fa_2147j_5dpf_SL-193-DR_512_Nh_merge",
    "200108Fa_2147l_5dpf_SL-193-DR_512_Nh_merge",
    "200117Fa_2148c_5dpf_SL-193-DR_512_Nh_merge",
    "200117Fa_2148j_5dpf_SL-193-DR_512_Nh_merge",
    "200117Fa_2149b_5dpf_SL-193-DR_512_Nh_merge",
    "200117Fa_2149l_5dpf_SL-193-DR_512_Nh_merge",

    # "200204Fa_2213a_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2213b_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2213c_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2213d_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2213e_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2213f_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2213g_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2213h_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2213i_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2213j_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2213k_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2213l_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2213m_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2213n_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2213o_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2213p_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2214a_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2214b_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2214c_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2214d_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2214e_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2214f_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2214g_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2214h_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2214i_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2214j_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2214k_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2214l_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2214m_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2214n_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2215a_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2215b_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2215c_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2215d_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2215e_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2215f_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2215g_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2215h_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2215i_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2216a_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2216b_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2216c_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2216d_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2216e_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2216f_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2216g_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2216h_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2216i_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2216j_5dpf_SL-194-DR_512_Nh_merge",
    # "200204Fa_2216k_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2201a_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2201b_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2201c_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2201d_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2201e_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2201f_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2201g_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2202a_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2202b_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2202c_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2202d_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2202e_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2202f_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2202g_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2202h_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2202i_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2202j_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2203a_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2203b_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2203c_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2203d_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2203e_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2203f_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2203g_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2203h_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2203i_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2203j_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2203k_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2204a_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2204b_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2204c_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2204d_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2204e_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2204f_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2204g_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2204h_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2205a_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2205b_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2205c_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2205d_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2205e_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2205f_5dpf_SL-194-DR_512_Nh_merge",
    # "200206Fa_2205g_5dpf_SL-194-DR_512_Nh_merge"
    ]

FOLDER_SPEC_1 = r"/data/data_for_bio_paper/Manual_segmentations/AJ-converted/"
FOLDER_SPEC_2 = r"/data/data_for_bio_paper/Manual_segmentations/MS-converted/"

# FOLDER_SPEC_1 = None
# FOLDER_SPEC_2 = None

main(
    list_files=SAMPLES,
    path_input=PATHIN,
    path_output=PATHOUT,
    folder_specs=(FOLDER_SPEC_1, FOLDER_SPEC_2),
    nomenclature={
        "delimitor": "_",
        "dye_channel": "C00",
        "huc_channel": "C01",
        "file_extension": ".mha",
    },
)
