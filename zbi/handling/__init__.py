"""
Data handling tools

================================== =============================================
DataHandling
================================================================================
add_step                           Add a step
get_current                        Get current step
get_larvae                         Get larvae
get_mask                           Get mask
get_original                       Get original
get_spacing                        Get image spacing
get_step                           Get the current step image
reboot                             Reset the image and stop to the begining
set_current                        Store an array as current step
set_larvae                         Store an array as larvae
set_original                       Store an array as original
set_mask                           Store an array as mask
write_step                         Write the current step ito the pathout folder
modelisation                       Don't know (Shoudl be understand then moved)
================================== =============================================
"""

from .data_handling import DataHandling
