## @package Classes.imageProcessing
# Class that perform the image processing
# @file
# This file contains the class that perform the image processing
"""
Classes and functions used to handle with data
"""

from os import makedirs

from os.path import (
    exists,
    realpath,
    split,
    splitext
    )

from numpy import (
    ndarray,
    zeros
    )

from SimpleITK import (
    GetImageFromArray,
    GetArrayFromImage,
    ReadImage,
    WriteImage
    )

class DataHandling:
    """
    This Class is used to store images to reduce the usage of ram

    Parameters
    ----------
    data_input: str or numpy.ndarray
        Path to the original image or array to use as original array

    path_output: str
        Results storage location

    printable: bool
        Allow to displays steps

    Attributes
    ----------
    _internal_attributes :tuple
        Lists the location used to store computation results,
        the name of the image and the printable status.

    _step: list
        list of computed step

    _spacing: tupple
        Physical dimensions of a voxel

    _original: numpy.ndarray
        Array corresponding to the input image

    _current: numpy.ndarray
        Current stade of the image

    _larva: numpy.ndarray
        Segmentation of the whole larva

    _bounding_box: numpy.ndarray
        Bounding box of the larva
    """
    def __init__(self,
                 data_input: (ndarray, str),
                 path_output: str = None,
                 printable: bool = False
                ):
        self._step = []

        if isinstance(data_input, str):
            if not exists(data_input):
                raise ValueError("Your input file does not exist")

            _, name = split(data_input)

            name, extension = splitext(name)
            if extension != ".mha":
                raise ValueError("Your input file is not a mha file")
            temp = ReadImage(data_input)

            self._original = GetArrayFromImage(temp)
            self._spacing = temp.GetSpacing()
            del temp


            self._internal_attributes = {
                "path_output": path_output,
                "name": name,
                "printable": printable,
                }

        elif isinstance(data_input, ndarray):
            self._original = data_input
            self._spacing = (1, 1, 1)

            self._internal_attributes = {
                "path_output": None,
                "name": None,
                "printable": printable,
                }

        if self._original.dtype != "uint16":
            self._original *= float(2 ** 12) / float(self._original.max())
            self._original = self._original.astype("uint16")

        if path_output is not None:
            if path_output[-1] != "/":
                path_output = path_output + "/"
            if exists(realpath(path_output)) is False:
                makedirs(path_output)

        self._current = None
        self._larva = None
        self._bounding_box = None

    def _type_verification(self):
        """
        TODO: write this documentation and deals with more kinf of data
        """
        # array = self.get_current().copy()
        # if issubclass(
        #         array.dtype.type,
        #         integer
        #     ):
        if self._current.max() < 2:
            self._current = self._current.astype("bool")
        elif self._current.max() < 2 ** 8:
            self._current = self._current.astype("uint8")
        elif self._current.max() < 2** 16:
            self._current = self._current.astype("uint16")
        else:
            self._current = self._current.astype('int32')
        return True

    def add_step(self,
                 step_name: str
                 ):
        """
        Add the name of the current step in a list.
        from this list, the number of the performed step could be found.

        If print are allowed, the given name will be print in terminal.

        Parameters
        ----------
        step_name: str
            Name of the step that will be performed
        """
        self._step.append(step_name)
        if self._internal_attributes["printable"]:
            print("\t" + step_name)
        return True

    def apply_bounding_box(self):
        """
        Apply bounding box to store arrays
        """

    def get_bounding_box(self):
        """
        Return bounding box of the current segmentation of the whole larva
        """
        return self._bounding_box

    def get_current(self):
        """
        return current status of the image
        """
        if self._current is None:
            self._current = self._original
        if self._bounding_box is not None:
            to_return = self._current[
                self._bounding_box[0][0]:self._bounding_box[0][1],
                self._bounding_box[1][0]:self._bounding_box[1][1],
                self._bounding_box[2][0]:self._bounding_box[2][1]
                ]
        else:
            to_return = self._current
        return to_return

    def get_larva(self):
        """
        Return larva segmentation.
        If a bounding box was computed, the bounded array will be returned.

        Returns
        -------
        numpy.ndarray
            Segmentation of the larve
        """
        if self._larva is None:
            self._larva = self.get_current()
        if self._bounding_box is not None:
            to_return = self._larva[
                self._bounding_box[0][0]:self._bounding_box[0][1],
                self._bounding_box[1][0]:self._bounding_box[1][1],
                self._bounding_box[2][0]:self._bounding_box[2][1]
                ]
        else:
            to_return = self._larva
        return to_return

    def get_original(self):
        """
        Return original image
        If a bounding box was computed, the bounded array will be returned.

        Returns
        -------
        numpy.ndarray
            Array of the raw image
        """
        if self._bounding_box is not None:
            out = self._original[
                self._bounding_box[0][0]:self._bounding_box[0][1],
                self._bounding_box[1][0]:self._bounding_box[1][1],
                self._bounding_box[2][0]:self._bounding_box[2][1]
                ]
        else:
            out = self._original
        return out

    def get_spacing(self):
        """
        Return spacing of the image
        """
        return self._spacing

    def get_step(self):
        """
        Return name of the current step
        """
        return self._step[len(self._step) - 1]

    def reboot(self):
        """
        Reboot every element of the class to the initial stage
        """
        # Reset the image.
        del self._current, self._larva
        self._current = self._original.copy()
        self._larva = None
        # Empty the step list.
        self._step = []
        return True

    def reset_bounding_box(self):
        """
        Reset the bounding box of the DataHandling instance
        """
        self._bounding_box = None

    def set_bounding_box(self,
                         bounding_box: ndarray
                         ):
        """
        Set the vounding box of this image

        Parameters
        ----------
        bounding_box: numpy.ndarray
            Bounding box to store
        """
        self._bounding_box = bounding_box
        del bounding_box
        self._internal_attributes["bounded"] = True

    def set_current(self,
                    array: ndarray,
                    tag: str = None
                    ):
        """
        Store an array as the current step

        Parameters
        ----------
        array: numpy.ndarray
            Array to store ass the current step
        """
        if tag:
            self._step.append(tag)
        if self._bounding_box is not None:
            self._current = zeros(
                self._original.shape,
                array.dtype
                )
            self._current[
                self._bounding_box[0][0]:self._bounding_box[0][1],
                self._bounding_box[1][0]:self._bounding_box[1][1],
                self._bounding_box[2][0]:self._bounding_box[2][1]
                ] = array
            del array
        else:
            self._current = array
        self._type_verification()
        return True

    def set_larva(self,
                  array: ndarray = None
                  ):
        """
        Store a whole larva segmentation

        If no array is given,
        the current stage will be consider as the segmentation

        Parameters
        ----------
        mask: numpy.ndarray
            (optionnal) array to store as a segmentation of the whole larva

        """
        if array is None:
            array = self.get_current()
        if self._bounding_box is not None:
            self._larva = zeros(
                self._original.shape,
                array.dtype
                )
            self._larva[
                self._bounding_box[0][0]:self._bounding_box[0][1],
                self._bounding_box[1][0]:self._bounding_box[1][1],
                self._bounding_box[2][0]:self._bounding_box[2][1]
                ] = array
            del array
        else:
            self._larva = array
        return True

    def set_original(self,
                     array: ndarray
                     ):
        """
        Change the image consider as original

        Parameters
        ----------
        array: numpy.ndarray
            array to store as the original image
        """
        self._original = array
        return True

    def get_path(self):
        """
        Return path and name of the iamge

        Parameters
        ----------
        path: str
            Path and name of the image
        """
        return (
            self._internal_attributes["path_output"]
            + self._internal_attributes["name"]
            )

    def write_step(self,
                   tag: str = None
                   ):
        """
        Write a mha file with the current step of the process.
        The file name will be store like imageName_stepNumber_stepName

        However, if you provide a specific tag,
        file name will be imageName_tag

        Parameters
        ----------
        tag: str
            optionnal : choose a specific tag
        """
        # If no tag is provided
        if self._internal_attributes["path_output"]:
            if tag is None:
                # If at least one step was computed
                if self._step:
                    # Creation of the save path that is formed with
                    # the output folder setup during the initialisation,
                    # the name of the image,
                    # the step number,
                    # and the descriptor of the current step.
                    path_saving = (
                        self._internal_attributes["path_output"]
                        + self._internal_attributes["name"]
                        + "_step"
                        + str(len(self._step))
                        + "_"
                        + self._step[len(self._step) - 1]
                        + '.mha'
                        )
                # If no process was performed
                else:
                    # The name is just formed with the ouptu path
                    # and the name of the iamge.
                    path_saving = (
                        self._internal_attributes["path_output"]
                        + self._internal_attributes["name"]
                        # +'_original'
                        + '.mha'
                        )
            # if a tog was given,
            else:
                # The saving path is formed using the ouput folder,
                # The name of the image and the tag.
                path_saving = (
                    self._internal_attributes["path_output"]
                    + self._internal_attributes["name"]
                    + "_"
                    + tag
                    + '.mha'
                    )
            if self._current is None:
                array = self._original
            else:
                array = self._current
            # The step is converted from an array to an itk image.
            # However, simpleITK can't save boolean image
            # This images are convert to uint8
            if array.dtype == 'bool':
                to_saved = GetImageFromArray(array.astype('uint8'))
            else:
                to_saved = GetImageFromArray(array)

            # The spacing of the original image is set to the image to save.
            to_saved.SetSpacing(self._spacing)
            # The file is written.
            WriteImage(to_saved, path_saving)
        return True
