"""
ZeBraInsprector
===============

Provides an image processing library to analyse tissue-cleared zebrafish,
and in particular, detect zebrafish brain morphology.

Available subpackages
---------------------
handling
    Class to handle images and reduce RAM usage

processing
    Core image processing tools
"""

from . import handling

from .processing import basics
from .processing import contrast_correction
from .processing import detection
from .processing import filtering
from .processing import labeling
from .processing import morphology
from .processing import thresholding

from . import scripts
