"""
Manual thresholding process
===========================

================================== =============================================
thresholding
================================================================================
manual                             Thresholding using the given value
manual_percent_histo               Thresholding using the given percent of histo
otsu                               Solo or Multi classes otsu
================================== =============================================
"""

from .manual import (
    manual,
    manual_percent_histo
    )

from .auto import otsu
