# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This modules contains any function that compute an minimum_error threshold.
"""
from numpy import ndarray
from numpy import (
    argmin,
    cumsum,
    log,
    histogram,
    inf,
    isfinite,
    sqrt
    )

from ....handling import DataHandling

def _minimum_error_array(
        array: ndarray
        ):
    """
    Computation of a minimum error
    thresholding

    Parameters
    ----------
    array: numpy.ndarray
        array used to compute the Otsu threshold

    classes: int
        Number of classes on the Otsu threshold.

    """
    if array.max() < 256:
        maximum = 256
    elif array.max() < 2 ** 12:
        maximum = 2 ** 12
    elif array.max() < 2 ** 16:
        maximum = 2 ** 16
    else:
        maximum = 2 ** 32
    histo, bin_edges = histogram(
        array.ravel(),
        maximum,
        [0, maximum]
        )
    histo = histo.astype("float32")
    bin_edges = bin_edges[:-1].astype("float32")
    cumul_histo = [] * 2 # Storage of cumulative histogram and its inverse
    moments = [] * 2 # Storage of moments and its inverse
    sigmas = [] * 2 # Storage of sigmas and its inverse
    cumul_histo[0] = cumsum(histo)
    moments[0] = cumsum(histo * bin_edges)
    sigmas[0] = cumsum(histo * bin_edges ** 2)
    sigma_f = sqrt(
        sigmas[0] / cumul_histo
        - (moments[0] / cumul_histo) ** 2
        )
    cumul_histo[1] = cumul_histo[-1] - cumul_histo
    moments[1] = moments[0][-1] - moments[0]
    sigmas[1] = sigmas[0][-1] - sigmas[0]
    sigma_b = sqrt(
        sigmas[1] / cumul_histo[1]
        - (moments[1] / cumul_histo[1]) ** 2
        )
    probability = cumul_histo / cumul_histo[-1]
    criterion = (
        probability * log(sigma_f)
        + (1 - probability) * log(sigma_b)
        - probability * log(probability)
        - (1 - probability) * log(1 - probability)
    )
    criterion[~isfinite(criterion)] = inf
    thresh = bin_edges[argmin(criterion)]
    array = array >= thresh
    return array

def _minimum_error_data_handling(
        data_handling_instance: DataHandling
        ):
    """
    Computation of a minimum error
    thresholding

    Parameters
    ----------
    data_handling_instance: DataHandling
        Current step of the DataHandling instance will be used

    classes: int
        Number of classes on the Otsu threshold.

    """
    data_handling_instance.add_step(
        "minimumErrorThreshold"
        )
    array = _minimum_error_array(
        data_handling_instance.get_current()
        )
    data_handling_instance.set_current(array)
    del array
    return True

def minimum_error(
        data
        ):
    """
    Computation of an
    `Otsu thresholding <http://web-ext.u-aizu.ac.jp/course/bmclass/documents/minimum_error1979.pdf>`. # pylint: disable=line-too-long
    with the given number of classes.

    Parameters
    ----------
    data
        data used to compute the Otsu threshold

    classes: int
        Number of classes on the Otsu threshold.

    """
    if isinstance(data,
                  ndarray
                  ):
        out = _minimum_error_array(
            data
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _minimum_error_data_handling(
            data
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    return out
