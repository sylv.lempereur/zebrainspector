# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This modules contains any function that compute an sauvola threshold.
"""
from numpy import ndarray

from skimage.filters import threshold_sauvola

from ....handling import DataHandling

def _sauvola_array(
        array: ndarray,
        window_size: int = 5
        ):
    """
    Computation of a sauvola
    thresholding

    Parameters
    ----------
    array: numpy.ndarray
        array used to compute the Otsu threshold

    classes: int
        Number of classes on the Otsu threshold.

    """
    if window_size % 2 == 0:
        window_size += 1
    array = threshold_sauvola(
        image=array,
        window_size=int(window_size),
        r=2 ** 7
        )
    return array

def _sauvola_data_handling(
        data_handling_instance: DataHandling,
        window_size: int = 5
        ):
    """
    Computation of a sauvola
    thresholding

    Parameters
    ----------
    data_handling_instance: DataHandling
        Current step of the DataHandling instance will be used

    classes: int
        Number of classes on the Otsu threshold.

    """
    data_handling_instance.add_step(
        "minimumErrorThreshold"
        )
    array = _sauvola_array(
        data_handling_instance.get_current(),
        window_size
        )
    data_handling_instance.set_current(array)
    del array
    return True

def sauvola(
        data,
        window_size: int = 5
        ):
    """
    Computation of an
    `Otsu thresholding <http://web-ext.u-aizu.ac.jp/course/bmclass/documents/sauvola1979.pdf>`. # pylint: disable=line-too-long
    with the given number of classes.

    Parameters
    ----------
    data
        data used to compute the Otsu threshold

    classes: int
        Number of classes on the Otsu threshold.

    """
    if isinstance(data,
                  ndarray
                  ):
        out = _sauvola_array(
            data,
            window_size
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _sauvola_data_handling(
            data,
            window_size
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    return out
