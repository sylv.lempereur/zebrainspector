# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This modules contains any function that compute an otsu threshold.
"""
from numpy import ndarray

from SimpleITK import (
    GetImageFromArray,
    GetArrayFromImage,
    OtsuMultipleThresholds
    )

try:
    from skimage.filters import threshold_otsu
except ModuleNotFoundError:
    from skimage.filter import threshold_otsu

from ....handling import DataHandling

def _otsu_array(
        array: ndarray,
        classes: int = 1
        ):
    """
    Computation of an
    Otsu thresholding of an array with the given number of classes

    Parameters
    ----------
    array: numpy.ndarray
        array used to compute the Otsu threshold

    classes: int
        Number of classes on the Otsu threshold.

    """
    if not isinstance(classes,
                      int):
        raise TypeError('invalid classes provided')
    if classes == 1:
        array = _otsu_one_class(array)
    else:
        array = _otsu_multi_classes(
            array,
            classes
            )
    return array

def _otsu_data_handling(
        data_handling_instance: DataHandling,
        classes: int = 1
        ):
    """
    Computation of an
    Otsu thresholding of an array with the given number of classes

    Parameters
    ----------
    data_handling_instance: DataHandling
        Current step of the DataHandling instance will be used

    classes: int
        Number of classes on the Otsu threshold.

    """
    if classes == 1:
        data_handling_instance.add_step("OtsuThresholds")
    else:
        data_handling_instance.add_step(
            "OtsuMultipleThresholds"
            + str(classes)
            )
    array = _otsu_array(
        data_handling_instance.get_current(),
        classes
        )
    data_handling_instance.set_current(array)
    del array
    return True

def _otsu_multi_classes(
        array: ndarray,
        classes=2
        ):
    """
    Computation of a multi class Otsu.

    Parameters
    ----------
    array: numpy.ndarray
        array used to compute the Otsu threshold

    classes: int
        Number of classes on the Otsu threshold.
    """
    image = GetImageFromArray(array)
    image = OtsuMultipleThresholds(
        image,
        classes
        )
    array = GetArrayFromImage(image)
    return array

def _otsu_one_class(
        array
        ):
    """
    Computation of an 1 class Otsu thresholding

    Parameters
    ----------
    array: numpy.ndarray
    """
    threshold = threshold_otsu(array)
    return array >= threshold

def otsu(
        data,
        classes: int = 1
        ):
    """
    Computation of an
    `Otsu thresholding <http://web-ext.u-aizu.ac.jp/course/bmclass/documents/otsu1979.pdf>`. # pylint: disable=line-too-long
    with the given number of classes.

    Parameters
    ----------
    data
        data used to compute the Otsu threshold

    classes: int
        Number of classes on the Otsu threshold.

    """
    if isinstance(data,
                  ndarray
                  ):
        out = _otsu_array(
            data,
            classes
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _otsu_data_handling(
            data,
            classes
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    return out
