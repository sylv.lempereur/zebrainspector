# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This modules contains any function that compute an triangle threshold.
"""
from numpy import ndarray

from skimage.filters import threshold_triangle

from ....handling import DataHandling

def _triangle_array(
        array: ndarray
        ):
    """
    Computation of a triangle
    thresholding

    Parameters
    ----------
    array: numpy.ndarray
        array used to compute the Otsu threshold

    classes: int
        Number of classes on the Otsu threshold.

    """
    if array.max() < 2 ** 8:
        bins = 2 ** 8
    elif array.max() < 2 ** 12:
        bins = 2 ** 12
    elif array.max() < 2 ** 16:
        bins = 2 ** 16
    else:
        bins = 2 ** 32
    thresh = threshold_triangle(array, nbins=bins)
    array = array >= thresh
    return array

def _triangle_data_handling(
        data_handling_instance: DataHandling
        ):
    """
    Computation of a triangle
    thresholding

    Parameters
    ----------
    data_handling_instance: DataHandling
        Current step of the DataHandling instance will be used

    classes: int
        Number of classes on the Otsu threshold.

    """
    data_handling_instance.add_step(
        "minimumErrorThreshold"
        )
    array = _triangle_array(
        data_handling_instance.get_current()
        )
    data_handling_instance.set_current(array)
    del array
    return True

def triangle(
        data
        ):
    """
    Computation of an
    `Otsu thresholding <http://web-ext.u-aizu.ac.jp/course/bmclass/documents/triangle1979.pdf>`. # pylint: disable=line-too-long
    with the given number of classes.

    Parameters
    ----------
    data
        data used to compute the Otsu threshold

    classes: int
        Number of classes on the Otsu threshold.

    """
    if isinstance(data,
                  ndarray
                  ):
        out = _triangle_array(
            data
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _triangle_data_handling(
            data
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    return out
