# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This modules contains any function that compute a threshold.
This thresholds cold be automatically assigned (Otsu, Sauvola, etc),
or manually.
"""
from numpy import ndarray

from ....handling import DataHandling

def _manual_percent_grey_array(
        array: ndarray,
        percent: float = 95.0
        ):
    """
    Computation of a threshold above
    a percentage of the histogram of grey values of the given array.

    Parameters
    ----------
    array : numpy.ndarray
        An instance of DataHanling class

    percent: float
        The percent of the gray value histogram of the image
        that will used as the threshold.
    """
    return array >= array.max() * percent / 100

def _manual_percent_grey_data_handling(
        data_handling_instance: DataHandling,
        percent: float = 95
        ):
    """
    Computation of a threshold above
    a percentage of the histogram of grey values.

    Parameters
    ----------
    data_handling_instance : DataHandling
        An instance of DataHanling class

    percent: float
        The percent of the gray value histogram of the image
        that will used as the threshold.

    """
    data_handling_instance.add_step(
        "manualThresholdOver" + str(percent) + "percentsOfGrey"
        )
    array = _manual_percent_grey_array(
        data_handling_instance.get_current(),
        percent
        )
    data_handling_instance.set_current(array)
    del array
    return True

def manual_percent_grey(
        data,
        percent: float = 95
        ):
    """
    Computation of a threshold above
    a percentage of the histogram of grey values.

    Parameters
    ----------
    data:
        Data used to computed the percent of histogram-based manual threshold.


    percent: float
        The percent of the gray value histogram of the image
        that will used as the threshold.

    See also
    --------
    manual
    manual_percent_histo
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _manual_percent_grey_array(
            data,
            percent
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _manual_percent_grey_data_handling(
            data,
            percent
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    return out
