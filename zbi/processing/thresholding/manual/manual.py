# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This modules contains any function that compute a threshold.
This thresholds cold be automatically assigned (Otsu, Sauvola, etc),
or manually.
"""
from numpy import ndarray

from ....handling import DataHandling

def _manual_array(
        array: ndarray,
        min_value: float,
        max_value: float = None
        ):
    """
    This function computes manual thresholds of an array.

    grey values smaller than min_value and bigger than max_value
    will be set to 0.
    remaining values will be set to 1.

    Parameters
    ----------
    data_handling_instance : DataHandling
        An instance of DataHanling class

    min_value : int
        minimum value

    max_value : int
        maximum value.
        If this value is set to None (default), no higher limit will be set.

    """
    if max_value is not None:
        array[array > max_value] = 0
    array = array >= min_value
    return array

def _manual_data_handling(
        data_handling_instance: DataHandling,
        min_value: float,
        max_value: float = None
        ):
    """
    This function computes manual thresholds of an array.

    grey values smaller than min_value and bigger than max_value
    will be set to 0.
    remaining values will be set to 1.

    Parameters
    ----------
    data_handling_instance : DataHandling
        An instance of DataHanling class

    min_value : int
        minimum value

    max_value : int
        maximum value.
        If this value is set to None (default), no higher limit will be set.

    """
    if max_value is not None:
        data_handling_instance.add_step(
            "manualThresholdBetween"
            + str(min_value)
            + "And"
            + str(max_value)
            )
    else:
        data_handling_instance.add_step(
            "manualThresholdOver"
            + str(min_value)
            )
    array = _manual_array(
        data_handling_instance.get_current(),
        min_value,
        max_value
        )
    data_handling_instance.set_current(array)
    del array
    return True

def manual(
        data,
        min_value: float,
        max_value: float = None
        ):
    """
    This function computes manual thresholds.

    grey values smaller than min_value and bigger than max_value
    will be set to 0.
    remaining values will be set to 1.

    Parameters
    ----------
    data:
        Data used to computed the manual threshold

    min_value : int
        minimum value

    max_value : int
        maximum value.
        If this value is set to None (default), no higher limit will be set.

    See also
    --------
    manual_percent_grey
    manual_percent_histo
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _manual_array(
            data,
            min_value,
            max_value
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _manual_data_handling(
            data,
            min_value,
            max_value
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    return out
