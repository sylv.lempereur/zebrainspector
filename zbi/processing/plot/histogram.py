# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file Contains founction to plot histogram of a piece of data.
"""
from gc import collect as garbage_collection

from numpy import (
    ndarray,
    unique
    )

from matplotlib import pyplot as plt

from ...handling import DataHandling

def _histogram_array(
        array: ndarray,
        bins: int = None
        ):
    """
    Plot the histogram of grey vbalues of an array
    with the asked number of bins.

    Parameters
    ----------
    array: numpy.ndarray

    bins: int
        (optionnal)
        Number of bins to display
        if no value was given, bins will be set as
        the number of value in the array divided by 10.
    """
    flat = array[array != 0].flatten()
    if bins is None:
        bins = unique(flat).size / 10
    plt.hist(
        flat,
        bins=bins
        )
    plt.show()
    return True

def _histogram_data_handling(
        data_handling_instance: DataHandling,
        bins: int = None
        ):
    """
    Plot the histogram of grey values of the current step
    with the asked number of bins.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    bins: int
        (optionnal)
        Number of bins to display
        if no value was given, bins will be set as
        the number of value in the array divided by 10.
    """
    out = _histogram_array(
        data_handling_instance.get_current(),
        bins
        )
    garbage_collection()
    return out

def histogram(
        data,
        bins: int = None
        ):
    """
    Plot the grey value histogram of a piece of data
    with the asked number of bins.

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance

    bins: int
        (optionnal)
        Number of bins to display
        if no value was given, bins will be set as
        the number of value in the array divided by 10.
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _histogram_array(
            data,
            bins
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _histogram_data_handling(
            data,
            bins
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
