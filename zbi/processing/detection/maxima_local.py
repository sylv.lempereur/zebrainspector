# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains function uses to detect local maxima.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from skimage.feature import peak_local_max

from ...handling import DataHandling

def _maxima_local_array(
        array: ndarray
        ):
    """
    Found local maximum of an numpy.ndarray

    Parameters
    ----------
    array: numpy.ndarray

    Returns
    -------
    numpy.ndarray
        Cooridnates of local maxima
    """
    return peak_local_max(
        array,
        indices=False
        )

def _maxima_local_data_handling(
        data_handling_instance: DataHandling
        ):
    """
    Find local maxima of the current step of a DataHandling isntance.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class
    """
    coordinates = _maxima_local_array(data_handling_instance.get_current())
    garbage_collection()
    return coordinates

def maxima_local(data):
    """
    Find local maxima of a given piece of data.

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _maxima_local_array(data)
    elif isinstance(data,
                    DataHandling
                    ):
        out = _maxima_local_data_handling(data)
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
