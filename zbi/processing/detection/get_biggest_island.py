# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains function to reduce a segmentation to the biggest island.
"""
from gc import collect as garbage_collection

from numpy import (
    ndarray,
    where,
    zeros
    )

from ...handling import DataHandling

from ..projection import binary_projection

def _bounding_box_positions_array(
        array: ndarray,
        tolerance: int = 5
        ):
    """
    Keep only the biggest island of a numpy.ndarray

    Parameters
    ----------
    array: numpy.ndarray

    Returns
    -------
    numpy.ndarray
        Biggest island only
    """
    if array.dtype != 'bool':
        array = array != 0
    positions = zeros(
        (len(array.shape), 2),
        'int16'
        )
    for axis in range(len(array.shape)):
        # Project the array on each axis.
        axis_projection = list(range(3))
        axis_projection.pop(axis_projection.index(axis))
        projection = binary_projection(
            array,
            tuple(axis_projection)
            )
        # Get extreme positions of the array around corresponding axis
        positions[axis, 0] = where(projection)[0][0]
        positions[axis, 1] = where(projection)[0][-1]
    # Add tolerance
    positions[:, 0] -= tolerance
    positions[:, 1] += tolerance
    # Limit to array shape
    positions[positions < 0] = 0
    for axis in range(len(array.shape)):
        if positions[axis, 1] > array.shape[axis]:
            positions[axis, 1] = array.shape[axis]
    return positions

def _bounding_box_positions_data_handling(
        data_handling_instance: DataHandling,
        tolerance: int = 5
        ):
    """
    Gounding the position of the bounding box of the current step
    with the given tolerance.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    tolerance: int
        radius add around the real bounding box.
    """
    positions = _bounding_box_positions_array(
        data_handling_instance.get_current(),
        tolerance
        )
    garbage_collection()
    return positions

def bounding_box_positions(
        data,
        tolerance: int = 5
        ):
    """
    Get corners of the bounding box of a piece of data.
    This bounding box is compute with the given tolerance
    around the smaller possible one.

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance

    tolerance: int
        radius add around the real bounding box.
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _bounding_box_positions_array(
            data,
            tolerance
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _bounding_box_positions_data_handling(
            data,
            tolerance
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
