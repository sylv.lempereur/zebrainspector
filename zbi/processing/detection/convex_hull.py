# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains function to compute convex hull
of any object in a piece of data.
"""
from gc import collect as garbage_collection

from numpy import (
    ndarray,
    where
    )
from numpy import array as nparray

from pymesh import (
    tetgen,
    VoxelGrid
    )
from pymesh import convex_hull as pymesh_convex_hull

from ..labeling import label
from ..morphology import surface

from ...handling import DataHandling # change X*. to the correct amount of .

def _convex_hull_array(
        array: ndarray
        ):
    """
    Compute convex hulls of any object in a boolean array.

    Parameters
    ----------
    array: numpy.ndarray
        A binary array

    Returns
    -------
    numpy.ndarray
        Convex hulls of any object in an array.
    """
    if array.dtype != 'bool':
        raise TypeError('Given piece of data is not boolean.')
    labels, nb_labels = label(array)
    array *= 0
    for label_temp in range(1, nb_labels + 1):
        array_temp = surface(labels == label_temp)
        coordinates = where(array_temp)
        vertices = nparray(
            [
                coordinates[0],
                coordinates[1],
                coordinates[2],
            ]).transpose()

        #Computation of the convex hull
        convexhull = tetgen()
        convexhull.points = vertices
        convexhull.run()
        mesh = convexhull.mesh
        hull = pymesh_convex_hull(mesh)
        grid = VoxelGrid(1.0, mesh.dim)
        grid.insert_mesh(hull)
        grid.create_grid()
        out_mesh = grid.mesh
        coordinates_out = (
            out_mesh.vertices.astype('uint32')[:, 0],
            out_mesh.vertices.astype('uint32')[:, 1],
            out_mesh.vertices.astype('uint32')[:, 2]
            )
        array[coordinates_out] = True

    return array

def _convex_hull_data_handling(
        data_handling_instance: DataHandling
        ):
    """
    Found convexh hulls of any object of the current step.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class
    """
    data_handling_instance.add_step("convexHull")
    array = _convex_hull_array(data_handling_instance.get_current())
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def convex_hull(data):
    """
    Compute convex hulls of each object in the given piece of data.

    Parameters
    ----------
    data:
        could be a numpy.ndarray or a DataHandling instance
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _convex_hull_array(data)
    elif isinstance(data,
                    DataHandling
                    ):
        out = _convex_hull_data_handling(data)
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
