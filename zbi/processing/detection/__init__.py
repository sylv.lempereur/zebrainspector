"""
Detection processing
==================================


================================== =============================================
detection
================================================================================
maximum_local                      Found local maximum
================================== =============================================
"""

from .maxima_local import maxima_local
from .bounding_box_positions import bounding_box_positions
