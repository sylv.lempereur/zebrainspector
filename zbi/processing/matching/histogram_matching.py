# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This modules contains functions two perform histogram matching
of two piece of data.
"""

# TODO: généraliser cette function

from numpy import (
    cumsum,
    interp,
    ndarray,
    unique
    )

def histogram_matching(
        source: ndarray,
        template: ndarray
        ):
    """
    Perform an histogram matching of a source image against a template image.

    Author
    ------
    `ali_m on stackoverflow.com <# https://stackoverflow.com/questions/32655686/histogram-matching-of-two-images-in-python-2-x>` # pylint: disable=line-too-long

    Parameters
    ----------
    source: numpy.ndarray
        image to improve

    template: numpy.ndarray
        image to use as a reference histogram

    Return
    ------
    matched: numpy.ndarray
        corrected image
    """

    oldshape = source.shape
    source = source.ravel()
    template = template.ravel()

    # get the set of unique pixel values and their corresponding indices and
    # counts
    _, bin_idx, s_counts = unique(
        source,
        return_inverse=True,
        return_counts=True
        )
    t_values, t_counts = unique(
        template,
        return_counts=True
        )

    # take the cumsum of the counts and normalize by the number of pixels to
    # get the empirical cumulative distribution functions for the source and
    # template images (maps pixel value --> quantile)
    s_quantiles = cumsum(s_counts).astype('float64')
    s_quantiles /= s_quantiles[-1]
    t_quantiles = cumsum(t_counts).astype('float64')
    t_quantiles /= t_quantiles[-1]

    # interpolate linearly to find the pixel values in the template image
    # that correspond most closely to the quantiles in the source image
    interp_t_values = interp(
        s_quantiles,
        t_quantiles,
        t_values
        )

    return interp_t_values[bin_idx].reshape(oldshape)
