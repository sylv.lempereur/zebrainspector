# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
# Due to a problem with pink, have to disable no-member
# pylint: disable=no-member
"""
This modules contains any function that compute a morphological tophat.

This files is used when pink is installed.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from .closing import closing
from .opening import opening

from ....handling import DataHandling

def _tophat_array(
        array: ndarray,
        rad: int = 1,
        shape: str = 'ball',
        method: str = 'black'
        ):
    """
    Compute the asked tophat on the given array
    using the given radius and the given method.

    Parameters
    ----------
    array: numpy.ndarray
        Array on wich the tophat will be computed

    rad: int
        Radius of the structuring element.

    method: str
        Method used to compute the tophat.
        Could be 'black' or 'white'.

    shape: str
        Shape of the structuring element.
        Could be 'ball' or 'rect'.
    """
    if method.lower() == 'black':
        pos = closing(
            array,
            rad,
            shape
            )
        neg = array
    else:
        pos = array
        neg = opening(
            array,
            rad,
            shape
            )
    array = pos - neg
    del neg, pos
    garbage_collection()
    return array

def _tophat_data_handling(
        data_handling_instance: DataHandling,
        rad: int = 1,
        shape: str = 'ball',
        method: str = 'black'
        ):
    """
    Compute the asked tophat on the current step of a DataHandling instance
    using the given radius and the given method.

    Parameters
    ----------
    data_handling_instance: DataHandling
        Storage of the current step of the image processing process

    rad: int
        Radius of the structuring element.

    method: str
        Method used to compute the tophat.
        Could be 'black' or 'white'.

    shape: str
        Shape of the structuring element.
        Could be 'ball' or 'rect'.
    """
    data_handling_instance.add_step("tophat" + method + str(rad))
    array = data_handling_instance.get_current().copy()
    array = _tophat_array(
        array,
        rad,
        shape,
        method
        )
    data_handling_instance.set_current(array)
    garbage_collection()
    return True

def tophat(
        data,
        rad: int = 1,
        shape: str = 'ball',
        method: str = 'black'
        ):
    """
    Compute a tophat with the given method using the given radius
    on the given image.
    DataHandling instance or numpy array could be provided.

    Parameters
    ----------
    data:
        Data used to compute the tophat.
        could be a numpy.ndarray or a DataHandling instance

    rad: int
        Radius of the structuring element.

    method: str
        Method used to compute the tophat.
        Could be 'black' or 'white'.

    shape: str
        Shape of the structuring element.
        Could be 'ball' or 'rect'.
    """
    if method.lower() not in ['black', 'white']:
        raise ValueError('Provided method is invalid')
    if shape.lower() not in ['ball', 'rect']:
        raise ValueError('Provided shape is invalid')
    if isinstance(data,
                  ndarray):
        out = _tophat_array(
            data,
            rad,
            shape,
            method
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _tophat_data_handling(
            data,
            rad,
            shape,
            method
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
