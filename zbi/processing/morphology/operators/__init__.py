"""
Mathematical morphology operators
=================================

================================== =============================================
morphology.operators
================================================================================
closing                            Closing with given radius and shape
dilation                           Dilation with given radius and shape
gradient                           Morphological gradients
erosion                            Erosion with given radius and shape
opening                            Opening with given radius and shape
================================== =============================================
"""

from .closing import (
    closing,
    closing_geodesic
    )

from .dilation import (
    dilation,
    dilation_geodesic
    )

from .erosion import (
    erosion,
    erosion_geodesic
    )

from .gradient import gradient

from .opening import (
    opening,
    opening_geodesic
    )

from .surface import surface

from .tophat import tophat
