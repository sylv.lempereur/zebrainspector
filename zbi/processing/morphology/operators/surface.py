# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
# Due to a problem with pink, have to disable no-member
# pylint: disable=no-member
"""
This modules contains any function that compute a surface.

This files is used when pink is installed.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from . import erosion

from ....handling import DataHandling

def _surface_array(
        array: ndarray
        ):
    """
    Compute the surface of the given array

    Parameters
    ----------
    array: numpy.ndarray
        Array on wich the surface will be computed
    """
    if array.dtype != 'bool':
        raise TypeError("Given array is not a binary image")
    pos = array
    neg = erosion(
        array,
        1
        )
    array = pos - neg
    del neg, pos
    garbage_collection()
    return array

def _surface_data_handling(
        data_handling_instance: DataHandling
        ):
    """
    Compute the surface on the current step of a DataHandling instance.

    Parameters
    ----------
    data_handling_instance: DataHandling
        Storage of the current step of the image processing process
    """
    data_handling_instance.add_step("surface")
    array = data_handling_instance.get_current().copy()
    array = _surface_array(array)
    data_handling_instance.set_current(array)
    garbage_collection()
    return True

def surface(
        data
        ):
    """
    Compute a surface with the given method using the given radius
    on the given image.
    DataHandling instance or numpy array could be provided.

    Parameters
    ----------
    data:
        Data used to compute the surface.
        could be a numpy.ndarray or a DataHandling instance
    """
    if isinstance(data,
                  ndarray):
        out = _surface_array(data)
    elif isinstance(data,
                    DataHandling
                    ):
        out = _surface_data_handling(data)
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
