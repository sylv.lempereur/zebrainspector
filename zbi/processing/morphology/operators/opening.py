# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
# Due to a problem with pink, have to disable no-member
# pylint: disable=no-member
"""
This modules contains any function that perform
a opening.

This files is used when pink is installed.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from ..pink_numba import fopenrect

from .erosion import erosion

from .dilation import (
    dilation,
    dilation_geodesic
    )

from ...basics import check_bounding_box

from ....handling import DataHandling

def _opening_array(
        array: ndarray,
        rad: int = 3,
        shape: str = 'ball'
        ):
    """
    Opening of a provided array by a given radius
    This script automatically take care of the dimension of the given array

    Parameters
    ----------
    array: numpy.ndarray
        Array used to c ompute a opening

    rad: int
        Radius of the structuring element

    shape: str
        Shape of the structuring element

    Out
    ---
    numpy.ndarray: result of the opening

    See also
    --------
    opening
    opening_array
    """
    dtype = array.dtype
    if shape == 'rect':
        array = _opening_rect(
            array,
            rad
            )
    elif shape == 'ball':
        array = _opening_ball(
            array,
            rad
            )
    return array.astype(dtype)

def _opening_ball(
        array: ndarray,
        rad: int = 3
        ):
    """
    Opening of a grey scale image
    using a spherical structuring element
    of radius rad.

    Parameters
    ----------
    array: numpy.ndarray
        array to close

    rad : radius
        Radius of the sphere used as a sructuring element

    Out
    ---
    numpy.ndarray: result of the spherical opening

    See also
    --------
    opening
    """

    array = erosion(
        array,
        rad
        )
    array = dilation(
        array,
        rad
        )
    garbage_collection()
    return array

def _opening_data_handling(
        data_handling_instance: DataHandling,
        rad: int = 3,
        shape: str = 'rect'
        ):
    """
    Opening of a grey scale image
    using a spherical structuring element
    of radius rad.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    rad : radius
        Radius of the sphere used as a sructuring element

    See also
    --------
    opening
    """
    if shape not in ['ball', 'rect']:
        raise ValueError("Provided shape is not valid.")
    data_handling_instance.add_step("opening" + str(rad))
    array = _opening_array(
        data_handling_instance.get_current(),
        rad,
        shape
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True



def _opening_geodesic_array(
        array: ndarray,
        rad: int = 5,
        shape: str = 'ball'
        ):
    """
    Geodesic erosion
    after a dilation using
    a spherical structuring element
    of radius rad of a binary image
    and the original image as a mask.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    rad : radius
        Radius of the sphere used as a sructuring element
        Radius of the sphere used as a sructuring element
        for the dilation

    See also
    --------
    opening_binary
    opening
    opening_binary_geodesic
    """
    if array.dtype != 'bool':
        raise TypeError("Current step is not a binary image")

    array_temp = erosion(
        array,
        rad=rad,
        shape=shape
        )
    array = dilation_geodesic(
        array_temp,
        array
        )
    del array_temp
    garbage_collection()
    return array

def _opening_geodesic_data_handling(
        data_handling_instance: DataHandling,
        rad: int = 5,
        shape: str = 'ball'
        ):
    """
    Geodesic erosion
    after a dilation using
    a spherical structuring element
    of radius rad of a binary image
    and the original image as a mask.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    mask : numpy.ndarray
        array used as a mask. Have to be a boolean array.
    """
    data_handling_instance.add_step('opening_binary_geodesic_')
    array = _opening_geodesic_array(
        data_handling_instance.get_current(),
        rad,
        shape
        )
    # Creation of the structuring element
    data_handling_instance.set_current(array)
    garbage_collection()
    del array
    return True


def _opening_rect(
        array: ndarray,
        rad: int = 3,
        ):
    """
    Opening of a grey scale image
    using a rectangular structuring element
    of edge size rad.

    Parameters
    ----------
    array: numpy.ndarray
        array to close

    rad : radius
        Size of edges of the cube used as a sructuring element

    Out
    ---
    numpy.ndarray: result of the cubic opening

    See also
    --------
    _opening_ball
    opening
    """
    array = fopenrect(
        array,
        rad,
        rad,
        rad
        )
    garbage_collection()
    return array

def opening(
        data,
        rad: int = 3,
        shape: str = 'ball'
        ):
    """
    opening of an image
    using a structuring element of a given shape
    with a radius rad.

    Parameters
    ----------
    data:
        Data use to perform the opening.
        could be a numpy.ndarray or a DataHandling instance

    rad : radius
        Radius of the sphere used as a sructuring element

    shape: str
        Shape of the structuring element. Could be ball or rect

    See also
    --------
    dilation
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _opening_array(
            data,
            rad,
            shape
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _opening_data_handling(
            data,
            rad,
            shape
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out

def opening_geodesic(
        data,
        rad: int = 3,
        shape: str = 'ball'
        ):
    """
    opening of an image
    using a structuring element of a given shape
    with a radius rad.

    Parameters
    ----------
    data:
        Data use to perform the opening.
        could be a numpy.ndarray or a DataHandling instance

    rad : radius
        Radius of the sphere used as a sructuring element

    shape: str
        Shape of the structuring element. Could be ball or rect

    See also
    --------
    dilation
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _opening_geodesic_array(
            data,
            rad,
            shape
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _opening_geodesic_data_handling(
            data,
            rad
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
