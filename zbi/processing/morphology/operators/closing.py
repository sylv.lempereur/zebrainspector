# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
# Due to a problem with pink, have to disable no-member
# pylint: disable=no-member
"""
This modules contains any function that perform
a closing.

This files is used when pink is installed.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from ..pink_numba import fcloserect

from .erosion import (
    erosion,
    erosion_geodesic
    )

from .dilation import dilation

from ...basics import check_bounding_box

from ....handling import DataHandling

def _closing_array(
        array: ndarray,
        rad: int = 3,
        shape: str = 'ball'
        ):
    """
    Closing of a provided array by a given radius
    This script automatically take care of the dimension of the given array

    Parameters
    ----------
    array: numpy.ndarray
        Array used to c ompute a closing

    rad: int
        Radius of the structuring element

    shape: str
        Shape of the structuring element

    Out
    ---
    numpy.ndarray: result of the closing

    See also
    --------
    closing
    opening_array
    """
    dtype = array.dtype 
    if shape == 'rect':
        array_shape = None
        if dtype =="bool":
            array, array_shape = check_bounding_box(
                array,
                rad
                )
        array = _closing_rect(
            array,
            rad
            )
        if array_shape is not None:
            print(array_shape[0] - 2 * rad)
            array = array[
                rad : rad + array_shape[0],
                rad : rad + array_shape[1],
                rad : rad + array_shape[2]
                ]
        del array_shape
    elif shape == 'ball':
        array = _closing_ball(
            array,
            rad
            )
    garbage_collection()
    return array.astype(dtype)

def _closing_ball(
        array: ndarray,
        rad: int = 3
        ):
    """
    Closing of a grey scale image
    using a spherical structuring element
    of radius rad.

    Parameters
    ----------
    array: numpy.ndarray
        array to close

    rad : radius
        Radius of the sphere used as a sructuring element

    Out
    ---
    numpy.ndarray: result of the spherical closing

    See also
    --------
    opening
    """
    dtype = array.dtype
    array = dilation(
        array,
        rad,
        'ball'
        )
    array = erosion(
        array,
        rad,
        'ball'
        )
    return array.astype(dtype)

def _closing_data_handling(
        data_handling_instance: DataHandling,
        rad: int = 3,
        shape: str = 'rect'
        ):
    """
    Closing of a grey scale image
    using a spherical structuring element
    of radius rad.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    rad : radius
        Radius of the sphere used as a sructuring element

    See also
    --------
    opening
    """
    if shape not in ['ball', 'rect']:
        raise ValueError("Provided shape is not valid.")
    data_handling_instance.add_step("closing" + str(rad))
    array = _closing_array(
        data_handling_instance.get_current(),
        rad,
        shape
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def _closing_geodesic_array(
        array: ndarray,
        rad: int = 5
        ):
    """
    Geodesic erosion
    after a dilation using
    a spherical structuring element
    of radius rad of a binary image
    and the original image as a mask.

    Parameters
    ----------
    array: numpy.ndarray
        Array used to compute the geodesic closing
        Must be a boolean array.

    rad : radius
        Radius of the sphere used as a sructuring element
        used for  dilation
    # """
    # data_handling_instance.add_step('opening_binary_geodesic_'+str(rad))
    if array.dtype != 'bool':
        raise TypeError("Provided array is not a boolean array.")
    array_temp = dilation(
        array,
        rad=rad
        )
    array = erosion_geodesic(
        array,
        array_temp
        )
    del array_temp
    return array

def _closing_geodesic_data_handling(
        data_handling_instance: DataHandling,
        rad: int = 5
        ):
    """
    Geodesic erosion
    after a dilation using
    a spherical structuring element
    of radius rad of a binary image
    and the original image as a mask.

    Parameters
    ----------
    data_handling_instance: DataHandling,
        An instance of DataHandling
        Current step will be used to compute the geodesic closing.
        Current step must be a boolean array.

    rad : radius
        Radius of the sphere used as a sructuring element
        used for  dilation
    # """
    data_handling_instance.add_step('opening_binary_geodesic_'+str(rad))
    array = _closing_geodesic_array(
        data_handling_instance.get_current(),
        rad
        )
    data_handling_instance.set_current(array)
    del array
    return True

def _closing_rect(
        array: ndarray,
        rad: int = 3,
        ):
    """
    Closing of a grey scale image
    using a rectangular structuring element
    of edge size rad.

    Parameters
    ----------
    array: numpy.ndarray
        array to close

    rad : radius
        Size of edges of the cube used as a sructuring element

    Out
    ---
    numpy.ndarray: result of the cubic closing

    See also
    --------
    _closing_ball
    closing
    """
    array = fcloserect(
        array,
        rad,
        rad,
        rad
        )
    garbage_collection()
    return array

def closing(
        data,
        rad: int = 3,
        shape: str = 'ball'
        ):
    """
    closing of an image
    using a structuring element of a given shape
    with a radius rad.

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance

    rad : radius
        Radius of the sphere used as a sructuring element

    shape: str
        Shape of the structuring element. Could be ball or rect

    See also
    --------
    dilation
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _closing_array(
            data,
            rad,
            shape
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _closing_data_handling(
            data,
            rad,
            shape
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out

def closing_geodesic(
        data,
        rad: int = 3
        ):
    """
    closing of an image
    using a structuring element of a given shape
    with a radius rad.

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance

    rad : radius
        Radius of the sphere used as a sructuring element

    shape: str
        Shape of the structuring element. Could be ball or rect

    See also
    --------
    dilation
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _closing_geodesic_array(
            data,
            rad
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _closing_geodesic_data_handling(
            data,
            rad
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
