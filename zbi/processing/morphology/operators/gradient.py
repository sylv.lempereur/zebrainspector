# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
# Due to a problem with pink, have to disable no-member
# pylint: disable=no-member
"""
This modules contains any function that compute a morphological gradient.

This files is used when pink is installed.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from . import (
    dilation,
    erosion
    )

from ....handling import DataHandling

def _gradient_full(
        data,
        rad: int = 1,
        shape: str = 'ball'
        ):
    """
    Compute the gradient of each object in an image.
    This image have to be boolean.
    DataHandling instance or numpy array could be provided.

    Parameters
    ----------
    data:
        Data use to compute the gradient.
        could be a numpy.ndarray or a DataHandling instance

    shape: str
        Shape of the structuring element.
        Could be 'ball' or 'rect'.

    rad: int
        Radius of the structuring element.
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _gradient_full_array(
            data,
            rad,
            shape
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _gradient_full_data_handling(
            data,
            rad,
            shape
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out

def _gradient_full_array(
        array: ndarray,
        rad: int = 1,
        shape: str = 'ball'
        ):
    """
    Computation of the morphological gradient of an array,
    i.e the difference between a dilation and an erosion
    of the given greyscale image.

    Parameters
    ----------
    array: numpy.ndarray
        numpy.ndarray on wich the gradient will be computed

    shape: str
        Shape of the structuring element.
        Could be 'ball' or 'rect'.

    rad : radius
        Radius of the sphere used as a sructuring element
    """
    array = (
        dilation(
            array,
            rad,
            shape
            )
        - erosion(
            array,
            rad,
            shape
            )
        )
    garbage_collection()
    return array

def _gradient_full_data_handling(
        data_handling_instance: DataHandling,
        rad: int = 1,
        shape: str = 'ball'
        ):
    """
    Computation of the morphological gradient of an array,
    i.e the difference between a dilation and an erosion
    of the given greyscale image.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class on wich the gradient will be computed.

    shape: str
        Shape of the structuring element.
        Could be 'ball' or 'rect'.

    rad : radius
        Radius of the sphere used as a sructuring element
    """
    data_handling_instance.add_step("gradient" + str(rad))
    array = data_handling_instance.get_current().copy()
    array = _gradient_full_array(
        array,
        rad,
        shape
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def _gradient_half(
        data,
        rad: int = 1,
        shape: str = 'ball',
        method: str = 'dilation'
        ):
    """
    Compute the half gradient of each object in an image.
    This image have to be boolean.
    DataHandling instance or numpy array could be provided.

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance

    rad: int
        Radius of the structuring element.

    shape: str
        Shape of the structuring element.
        Could be 'ball' or 'rect'.

    method: str
        Method used to compute the half gradient.
        Could be 'dilation', 'erosion' or 'half.
        'half' will be considered as the computation
        of dilation-based half gradient.
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _gradient_half_array(
            data,
            rad,
            shape,
            method
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _gradient_half_data_handling(
            data,
            rad,
            shape,
            method
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out

def _gradient_half_array(
        array: ndarray,
        rad: int = 1,
        shape: str = 'ball',
        method: str = 'dilation'
        ):
    """
    Compute the half gradient of each numpy.ndarray
    using the given radius and the given method.

    Parameters
    ----------
    array: numpy.ndarray
        Data use to compute the half gradient.
        could be a numpy.ndarray or a DataHandling instance

    rad: int
        Radius of the structuring element.

    shape: str
        Shape of the structuring element.
        Could be 'ball' or 'rect'.

    method: str
        Method used to compute the half gradient.
        Could be 'dilation', 'erosion' or 'half.
        'half' will be considered as the computation
        of dilation-based half gradient.
    """
    if method in ['half', 'dilation']:
        neg = array
        pos = dilation(
            array,
            rad,
            shape=shape
            )
    else:
        neg = erosion(
            array,
            rad,
            shape=shape
            )
        pos = array
    array = pos - neg
    del neg
    del pos
    garbage_collection()
    return array

def _gradient_half_data_handling(
        data_handling_instance: DataHandling,
        rad: int = 1,
        shape: str = 'ball',
        method: str = 'dilation'
        ):
    """
    Computation of the morphological gradient of an array,
    i.e the difference between a dilation and an erosion
    of the given greyscale image.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class used to compute the half gradient

    rad : radius
        Radius of the sphere used as a sructuring element

    shape: str
        Shape of the structuring element.
        Could be 'ball' or 'rect'.

    method: str
        Method used to compute the half gradient.
        Could be 'dilation', 'erosion' or 'half.
        'half' will be considered as the computation
        of dilation-based half gradient.
    """
    data_handling_instance.add_step("halfGradient" + method + str(rad))
    array = data_handling_instance.get_current().copy()
    array = _gradient_half_array(
        array,
        rad,
        shape,
        method
        )
    data_handling_instance.set_current(array)
    garbage_collection()
    return True

def gradient(
        data,
        rad: int = 1,
        shape: str = 'ball',
        method: str = 'full'
        ):
    """
    Compute the asked gradient of each object in an image.
    This image have to be boolean.
    DataHandling instance or numpy array could be provided.

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance

    rad: int
        Radius of the structuring element.

    shape: str
        Shape of the structuring element.
        Could be 'ball' or 'rect'.

    method: str
        Method used to compute the gradient.
        Could be 'dilation', 'erosion', 'full' or 'half.
        if dilation, erosion of half is chosen,
        an half-gradient will be computed.
        Half will be considered as the computation
        of dilation-based half gradient.
    """
    if method == 'full':
        out = _gradient_full(
            data,
            rad
            )
    elif method in ['dilation', 'erosion', 'half']:
        out = _gradient_half(
            data,
            rad,
            shape,
            method
            )
    else:
        raise ValueError("Wrong method")
    garbage_collection()
    return out
