"""
Mathematical morphology image processing tools
==============================================

================================== =============================================
morphology
================================================================================
closing                            Closing with given radius and shape
dilation                           Dilation with given radius and shape
erosion                            Erosion with given radius and shape
gradient                           Morphological gradient
opening                            Opening with given radius and shape
watershed                          Watershed computation
================================== =============================================
"""

from .operators import (
    closing,
    closing_geodesic,
    dilation,
    dilation_geodesic,
    gradient,
    erosion,
    erosion_geodesic,
    opening,
    opening_geodesic,
    surface,
    tophat,
    )

from .transforms import (
    dist,
    remove_holes,
    skeletonization,
    watershed
    )
