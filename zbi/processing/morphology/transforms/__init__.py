"""
Mathematical morphology transforms
==================================

================================== =============================================
morphology
================================================================================
dist                               Compute distance map
remove_holes                       Fulfill object using
skeletonization                    Compute skeleton of a binary image
watershed                          Watershed computation
================================== =============================================
"""

from .dist import dist
from .remove_holes import remove_holes
from .skeletonization import skeletonization
from .watershed import watershed
