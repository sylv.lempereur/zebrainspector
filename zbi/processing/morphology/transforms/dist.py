# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
# Due to a problem with pink, have to disable no-member
# pylint: disable=no-member
"""
This modules contains any function that compute a distance map.

This files is used when pink is installed.
"""
from gc import collect as garbage_collection

from numpy import ndarray

# pylint: disable=no-name-in-module
# from ..pink_numba import dist as distpink
from scipy.ndimage import distance_transform_edt as distsp

from ...basics import inversion
from ....handling import DataHandling

def _dist_array(
        array: ndarray
        ):
    """
    Computation of the distance map of a binary image

    Parameters
    ----------
    array: numpy.ndarray
        A binary array

    Returns
    -------
    numpy.ndarray
        Result of the computation of the distance map

    See also
    --------
    dist
    """
    if array.dtype != "bool":
        raise TypeError("provided image is not a binary image")
    array = distsp(inversion(array))
    garbage_collection()
    return array

def _dist_data_handling(
        data_handling_instance: DataHandling
        ):
    """
    Computation of the distance map of a binary image

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    See also
    --------
    dist_array
    """
    data_handling_instance.add_step("distanceMap")
    array = _dist_array(data_handling_instance.get_current())
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def dist(data):
    """
    Compute the distance map of each object in an image.
    This image have to be boolean.
    DataHandling instance or numpy array could be provided.

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _dist_array(data)
    elif isinstance(data,
                    DataHandling
                    ):
        out = _dist_data_handling(data)
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
