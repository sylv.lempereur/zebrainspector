# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
# Due to a problem with pink, have to disable no-member
# pylint: disable=no-member
"""
This modules contains any function that compute a morphological remove_holes.

This files is used when pink is installed.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from ..operators import (
    dilation_geodesic,
    # erosion_geodesic
    )

from ... import basics

from ....handling import DataHandling

def _remove_holes_array(
        array: ndarray,
        method: str = 'black'
        ):
    """
    Removing selected holes on an array
    by geodesic dilation of a frame.

    Parameters
    ----------
    array: numpy.ndarray
        Array on wich the holes will be removes
        Should be a boolean array.

    method: str
        Kind of holes to removes.
        Could be 'black' or 'white'.
    """
    if array.dtype != "bool":
        raise TypeError("Provided array should be a binary image")
    if method.lower() == 'black':
        array = _removes_holes_black(array)
    else:
        raise ValueError('remove White holes is not implemented yet.')
        # array = _removes_holes_white(array)
    garbage_collection()
    return array

def _remove_holes_data_handling(
        data_handling_instance: DataHandling,
        method: str = 'black'
        ):
    """
    Removing selected holes on the current step.
    by geodesic dilation of a frame.

    Parameters
    ----------
    data_handling_instance: DataHandling
        Storage of the current step of the image processing process
        The current step have to a boolean array.

    method: str
        Kind of holes to removes.
        Could be 'black' or 'white'.
    """
    data_handling_instance.add_step("remove_holes_" + method)
    array = data_handling_instance.get_current().copy()
    array = _remove_holes_array(
        array,
        method
        )
    data_handling_instance.set_current(array)
    garbage_collection()
    return True

def _removes_holes_black(
        array
        ):
    """
    Removing black holes in the element of interest
    by geodesic dilation of a frame
    using the inverted image as mask.

    Current step have to ba a binary image

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    See also
    --------
    closing_binary_geodesic
    opening_binary_geodesic
    remove_holes_black_array
    """
    frame = basics.frame_creation(array)
    array = basics.inversion(array)
    array = dilation_geodesic(
        frame,
        array
        )
    del frame
    array = basics.inversion(array)
    garbage_collection()
    return  array

def remove_holes(
        data,
        method: str = 'black'
        ):
    """
    Removing selected holes
    by geodesic dilation of a frame.
    DataHandling instance or numpy array could be provided.

    Parameters
    ----------
    data:
        Data used to compute the remove_holes.
        could be a numpy.ndarray or a DataHandling instance

    method: str
        Kind of holes to removes.
        Could be 'black' or 'white'.
    """
    if method.lower() not in ['black', 'white']:
        raise ValueError('Provided method is invalid')
    if isinstance(data,
                  ndarray):
        out = _remove_holes_array(
            data,
            method
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _remove_holes_data_handling(
            data,
            method
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
