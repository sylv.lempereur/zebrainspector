# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This modules contains any function that perform
the computation of a skeletonization.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from skimage.morphology import skeletonize_3d

from ....handling import DataHandling

def _skeletonization_array(
        array: ndarray
        ):
    """
    Computation of the skeleton map of a boolean image

    Parameters
    ----------
    array: numpy.ndarray
        A binary array

    Returns
    -------
    numpy.ndarray
        Result of the computation of the distance map

    See also
    --------
    dist
    """
    if array.dtype != "bool":
        raise TypeError("provided piece of data is not a boolean.")
    array = skeletonize_3d(array)
    garbage_collection()
    return array

def _skeletonization_data_handling(
        data_handling_instance: DataHandling
        ):
    """
    Computation of the distance map of a binary image

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    See also
    --------
    dist_array
    """
    data_handling_instance.add_step("skeletonization")
    array = _skeletonization_array(data_handling_instance.get_current())
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def skeletonization(data):
    """
    Skeletonization of a boolean image.

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance

    """
    if isinstance(data,
                  ndarray
                  ):
        out = _skeletonization_array(data)
    elif isinstance(data,
                    DataHandling
                    ):
        out = _skeletonization_data_handling(data)
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
