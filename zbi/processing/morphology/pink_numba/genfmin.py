import numpy as np
from numba import jit

@jit(nopython=True)
def mnx(a,b,usemin) :
    if usemin :
       if a < b : return a
       return b
    else :
       if a > b : return a
       return b


@jit(nopython=True)
def genfminmax(Tf : np.ndarray ,f : int, Tg : np.ndarray, Th : np.ndarray, Tp : np.ndarray, nx : int, K : int, usemin : bool ) :
    u = usemin
    # Tf: image applatie en tableau 1D
    """
    ** g: forward array
    ** h: backward array
    ** p: array of offsets
    ** nx: the number of elements in each array
    ** K:  the extent of mask
    """

    g = h = p = 0                               # indices équivalents aux pointeurs

    #unsigned int i,j,k,r,r1;

    if K % 2 == 0:                              # (!(K%2))  enforce the odd extent
       K += 1
    k = nx // K                                 # k = nx/K
    r1 = nx % K

    # do forward array
    for j in range(k):                          # for(j=0;j<k;j++)
        Tg[g] = Tf[f + Tp[p]]                   # *g = *(f+*p)
        g += 1                                  # for(g++,p++,  i=1;i<K;i++,  g++,p++)
        p += 1
        for i in range(1,K) :
            Tg[g] = mnx(Tf[f+Tp[p]],Tg[g-1],u)  # *g = liarmin(*(f+*p),*(g-1))
            g += 1
            p += 1

    if r1 > 0:                                  # if (r1)
        Tg[g] = Tf[f+Tp[p]]                     # *g = *(f+*p);
        g += 1
        p += 1
        for i in range(1,r1) :                  # for(g++,p++,i=1;i<r1;i++,g++,p++)
            Tg[g] = mnx(Tf[f+Tp[p]],Tg[g-1],u)  # *g = liarmin(*(f+*p),*(g-1));
            g +=1
            p +=1
    p -= 1

    if nx <= (K>>1) :                           # if(nx <= (K>>1))
        g-=1
        for i in range(nx):                     # for(i=0;i<nx;i++, p--)
            Tf[f+Tp[p]] = Tg[g]                 # *(f+*p) = *g;
            p -= 1
        return;

    h += nx - 1
    g -= nx

    # do backward array
    if r1 > 0 :                                 # if(r1)
        Th[h] = Tf[f+Tp[p]]                     # *h = *(f+*p);
        h-=1
        p-=1
        for i in range(1,r1) :                  # for(h--,p--,  i=1;i<r1;i++,  h--,p--)
            Th[h] = mnx(Tf[f+Tp[p]],Th[h+1],u)  # *h = liarmin(*(f+*p),*(h+1));
            h -= 1
            p -= 1

    for j in range(k):                          # for(j=0;j<k;j++)
        Th[h] = Tf[f+Tp[p]]                     # *h = *(f+*p);
        h -= 1
        p -= 1
        for i in range(1,K):                    # for(h--,p--,  i=1;i<K;i++,  h--,p--)
            Th[h] = mnx(Tf[f+Tp[p]],Th[h+1],u)  # *h = liarmin(*(f+*p),*(h+1))
            h -= 1
            p -= 1


    # reset pointers
    p += 1
    h += 1
    r = K>>1
    g += r

    if nx <= K :
        r1 = nx - r - 1
        i = 0
        while i < r1 :                          # for(i=0;i<r1;i++,  p++,g++)  # dependance de la valeur i en sortie => while
            Tf[f+Tp[p]] = Tg[g];                # *(f+*p) = *g;
            p += 1
            g += 1
            i += 1
                                         # correction car en C, i sort en i+1
        r1 +=  K - nx + 1
        while i < r1 :                          # for(;i<r1;  i++,p++)
            Tf[f+Tp[p]] = Tg[g]                 # *(f+*p) = *g;
            i += 1
            p += 1
        h += 1
        while i < nx :                          # for(h++; i<nx;  i++,h++,p++)
            Tf[f+Tp[p]] = Th[h]                 # *(f+*p) = *h;
            i += 1
            h += 1
            p += 1
        return

    # do left border
    for i in range(0,r) :                       # for(i=0;i<r;i++,  p++,g++)
        Tf[f+Tp[p]] = Tg[g]                     # *(f+*p) = *g
        p += 1
        g += 1


    # do middle values
    for i in range(K-1,nx) :                   # for(i=K-1;i<nx;i++,  p++,h++,g++)
        Tf[f+Tp[p]] = mnx(Tg[g],Th[h],u)       # *(f+*p) = liarmin(*g,*h);
        p += 1
        h += 1
        g += 1

    # reset pointers to end position
    h += (K-2)
    p += (r-1)

    # do right border
    if r1 > 0 and k > 0:                       # (r1 && k)
        h -= r1
        for i in range(r1,K):                  # for(h-=r1,  i=r1;i<K;i++,  h--)
            Th[h] = mnx(Th[h],Th[h+1],u)       # *h = liarmin(*(h),*(h+1))
            h -= 1
        h += K

    h -= r
    for i in range(0,r):                       # for(i=0;i<r;i++,  h--,p--)
        Tf[f+Tp[p]] = Th[h]                    # *(f+*p) = *(h)
        h -= 1
        p -= 1
