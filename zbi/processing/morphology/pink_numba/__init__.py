"""
Contains functions developped in Pink
and translate by L. Buzer
"""
from .dist import dist

from .genball import genball

from .watershed import watershed

from .operators import (
    erosion,
    dilation,
    fcloserect,
    fdilaterect,
    ferodrect,
    fopenrect,
    geodilat,
    geoeros
    )
