"""
Contains function ustructs to compute morphological operators
"""

from numpy import ndarray

from .ldilatlong3d import ldilatlong3d
from .leroslong3d import leroslong3d
from .rect3dmm import rect3dminmax
from .geodilat3d import geoeros as geoeros_numba
from .geodilat3d import geodilat as geodilat_numba

def dilation(
        array: ndarray,
        struct: ndarray
        ):
    """
    Compute dilation of the given image using the given structuring element

    Parameters
    ----------
    array: numpy.ndarray
        Image to dilate

    struct: numpy.ndarray
        Structurign element

    Returns
    -------
    numpy.ndarray
        Result of the dilation
    """
    array_local = array.copy()
    ldilatlong3d(
        array_local,
        struct
        )
    return array_local

def erosion(
        array: ndarray,
        struct: ndarray
        ):
    """
    Compute erosion of the given image using the given structuring element

    Parameters
    ----------
    array: numpy.ndarray
        Image to erode

    struct: numpy.ndarray
        Structurign element

    Returns
    -------
    numpy.ndarray
        Result of the erosion
    """
    array_local = array.copy()
    leroslong3d(
        array_local,
        struct
        )
    return array_local

def fcloserect(
        array,
        radius_1,
        radius_2,
        radius_3
        ):
    """
    Compute fast closing using a rectangular structuring element

    Parameters
    ----------
    array: numpy.ndarray
        Array to close

    radius_1: int
        Size of the rectangular in the first dimension of the array

    radius_2: int
        Size of the rectangular in the second dimension of the array

    radius_3: int
        Size of the rectangular in the third dimension of the array*

    Returns
    -------
    numpy.ndarray
        Closed array
    """
    array_local = array.copy()
    rect3dminmax(
        array_local,
        radius_1,
        radius_2,
        radius_3,
        usemin=False
        )
    rect3dminmax(
        array_local,
        radius_1,
        radius_2,
        radius_3,
        usemin=True
        )
    return array_local

def fdilaterect(
        array,
        radius_1,
        radius_2,
        radius_3
        ):
    """
    Compute fast dilation using a rectangular structuring element

    Parameters
    ----------
    array: numpy.ndarray
        Array to dilate

    radius_1: int
        Size of the rectangular in the first dimension of the array

    radius_2: int
        Size of the rectangular in the second dimension of the array

    radius_3: int
        Size of the rectangular in the third dimension of the array*

    Returns
    -------
    numpy.ndarray
        Dilated array
    """
    array_local = array.copy()
    rect3dminmax(
        array_local,
        radius_1,
        radius_2,
        radius_3,
        usemin=False
        )
    return array_local

def ferodrect(
        array,
        radius_1,
        radius_2,
        radius_3
        ):
    """
    Compute fast erosion using a rectangular structuring element

    Parameters
    ----------
    array: numpy.ndarray
        Array to erode

    radius_1: int
        Size of the rectangular in the first dimension of the array

    radius_2: int
        Size of the rectangular in the second dimension of the array

    radius_3: int
        Size of the rectangular in the third dimension of the array*

    Returns
    -------
    numpy.ndarray
        Eroded array
    """
    array_local = array.copy()
    rect3dminmax(
        array_local,
        radius_1,
        radius_2,
        radius_3,
        usemin=True
        )
    return array_local


def fopenrect(
        array,
        radius_1,
        radius_2,
        radius_3
        ):
    """
    Compute fast opening using a rectangular structuring element

    Parameters
    ----------
    array: numpy.ndarray
        Array to open

    radius_1: int
        Size of the rectangular in the first dimension of the array

    radius_2: int
        Size of the rectangular in the second dimension of the array

    radius_3: int
        Size of the rectangular in the third dimension of the array*

    Returns
    -------
    numpy.ndarray
        Opened array
    """
    array_local = array.copy()
    rect3dminmax(
        array_local,
        radius_1,
        radius_2,
        radius_3,
        usemin=True
        )
    rect3dminmax(
        array_local,
        radius_1,
        radius_2,
        radius_3,
        usemin=False
        )
    return array_local

def geodilat(
        seeds,
        mask,
        n_iter: int = 30
        ):
    """
    Compute a geodesic dilation

    Parameters
    ----------
    seeds: numpy.ndarray

    mask: numpy.ndarray

    n_iter: int
        (Optionnal) Setup the maximum number of iteration
        If -1 is setup, the process will stop when no change happens between two steps.
        Default is -1.

    Returns
    -------
    numpy.ndarray
            Result of the geodesic dilation
    """
    geodilat_numba(
        f=seeds,
        g=mask,
        niter=n_iter
        )

def geoeros(
        seeds,
        mask,
        n_iter: int = -1
        ):
    """
    Compute a geodesic erosion

    Parameters
    ----------
    seeds: numpy.ndarray

    mask: numpy.ndarray

    n_iter: int
        (Optionnal) Setup the maximum number of iteration
        If -1 is setup, the process will stop when no change happens between two steps.
        Default is -1.

    Returns
    -------
    numpy.ndarray
            Result of the geodesic erosion
    """

    geoeros_numba(
        f=seeds,
        g=mask,
        niter=n_iter)
