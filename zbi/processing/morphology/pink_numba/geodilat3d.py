import numpy as np
from numba import jit


Maxuint64 = np.iinfo(np.uint64).max

pStart = 3
iMax = 2
iOut = 1
iIn  = 0

@jit
def CreeDoubleFifoVide(taillemax):
    Fifo = np.zeros((taillemax+10,2),dtype=np.uint64)

    Fifo[iMax,0] = taillemax+9  # L->Max = taillemax+1;
    Fifo[iIn ,0] = 3            # L->In = 0;
    Fifo[iOut,0] = 3            # L->Out = 0;

    Fifo[iMax,1] = taillemax+9
    Fifo[iIn ,1] = 3
    Fifo[iOut,1] = 3

    return Fifo

@jit
def FifoVide(Fifo,id) :
    return   Fifo[iOut,id] == Fifo[iIn,id]    # (L->In == L->Out);

@jit
def FifoPush(Fifo,id, V):
    pos = Fifo[iIn,id]
    Fifo[pos,id] = V           # L->Pts[L->In] = V;
    pos += np.uint64(1)

    if pos > Fifo[iMax,id] :
        pos = pStart
    Fifo[iIn,id] = pos         #   L->In = (L->In + 1) % L->Max;


@jit
def FifoPop(Fifo,id):
    pos = Fifo[iOut,id]

    V =  Fifo[pos,id]           #  L->Pts[L->Out];
    pos += np.uint64(1)
    if pos > Fifo[iMax,id] :
        pos =  pStart
    Fifo[iOut,id] = pos         #  L->Out = (L->Out + 1) % L->Max;
    return V;



##########################################################################

@jit
def voisin18(i,rs,ps,N,T): #(index_t i, index_t rs, index_t ps, index_t N, index_t * T)
    T.fill(Maxuint64)

    # les 5 premiers (0 a 4) sont les 5 pixels du plan "ARRIERE" (+ps)
    if ((i < N - ps) and (i%rs != rs - 1))    :T[0] = ps + i + 1
    if ((i < N - ps) and (i%ps >= rs))        :T[1] = ps + i - rs
    if ((i < N - ps) and (i%rs != 0))         :T[2] = ps + i - 1
    if ((i < N - ps) and (i%ps < ps - rs))    :T[3] = ps + i + rs
    if ((i < N - ps))                         :T[4] = ps + i

    # les 8 suivants (5 a 12) sont les 8 pixels du plan "COURANT" ()
    if ((i%rs != rs - 1))                         :T[5]  = i + 1
    if ((i%rs != rs - 1) and (i%ps >= rs))        :T[6]  = i + 1 - rs
    if ((i%ps >= rs))                             :T[7]  = i - rs
    if ((i%ps >= rs) and (i%rs != 0))             :T[8]  = i - rs - 1
    if ((i%rs != 0))                              :T[9]  = i - 1
    if ((i%rs != 0) and (i%ps < ps - rs))         :T[10] = i - 1 + rs
    if ((i%ps < ps - rs))                         :T[11] = i + rs
    if ((i%ps < ps - rs) and (i%rs != rs - 1))    :T[12] = i + rs + 1

    # les 5 derniers (13 a 17) sont les 5 pixels du plan "AVANT" (-ps)
    if ((i >= ps) and (i%rs != rs - 1))            :T[13] =  i - ps + 1
    if ((i >= ps) and (i%ps >= rs))                :T[14] =  i - ps - rs
    if ((i >= ps) and (i%rs != 0))                 :T[15] =  i - ps - 1
    if ((i >= ps) and (i%ps < ps - rs))            :T[16] =  i - ps + rs
    if ((i >= ps))                                 :T[17] =  i - ps

###########################################################################

@jit
def Set(x,p,Ind):
   Ind[x]  |=  (1<<p)

@jit
def UnSet(x,p,Ind):
   Ind[x]  &=~ (1<<p)

@jit
def IsSet(x,p,Ind):
   return ( Ind[x] & (1<<p) )

########################################################################



# Operateurs morphologiques geodesiques
# methode : propagation des changements par fifo


# lgeodilat3d_short(struct xvimage *g, struct xvimage *f, int32_t connex, int32_t niter)
# reconstruction de g sous f
# g : image marqueur
# f : image masque
# niter : nombre d'iterations (ou -1 pour saturation)
# resultat dans g

@jit
def geodilat(g,f,niter):

    d, cs, rs = g.shape
    n = rs * cs  # index_t n = rs * cs                 # taille plan */
    N = n * d    # index_t N = n * d                     # taille image */

    F = f.reshape(N)    #    int16_t *F = SSHORTDATA(f);      # l'image masqu
    G = g.reshape(N)    #    int16_t *G = SSHORTDATA(g);      # l'image marqueur

    FIFO = CreeDoubleFifoVide(N);

    Indics = np.zeros(N,dtype =np.uint8)    # typedef uint8_t Indicstype;    IndicsInit(N);

    for x in range(N):              # for (x = 0; x < N; x++)  # mise en fifo initiale de tous les points
        FifoPush(FIFO,1, x);
        Set(x, 1,Indics);


    h = np.zeros(g.shape,dtype=np.uint8)  # H = (int16_t *)calloc(1,N*sizeof(int16_t));
    H = h.reshape(N)


    for x in range(N):              #  for (x = 0; x < N; x++)        force G à être <= F
        if (G[x] > F[x]) :
          G[x] = F[x];

    KKK = np.empty(18,dtype=np.uint64)
    iter = 0;
    nbchang = 1

    while ( (niter == -1) or (iter < niter)) and nbchang != 0 :
        iter += 1;
        nbchang = 0;
        itermod2 = iter % 2
        iterUN   = (iter + 1) % 2

        while (not FifoVide(FIFO,itermod2)):    # (! FifoVide(FIFO[iter % 2]))

            x = FifoPop(FIFO,itermod2)          # x = FifoPop(FIFO[iter % 2]);
            UnSet(x, itermod2,Indics);          # UnSet(x, iter % 2);
            sup = G[x];
            voisin18(x, rs, n, N, KKK)


            for k in range(18):                 # for (k = 0; k < 18; k += 1)

                y = KKK[k]
                if ((y != Maxuint64) and (G[y] > sup)):
                     sup = G[y];

            if sup > F[x]:
                sup = F[x]                      # sup = mcmin(sup, F[x]);

            if G[x] != sup :

                #  changement: on enregistre x ainsi que ses voisins
                nbchang += 1;

                if not IsSet(x,iterUN,Indics) :         # (! IsSet(x, (iter + 1) % 2),Indics) :
                    FifoPush(FIFO,iterUN, x);
                    Set(x, iterUN,Indics);

                for k in range(18):                     # for (k = 0; k < 18; k += 1)
                    y = KKK[k]
                    if ((y != Maxuint64) and (not IsSet(y, iterUN,Indics))) :

                        FifoPush(FIFO,iterUN, y);
                        Set(y,iterUN,Indics);g

            H[x] = sup;

        temp = G; # echange les roles de G et H
        G = H;
        H = temp;



    # remet le resultat dans g si necessaire
    if iter%2 == 1 :                                        # if (G != SSHORTDATA(g))
        for x in range(N):                                  # for (x = 0; x < N; x++)
           H[x] = G[x];

#######################################################################################################################




@jit
def geoeros(g,f,niter):

    d, cs, rs = g.shape
    n = rs * cs  # index_t n = rs * cs                 # taille plan */
    N = n * d    # index_t N = n * d                     # taille image */

    F = f.reshape(N)    #    int16_t *F = SSHORTDATA(f);      # l'image masqu
    G = g.reshape(N)    #    int16_t *G = SSHORTDATA(g);      # l'image marqueur

    FIFO = CreeDoubleFifoVide(N);

    Indics = np.zeros(N,dtype =np.uint8)    # typedef uint8_t Indicstype;    IndicsInit(N);

    for x in range(N):              # for (x = 0; x < N; x++)  # mise en fifo initiale de tous les points
        FifoPush(FIFO,1, x);
        Set(x, 1,Indics);


    h = np.zeros(g.shape,dtype=np.uint8)  # H = (int16_t *)calloc(1,N*sizeof(int16_t));
    H = h.reshape(N)


    for x in range(N):              #  for (x = 0; x < N; x++)        force G à être <= F
        if (G[x] < F[x]) :
          G[x] = F[x];

    KKK = np.empty(18,dtype=np.uint64)
    iter = 0;
    nbchang = 1

    while ( (niter == -1) or (iter < niter)) and nbchang != 0 :

        iter += 1;
        nbchang = 0;
        itermod2 = iter % 2
        iterUN   = (iter + 1) % 2

        while (not FifoVide(FIFO,itermod2)):    # (! FifoVide(FIFO[iter % 2]))

            x = FifoPop(FIFO,itermod2)          # x = FifoPop(FIFO[iter % 2]);
            UnSet(x, itermod2,Indics);          # UnSet(x, iter % 2);
            inf = G[x];
            voisin18(x, rs, n, N, KKK)


            for k in range(18):                 # for (k = 0; k < 18; k += 1)

                y = KKK[k]
                if ((y != Maxuint64) and (G[y] < inf)):
                     inf = G[y];

            if inf < F[x]:
                inf = F[x]                      # inf = mcmin(inf, F[x]);

            if G[x] != inf :

                #  changement: on enregistre x ainsi que ses voisins
                nbchang += 1;

                if not IsSet(x,iterUN,Indics) :         # (! IsSet(x, (iter + 1) % 2),Indics) :
                    FifoPush(FIFO,iterUN, x);
                    Set(x, iterUN,Indics);

                for k in range(18):                     # for (k = 0; k < 18; k += 1)
                    y = KKK[k]
                    if ((y != Maxuint64) and (not IsSet(y, iterUN,Indics))) :

                        FifoPush(FIFO,iterUN, y);
                        Set(y,iterUN,Indics);g


            H[x] = inf;

        temp = G; # echange les roles de G et H
        G = H;
        H = temp;



    # remet le resultat dans g si necessaire
    if iter%2 == 1 :                                        # if (G != SSHORTDATA(g))
        for x in range(N):                                  # for (x = 0; x < N; x++)
           H[x] = G[x];


