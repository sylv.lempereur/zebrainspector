import numpy as np
import math
from numba import jit,prange


"""
int32_t leroslong3d(struct xvimage *f, struct xvimage *m, int32_t xc, int32_t yc, int32_t zc)
/* operateur d erosion numerique par un element structurant plan de taille quelconque */
/* m : masque representant l'element structurant */
/* xc, yc, zc : coordonnees du "centre" de l'element structurant */

  register index_t x, y, z;        /* index muet */
  register index_t i, j, k, h;     /* index muet */
  index_t rs = rowsize(f);         /* taille ligne */
  index_t cs = colsize(f);         /* taille colonne */
  index_t ds = depth(f);           /* nb plans */
  index_t ps = rs * cs;            /* taille plan */
  index_t N = ps * ds;             /* taille image */
  index_t rsm = rowsize(m);        /* taille ligne masque */
  index_t csm = colsize(m);        /* taille colonne masque */
  index_t dsm = depth(m);          /* nb plans masque */
  index_t psm = rsm * csm;
  index_t Nm = psm * dsm;
  uint8_t *M = UCHARDATA(m);
  int32_t *F = SLONGDATA(f);
  int32_t *H;                    /* image de travail */
  int32_t inf;
  int32_t nptb;                    /* nombre de points de l'e.s. */
  int32_t *tab_es_x;               /* liste des coord. x des points de l'e.s. */
  int32_t *tab_es_y;               /* liste des coord. y des points de l'e.s. */
  int32_t *tab_es_z;               /* liste des coord. z des points de l'e.s. */
  int32_t c;
"""

@jit(nopython=True)
def leroslong3d(f : np.ndarray, m : np.ndarray) :
  ds,cs,rs = f.shape          # rs = rowsize(f); cs = colsize(f); ds = depth(f);
  ps = rs * cs                # ps = rs * cs;            /* taille plan */
  N = ps * ds;                # N = ps * ds;             /* taille image */
  dsm,csm,rsm = m.shape       # rsm = rowsize(m); csm = colsize(m); dsm = depth(m);
  psm = rsm * csm;
  Nm = psm * dsm;
  zc,yc,xc = dsm // 2, csm // 2, rsm // 2
  F = f.reshape(N)            # liaison entre f et F


  H = F.copy()                 # memcpy(H, F, 4*N);
  # maintient une liaison entre f et F

  M = m.reshape(Nm)                # *M = UCHARDATA(m);

  nptb = 0;
  for i in range(Nm) :             # for (i = 0; i < Nm; i++) if (M[i]) nptb += 1;
     if M[i] > 0 : nptb += 1

  tab_es_x = np.zeros(nptb,dtype=np.int32)  # tab_es_x = (int32_t *)calloc(1,nptb * sizeof(int32_t));
  tab_es_y = np.zeros(nptb,dtype=np.int32)  # tab_es_y = (int32_t *)calloc(1,nptb * sizeof(int32_t));
  tab_es_z = np.zeros(nptb,dtype=np.int32)  # tab_es_z = (int32_t *)calloc(1,nptb * sizeof(int32_t));

  h = 0;
  for k in range(dsm):                       # for (k = 0; k < dsm; k += 1)
    for j in range(csm):                     # for (j = 0; j < csm; j += 1)
      for i in range(rsm):                   # for (i = 0; i < rsm; i += 1)
        if M[k*psm + j*rsm + i] :            # if (M[k*psm + j*rsm + i])
           tab_es_x[h] = rsm - 1 - i;        # symetrique de l'e.s.
           tab_es_y[h] = csm - 1 - j;
           tab_es_z[h] = dsm - 1 - k;
           h += 1;

  xc = rsm - 1 - xc;
  yc = csm - 1 - yc;
  zc = dsm - 1 - zc;

  for z in range(ds):                    # for (z = 0; z < ds; z++)
   for y in range(cs):                    # for (y = 0; y < cs; y++)
    for x in range(rs):                   # for (x = 0; x < rs; x++)

        inf = np.iinfo(f.dtype).max        # inf = INT32_MAX;
        for c in range(nptb) :             # for (c = 0; c < nptb ; c += 1)

            k = z + tab_es_z[c] - zc;
            j = y + tab_es_y[c] - yc;
            i = x + tab_es_x[c] - xc;
            if k >= 0 and k < ds and j >= 0 and j < cs and i >= 0 and i < rs:
                inf = min(inf, H[k*ps + j*rs + i] )

        F[z*ps + y*rs + x] = inf;

  # del H                     # free(H);
  # del tab_es_x              # free(tab_es_x);
  # del tab_es_y              # free(tab_es_y);
  # del tab_es_z              # free(tab_es_z);
  return 1;

