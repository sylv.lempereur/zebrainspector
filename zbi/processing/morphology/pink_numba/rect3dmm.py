# portage du fichier  rect3dmm.hpp  @ pink0.9
import numpy as np
from numba import jit,prange

from .genfmin import genfminmax
"""
    brief function to replace each voxel within a 3d image with
    max/min within a given 3d structuring element (rectangular)

    This function performs erosion or dilation using the three dimensional
    equivelant of a rectangular structuring element.  The original image is
    described both by the image itself (*in) and the dimensions (nx, ny and
    nz) while the structuring element is described by its width, breadth and
    depth (w, b and d).  The (*func)() argument is then used to specify
    either erosion of dilation (genfmin for erosion and genfmax for dilation).
    Note that this function does not create a new image but rather modifies the
    image supplied (*in).


 * \param *in:            input image
 * \param nx:             number of columns in input image
 * \param ny:             number of rows in input image
 * \param nz:             number of slices in input image
 * \param w:              width (x dimension) of SE
 * \param b:              breadth (y dimension) of SE
 * \param d:              depth (z dimension) of SE
 * \param (*func)():      min or max operation (genfmin/genfmax)
"""

@jit(nopython=True)
def rect3dminmax(I : np.ndarray ,  w : int, b : int, d : int, usemin : bool ) :
  """
  int        i, j;               /* indexing variables */
  int        maxdim;             /* maximum dimension */
  Type      *row, *col, *slice;  /* row/col/slice pointers */
  Type      *h, *g;              /* fowards and backwards arrays (for func) */
  long      * p;                  /* row/col/slice offset array */
  """

  nz,ny,nx = I.shape
  Type = I.dtype
  In = I.reshape(nx*ny*nz)   # maintenant l'image est un tableau 1D comme dans pink
                             # maintient une liaison entre In et I


  # Calculate max dimension
  maxdim = max(nx,ny,nz)

  # Allocate memory for max/min and offset buffers

  Tg = np.zeros((maxdim),dtype = Type)     # g = (Type *)calloc(maxdim, sizeof(Type));
  Th = np.zeros((maxdim),dtype = Type)     # h = (Type *)calloc(maxdim, sizeof(Type));
  Tp = np.zeros((maxdim),dtype = np.int32) # p = (long *)calloc(maxdim, sizeof(long));


  # set row, col and slice buffers to start of image buffer
  row = col = slice = 0    # row = col = slice = in;

  ###################################################
  # if width of SE > 1 then perform max/min on each row
  if w > 1 :
    # set row element offsets
    Tp[0] = 0                              # p[0] = 0
    for i in range(1,nx) :                 # for (i=1; i<nx; ++i)
      Tp[i] = 1 + Tp[i-1]                  # p[i] = 1 + p[i-1]

    # gen max/min for each row (within each slice)
    for i in range(ny*nz) :                # for(i=0; i<(ny*nz); ++i, row+=nx)
        genfminmax(In, row, Tg, Th, Tp, nx, w,usemin)
        row+=nx                            # for(i=0; i<(ny*nz); ++i, row+=nx)


  ###################################################
  #  if y dimension of SE > 1 then perform max/min on each column
  if b > 1 :
    #  set column element offsets
    Tp[0] = 0                              # for (p[0]=0, i=1; i<ny; ++i)
    for i in range(1,ny):                  # for (p[0]=0, i=1; i<ny; ++i)
       Tp[i] = nx + Tp[i-1]

    # gen max/min for each column (within each slice)
    for i in range(nz) :                   # for (i=0; i<nz; ++i, col+=((nx*ny)-nx))
        for j in range(nx) :               # for (j=0; j<nx; ++j, ++col)
            genfminmax(In, col, Tg, Th, Tp, ny, b,usemin)
            col += 1
        col+=((nx*ny)-nx)                  # for (i=0; i<nz; ++i, col+=((nx*ny)-nx))


  ###################################################
  # finally, if depth of SE > 1 then perform max/min on each slice

  if d > 1 :
    # set slice element offsets
    Tp[0] = 0                             # for (p[0]=0, i=1; i<nz; ++i)
    for i in range(1,nz):                 # for (p[0]=0, i=1; i<nz; ++i)
      Tp[i] = (nx*ny) + Tp[i-1]

    # gen max/min for each slice
    for i in range(nx*ny):               # for (i=0; i<(nx*ny); ++i, ++slice)
        genfminmax(In, slice, Tg, Th, Tp, nz, d,usemin)
        slice+= 1                        # for (i=0; i<(nx*ny); ++i, ++slice)


  # del Tg                                 #free(g);
  # del Th                                 #free(h);
  # del Tp                                 #free(p);


