import numpy as np
from numba import jit

# i : index du point dans l'image */
# k : direction du voisin */
# rs : taille d'une rangee */
# ps : taille d'un plan */
# N : taille de l'image 3D */
# retourne -1 si le voisin n'existe pas */

@jit
def voisin18(i,rs,ps,N,T): #(index_t i, index_t rs, index_t ps, index_t N, index_t * T)
    T.fill(Maxuint64)

    # les 5 premiers (0 a 4) sont les 5 pixels du plan "ARRIERE" (+ps) */
    if ((i < N - ps) and (i%rs != rs - 1))    :T[0] = ps + i + 1
    if ((i < N - ps) and (i%ps >= rs))        :T[1] = ps + i - rs
    if ((i < N - ps) and (i%rs != 0))         :T[2] = ps + i - 1
    if ((i < N - ps) and (i%ps < ps - rs))    :T[3] = ps + i + rs
    if ((i < N - ps))                         :T[4] = ps + i

    # les 8 suivants (5 a 12) sont les 8 pixels du plan "COURANT" () */
    if ((i%rs != rs - 1))                         :T[5]  = i + 1
    if ((i%rs != rs - 1) and (i%ps >= rs))        :T[6]  = i + 1 - rs
    if ((i%ps >= rs))                             :T[7]  = i - rs
    if ((i%ps >= rs) and (i%rs != 0))             :T[8]  = i - rs - 1
    if ((i%rs != 0))                              :T[9]  = i - 1
    if ((i%rs != 0) and (i%ps < ps - rs))         :T[10] = i - 1 + rs
    if ((i%ps < ps - rs))                         :T[11] = i + rs
    if ((i%ps < ps - rs) and (i%rs != rs - 1))    :T[12] = i + rs + 1

    # les 5 derniers (13 a 17) sont les 5 pixels du plan "AVANT" (-ps) */
    if ((i >= ps) and (i%rs != rs - 1))            :T[13] =  i - ps + 1
    if ((i >= ps) and (i%ps >= rs))                :T[14] =  i - ps - rs
    if ((i >= ps) and (i%rs != 0))                 :T[15] =  i - ps - 1
    if ((i >= ps) and (i%ps < ps - rs))            :T[16] =  i - ps + rs
    if ((i >= ps))                                 :T[17] =  i - ps


######################################

NPRIO = 2 ** 16   # nombre max de valeurs dans l'image
NULLL = 0
Maxuint64 = np.iinfo(np.uint64).max
NpInd64 = np.uint64


# LL est à packager les différentes infos dans une sorte de struct
# constantes :
Max              = 1
Niv              = 2
NbPtCourant      = 3
NbPtMaxFromStart = 4
Queue            = 5
QueueUrg         = 6
TeteUrg          = 7
Libre            = 8



@jit
def  SetInFah(x,Ind):    #(index_t x)
    Ind[x] |= 1

@jit
def  SetOutFah(x,Ind):   #(index_t x)
    Ind[x] &= ~1

@jit
def  IsInFah(x,Ind):     #(index_t x)
    return Ind[x] & 1



@jit
def CreeFahVide(taillemax):                      # (index_t taillemax)

    LL     = np.zeros(10,NpInd64)                # index_t LL[10]
    Tete   = np.zeros(NPRIO,NpInd64)             # int Tete[NPRIO]

    Point  = np.zeros(taillemax,dtype=NpInd64)   #  (index_t *) calloc(1, taillemax * sizeof(index_t))
    Next   = np.zeros(taillemax,dtype=NpInd64)   #  (index_t *) calloc(1, taillemax * sizeof(index_t))
    Indics = np.zeros(taillemax,dtype=np.uint8)  #  (uint8 *)   calloc(taillemax, sizeof(uint8))

    #######################################################

    LL[Niv] = 0                  # L->Niv = 0
    LL[NbPtCourant] = 0          # L->NbPtCourant = 0

    # tous chainés depuis Libre
    for i in range(Next.shape[0]-1):   # (index_t i = 0  i < nnn - 1  i++)
        Next[i] = i + 1                # L->Elts[i].Next = &(L->Elts[i + 1])
    Next[Next.shape[0]-1] = NULLL      # L->Elts[nnn - 1].Next = NULL
    Next[0] = Maxuint64                # utilisée par NULL

    # premièce case dispo = 1
    LL[Libre] = 1                # L->Libre = &(L->Elts[0])

    for i in range(NPRIO):       # (index_t i = 0  i < NPRIO  i++)
        Tete[i] = NULLL          # L->Tete[i] = NULL

    LL[Queue]    = NULLL         # L->Queue = NULL
    LL[QueueUrg] = NULLL         # L->QueueUrg = NULL
    LL[TeteUrg]  = NULLL         # L->TeteUrg = NULL

    LL[Max] = taillemax          # L->Max = taillemax
    LL[NbPtMaxFromStart] = 0     # L->NbPtMaxFromStart = 0


    return LL,Tete,Point,Next,Indics



@jit
def FahVide(LL):

    #return ((L->QueueUrg == NULL) and (L->Queue == NULL))
    return ((LL[QueueUrg] == NULLL) and (LL[Queue] == NULLL))


@jit
def FahPop(LL, Tete, Point, Next):

    # index_t V
    # FahElt * FE
    # index_t FFE
    # if ( FahVide() )    fprintf(stderr, "erreur Fah vide\n")

    LL[NbPtCourant]-=1                      # L->NbPtCourant--

    if ( LL[QueueUrg] != NULLL ):           # if (L->QueueUrg != NULL)

        V = Point[LL[QueueUrg]]             # V = L->QueueUrg->Point
        FFE = Next[LL[QueueUrg]]            # FE = L->QueueUrg->Next
        Next[LL[QueueUrg]] = LL[Libre]      # L->QueueUrg->Next = L->Libre
        LL[Libre] = LL[QueueUrg]            # L->Libre = L->QueueUrg
        LL[QueueUrg] = FFE                  # L->QueueUrg = FE
        if (LL[QueueUrg] == NULLL):         # if (L->QueueUrg == NULL)
            LL[TeteUrg] = NULLL             # L->TeteUrg = NULL
        return V


    #  on se prepare a effacer le dernier  element du niveau courant: il faut
    #  annuler le pointeur de tete et incrementer le niveau

    if (LL[Queue] == Tete[LL[Niv]]):   #     if (L->Queue == L->Tete[L->Niv])

        Tete[LL[Niv]] = NULLL           # L->Tete[L->Niv] = NULL
        while (True):

            LL[Niv] += 1                # do (L->Niv)++
            # ((L->Niv < NPRIO) and (L->Tete[L->Niv] == NULL))
            if  ( not ((LL[Niv] < NPRIO) and (Tete[LL[Niv]] == NULLL)) ):
                break


    V   = Point[LL[Queue]]             # V = L->Queue->Point
    FFE = Next[LL[Queue]]              # FE = L->Queue->Next
    Next[LL[Queue]] = LL[Libre]        # L->Queue->Next = L->Libre
    LL[Libre] = LL[Queue]              # L->Libre = L->Queue
    LL[Queue] = FFE                    # L->Queue = FE
    return V


@jit
def FahPush(LL, Tete, Point, Next,Po,Ni):        # (index_t Po, uint8 Ni)

    #if ( LL[Libre] == NULLL ):    #  if (L->Libre == NULL)
    #    print(stderr, "erreur Fah pleine\n")

    #if ( Ni >= NPRIO )            #   if (Ni >= NPRIO)
    #    fprintf(stderr, "erreur niveau = %d  max autorise = %d\n", Ni, NPRIO - 1)

    LL[NbPtCourant] +=1            #     L->NbPtCourant++

    if (LL[NbPtCourant] > LL[NbPtMaxFromStart]):        # if (L->NbPtCourant > L->NbPtMaxFromStart)
        LL[NbPtMaxFromStart] = LL[NbPtCourant]          # L->NbPtMaxFromStart = L->NbPtCourant

    if (LL[Queue] == NULLL):              #   (L->Queue == NULL)    # insertion dans une Fah vide */

        LL[Queue] = LL[Libre]             # L->Queue = L->Libre
        LL[Libre] = Next[LL[Libre]]       # L->Libre = L->Libre->Next
        Point[LL[Queue]] = Po             # L->Queue->Point = Po
        LL[Niv] = Ni                      # L->Niv = Ni
        Next[LL[Queue]] = NULLL           # L->Queue->Next = NULL
        Tete[Ni] = LL[Queue]              # L->Tete[Ni] = L->Queue

    else:   # si niveau depasse alors liste d'urgence */

        if (Ni < LL[Niv]):                    # if (Ni < L->Niv)

            # insertion dans une liste d'urgence vide */
            if (LL[QueueUrg] == NULLL):         # if (L->QueueUrg == NULL)

                LL[QueueUrg] = LL[Libre]        # L->QueueUrg = L->Libre
                LL[Libre] = Next[LL[Libre]]     # L->Libre = L->Libre->Next
                Point[LL[QueueUrg]] = Po        # L->QueueUrg->Point = Po
                Next[LL[QueueUrg]] = NULLL      # L->QueueUrg->Next = NULL
                LL[TeteUrg] = LL[QueueUrg]      # L->TeteUrg = L->QueueUrg

            else:

                FFE = Next[LL[TeteUrg]]             # FahElt * FE = L->TeteUrg->Next
                Next[LL[TeteUrg]] = LL[Libre]       # L->TeteUrg->Next = L->Libre
                LL[Libre] = Next[LL[Libre]]         # L->Libre = L->Libre->Next
                LL[TeteUrg] = Next[LL[TeteUrg]]     # L->TeteUrg = L->TeteUrg->Next
                Next[LL[TeteUrg]] = FFE             # L->TeteUrg->Next = FE
                Point[LL[TeteUrg]] = Po             # L->TeteUrg->Point = Po


        # fin traitement des cas d'urgence */
        # insertion dans la liste de niveau Ni */
        else:

            if (Tete[Ni] != NULLL):                  # ( L->Tete[Ni] != NULL )
                FFE = Next[Tete[Ni]]                 # FahElt * FE = L->Tete[Ni]->Next
                Next[Tete[Ni]] = LL[Libre]           # L->Tete[Ni]->Next = L->Libre
                LL[Libre] = Next[LL[Libre]]          # L->Libre = L->Libre->Next
                Tete[Ni] = Next[Tete[Ni]]            # L->Tete[Ni] = L->Tete[Ni]->Next
                Next[Tete[Ni]] = FFE                 # L->Tete[Ni]->Next = FE
                Point[Tete[Ni]] = Po                 # L->Tete[Ni]->Point = Po

            else: # (L->Tete[Ni] == NULL) */

                #index_t FFE                             # FahElt * FE
                NiPrec = Ni
                #while ((NiPrec >= 0) and (L->Tete[NiPrec] == NULL)) NiPrec--
                while ((NiPrec >= 0) and (Tete[NiPrec] == NULLL)):
                    NiPrec -= 1

                #if (NiPrec < 0)   fprintf(stderr, "erreur Fah pas de niveau precedent\n")

                Tete[Ni] = LL[Libre]                 # L->Tete[Ni] = L->Libre
                LL[Libre] = Next[LL[Libre]]          # L->Libre = L->Libre->Next
                Point[Tete[Ni]] = Po                 # L->Tete[Ni]->Point = Po
                FFE = Next[Tete[NiPrec]]             # FE = L->Tete[NiPrec]->Next
                Next[Tete[NiPrec]] = Tete[Ni]        # L->Tete[NiPrec]->Next = L->Tete[Ni]
                Next[Tete[Ni]] = FFE                 # L->Tete[Ni]->Next = FE



######################################

@jit
def NotIn(e,list,n):         # NotIn(int32_t e, int32_t *list, int32_t n):

    # renvoie 1 si e n'est pas dans list, 0 sinon */
    # e : l'element a rechercher */
    # list : la liste (tableau d'entiers) */
    # n : le nombre d'elements dans la liste */
    while  n > 0 :
        n -= 1
        if  list[n] == e :
             return 0
    return 1



# marqueurs: image initiale de labels
# le résultat du traitement se trouve dans marqueurs (image de labels)
# et dans image (binaire)
# LPE avec ligne de séparation

#void llpemeyer_llpemeyer3d2(struct xvimage *image, struct xvimage *marqueurs)
@jit
def watershed(Img,Mrk):

    #register index_t x, y, k

    # index_t rs = image->row_size        # taille ligne */
    # index_t cs = image->col_size        # taille colonne */
    # index_t d  = image->depth_size      # nb plans */
    d, cs, rs = Img.shape
    n = rs * cs  # index_t n = rs * cs                 # taille plan */
    N = n * d    # index_t N = n * d                     # taille image */

    F = Img.reshape(N)
    M = Mrk.reshape(N)


    LL, Tete, Point, Next, Indics = CreeFahVide(N + 10)

    # cree le label pour les points de la LPE */
    LabLPE = 0
    for x in range(N):                        # for (x = 0  x < N  x++)
        if M[x] > LabLPE :
            LabLPE = M[x]
    LabLPE += 1


    # ================================================ */
    # INITIALISATION DE LA FAH                         */
    # ================================================ */
    KKK = np.empty(18,dtype=NpInd64)              # index_t KKK[18]


    FahPush(LL, Tete, Point, Next, -1, 0)        # force la creation du niveau 0 dans la Fah. */
                                                 # NECESSAIRE pour eviter la creation prematuree */
                                                 # de la file d'urgence */

    for x in range(N):     # for (x = 0  x < N  x++)

        if M[x] :          # on va empiler les voisins des regions marquees */

            voisin18(x, rs, n, N, KKK)

            for k in range(18):               # for (k = 0  k < 18  k += 1) # parcourt les 18 voisins */
                y = KKK[k]
                if (y != Maxuint64) and not M[y] and not IsInFah(y,Indics) :
                    FahPush(LL, Tete, Point, Next, y, F[y])
                    SetInFah(y,Indics)

    x = FahPop(LL, Tete, Point, Next)

    #assert(x == -1)  # autrement: ordre fifo non respecte par la fah


    # ================================================ */
    # INONDATION                                       */
    # ================================================ */

    ListLabelVoisins = np.empty(18,dtype=np.int32)    # int32_t ListLabelVoisins[6]


    while  not FahVide(LL) :

        x = FahPop(LL, Tete, Point, Next)
        SetOutFah(x,Indics)

        nbLabelVoisins = 0

        # recherche les labels autours du point  x

        voisin18(x, rs, n, N, KKK)

        for k in range(18):    # for (k = 0  k < 18  k += 1)
            y = KKK[k]
            if (y != Maxuint64)  and  (M[y] != 0)  and  (M[y] != LabLPE)  and  NotIn(M[y], ListLabelVoisins, nbLabelVoisins)  :
                ListLabelVoisins[nbLabelVoisins] = M[y]
                nbLabelVoisins += 1



        # si 1 seul voisin on continue le flooding
        if   nbLabelVoisins == 1 :
            M[x] = ListLabelVoisins[0]    # hérite de l'unique label voisin

            for k in range(18):           # for ( k = 0  k < 18  k += 1 )
                y = KKK[k]
                if  (y != Maxuint64) and (M[y] == 0) and ( not IsInFah(y,Indics)) :
                    FahPush(LL, Tete, Point, Next,y, F[y])
                    SetInFah(y,Indics)


        # si plusieurs labels voisins differents => on le marque comme bordure de LPE
        if  nbLabelVoisins > 1  :
            M[x] = LabLPE


    # FIN PROPAGATION */

    for x in range(N):     # for (x = 0  x < N  x++)

        if ( (M[x] == LabLPE) or (M[x] == 0) ):
            F[x] = 255
        else:
            F[x] = 0
