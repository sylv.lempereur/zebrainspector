import numpy as np
from numba import jit

# gestion de la lifo

@jit
def CreeLifoVide(taille):
    Lifo = np.empty(taille + 1, dtype=np.int64)
    Lifo[0] = 1
    return Lifo

@jit
def LifoVide(Lifo):
    return Lifo[0] == 1

@jit
def LifoPop(Lifo):
    Lifo[0] -= 1
    v = Lifo[Lifo[0]]
    return v

@jit
def LifoPush(
        Lifo,
        v
        ):
    Lifo[Lifo[0]] = v
    Lifo[0] += 1

# i  : index du point dans l'image
# k  : direction du voisin
# rs : taille d'une rangee
# ps : taille d'un plan
# N  : taille de l'image 3D
# retourne -1 si le voisin n'existe pas
@jit
def voisin26(
        i,
        rs,
        ps,
        N
        ):
    L = np.empty(26, dtype=np.int64)
    L.fill(-1)
    # switch(k)1

    # les 9 premiers 0 a 8) sont les 9 pixels du plan "ARRIERE" +ps)
    if i < N - ps and i % rs != rs - 1:
        L[0] = ps + i + 1
    if i < N - ps and i % rs != rs - 1 and i % ps >= rs:
        L[1] = ps + i + 1 - rs
    if i < N - ps and i % ps >= rs:
        L[2] = ps + i - rs
    if i < N - ps and i % ps >= rs and i % rs != 0:
        L[3] = ps + i - rs - 1
    if i < N - ps and i % rs != 0:
        L[4] = ps + i - 1
    if i < N - ps and i % rs != 0 and i % ps < ps - rs:
        L[5] = ps + i - 1 + rs
    if i < N - ps and i % ps < ps - rs:
        L[6] = ps + i + rs
    if i < N - ps and i % ps < ps - rs and i % rs != rs - 1:
        L[7] = ps + i + rs + 1
    if i < N - ps:
        L[8] = ps + i

    # les 8 suivants 9 a 16 sont les 8 pixels du plan "COURANT"
    if i % rs != rs - 1:
        L[9] = i + 1
    if i % rs != rs - 1 and i % ps >= rs:
        L[10] = i + 1 - rs
    if i % ps >= rs:
        L[11] = i - rs
    if i % ps >= rs and i % rs != 0:
        L[12] = i-rs-1
    if i % rs != 0:
        L[13] = i - 1
    if i % rs != 0 and i % ps < ps - rs:
        L[14] = i - 1 + rs
    if i % ps < ps - rs:
        L[15] = i + rs
    if i % ps < ps - rs and i % rs != rs - 1:
        L[16] = i + rs+1

    # les 9 derniers 17 a 25 sont les 9 pixels du plan "AVANT" -ps
    if i >= ps and i % rs != rs-1:
        L[17] = - ps + i + 1
    if i >= ps and i % rs != rs-1 and i % ps >= rs:
        L[18] = - ps + i + 1 - rs
    if i >= ps and i % ps >= rs:
        L[19] = - ps + i - rs
    if i >= ps and i % ps >= rs and i % rs != 0:
        L[20] = - ps + i - rs - 1
    if i >= ps and i % rs!=0:
        L[21] = - ps + i - 1
    if i >= ps and i % rs!=0 and i % ps < ps - rs:
        L[22] = - ps + i - 1 + rs
    if i >= ps and i % ps < ps - rs:
        L[23] = - ps + i + rs
    if i >= ps and i % ps < ps - rs and i % rs != rs-1:
        L[24] = - ps + i + rs+1
    if i >= ps:
        L[25] = - ps +i

    return L


##############################################################################
#
#    Computes the external distance (distance to nearest object point)

# uint8_t *F                /* pointeur sur l'image */

@jit
def dist(img):
    ds, cs, rs = img.shape
    ps = rs * cs
    N = ps * ds

    Dist = np.empty(img.shape, dtype=np.int32)

    F = img.reshape(N)
    D = Dist.reshape(N)

    LIFO1 = CreeLifoVide(N)
    LIFO2 = CreeLifoVide(N)

    for i in range(N):
        if F[i]:
            D[i] = 0
        else:
            D[i] = -1
            L = voisin26(i, rs, ps, N)
            for k in range(26):
                j = L[k]
                if j != -1 and F[j]:
                    D[i] = 1
                    LifoPush(LIFO1, i)
                    break

    while not LifoVide(LIFO1):
        while not LifoVide(LIFO1):

            i = LifoPop(LIFO1)
            d = D[i]
            L = voisin26(i, rs, ps, N)
            for k in range(26):

                j = L[k]
                if j != -1 and D[j] == - 1:
                    D[j] = d + 1
                    LifoPush(LIFO2, j)


        LIFOtmp = LIFO2
        LIFO2 = LIFO1
        LIFO1 = LIFOtmp

    return Dist
