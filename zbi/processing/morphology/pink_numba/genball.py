import numpy as np
import math
from numba import jit

@jit(nopython=True)
def genball(radius) :
    size = 2 * radius + 1
    c = radius
    center = (radius,radius,radius)

    B = np.zeros((size,size,size),dtype=np.uint8)

    for x in range(size):
        for y in range(size):
            for z in range(size):
                dx = c - x
                dy = c - y
                dz = c - z
                dd = math.sqrt(dx**2+dy**2+dz**2)
                if dd <= radius :
                    B[x,y,z] = 255
    return B
