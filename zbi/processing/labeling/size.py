# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains functions to compute size of elements in an image.
"""

from gc import collect as garbage_collection

from numpy import (
    digitize,
    ndarray,
    unique
    )

from .label import label

from ...handling import DataHandling

def _size_array(
        array: ndarray
        ):
    """
    Compute size of elemnts in the given array ie
    each object in the array will be label with its number of voxels.
    array have to be a boolean array.

    Parameters
    ----------
    array: numpy.ndarray
        A boolean array

    Returns
    -------
    numpy.ndarray
        Size of objects
    """
    if array.dtype != 'bool':
        raise TypeError("Given piece of data is not boolean.")
    # Label each object in the image.
    array_label, _ = label(array)
    # Get labels and the number of voxels for each label.
    unique_values, counts = unique(
        array_label,
        return_counts=True
        )
    # Get the type of the returned array
    if counts.max() < 2**8:
        dtype = 'uint8'
    elif counts.max() < 2**16:
        dtype = 'uint16'
    else:
        dtype = 'uint32'
    unique_values = unique_values.tolist()
    if 0 in unique_values:
        counts[unique_values.index(0)] = 0
    # Find indexes for each label
    array = counts[
        digitize(
            array_label.ravel(),
            unique_values,
            right=True
            )].reshape(array.shape).astype(dtype)
    del array_label, unique_values, counts, dtype
    return array

def _size_data_handling(
        data_handling_instance: DataHandling
        ):
    """
    Label each object of the current step of a DataHandling instance
    with its size ie its number of voxels.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class
        Have to be boolean
    """
    data_handling_instance.add_step("size")
    array = _size_array(data_handling_instance.get_current())
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def size(data):
    """
    Compute the size of each element in an piece of data
    ie set voxels grey value with the

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _size_array(data)
    elif isinstance(data,
                    DataHandling
                    ):
        out = _size_data_handling(data)
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
