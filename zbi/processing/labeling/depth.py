# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file Contains function to get the depth into a object over a given axis.
"""
from gc import collect as garbage_collection

from numpy import (
    cumsum,
    ndarray
    )

from ...handling import DataHandling

def _depth_array(
        array: ndarray,
        axis: int = 0
        ):
    """
    Compute the deepness of each voxels
    into the sample over the given axis.
    array have to be a boolean array.

    Parameters
    ----------
    array: numpy.ndarray
        A binary array

    axis: int
        Axis consider as the depth.
        Using numpy nomenclatur, 0,1 and 2 are z, x and y axis respectivelly.
        Default value is 0.

    Returns
    -------
    numpy.ndarray
        Depth map
    """
    if array.dtype != 'bool':
        raise TypeError("Provided piece of data have to be boolean.")
    temp = cumsum(
        array,
        axis
        )
    if temp.max() < 2 ** 8:
        dtype = 'uint8'
    elif temp.max() < 2 ** 16:
        dtype = 'uint16'
    else:
        dtype = 'uint32'
    array = array.astype(dtype)
    del dtype
    array *= temp.astype(array.dtype)
    del temp
    return array

def _depth_data_handling(
        data_handling_instance: DataHandling,
        axis: int = 0
        ):
    """
    Compute the deepness map of the current step of a DataHandling isntance.
    current step has to be boolean.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    axis: int
        Axis consider as the depth.
        Using numpy nomenclatur, 0,1 and 2 are z, x and y axis respectivelly.
        Default value is 0.

    """
    data_handling_instance.add_step("deepnessMapOverAxis"+str(axis))
    array = _depth_array(
        data_handling_instance.get_current(),
        axis
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def depth(
        data,
        axis: int = 0):
    """
    Compute the deepness map of the given piece of data over the given axis.
    data have to be boolean.

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance

    axis: int
        Axis consider as the depth.
        Using numpy nomenclatur, 0,1 and 2 are z, x and y axis respectivelly.
        Default value is 0.
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _depth_array(
            data,
            axis
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _depth_data_handling(
            data,
            axis
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
