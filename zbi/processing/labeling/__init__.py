"""
Labelling tools
===========================

================================== =============================================
labeling
================================================================================
depth                              Compute deepness map
label                              Labels each object
size                               Found size of elements in an image
================================== =============================================
"""

from .depth import depth
from .label import label
from .size import size
