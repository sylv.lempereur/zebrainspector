"""
Core image processing tools
===========================

================================== =============================================
basics
================================================================================
add                                Sum two piece of data
back_to_grey                       Put grey value on none black pixel
bounding_box                       Reduce a piece of data to its bounding box
frame_creation                     Creation of a frame array
intersection                       Intersection of two piece of data
intersection_removal               Remove intersection between two piece of data
inversion                          Inversion of given piece of data
maximum                            Maximum of two piece of data
mean                               Mean of two piece of data
minimum                            Minimum of two piece of data
normalization                      Normalization of the given piece of data
remove values                      Remove values to a piece of data
substract                          Substract two piece of data
sum_cummulative                    Cummulative sum over a given axis
type_verification                  Check data type
union                              Compute the union of two boolean
================================== =============================================

================================== =============================================
contrast_correction
================================================================================
hist match                         Histogram matching computation
shape_aware_depth_dependant_median Median-based SADDCC
shape_aware_depth_dependant_histo  Histogram matching-based SADDCC
================================== =============================================

================================== =============================================
detection
================================================================================
bounding_box_positions             return position of the bounding box
convex_hull                        Comute convex hull of each object
maximum_local                      Found local maximum
================================== =============================================

================================== =============================================
filtering
================================================================================
gaussian                           Perform a gaussian filter
median                             Perform a median filter
================================== =============================================

================================== =============================================
labeling
================================================================================
depth_recognition                  Found depth level into the sample
label                              Labels each object
size                               Found size of elements in an image
================================== =============================================

================================== =============================================
morphology
================================================================================
closing                            Closing with given radius and shape
closing_geodesic                   Geodesic erosion after a dilation
dilation                           Dilation with given radius and shape
dilation_geodesic                  Geodesic dilation
dist                               Distance map
erosion                            Erosion with given radius and shape
erosion_geodesic                   Geodesic erosion
gradient                           Morphological gradient
gradient_half                      Morphological half gradient
opening                            Opening with given radius and shape
opening_geodesic                   Geodesic dilation after a erosion
remove_holes_black                 Geodesic dilation of a frame
surface                            Get the surface of the current step
tophat                             Top hat computation
skeletonization                    Compute skeleton of a binary image
spread_seed                        Geodesic dilation of a seed
spread_to_mask                     Geodesic dilation into a mask
watershed                          Watershed computation
================================== =============================================

================================== =============================================
plot
================================================================================
hitsogram                          Plot the histogram of a piece of data
================================== =============================================

================================== =============================================
projection
================================================================================
average_projection                 Average intensity projection
binary                             Project a binary image into a 2D array
================================== =============================================

================================== =============================================
thresholding
================================================================================
manual                             Thresholding using the given value
manual_percent                     Thresholding using the given percent
minimum_error                      Thresholding by a minimum error algorithm
otsu                               Thresholding using an Otsu algorithm
otsu_multiples_classes             Thresholding using a multi Otsu algorithm
triangle                           Thresholding using a traingle algorithm
sauvola                            Thresholding using a Sauvola algotirhm
================================== =============================================
"""

from . import basics
from . import contrast_correction
from . import detection
from . import filtering
from . import labeling
from . import morphology
# from . import plot
from . import projection
from . import thresholding
