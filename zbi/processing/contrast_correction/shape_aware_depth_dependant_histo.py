# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains fucntion to compute histogram matching-based SADDCC
"""
from gc import collect as garbage_collection

from numpy import ndarray

from ..matching import histogram_matching
from ..projection import average_projection

from ...handling import DataHandling

def _shape_aware_depth_dependant_histo_array(
        array: ndarray,
        deepness_map: ndarray
        ):
    """
    Histogram matching of each deepness into an array
    with the average projection of the array to improve.

    Parameters
    ----------
    array: numpy.ndarray
        array to improve


    deepness_map: numpy.ndarray
        map of the depth into the object

    Returns
    -------
    numpy.ndarray
        Result of the computation of histogram matching-bases SADDCC
    """
    reference = average_projection(
        array,
        axis=0
        )
    start_depth = 1
    test = False
    while test:
        if start_depth < deepness_map.max():
            maxi = array[deepness_map == start_depth].max()
            if maxi < array.max() * 0.90:
                start_depth += 1
            else:
                test = True
        else:
            test = True
            start_depth = 1

    for depth in range(start_depth, deepness_map.max()):
        array[
            (deepness_map == depth) & (array != 0)
            ] = histogram_matching(
                array[
                    (deepness_map == depth) & (array != 0)
                    ],
                reference[reference != 0]
                )
    return array

def _shape_aware_depth_dependant_histo_data_handling(
        data_handling_instance: DataHandling,
        deepness_map: ndarray = None
        ):
    """
    Compute the median-based SADDCC of the original image
    If no deepness_map is provided, the current step will replace it.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    deepness_map: numpy.ndarray
        (optionnal) deepness map into the image.
    """
    data_handling_instance.add_step("SADDCCHisto")
    if not deepness_map:
        deepness_map = data_handling_instance.get_current()
    array = _shape_aware_depth_dependant_histo_array(
        data_handling_instance.get_original(),
        deepness_map
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def shape_aware_depth_dependant_histo(
        data,
        deepness_map: ndarray = None
        ):
    """
    Compute a histogram matching-based SADDCC of the given piece of data.
    Is a DataHandling instance is provided, the original array will be improve
    In this case, if no deepness map is provided,
    it will be replace by the current step.


    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance

    deepness_map: numpy.ndarray
        Deepness map of the image.
        If data is a DataHandling isntance, this is optionnal.
        In this case, the current step will be consider as the deepness map.
    """
    if isinstance(data,
                  ndarray
                  ):
        if not deepness_map:
            raise ValueError(" No deepness_map provided.")
        out = _shape_aware_depth_dependant_histo_array(
            data,
            deepness_map
            )
    elif isinstance(data,
                    DataHandling
                    ):
        if not deepness_map:
            out = _shape_aware_depth_dependant_histo_data_handling(data)
        else:
            out = _shape_aware_depth_dependant_histo_data_handling(
                data,
                deepness_map
                )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
