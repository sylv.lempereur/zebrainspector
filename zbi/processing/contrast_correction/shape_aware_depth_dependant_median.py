# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains functions to compute the median-based SADDCC
of a piece of data.
"""
from gc import collect as garbage_collection

from numpy import (
    isnan,
    nan,
    nanmedian,
    ndarray
    )

from ...handling import DataHandling

def _shape_aware_depth_dependant_median_array(
        array: ndarray,
        deepness_map: ndarray
        ):
    """
    Computetation of the median-based SADDCC of an array

    Parameters
    ----------
    array: numpy.ndarray

    deepness_map: numpy.ndarray
        Map of the depth into the sample

    Returns
    -------
    numpy.ndarray
        Result of the median-based SADDCC
    """

    array = array.astype('float64')
    array[deepness_map == 0] = 0
    array[array == 0] = nan
    lines = []
    medians = []
    for depth in range(1, deepness_map.max() + 1):
        lines.append(depth)
        medians.append(
            nanmedian(
                array[deepness_map == depth]
            )
        )
    start_depth = medians.index(max(medians)) + 1

    #TODO : Optimize this step (see labeling.size)


    for computing_depth in range(start_depth, deepness_map.max()):
        array[deepness_map == computing_depth] *= (
            medians[start_depth] / medians[computing_depth]
            )
    array[isnan(array)] = 0
    # array[array > original.max()] = original.max()
    return array

def _shape_aware_depth_dependant_median_data_handling(
        data_handling_instance: DataHandling,
        deepness_map: ndarray = None
        ):
    """
    Compute the median-based SADDCC of the original image
    If no deepness_map is provided, the current step will replace it.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    deepness_map: numpy.ndarray
        (optionnal) deepness map into the image.
    """
    data_handling_instance.add_step("SADDCCMEdian")
    if not deepness_map:
        deepness_map = data_handling_instance.get_current()
    array = _shape_aware_depth_dependant_median_array(
        data_handling_instance.get_original(),
        deepness_map
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def shape_aware_depth_dependant_median(
        data,
        deepness_map: ndarray = None
        ):
    """
    Compute a median-based SADDCC of the given piece of data.
    Is a DataHandling instance is provided, the original array will be improve
    In this case, if no deepness map is provided,
    it will be replace by the current step.


    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance

    deepness_map: numpy.ndarray
        Deepness map of the image.
        If data is a DataHandling isntance, this is optionnal.
        In this case, the current step will be consider as the deepness map.
    """
    if isinstance(data,
                  ndarray
                  ):
        if not deepness_map:
            raise ValueError(" No deepness_map provided.")
        out = _shape_aware_depth_dependant_median_array(
            data,
            deepness_map
            )
    elif isinstance(data,
                    DataHandling
                    ):
        if not deepness_map:
            out = _shape_aware_depth_dependant_median_data_handling(data)
        else:
            out = _shape_aware_depth_dependant_median_data_handling(
                data,
                deepness_map
                )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
