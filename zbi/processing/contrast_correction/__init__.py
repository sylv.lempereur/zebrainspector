"""
Contrast correction tools
=========================

================================== =============================================
Contrast correction procedure
================================================================================
segmentation_based_depth_dependant Compute a SBDDCC
================================== =============================================
"""

from .segmentation_based_depth_dependant import (
    segmentation_based_depth_dependant
    )
