# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains function to project a binary 3D image into a 2D image.
"""
from gc import collect as garbage_collection

from numpy import (
        ndarray,
        nan,
        nanmean
        )

from ...handling import DataHandling

def _average_projection_array(
        array: ndarray,
        axis: int = 0
        ):
    """
    Average intensity projection of a 3D array on the given axis.
    Gray value of 0 will be ignored.

    Parameters
    ----------
    array: numpy.ndarray

    axis: int
        (optionnal)
        Axis used for the projection.
        On numpy base, 0, 1 and 2 are Z, X and Y axis respectivelly.
        Default is 0.

    Returns
    -------
    numpy.ndarray
        Result of the computation of the distance map
    """
    array = array.astype('float32')
    array[array == 0] = nan
    array = nanmean(
        array,
        axis
        )
    return array

def _average_projection_data_handling(
        data_handling_instance: DataHandling,
        axis: int = 0
        ):
    """
    Compute the maximum intensity projection
    of the current step over the given axis.
    Gray value of 0 will be ignored.

    Parameters
    ----------
    data_handling_instance: DataHandling

    axis: int
        (optionnal)
        Axis used for the projection.
        On numpy base, 0, 1 and 2 are Z, X and Y axis respectivelly.
        Default is 0.
    """
    data_handling_instance.add_step("aipProjection")
    array = _average_projection_array(
        data_handling_instance.get_current(),
        axis
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def average_projection(
        data,
        axis: int = 0):
    """
    Maximum intensity projection of the given piece of data on the given axis.
    data could be a numpy.ndarray or a DataHandling instance.
    Gray value of 0 will be ignored.



    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance.

    axis: int
        (optionnal)
        Axis used for the projection.
        On numpy base, 0, 1 and 2 are Z, X and Y axis respectivelly.
        Default is 0.
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _average_projection_array(
            data,
            axis
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _average_projection_data_handling(
            data,
            axis
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
