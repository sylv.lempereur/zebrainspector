# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains function to project a binary 3D image into a 2D image.
"""
from gc import collect as garbage_collection

from numpy import ndarray
from numpy import any as npany

from ...handling import DataHandling

def _binary_projection_array(
        array: ndarray,
        axis: int = 0
        ):
    """
    Projection of a 3D boolean array on the given axis.

    Parameters
    ----------
    array: numpy.ndarray
        A binary array

    axis: int
        (optionnal)
        Axis used for the projection.
        On numpy base, 0, 1 and 2 are Z, X and Y axis respectivelly.
        Default is 0.

    Returns
    -------
    numpy.ndarray
        Result of the computation of the distance map
    """
    if array.dtype != 'bool':
        raise TypeError("Given data is not a boolean array")
    array = npany(
        array,
        axis
        ).astype('bool')
    return array

def _binary_projection_data_handling(
        data_handling_instance: DataHandling,
        axis: int = 0
        ):
    """
    Project the current axis on the given axis.
    Current step have to be a boolean array.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    axis: int
        (optionnal)
        Axis used for the projection.
        On numpy base, 0, 1 and 2 are Z, X and Y axis respectivelly.
        Default is 0.
    """
    data_handling_instance.add_step("ProjectionBinary")
    array = _binary_projection_array(
        data_handling_instance.get_current(),
        axis
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def binary_projection(
        data,
        axis: int = 0):
    """
    Project the given piece of data on the given axis.
    data could be a numpy.ndarray or a DataHandling instance.


    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance.
        store dataset must be boolean

    axis: int
        (optionnal)
        Axis used for the projection.
        On numpy base, 0, 1 and 2 are Z, X and Y axis respectivelly.
        Default is 0.
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _binary_projection_array(
            data,
            axis
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _binary_projection_data_handling(
            data,
            axis
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
