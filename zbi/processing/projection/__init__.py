"""
Projection tools
================

================================== =============================================
projection
================================================================================
average_projection                 Average intensity projection
binary_projection                  Project a binary image into a 2D array
maximum_projection                 Maximum intensity projection
================================== =============================================
"""

from .average_projection import average_projection
from .binary_projection import binary_projection
from .maximum_projection import maximum_projection
