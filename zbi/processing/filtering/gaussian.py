# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains functions to compute a gaussian filter on a piece of data.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from scipy.ndimage.filters import gaussian_filter

from ...handling import DataHandling

def _gaussian_array(
        array: ndarray,
        sigma: float = 3.0
        ):
    """
    Computation of a gaussian filter on an array with the given sigma.

    Parameters
    ----------
    array: numpy.ndarray

    sigma: float
        Standard deviation of the distribution

    Returns
    -------
    numpy.ndarray
        Result of the gaussian filter
    """
    array = gaussian_filter(
        array,
        sigma)
    return array

def _gaussian_data_handling(
        data_handling_instance: DataHandling,
        sigma: float = 3.0
        ):
    """
    Extratction of the current array in a DataHandling instance
    to compute the gaussian.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    sigma: float
        Standard deviation of the distribution
    """
    data_handling_instance.add_step("gaussian_filter" + str(sigma))
    array = _gaussian_array(
        data_handling_instance.get_current(),
        sigma
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def gaussian(
        data,
        sigma: float = 3.0
        ):
    """
    Computation on the given piece of data
    of a gaussian filter usign the given standard deviation.
    data could be numpy.ndarray or DataHandling isntance.

    Parameters
    ----------
    data:
        Data use to perform the gaussian filter.
        could be a numpy.ndarray or a DataHandling instance

    sigma: float
        Standard deviation of the distribution
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _gaussian_array(
            data,
            sigma
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _gaussian_data_handling(
            data,
            sigma
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
