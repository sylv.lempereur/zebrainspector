# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains function uses to compute median filters.
"""
from gc import collect as garbage_collection

from numpy import ndarray, ones

from skimage.filters import median as skmedian

from ...handling import DataHandling

def _median_array(
        array: ndarray,
        rad: int = 5
        ):
    """
    Computation of a median filter of the given array
    with a kernel of the given radius.

    Parameters
    ----------
    array: numpy.ndarray

    rad: int
        Size of edges of the cube use as kernel

    Returns
    -------
    numpy.ndarray
        Result of the median filter
    """
    kernel = [rad] * len(array.shape)
    shape = (
        rad,
        rad,
        rad
        )
    kernel = ones(shape, 'int8')
    array = skmedian(
        array,
        kernel
        )
    del kernel
    return array

def _median_data_handling(
        data_handling_instance: DataHandling,
        rad: int = 5
        ):
    """
    Median filter of the current step of a DataHandling instance
    using a kernel of the given radius.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    rad: int
        Radius of the computation kernel
    """
    data_handling_instance.add_step("medianFilter" + str(rad))
    array = _median_array(
        data_handling_instance.get_current(),
        rad
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def median(
        data,
        rad: int = 5
        ):
    """
    Computation of a median filter on the given piece of data
    using a kernel of the given radius.
    data could be a numpy.ndarray or a DataHandling instance.

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance

    rad: int
        Radius of the computation kernel
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _median_array(
            data,
            rad
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _median_data_handling(
            data,
            rad
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
