# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This files contains type_verifications to check type of an image
"""

from gc import collect as garbage_collection

from numpy import (
    integer,
    ndarray
    )

from ...handling import DataHandling

def _type_verification_array(
        array: ndarray
        ):
    """
    Check the minimal usable type for the given array.
    Will reduce usage of ram or hard drive disk usages.

    Parameters
    ----------
    array: numpy.ndarray

    Returns
    -------
    numpy.ndarray
        Array of the minimal type
    """
    if issubclass(
            array.dtype.type,
            integer
        ):
        if array.max() < 2:
            array = array.astype("bool")
        elif array.max() < 2 ** 8:
            array = array.astype("uint8")
        elif array.max() < 2 ** 16:
            array = array.astype("uint16")
        else:
            array = array.astype('int32')
    else:
        array = array.astype('float32')
    return array

def _type_verification_data_handling(
        data_handling_instance: DataHandling
        ):
    """
    Change the current step dtype for the smallest possible dtype.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class
    """
    array = _type_verification_array(data_handling_instance.get_current())
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def type_verification(data):
    """

    Change data dtype for the smallest dtype possible.

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _type_verification_array(data)
    elif isinstance(data,
                    DataHandling
                    ):
        out = _type_verification_data_handling(data)
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
