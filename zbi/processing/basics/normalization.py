# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file Contains normalizations that computes a normalization.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from ...handling import DataHandling

def _normalization_array(
        array: ndarray,
        limit_high: int = 2**12 - 1,
        limit_low: int = 0
        ):
    """
    Linear normalization of the given array

    Parameters
    ----------
    array: numpy.ndarray
        array to normalize

    limit: int
        (optionnal)
        Highest value after normalization.
        Default is set to 2**12-1

    limit: int
        (optionnal)
        Lowest value after normalization.
        Default is setup to 0.

    Returns
    -------
    numpy.ndarray
        Normalized array
    """
    array = array.astype('float32')
    max_array = array.max()
    min_array = array.min()

    coef = (limit_high - limit_low) / (max_array - min_array)

    array = (array - min_array) * coef + limit_low

    return array

def _normalization_data_handling(
        data_handling_instance: DataHandling,
        limit_high: int = 2**12 - 1,
        limit_low: int = 0
        ):
    """
    Extratction of the current array in a DataHandling instance
    to compute a linear normalization.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class
    """
    data_handling_instance.add_step("normalization")
    array = _normalization_array(
        data_handling_instance.get_current(),
        limit_high,
        limit_low
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def normalization(
        data,
        limit_high: int = 2**12 - 1,
        limit_low: int = 0
        ):
    """
    Computation of a linear normalization.

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance

    limit: int
        (optionnal)
        Highest value after normalization.
        Default is set to 2**12-1

    limit: int
        (optionnal)
        Lowest value after normalization.
        Default is setup to 0.
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _normalization_array(
            data,
            limit_high,
            limit_low
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _normalization_data_handling(
            data,
            limit_high,
            limit_low
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
