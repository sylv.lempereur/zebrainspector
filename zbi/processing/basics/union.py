# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains functions
that computes union between two boolean piece of data.
"""

from gc import collect as garbage_collection

from numpy import ndarray

from ...handling import DataHandling

def _union_array(
        array1: ndarray,
        array2: ndarray
        ):
    """
    Union between two arrays. Both have to be booleans.

    Parameters
    ----------
    array: numpy.ndarray
        A binary array

    array2: numpy.ndarray
        A binary array

    Returns
    -------
    numpy.ndarray
        Result of the computation of the union map
    """
    if array2.dtype != 'bool':
        raise TypeError("Provided array1 is not a boolean array")
    if array2.dtype != 'bool':
        raise TypeError("Provided array2 is not a boolean array")
    array1[array2 != 0] = 1
    return array1

def _union_data_handling(
        data_handling_instance: DataHandling,
        array: ndarray
        ):
    """
    Compute the union between the current step
    of a DataHandling instance and a given array.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class
    """
    data_handling_instance.add_step("union")
    array = _union_array(
        data_handling_instance.get_current(),
        array
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def union(
        data,
        array: ndarray):
    """
    Compute the union of a given data and a given array.
    Data could a numpy.ndarray or a DataHandling instance.

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _union_array(
            data,
            array
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _union_data_handling(
            data,
            array
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
