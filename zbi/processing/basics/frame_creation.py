# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file Contains functions used to create a frame array.
"""
from gc import collect as garbage_collection

from numpy import (
    ndarray,
    zeros
    )

from ...handling import DataHandling

def _frame_creation_array(
        array: ndarray
        ):
    """
    Creation of a frame array of same shape as the given array.

    Parameters
    ----------
    array: numpy.ndarray
        Array used to know the shape

    Returns
    -------
    numpy.ndarray
        Frame array
    """
    frame = zeros(array.shape)
    frame[0, :, :] = 1
    frame[:, 0, :] = 1
    frame[:, :, 0] = 1
    frame[-1, :, :] = 1
    frame[:, -1, :] = 1
    frame[:, :, -1] = 1
    return frame.astype('bool')

def _frame_creation_data_handling(
        data_handling_instance: DataHandling
        ):
    """
    Creation of a frame of same shape as the stored image.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class
    """
    data_handling_instance.add_step("frameCreation")
    array = _frame_creation_array(data_handling_instance.get_current())
    garbage_collection()
    return array

def frame_creation(data):
    """
    Cretaion of frame array with the same shape as the given piece of data.
    Both numpy.ndarray or DataHandling isntance could be used.

    Parameters
    ----------
    data:
        Piece of data used to get the shape to return.
        could be a numpy.ndarray or a DataHandling instance

    Return
    ------
    frame: numpy.ndarray
        Frame array of same shape as the given piece of data.
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _frame_creation_array(data)
    elif isinstance(data,
                    DataHandling
                    ):
        out = _frame_creation_data_handling(data)
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
