# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains function to perform mean between two pieces of data.
"""
from gc import collect as garbage_collection

from numpy import ndarray
from numpy import mean as npmean

from ...handling import DataHandling

def _mean_array(
        array1: ndarray,
        array2: ndarray,
        ):
    """
    Compute mean value between arra1 and array 2.

    Parameters
    ----------
    array1: numpy.ndarray

    array1: numpy.ndarray

    method: str
        (Optionnal)
        Could be "maximum", "mean" or "mean".
        Default is "maximum"

    Returns
    -------
    numpy.ndarray
        Result of the computation of the distance map
    """
    return npmean(
        array1,
        array2
        )

def _mean_data_handling(
        data_handling_instance: DataHandling,
        array: ndarray
        ):
    """
    Compute the mean of the current step
    of a DataHandling instance and the given array.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    array: numpy.ndarray
        Array used to perform the mean

    """
    data_handling_instance.add_step("FusedByMinimum")
    array = _mean_array(
        data_handling_instance.get_current(),
        array
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def mean(
        data1,
        data2
        ):
    """
    Compute the mean between two piece of data.
    data1 and data2 could be numpy.ndarray or DataHandling instance.
    If a DataHandling is given,
    the current step will be used to compute the mean.
    If data1 is a DataHandling isntance, the mean will be store in it.

    Parameters
    ----------
    data1:
        Piece of data that will store the result of the mean.
        could be a numpy.ndarray or a DataHandling instance

    data2:
        Piece of data that will be fused with data1
        could be a numpy.ndarray or a DataHandling instance

    See also
    --------
    intersection
    maximum
    mean
    mean
    union
    """
    if isinstance(data2,
                  ndarray
                  ):
        array = data2
    elif isinstance(data2,
                    DataHandling
                    ):
        array = data2.get_current().copy()
    else:
        raise TypeError(
            'Provided data2 is not a numpy.ndarray nor a DataHandling instance'
            )
    if isinstance(data1,
                  ndarray
                  ):
        out = _mean_array(
            data1,
            array
            )
    elif isinstance(data1,
                    DataHandling
                    ):
        out = _mean_data_handling(
            data1,
            array
            )
    else:
        raise TypeError(
            'Provided data1 is not a numpy.ndarray nor a DataHandling instance'
            )
    del array
    garbage_collection()
    return out
