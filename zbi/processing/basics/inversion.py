# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains functions used to compute the invert version
of a given piece of data.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from ...handling import DataHandling

def _inversion_array(
        array: ndarray
        ):
    """
    Inverte an array, ie array = maximum(array) - array.

    Parameters
    ----------
    array: numpy.ndarray
        array to invert

    Returns
    -------
    numpy.ndarray
        Inverted array
    """
    if array.dtype == 'bool':
        array = array == 0
    else:
        array = array.max() - array
    return array

def _inversion_data_handling(
        data_handling_instance: DataHandling
        ):
    """
    Inversion of teh current step.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class.
    """
    data_handling_instance.add_step("inversion")
    array = _inversion_array(data_handling_instance.get_current())
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def inversion(data):
    """
    Inversion of teh given piece of data.

    Parameters
    ----------
    data:
        Data to invert.
        could be a numpy.ndarray or a DataHandling instance
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _inversion_array(data)
    elif isinstance(data,
                    DataHandling
                    ):
        out = _inversion_data_handling(data)
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
