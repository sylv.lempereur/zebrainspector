# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains function to perform fusion between two pieces of data.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from .maximum import maximum
from .mean import mean
from .minimum import minimum

from ...handling import DataHandling

def _fusion_array(
        array1: ndarray,
        array2: ndarray,
        method: "maximum",
        ):
    """
    Fused array1 with array2 using the given method.

    Parameters
    ----------
    array1: numpy.ndarray

    array1: numpy.ndarray

    method: str
        (Optionnal)
        Could be "maximum", "minimum" or "mean".
        Default is "maximum"

    Returns
    -------
    numpy.ndarray
        Result of the computation of the distance map
    """
    if method == "maximum":
        array = maximum(
            array1,
            array2
            )
    elif method == "minimum":
        array = minimum(
            array1,
            array2
            )
    elif method == "mean":
        array = mean(
            array1,
            array2
            )
    else:
        raise ValueError('Given method is invalid.')
    return array

def _fusion_data_handling(
        data_handling_instance: DataHandling,
        array: ndarray,
        method: str = "maximum"
        ):
    """
    Extratction of the current array in a DataHandling instance
    to compute the fusion.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    array: numpy.ndarray
        Array used to perform the fusion

    """
    data_handling_instance.add_step("FusionBy" + method)
    array = _fusion_array(
        data_handling_instance.get_current(),
        array,
        method
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def fusion(
        data1,
        data2,
        method: int = "maximum"
        ):
    """
    Compute the fusion between two piece of data.
    data1 and data2 could be numpy.ndarray or DataHandling instance.
    If a DataHandling is given,
    the current step will be used to compute the fusion.
    If data1 is a DataHandling isntance, the fusion will be store in it.

    Parameters
    ----------
    data1:
        Piece of data that will store the result of the fusion.
        could be a numpy.ndarray or a DataHandling instance

    data2:
        Piece of data that will be fused with data1
        could be a numpy.ndarray or a DataHandling instance

    See also
    --------
    intersection
    maximum
    mean
    minimum
    union
    """
    if isinstance(data2,
                  ndarray
                  ):
        array = data2
    elif isinstance(data2,
                    DataHandling
                    ):
        array = data2.get_current().copy()
    else:
        raise TypeError(
            'Provided data2 is not a numpy.ndarray nor a DataHandling instance'
            )
    if isinstance(data1,
                  ndarray
                  ):
        out = _fusion_array(
            data1,
            array,
            method
            )
    elif isinstance(data1,
                    DataHandling
                    ):
        out = _fusion_data_handling(
            data1,
            array,
            method
            )
    else:
        raise TypeError(
            'Provided data1 is not a numpy.ndarray nor a DataHandling instance'
            )
    del array
    garbage_collection()
    return out
