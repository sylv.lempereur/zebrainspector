# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains function to perform maximum between two pieces of data.
"""
from gc import collect as garbage_collection

from numpy import ndarray
from numpy import maximum as npmaximum

from .union import union

from ...handling import DataHandling

def _maximum_array(
        array1: ndarray,
        array2: ndarray,
        ):
    """
    Fused array1 with array2 using the given method.

    Parameters
    ----------
    array1: numpy.ndarray

    array1: numpy.ndarray

    method: str
        (Optionnal)
        Could be "maximum", "maximum" or "mean".
        Default is "maximum"

    Returns
    -------
    numpy.ndarray
        Result of the computation of the distance map
    """

    if array1.dtype == 'bool' and array2.dtype == 'bool':
        array = union(
            array1,
            array2
            )
    else:
        array = npmaximum(
            array1,
            array2
            )
    return array

def _maximum_data_handling(
        data_handling_instance: DataHandling,
        array: ndarray
        ):
    """
    Extratction of the current array in a DataHandling instance
    to compute the maximum.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    array: numpy.ndarray
        Array used to perform the maximum

    method: str
        (Optionnal)
        Could be "maximum", "maximum" or "mean".
        Default is "maximum"

    """
    data_handling_instance.add_step("FusedByMinimum")
    array = _maximum_array(
        data_handling_instance.get_current(),
        array
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def maximum(
        data1,
        data2
        ):
    """
    Compute the maximum between two piece of data.
    data1 and data2 could be numpy.ndarray or DataHandling instance.
    If a DataHandling is given,
    the current step will be used to compute the maximum.
    If data1 is a DataHandling isntance, the maximum will be store in it.

    Parameters
    ----------
    data1:
        Piece of data that will store the result of the maximum.
        could be a numpy.ndarray or a DataHandling instance

    data2:
        Piece of data that will be fused with data1
        could be a numpy.ndarray or a DataHandling instance

    method: str
        (Optionnal)
        Could be "maximum", "maximum" or "mean".
        Default is "maximum"

    See also
    --------
    intersection
    maximum
    mean
    maximum
    union
    """
    if isinstance(data2,
                  ndarray
                  ):
        array = data2
    elif isinstance(data2,
                    DataHandling
                    ):
        array = data2.get_current().copy()
    else:
        raise TypeError(
            'Provided data2 is not a numpy.ndarray nor a DataHandling instance'
            )
    if isinstance(data1,
                  ndarray
                  ):
        out = _maximum_array(
            data1,
            array
            )
    elif isinstance(data1,
                    DataHandling
                    ):
        out = _maximum_data_handling(
            data1,
            array
            )
    else:
        raise TypeError(
            'Provided data1 is not a numpy.ndarray nor a DataHandling instance'
            )
    del array
    garbage_collection()
    return out
