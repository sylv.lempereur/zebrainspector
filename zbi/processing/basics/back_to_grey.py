# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This files contains function used to restore the grey image
of a DataHandling instance.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from ...handling import DataHandling



def back_to_grey(
        data_handling_instance: DataHandling,
        array: ndarray = None
        ):
    """
    Set the current step of a DataHandling instance to a grey value, i.e.,
    each non black pixel of the current step will be setup with a grey value.
    If no additional array is given,
    the original image contained in the DataHandling instance will used.

    Parameters
    ----------
    data_handling_instance: DataHandling
        The current step will be changed
        to put grey value on its non black pixels.

    array: numpy.ndarray
        (optionnal) array used to setup grey values.
        If no array is given,
        the original image stored in the current step will be used.
    """
    if not array:
        array = data_handling_instance.get_original()
    array[data_handling_instance.get_current() == 0] = 0
    data_handling_instance.set_current(array.copy())
    garbage_collection()
    return True
