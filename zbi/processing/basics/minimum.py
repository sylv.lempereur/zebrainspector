# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains function to perform minimum between two pieces of data.
"""
from gc import collect as garbage_collection

from numpy import ndarray
from numpy import minimum as npminimum

from . import intersection

from ...handling import DataHandling

def _minimum_array(
        array1: ndarray,
        array2: ndarray,
        ):
    """
    Compute minimum value between arra1 and array 2.

    Parameters
    ----------
    array1: numpy.ndarray

    array1: numpy.ndarray

    method: str
        (Optionnal)
        Could be "maximum", "minimum" or "minimum".
        Default is "maximum"

    Returns
    -------
    numpy.ndarray
        Result of the computation of the distance map
    """
    if array1.dtype == 'bool' and array2.dtype == 'bool':
        array = intersection(
            array1,
            array2
            )
    else:
        array = npminimum(
            array1,
            array2
            )
    return array

def _minimum_data_handling(
        data_handling_instance: DataHandling,
        array: ndarray
        ):
    """
    Compute the minimum of the current step
    of a DataHandling instance and the given array.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    array: numpy.ndarray
        Array used to perform the minimum

    """
    data_handling_instance.add_step("FusedByMinimum")
    array = _minimum_array(
        data_handling_instance.get_current(),
        array
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def minimum(
        data1,
        data2
        ):
    """
    Compute the minimum between two piece of data.
    data1 and data2 could be numpy.ndarray or DataHandling instance.
    If a DataHandling is given,
    the current step will be used to compute the minimum.
    If data1 is a DataHandling isntance, the minimum will be store in it.

    Parameters
    ----------
    data1:
        Piece of data that will store the result of the minimum.
        could be a numpy.ndarray or a DataHandling instance

    data2:
        Piece of data that will be fused with data1
        could be a numpy.ndarray or a DataHandling instance

    See also
    --------
    intersection
    maximum
    minimum
    minimum
    union
    """
    if isinstance(data2,
                  ndarray
                  ):
        array = data2
    elif isinstance(data2,
                    DataHandling
                    ):
        array = data2.get_current().copy()
    else:
        raise TypeError(
            'Provided data2 is not a numpy.ndarray nor a DataHandling instance'
            )
    if isinstance(data1,
                  ndarray
                  ):
        out = _minimum_array(
            data1,
            array
            )
    elif isinstance(data1,
                    DataHandling
                    ):
        out = _minimum_data_handling(
            data1,
            array
            )
    else:
        raise TypeError(
            'Provided data1 is not a numpy.ndarray nor a DataHandling instance'
            )
    del array
    garbage_collection()
    return out
