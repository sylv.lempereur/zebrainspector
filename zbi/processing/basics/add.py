# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains function to add grey values of two piece of data.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from ...handling import DataHandling


def _add_array(
        array1: ndarray,
        array2: ndarray
        ):
    """
    Compute the sum of two array.

    Parameters
    ----------
    array: numpy.ndarray
    array2 : numpy.ndarray

    Returns
    -------
    numpy.ndarray
        Result of the sum
    """
    if array1.max() + array2.max() > 2**16:
        array = array1.astype('uint32') + array2.astype('uint32')
    if array1.max() + array2.max() > 2**8:
        array = array1.astype('uint16') + array2.astype('uint16')
    else:
        array = array1.astype('uint8') + array2.astype('uint8')
    return array

def _add_data_handling(
        data_handling_instance: DataHandling,
        array: ndarray
        ):
    """
    Computation of the sum between the current step and a given array.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    array: numpy.ndarray
        Array to add
    """
    data_handling_instance.add_step("sum")
    array = _add_array(
        data_handling_instance.get_current(),
        array
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def add(
        data,
        data2
        ):
    """
    Computation of a sum between two piece of data.
    Both piece of data could be a numpy.ndarray or a DataHandling instance.

    Parameters
    ----------
    data:
        Piece of Data change by the sum.
    data2:
        Piece of data that will be added.
        if it is a DataHandling instance, the current step will be add.
    """
    if isinstance(data2,
                  ndarray):
        array_add = data2.copy()
    elif isinstance(data2,
                    DataHandling):
        array_add = data2.get_current.copy()
    else:
        raise TypeError(
            'Provided data2 is not a numpy.ndarray nor a DataHandling instance'
            )
    if isinstance(data,
                  ndarray
                  ):
        out = _add_array(
            data,
            array_add
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _add_data_handling(
            data,
            array_add
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
