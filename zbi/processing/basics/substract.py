# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains any function used to substract.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from ...handling import DataHandling

def _substract_array(
        array1: ndarray,
        array2: ndarray,
        method: str = "1-2"
        ):
    """
    return the chosen substraction.
    Negative values will be put to 0.

    Parameters
    ----------
    array1: numpy.ndarray
    array1: numpy.ndarray
    method: str
        (Optionnal)
        Could be "1-2" or "2-1".
        "1-2" and "2-1" will subtract array2 to array1
        and array1 to array2 respectivelly.
        default is "1-2"

    Returns
    -------
    numpy.ndarray
        Result of the Substraction
    """
    if method not in ["1-2", "2-1"]:
        raise ValueError("The chosen method is not a valid one")
    if method == "1-2":
        array = array1.astype('int16') - array2.astype('int16')
    else:
        array = array2.astype('int16') - array1.astype('int16')
    array[array < 0] = 0
    return array.astype('uint16')

def _substract_data_handling(
        data_handling_instance: DataHandling,
        array: ndarray,
        method: str = "2-1"
        ):
    """
    Compute the chosen subtraction
    of the current step of the given DataHandling instance and the given array.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    array: numpy.ndarray
        Array used to compute the substraction

    method: str
        Method used to chose the substraction
    """
    data_handling_instance.add_step("Substraction")
    array = _substract_array(
        data_handling_instance.get_current(),
        array,
        method
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def substract(
        data1,
        data2,
        method: str = "2-1"
        ):
    """
    Compute the subtraction of two piece of data1 with the chosen method.
    Both data1 and data2 could be a numpy.ndarray or a DataHandling isntance.
    if both data1 and data2 are DataHandling instance,
    the result will be stored in data1.

    Parameters
    ----------
    data1:
        A piece of data1 used to compute the substraction.
        could be a numpy.ndarray or a DataHandling instance
        If it is a DataHandling isntance,
        result of the computation will be sotre in this variable
        and the current step will be used.

    data2:
        A piece of data1 usde to compute the substraction.
        could be a numpy.ndarray or a DataHandling instance
        If it is a DataHandling isntance, current step will be used.

    method:
        (optionnal)
        Could be "1-2" or "2-1".
        "1-2" and "2-1" will substract data2 to data1
        or data1 to data2 respectivelly.

    """
    if isinstance(data2,
                  ndarray
                  ):
        temp = data2
    elif isinstance(data2,
                    DataHandling
                    ):
        temp = data2.get_current().copy()
    else:
        raise TypeError(
            'Provided data2 is not a numpy.ndarray nor a DataHandling instance'
            )
    if isinstance(data1,
                  ndarray,
                  ):
        out = _substract_array(
            data1,
            temp,
            method
            )
    elif isinstance(data1,
                    DataHandling
                    ):
        out = _substract_data_handling(
            data1,
            temp,
            method
            )
    else:
        raise TypeError(
            'Provided data1 is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
