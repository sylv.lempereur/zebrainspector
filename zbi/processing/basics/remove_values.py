# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains functions that set a specific value to 0.
"""
from gc import collect as garbage_collection

from numpy import (
    ndarray,
    unique
    )

from ...handling import DataHandling

def _remove_values_array(
        array: ndarray,
        values
        ):
    """
    Remove chosen values from an array,
    ie set every pixel containing these value to 0.

    Parameters
    ----------
    array: numpy.ndarray
        Values will be removed from this array

    values
        Values to remove.
        Could be an integer, a float, a numpy.ndarray, a listor or a tupple.
        array, list and tupple have to be fulfill with integer or float.

    Returns
    -------
    numpy.ndarray
        Result of the computation of the distance map
    """

    if isinstance(values,
                  int
                  ):
        array[array == values] = 0
    elif isinstance(values,
                    float
                    ):
        array[array == values] = 0
    elif isinstance(values,
                    list
                    ):
        for value in values:
            array[array == value] = 0
    elif isinstance(values,
                    tuple
                    ):
        for value in values:
            array[array == value] = 0
    elif isinstance(values,
                    ndarray
                    ):
        values_unique = unique(values)
        for value in values_unique:
            array[array == value] = 0
    else:
        raise TypeError(
            "Given values are not an int, nor a float,"
            + "nor a tupple nor a list nor a numpy.ndarray."
            )
    return array

def _remove_values_data_handling(
        data_handling_instance: DataHandling,
        values
        ):
    """
    Remove wanted values from the current step of a DataHandling instance.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    values
        Values to remove.
        Could be an integer, a float, a numpy.ndarray, a listor or a tupple.
        array, list and tupple have to be fulfill with integer or float.
    """
    data_handling_instance.add_step("removeValues")
    array = _remove_values_array(
        data_handling_instance.get_current(),
        values)
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def remove_values(
        data,
        values
        ):
    """
    Remove desired values from a piece of data.
    Data could be a numpy.ndarray or a DataHandling instance.
    values could an integer, a float, a tupple, a list or an numpy.ndarray.

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance

    values
        Values to remove.
        Could be an integer, a float, a numpy.ndarray, a listor or a tupple.
        array, list and tupple have to be fulfill with integer or float.
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _remove_values_array(
            data,
            values
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _remove_values_data_handling(
            data,
            values
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
