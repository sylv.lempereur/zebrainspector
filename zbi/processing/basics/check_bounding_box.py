# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains function to reduce a piece of data
to its bounding box + an offset.
"""

from gc import collect as garbage_collection

from numpy import ndarray, zeros

from ..detection import bounding_box_positions

def check_bounding_box(
        array: ndarray,
        to_add: int = 5
        ):
    """
    """

    not_enough_space_around_object = False
    shape = None

    positions = bounding_box_positions(
        array,
        0
        )

    for axis in range(3):
        if positions[axis][0] < to_add:
            not_enough_space_around_object = True
        if positions[axis][1] > array.shape[axis] - to_add:
            not_enough_space_around_object = True
    if not_enough_space_around_object:
        shape = array.shape
        array_2 = zeros(
            (
                array.shape[0] + 2 * to_add,
                array.shape[1] + 2 * to_add,
                array.shape[2] + 2 * to_add
                )
            )
        array_2[
            to_add : to_add + array.shape[0],
            to_add : to_add + array.shape[1],
            to_add : to_add + array.shape[2]
            ] = array.copy()
        array = array_2.copy()

    return array, shape
