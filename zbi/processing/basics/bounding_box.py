# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains function to reduce a piece of data
to its bounding box + an offset.
"""

from gc import collect as garbage_collection

from numpy import ndarray

from ..detection import bounding_box_positions

from ...handling import DataHandling

def _bounding_box_array(
        array: ndarray,
        tolerance: int = 5
        ):
    """
    Reduce an array to its bounding box with an offset

    Parameters
    ----------
    array: numpy.ndarray
        array to reduce

    tolerance: int
        (optionnal)
        Offset to add around the bounding box.
        Default is 5.

    Returns
    -------
    numpy.ndarray
        Array reduced to the bounding box
    """

    positions = bounding_box_positions(
        array,
        tolerance
        )

    return array[
        positions[0][0]:positions[0][1],
        positions[1][0]:positions[1][1],
        positions[2][0]:positions[2][1]
        ]

def _bounding_box_data_handling(
        data_handling_instance: DataHandling,
        tolerance: int = 5
        ):
    """
    Compute the bounding box of the current step
    and reduce original, current, mask and larvae to bounding box.

    Parameters
    ----------
    data_handling_instance: DataHandling
        DataHanling instance to reduce
        The current step will be reduced

    tolerance: int
        (optionnal)
        Offset to add around the bounding box.
        Default is 5.
    """
    data_handling_instance.add_step("boudingBox")

    positions = bounding_box_positions(
        data_handling_instance.get_current(),
        tolerance
        )

    bounding_range = [
        range(positions[0, :]),
        range(positions[1, :]),
        range(positions[2, :])
        ]

    del positions

    data_handling_instance.set_original(
        data_handling_instance.get_original()[bounding_range]
        )

    data_handling_instance.set_current(
        data_handling_instance.get_current()[bounding_range]
        )

    if data_handling_instance.get_mask():
        data_handling_instance.set_mask(
            data_handling_instance.get_mask()[bounding_range]
            )
    if data_handling_instance.get_larvae():
        data_handling_instance.set_larvae(
            data_handling_instance.get_larvae()[bounding_range]
            )

    del bounding_range
    garbage_collection()
    return True

def bounding_box(
        data,
        tolerance: int = 5
        ):
    """
    Reduction of the given piece of data to its bounding box
    with a given tolerance around it.

    If a DataHandling isntance is given,
    all original, current, mask and larvae will be changed.

    Parameters
    ----------
    data:
        Data to reduced
        Could be DataHandling isntance or numpy.ndarray

    tolerance: int
        (optionnal)
        Offset to add around the bounding box.
        Default is 5.
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _bounding_box_array(
            data,
            tolerance
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _bounding_box_data_handling(
            data,
            tolerance
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
