"""
Basics processing
==================================

================================== =============================================
basics
================================================================================
frame_creation                     Creation of a frame array
fusion                             Fuse two arrays
intersection_removal               Remove intersection between two piece of data
================================== =============================================
"""

from .bounding_box import bounding_box
from .check_bounding_box import check_bounding_box
from .frame_creation import frame_creation
from .inversion import inversion
from .fusion import fusion
from .intersection import intersection
from .intersection_removal import intersection_removal
from .sampling import sampling
