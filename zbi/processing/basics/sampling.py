# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains function to sample an numpy.ndarray.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from scipy.ndimage import zoom

from ...handling import DataHandling


def _sampling_array(
        array: ndarray,
        factor: float
        ):
    """
    Sample an array

    Parameters
    ----------
    array: numpy.ndarray
        array to sample
    factor : float
        Sampling factor.
        For example, a factor of 2 will double the size of the array,
        a factor of 0.5 will divided by 2 the size of the array


    Returns
    -------
    numpy.ndarray
        Result of the sampling
    """
    if factor < 1:
        array_sampled = zoom(array, factor)
    elif factor == 1:
        array_sampled = array
    else:
        array_sampled = array.repeat(
            int(factor),
            axis=0
            ).repeat(
                int(factor),
                axis=1
                ).repeat(
                    int(factor),
                    axis=2
                    )
    return array_sampled

def _sampling_data_handling(
        data_handling_instance: DataHandling,
        factor: float
        ):
    """
    Computation of the sampling of the current step of a DataHandling instance.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    factor: float
        Sampling factor
    """
    data_handling_instance.add_step("sampling"+str(factor))
    array = _sampling_array(
        data_handling_instance.get_current(),
        factor
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def sampling(
        data,
        factor: float
        ):
    """
    Computation of of teh sampling of a piece of data by a given factor.
    data could be an DataHandling instance or a numpy.ndarray

    Parameters
    ----------
    data:
        Piece of Data change by the sum.
    factor: float
        Sampling factor
    """
    if not isinstance(factor,
                      (int, float)
                      ):
        raise TypeError("Factor has to be a numeric value.")
    if factor <= 0:
        raise ValueError(
            "Factor have to be a positive value.\n Given value is "
            + str(factor)
            + '.'
            )
    if isinstance(data,
                  ndarray
                  ):
        out = _sampling_array(
            data,
            factor
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _sampling_data_handling(
            data,
            factor
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
