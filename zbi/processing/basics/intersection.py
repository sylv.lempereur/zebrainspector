# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains function to compute intersection
of two boolean piece of data.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from ...handling import DataHandling

def _intersection_array(
        array1: ndarray,
        array2: ndarray
        ):
    """
    Compute the intersection of two boolean array.

    Parameters
    ----------
    array1: numpy.ndarray
        A boolean array


    array2: numpy.ndarray
        A boolean array

    Returns
    -------
    numpy.ndarray
        Intersection between array1 and array2
    """
    if array1.dtype != 'bool' and array2.dtype != 'bool':
        raise TypeError("Given arrays have to be boolean arrays.")
    return array1 == array2

def _intersection_data_handling(
        data_handling_instance: DataHandling,
        array: ndarray
        ):
    """
    Compute the intersection of the current step of a DataHandling isntance
    and a given array.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    array2: numpy.ndarray
        A boolean array to intersect
    """
    data_handling_instance.add_step("intersection")
    array = _intersection_array(
        data_handling_instance.get_current(),
        array
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def intersection(
        data1,
        data2
        ):
    """
    Compute the intersection of two piece of data.
    Both data1 and data2 have to be boolean,
    but could numpy.ndarray or DataHandling isntance.

    if data1 is a DataHandling instance,
    the result of the intersection will be store in it.

    Parameters
    ----------
    data1:
        Piece of data used to compute the intersection
        could be a numpy.ndarray or a DataHandling instance

    data2:
        Piece of data used to compute the intersection
        could be a numpy.ndarray or a DataHandling instance
    """
    if isinstance(data2,
                  ndarray
                  ):
        array = data2
    elif isinstance(data2,
                    DataHandling
                    ):
        array = data2.get_current().copy()
    else:
        raise TypeError(
            'Provided data2 is not a numpy.ndarray nor a DataHandling instance'
            )
    if isinstance(data1,
                  ndarray
                  ):
        out = _intersection_array(
            data1,
            array
            )
    elif isinstance(data1,
                    DataHandling
                    ):
        out = _intersection_data_handling(
            data1,
            data2
            )
    else:
        raise TypeError(
            'Provided data1 is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
