# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file is a template to write sum_cummulative in the core folder.
"""
from gc import collect as garbage_collection
from numpy import (
    cumsum,
    ndarray
    )

from ...handling import DataHandling

def _sum_cummulative_array(
        array: ndarray,
        axis: int = 1
        ):
    """
    Compute the cummulative sum of the given array laong the current axis.

    Parameters
    ----------
    array: numpy.ndarray

    axis: int
        Axis used to compute the cummulative sum

    Returns
    -------
    numpy.ndarray
        Result of the computation of the cummulative sum
    """
    return cumsum(
        array,
        axis
        )

def _sum_cummulative_data_handling(
        data_handling_instance: DataHandling,
        axis: int = 0
        ):
    """
    Compute the cummulative sum of the current step over the given axis.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    axis: int
        Axis used to compute the cummulative sum
    """
    data_handling_instance.add_step("cummulativSum")
    array = _sum_cummulative_array(
        data_handling_instance.get_current(),
        axis
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def sum_cummulative(
        data,
        axis: int = 0
        ):
    """

    Compute the cummulative sum of data along the given axis.
    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance

    axis: int
        Axis used to compute the cummulative sum
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _sum_cummulative_array(
            data,
            axis
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _sum_cummulative_data_handling(
            data,
            axis
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
