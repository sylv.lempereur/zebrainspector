"""
Compute matthew correlation coefficient.
"""
from numpy import asarray, sqrt

def compute_mcc(
        pixel_set: list
    ):
    """
    This function computes Matthew correlation coefficient(MCC).

    Parameters
    ----------

    pixel_set: list
        List of counts of true negative, false negative, false positive and
        true positive voxels.
        Values should be stores as follows:
        [
            true negative, false negative, false positive, true positive
        ]

    Returns
    -------

    float:
        Computed Matthew correlation coefficient
    """
    #if TN=0 xor TN=0
    if (pixel_set[0] == 0) != (pixel_set[3] == 0):
        #if FP=0 xor FN=0
        if (pixel_set[1] == 0) or (pixel_set[2] == 0):
            # MCC = 0
            raise ValueError(
                "In this case, MCC is impossible to compute.\n"
                + " "
                )
    # If TN=TP=0
    elif pixel_set[0] == 0:
        # If TP = 0 or TN = 0
        if (pixel_set[1] == 0) or (pixel_set[2] == 0):
            # Raise error (denominator = 0)
            return 'NA'
        #MCC=-1
        return -1

    # Change data type to improve prec ision of the computation.
    # Computation of this value in float 32 leads to voerbounds.
    pixel_array = asarray(pixel_set).astype('float64')



    # Computation of the numerator of the MCC.
    # numerator = |TP|.|TN|-|FP||FN|
    numerator = (
        (pixel_array[0] * pixel_array[3])
        - (pixel_array[1] * pixel_array[2])
        ).astype('float64')
    # print(numerator)

    # Computation of the denominator of the MCC.
    # denominator = sqrt( (|TP|+|FP|)(|TP|+|FN|)(|TN|+|FP|)(|TN|+|FN|)
    denominator = sqrt(
        (pixel_array[3] + pixel_array[2])
        * (pixel_array[3] + pixel_array[1])
        * (pixel_array[0] + pixel_array[2])
        * (pixel_array[0] + pixel_array[1])
        ).astype('float64')
    # print(denominator)

    return numerator/denominator
