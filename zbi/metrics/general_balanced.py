"""
Compute matrics to evaluate image segmentation algorithms.


For further explanation, read
`F. A. M. Cappabianco,
P. F. O. Ribeiro,
P. A. V. de Miranda and J. K. Udupa,
"A General and Balanced Region-Based Metric
for Evaluating Medical Image Segmentation Algorithms",
2019 IEEE International Conference on Image Processing (ICIP),
Taipei, Taiwan, 2019, pp. 1525-1529.
doi: 10.1109/ICIP.2019.8803083
<https://ieeexplore.ieee.org/abstract/document/8803083>`.
"""

from numpy import asarray
from numpy import sum as npsum

def _compute_fpsn(
        false_positive_count: int,
        ground_truth_size: int,
        epsilon: float = 10**(-6),
        iteration_max: int = None
    ):
    """
    Compute values of FPs used to compute general balanced metric.

    Parameters
    ----------

    false_positive_count: int
        Number of false positive voxels

    ground_truth_size: int
        Number of voxels in the ground truth

    epsilon: float
        This value is used to stop computation.
        When FPsi is tinier than epsilon, the computation stops.

    iteration_max: int
        (Optionnal) Maximal number of iteration.
        By default, this value is set to None.
        Set to None,
        the computation will continue until fpsi is tinier than epsilon.

    Returns
    -------

    float:
        Value of FPsn
    """
    iteration = 0
    fpi = (
        false_positive_count
        * (
            (
                false_positive_count / ground_truth_size
                )
            ** iteration
            )
        )
    fpsn = [fpi]
    status_continue = True
    while status_continue:
        if (iteration_max is None) or (iteration < iteration_max):
            iteration += 1
            fpi = (
                false_positive_count
                * (
                    (
                        false_positive_count / ground_truth_size
                        )
                    ** iteration
                    )
                )
            if fpi > epsilon:
                fpsn.append(fpi)
            else:
                status_continue = False
        else:
            status_continue = False
    return npsum(asarray(fpsn))

def compute_gb(
        pixel_set: list,
        delta: int = 2,
        epsilon: float = 10 ** (-6),
        iteration_max: int = None
        ):
    """
    This function computes general balance coefficient.

    Parameters
    ----------

    pixel_set: list
        List of counts of true negative, false negative, false positive and
        true positive voxels.
        Values should be stores as follows:
        [
            true negative, false negative, false positive, true positive
        ]

    delta: int
        Delta used for the computation of general balanced coefficient.

    espilon: float
        Value use to stop the computation of FPsn.

    iteration_max: int
        (Optionnal) Maximal number of iteration.
        By default, this value is set to None.
        Set to None,
        the computation will continue until fpsi is tinier than epsilon.

    Returns
    -------

    float:
        Asked general balanced coefficient.
    """
    if npsum(pixel_set[1:3]) == 0:
        raise ValueError(
            "Invalid values."
            + "\n"
            + "At least one voxel should be in the automatic segmentation"
            + " to avoid a zero denominator."
            )
    # If the size of the false positive
    # domain is bigger than the size of the object:
    if pixel_set[2] > (pixel_set[1] + pixel_set[3]) and not iteration_max:
        return None
    # Computation of fpsn
    fpsn = _compute_fpsn(
        pixel_set[2],
        pixel_set[1] + pixel_set[3],
        epsilon,
        iteration_max
        )

    # Computation of the numerator
    # numerator =  δ |TP|
    numerator = delta * pixel_set[3]


    # Computation of the denumerator
    # denominator = δ |FP| + |FN| +FPsn
    denominator = delta * pixel_set[3] + pixel_set[1] + fpsn

    # Return the asked coefficient.
    return numerator / denominator

def compute_dice(
        pixel_set: list,
        ):
    """
    This function computes Sørensen-Dice coefficient.

    Parameters
    ----------

    pixel_set: list
        List of counts of true negative, false negative, false positive and
        true positive voxels.
        Values should be stores as follows:
        [
            true negative, false negative, false positive, true positive
        ]

    Returns
    -------

    float:
        Asked general balanced coefficient.
    """
    return compute_gb(
        pixel_set=pixel_set,
        delta=2,
        iteration_max=0
        )

def compute_balanced_dice(
        pixel_set: list,
        ):
    """
    This function computes balanced Sørensen-Dice coefficient.

    Parameters
    ----------

    pixel_set: list
        List of counts of true negative, false negative, false positive and
        true positive voxels.
        Values should be stores as follows:
        [
            true negative, false negative, false positive, true positive
        ]

    Returns
    -------

    float:
        Asked general balanced coefficient.
    """
    return compute_gb(
        pixel_set=pixel_set,
        delta=2,
        iteration_max=1
        )

def compute_jacquard(
        pixel_set: list,
        ):
    """
    This function computes Jacquard coefficient.

    Parameters
    ----------

    pixel_set: list
        List of counts of true negative, false negative, false positive and
        true positive voxels.
        Values should be stores as follows:
        [
            true negative, false negative, false positive, true positive
        ]

    Returns
    -------

    float:
        Asked general balanced coefficient.
    """
    return compute_gb(
        pixel_set=pixel_set,
        delta=1,
        iteration_max=0
        )

def compute_balanced_jacquard(
        pixel_set: list,
        ):
    """
    This function computes balanced Jacquard coefficient.

    Parameters
    ----------

    pixel_set: list
        List of counts of true negative, false negative, false positive and
        true positive voxels.
        Values should be stores as follows:
        [
            true negative, false negative, false positive, true positive
        ]

    Returns
    -------

    float:
        Asked general balanced coefficient.
    """
    return compute_gb(
        pixel_set=pixel_set,
        delta=1,
        iteration_max=1
        )
