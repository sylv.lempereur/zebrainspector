# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This modules contains any function that restrict a segmentation
to a specific part.
"""
import numpy

import skimage.draw

from ..handling import DataHandling
from .. import processing

# TODO: cut into smaller functions

def cutting_tail(
        data_handling_instance: DataHandling,
        element_size: int = 20
        ):
    """
    Algorithm to extract the brain from the central nervous sytem.
    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    spacer: int

    element_size: int
        distance tinier than this value will be cut
    """
    data_handling_instance.add_step("cuttingTail")
    array = data_handling_instance.get_current().copy()
    cutting_point = [] * 3
    points_of_interest = [] * 3

    ## Working on XY

    # Creation of a 2D projection
    array_xy = processing.projection.binary_projection(
        data_handling_instance,
        axis=0
        )

    # Fulfill some small holes
    array_xy = processing.morphology.remove_holes(array_xy)

    # Reducing shape complexity
    array_xy = processing.morphology.closing(
        array_xy,
        element_size // 4
        )

    # Copute the skeleton of the segmentation on the XY image
    skel_xy = processing.morphology.skeletonization(array_xy)

    # Computation of the distance map
    distance_xy = processing.morphology.dist(array_xy)

    # Found value of the distance map on the skeleton
    # and delete element tinier thant the provided width
    skel_cross_map_xy = processing.basics.fusion(
        distance_xy,
        skel_xy,
        'multiply') < element_size /2

    # Projection into a 1D array in each directio
    points_of_interest[1] = numpy.any(
        skel_cross_map_xy,
        axis=1
        )

    points_of_interest[2] = numpy.any(
        skel_cross_map_xy,
        axis=0
        )

    _, c_max = numpy.where(points_of_interest[2])[0][[0, -1]]

    # For the rest of this computation,
    # we will assumed that the last column is the column of interest,
    # due to the skeletonization
    coi = skel_cross_map_xy[:, c_max]
    rois = numpy.where(coi)[0]
    cutting_point[1] = rois[(rois.size - 1) / 2]
    cutting_point[2] = c_max

    # Creation of a circle_xy around the last point of the skeletonization
    circle_xy = skimage.draw.circle_perimeter(
        r=cutting_point[1],
        c=cutting_point[2],
        radius=element_size / 2,
        shape=array_xy.shape
        )

    # Find the local skeletone around the point of interest
    vect_limit_xy = circle_xy.copy()
    vect_limit_xy *= skel_xy

    if numpy.count_nonzero(vect_limit_xy) != 2:
        print("problem ")
        raise "Error in vector_xy computation"

    # Computation of the vector of this local skeletone
    points_of_interest[1] = numpy.any(
        vect_limit_xy,
        axis=1
        )
    x_start, x_end = numpy.where(points_of_interest[1])[0][[0, -1]]
    y_start = numpy.where(vect_limit_xy[x_start, :])[0][0]
    y_end = numpy.where(vect_limit_xy[x_end, :])[0][0]

    # Computation of the direction vector between this two points

    vect_xy = numpy.array(
        (0, x_end - x_start, y_end - y_start),
        dtype="float32"
        )
    vect_xy /= numpy.linalg.norm(vect_xy)
    vect_xy = numpy.dot(
        numpy.array(
            [
                [1, 0, 0],
                [0, 0, 1],
                [0, -1, 0]
            ]
            ),
        vect_xy)

    # Using the positon found on XY, we will find an other cutting vector
    array_yz = numpy.any(array, axis=1).astype("uint8")
    # Sum over the Y axis

    # Fulfill some small holes
    clean_yz = skimage.morphology.remove_small_holes(array_yz.astype("bool"))
    # Reducing shape complexity
    clean_yz = skimage.morphology.binary_opening(
        clean_yz,
        skimage.morphology.disk(element_size / 4)
        )

    # Compute the skeleton of the segmentation on the XY image
    skel_yz = skimage.morphology.skeletonize(clean_yz)

    # Compute where array_yz is white in the given column
    points_of_interest[0] = numpy.where(skel_yz[:, cutting_point[2]])[0]
    # Find the position of the middle point
    cutting_point[0] = points_of_interest[0][points_of_interest[0].size / 2]

    # Creation of a circle_xy around the last point of the skeletonization
    circle_yz = skimage.draw.circle_perimeter(
        r=cutting_point[0],
        c=cutting_point[2],
        radius=element_size / 2,
        shape=array_yz.shape
        )

    # Find the local skeletone around the point of interest
    vect_yz = circle_yz.copy()
    vect_yz[skel_yz == 0] = 0

    # Computation of the vector of this local skeletone
    points_of_interest[2] = numpy.any(vect_yz, axis=0)
    y_start, y_end = numpy.where(points_of_interest[2])[0][[0, -1]]

    z_start = numpy.where(vect_yz[:, y_start])[0][0]
    z_end = numpy.where(vect_yz[:, y_end])[0][0]

    ## Computation of the vector of the cutting line on the _yz projection
    # Computation of the direction vector of the skeletonization
    # at the cutting point
    v_yz = numpy.array(
        (z_end - z_start, 0, y_end - y_start),
        dtype="float32"
        )
    # Normalisation of this vector
    v_yz /= numpy.linalg.norm(v_yz)
    # Rotation of 90 of this vector to have the direction vector
    # of the cuting line
    v_yz = numpy.dot(numpy.array([[0, 0, 1], [0, 1, 0], [-1, 0, 0]]), v_yz)

    # Computation of the normal vector of the plane of interest
    normal_vector = numpy.cross(vect_xy, v_yz)

    mask = array.copy() * 0
    # Creation of a indices array
    grid = numpy.indices(array.shape)

    # Computation of the distance to the plane
    mask = (
        normal_vector[0] * (grid[0] - cutting_point[0])
        + normal_vector[1] * (grid[1] - cutting_point[1])
        + normal_vector[2] * (grid[2] - cutting_point[2])
        )

    # Everything negative is kept
    print(mask.max())
    mask = numpy.array(
        [mask <= 0],
        'bool'
        )

    data_handling_instance.set_mask(mask)
    data_handling_instance.write_step("maskCut")

    data_handling_instance.set_current(data_handling_instance.get_mask())
