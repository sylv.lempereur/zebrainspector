from os.path import (
	split as splitpath, splitext
	)

from re import split as resplit, match

from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from openpyxl.styles import Alignment

HEADERS = [
		"filename",
		"larva volume",
		"white matter volume",
		"ratio"]

def _add_values(
		sheet,
		row: int,
		basename: str,
		values: list
		):
	sheet["A" + str(row)] = basename
	sheet["B" + str(row)] = values[0]
	sheet["C" + str(row)] = values[1]
	sheet["D" + str(row)] = values[2]

def _create_workbook():
	wb = Workbook()
	wb.remove(wb["Sheet"])
	return wb

def _create_sheet(
		wb: Workbook,
		sheet_name: str,
		headers: list = HEADERS,
		):
	if sheet_name in wb.sheetnames:
		raise ValueError(sheet_name + " already exists.")
	wb.create_sheet(sheet_name)
	sheet = wb[sheet_name]
	for index, header in enumerate(headers):
		column_letter = get_column_letter(index +1 )
		cell = column_letter + "1"
		sheet[cell] = header
		sheet.column_dimensions[column_letter].width = len(header) + 1
		sheet[cell].alignment  = Alignment(horizontal="center")

def _get_basename(
		path: str,
		ref_pattern: str
		):
	"""
	"""
	basename = splitpath(path)[1]
	basename = splitext(basename)[0]
	return basename[:basename.index(ref_pattern)]

def _get_sample_id_and_exp_id(basename: str):
	"""
	"""
	sample_id = basename.split("_")[1]
	exp_id = resplit(r'\D+', sample_id)[0]
	return sample_id, exp_id

def _resize_column(wb):
	for sheet_name in wb.sheetnames:
		sheet = wb[sheet_name]
		column = 1
		while sheet[get_column_letter(column) + str(1)].value:
			row = 2
			while sheet[get_column_letter(column) + str(row)].value:
				cell = get_column_letter(column) + str(row)
				if not str(sheet[cell].value)[0] == "=":
					if sheet.column_dimensions[get_column_letter(column)].width < len(str(sheet[cell].value)):
						sheet.column_dimensions[get_column_letter(column)].width = len(str(sheet[cell].value)) + 1
				row += 1
			column += 1

def generate_xlsx_per_exp_ids(
		dict_of_values: dict,
		ref_pattern: str = "_C00_segmentation"
		):
	"""
	"""
	wb = _create_workbook()
	for key in sorted(dict_of_values):
		basename = _get_basename(
			key,
			ref_pattern
			)
		sample_id, exp_id = _get_sample_id_and_exp_id(basename)
		if not exp_id in wb.sheetnames:
			_create_sheet(
				wb,
				exp_id
				)
			row = 2
		else:
			row = 2
			while wb[exp_id]["A" + str(row)].value:
				row += 1
		_add_values(
			wb[exp_id],
			row,
			basename,
			dict_of_values[key]
			)
	_create_sheet(
		wb,
		"summary"
		)
	for index, sheetname in enumerate(wb.sheetnames):
		if sheetname != "summary":
			wb["summary"]["A" + str(index + 2)] = sheetname
			wb["summary"]["B" + str(index + 2)] = (
				"=AVERAGEA('"
				+ sheetname
				+ "'!B2:B10000)"
				)
			wb["summary"]["C" + str(index + 2)] = (
				"=AVERAGEA('"
				+ sheetname
				+ "'!C2:C10000)"
				)
			wb["summary"]["D" + str(index + 2)] = (
				"=AVERAGEA('"
				+ sheetname
				+ "'!D2:D10000)"
				)
	_resize_column(wb)
	return wb

def generate_xlsx_simple(
		dict_of_values: dict,
		ref_pattern: str = "_C00_segmentation"
		):
	"""
	"""
	wb = _create_workbook()
	headers =  [
		"filename",
		"Volume of the larva",
		"Volume of the white matter",
		"ratio of white matter over whole larva"]
	_create_sheet(
		wb,
		"results"
		)
	row = 2
	for key in sorted(dict_of_values):
		basename = _get_basename(
			key,
			ref_pattern
			)
		_add_values(
			wb["results"],
			row,
			basename,
			dict_of_values[key]
			)
		row += 1
	_resize_column(wb)
	return wb

dict_of_values = {
	"200117Fa_2148c_5dpf_SL-193-DR_512_Nh_merge_C01.mha": [1, 2, 3],
	"/data/test/200117Fa_2148b_5dpf_SL-193-DR_512_Nh_merge_C01.mha": [4, 5, 6],
	"/data/test/200117Fa_2147a_5dpf_SL-193-DR_512_Nh_merge_C01.mha": [7, 8, 9]
	}

wb = generate_xlsx_per_exp_ids(
	dict_of_values,
	"_C01"
	)

wb.save(r"C:/Users/Lempereur/scripts/zebrainspector/test_per_exp_id.xlsx")

wb = generate_xlsx_simple(
	dict_of_values,
	"_C01"
	)
wb.save(r"C:/Users/Lempereur/scripts/zebrainspector/test_simple.xlsx")