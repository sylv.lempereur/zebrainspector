import SimpleITK as sitk

from os.path import split as split_path
from os.path import exists
from os import makedirs
from math import log, floor
import re

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec

import imageio

from numpy import count_nonzero
from numpy import var as npvar
from numpy.random import normal

import openpyxl
from openpyxl import Workbook
from openpyxl.utils import get_column_letter

from scipy.stats import ttest_ind, f
from SimpleITK import (
    ReadImage,
    GetImageFromArray,
    GetArrayFromImage,
    WriteImage
    )

def found_exp_in_cluster(
        exp_id: str,
        cluster: dict
        ):
    """
    """
    test = True
    index = 1
    for key in cluster:
        if exp_id in cluster[key]["samples"]:
            return key, cluster[key]["samples"].index(exp_id)
    raise ValueError(
        exp_id
        + " is not on the provided cluster"
        )

def computes_sizes(
        file_list: list,
        path_output: str,
        clusters : dict
        ):
    """
    """

    def _draw_figures(
            datas,
            labels,
            t_tests,
            path_out
        ):
        folder, _ = split_path(path_out)
        if not exists(folder):
            makedirs(folder)
        plt.figure(figsize=(5, 5), linewidth=0)
        matplotlib.rcParams.update({'font.size': 22})
        magnitude = floor(
            log(
                max([item for sublist in datas for item in sublist]),
                10
                )
            )
        if magnitude > 2:
            for pos_1 in range(len(datas)):
                for pos_2 in range(len(datas[pos_1])):
                    datas[pos_1][pos_2] /= 10 ** magnitude
        bplot1 = plt.boxplot(
            datas,
            labels=labels
            )
        height_max = 0
        for col_start in range(len(labels)):
            height = 0
            for col_end in range(col_start + 1, len(labels)):
                if t_tests[col_start] != "ns":
                    height += 1
                    y = max([item for sublist in datas for item in sublist]) * (1 + 0.01 + height * 0.05)
                    h = max([item for sublist in datas for item in sublist]) * 0.01
                    color = 'k'
                    plt.plot(
                        [
                            1+col_start,
                            1+col_start,
                            1+col_end,
                            1+col_end
                            ],
                        [
                            y,
                            y+h,
                            y+h,
                            y
                            ],
                        lw=1.5,
                        c=color)
                    plt.text(
                        (0.5 + col_start + col_end),
                        y + h,
                        t_tests[col_start],
                        ha='center',
                        va='bottom', 
                        color = color
                        )
                if height > height_max:
                    height_max = height
        axes = plt.gca()
        print(height_max)
        if height_max != 0:
            axes.set_ylim(
                None,
                (1 + height_max * 0.05 + 0.01) * max([item for sublist in datas for item in sublist])
                )
        if magnitude > 6:
            plt.ylabel(r"x " + str(10 ** (magnitude - 9)) + " mm$^3$")
        plt.savefig(
            path_out,
            dpi = 1200,
            bbox_inches='tight'
            )

    book = Workbook()
    book.active.title = 'samples'
    book['samples']["A1"] = "sample"
    book['samples']["A2"] = "whole larva"
    book['samples']["A2"] = "white matter"
    exps = []
    book.create_sheet('whole larvae per experiment')
    book.create_sheet('white matter per experiment')
    book['whole larvae per experiment']['A1'] = 'Experiment number'
    book['white matter per experiment']['A1'] = 'Experiment number'
    for index in range(1,25):
        book['whole larvae per experiment'][get_column_letter(index + 1) + '1'] = get_column_letter(index).lower()
        book['white matter per experiment'][get_column_letter(index + 1) + '1'] = get_column_letter(index).lower()
    row = 2
    datas = {
        "larva": [],
        "white": [],
        "ratio": []
        }
    for file in file_list:
        print(file)
        _, sample = split_path(file)
        exp_id = sample.split('_')[1]
        exp_letter = re.search(r'\D+', exp_id).group(0)
        exp_number = exp_id[:exp_id.index(exp_letter)]
        genotype, index = found_exp_in_cluster(exp_id, clusters)
        if not exp_number in exps:
            exps.append(exp_number)
            book['whole larvae per experiment']["A" + str(exps.index(exp_number) + 2)] = exp_number
            book['white matter per experiment']["A" + str(exps.index(exp_number) + 2)] = exp_number
            datas["larva"].append([])
            datas["white"].append([])
            datas["ratio"].append([])
        book['samples']["A" + str(row)] = sample

        image = sitk.ReadImage(file + "_C00_larva.mha")
        array_larva = sitk.GetArrayFromImage(image)
        spacing = image.GetSpacing()
        voxel_volume = spacing[0] * spacing[1] * spacing[2]
        size_larva = count_nonzero(array_larva) * voxel_volume

        clusters[genotype]["size"]["larva"].append(size_larva)

        book['samples']["B" + str(row)] = size_larva
        book['whole larvae per experiment'][get_column_letter(ord(exp_letter ) - 96 + 1) + str(2 + exps.index(exp_number))] = size_larva
        datas["larva"][exps.index(exp_number)].append(size_larva)

        image = sitk.ReadImage(file + "_C00_white_matter_2.mha")
        array = sitk.GetArrayFromImage(image)
        spacing = image.GetSpacing()
        voxel_volume = spacing[0] * spacing[1] * spacing[2]
        size_white = count_nonzero(array) * voxel_volume

        clusters[genotype]["size"]["white"].append(size_white)

        clusters[genotype]["size"]["ratio"].append(size_white / size_larva)
        book['samples']["C" + str(row)] = size_white
        book[
            'white matter per experiment'
            ][
                get_column_letter(
                    ord(exp_letter) - 96 + 1
                    ) + str(2 + exps.index(exp_number))
                ] = size_white
        datas["white"][exps.index(exp_number)].append(size_white)

        datas["ratio"][exps.index(exp_number)].append(size_white / size_larva)
        row += 1

    #     array = array.astype('uint8')
    #     array += array_larva
    #     image = sitk.GetImageFromArray(array)
    #     image.SetSpacing(spacing)
    #     sitk.WriteImage(
    #         image,
    #         file + 'combined_segmentation.mha'
    #         )
    datas = {
        "labels" : list(clusters.keys()),
        "larva": [[]]*len(clusters.keys()),
        "white": [[]]*len(clusters.keys()),
        "ratio": [[]]*len(clusters.keys())
    }
    for keys in clusters:
        print("=" * 80)
        print(keys)
        print(clusters[keys]["size"]["white"])
        index = datas["labels"].index(keys)
        datas["larva"][index] = clusters[keys]["size"]["larva"]
        datas["white"][index] = clusters[keys]["size"]["white"]
        datas["ratio"][index] = clusters[keys]["size"]["ratio"]
    for index in range(len(datas["labels"])):
        datas["labels"][index] += "\n n: " + str(len(datas["larva"][index]))

    #print p values
    t_tests = {
        "larva": [],
        "ratio": [],
        "white": [],
        }
    pos = -1
    for key in ["white", "larva", "ratio"]:
        pos += 1
        if len(datas[key]) == 2:
            var0 = npvar(datas[key][0])
            var1 = npvar(datas[key][1])
            F = var0 / var1
            if F < 1:
                F = 1 / F
                df1 = len(datas[key][0]) - 1
                df0 = len(datas[key][1]) - 1
            else:
                df0 = len(datas[key][0]) - 1
                df1 = len(datas[key][1]) - 1
            ftest = f.cdf(var0 / var1, df0, df1) < 0.05
            ttest = ttest_ind(datas[key][0],datas[key][1], equal_var = ftest)
            if ttest[1] > 0.05:
                t_tests[key].append("ns")
            elif ttest[1] > 0.01:
                t_tests[key].append("*")
            elif ttest[1] > 0.001:
                t_tests[key].append("**")
            else:
                t_tests[key].append("***")
        else:
            for i in range(len(datas[key])):
                for j in range(i + 1,len(datas[key])):
                    var0 = npvar(datas[key][i])
                    var1 = npvar(datas[key][j])
                    F = var0 / var1
                    if F < 1:
                        F = 1 / F
                        df1 = len(datas[key][i]) - 1
                        df0 = len(datas[key][j]) - 1
                    else:
                        df0 = len(datas[key][i]) - 1
                        df1 = len(datas[key][j]) - 1
                    ftest = f.cdf(var0 / var1, df0, df1) < 0.05
                    ttest = ttest_ind(
                            datas[key][i],
                            datas[key][j],
                            equal_var = ftest
                            )
                    if ttest[0] > 0.05:
                        t_tests[pos].append("ns")
                    elif ttest[0] > 0.01:
                        t_tests[pos].append("*")
                    elif ttest[0] > 0.001:
                        t_tests[pos].append("**")
                    else:
                        t_tests[pos].append("***")

    _draw_figures(
        datas["larva"],
        datas["labels"],
        t_tests["larva"],
        "/data/réseau/WDR12/larva.png",
        )
    _draw_figures(
        datas["ratio"],
        datas["labels"],
        t_tests["ratio"],
        "/data/réseau/WDR12/ratio.png",
        )
    _draw_figures(
        datas["white"],
        datas["labels"],
        t_tests["white"],
        "/data/réseau/WDR12/white.png",
        )

FILES = sorted(
        [
        # "/data/hcsProcessing/5Fu/200204Fa_2213a_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2213a_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2213b_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2213b_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2213c_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2213c_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200204Fa_2213d_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2213d_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200204Fa_2213e_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2213e_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2213f_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2213f_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2213g_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2213g_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200204Fa_2213h_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2213h_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2213i_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2213i_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2213j_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2213j_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2213k_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2213k_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200204Fa_2213l_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2213l_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200204Fa_2213m_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2213m_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2213n_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2213n_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2213o_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2213o_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200204Fa_2213p_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2213p_5dpf_SL-194-DR_512_Nh_merge",

        # # "/data/hcsProcessing/5Fu/200204Fa_2214a_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2214a_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2214b_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2214b_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2214c_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2214c_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200204Fa_2214d_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2214d_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2214e_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2214e_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2214f_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2214f_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2214g_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2214g_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2214h_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2214h_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2214i_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2214i_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2214j_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2214j_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2214k_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2214k_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2214l_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2214l_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200204Fa_2214m_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2214m_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200204Fa_2214n_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2214n_5dpf_SL-194-DR_512_Nh_merge",

        # "/data/hcsProcessing/5Fu/200204Fa_2215a_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2215a_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2215b_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2215b_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2215c_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2215c_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200204Fa_2215d_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2215d_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200204Fa_2215e_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2215e_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2215f_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2215f_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2215g_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2215g_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200204Fa_2215h_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2215h_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2215i_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2215i_5dpf_SL-194-DR_512_Nh_merge",

        # "/data/hcsProcessing/5Fu/200204Fa_2216a_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2216a_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2216b_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2216b_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2216c_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2216c_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2216d_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2216d_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2216e_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2216e_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2216f_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2216f_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2216g_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2216g_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2216h_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2216h_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2216i_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2216i_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200204Fa_2216j_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2216j_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200204Fa_2216k_5dpf_SL-194-DR_512_Nh_merge/200204Fa_2216k_5dpf_SL-194-DR_512_Nh_merge",

        # "/data/hcsProcessing/5Fu/200206Fa_2201a_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2201a_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2201b_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2201b_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2201c_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2201c_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2201d_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2201d_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200206Fa_2201e_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2201e_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2201f_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2201f_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2201g_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2201g_5dpf_SL-194-DR_512_Nh_merge",

        # "/data/hcsProcessing/5Fu/200206Fa_2202a_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2202a_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2202b_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2202b_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2202c_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2202c_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2202d_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2202d_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2202e_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2202e_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2202f_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2202f_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2202g_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2202g_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2202h_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2202h_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200206Fa_2202i_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2202i_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2202j_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2202j_5dpf_SL-194-DR_512_Nh_merge",

        # "/data/hcsProcessing/5Fu/200206Fa_2203a_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2203a_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2203b_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2203b_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2203c_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2203c_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2203d_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2203d_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2203e_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2203e_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2203f_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2203f_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2203g_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2203g_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2203h_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2203h_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2203i_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2203i_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2203j_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2203j_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2203k_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2203k_5dpf_SL-194-DR_512_Nh_merge",

        # "/data/hcsProcessing/5Fu/200206Fa_2204a_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2204a_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2204b_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2204b_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2204c_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2204c_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200206Fa_2204d_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2204d_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2204e_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2204e_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2204f_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2204f_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2204g_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2204g_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2204h_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2204h_5dpf_SL-194-DR_512_Nh_merge",

        # "/data/hcsProcessing/5Fu/200206Fa_2205a_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2205a_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2205b_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2205b_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2205c_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2205c_5dpf_SL-194-DR_512_Nh_merge",
        # # "/data/hcsProcessing/5Fu/200206Fa_2205d_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2205d_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2205e_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2205e_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2205f_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2205f_5dpf_SL-194-DR_512_Nh_merge",
        # "/data/hcsProcessing/5Fu/200206Fa_2205g_5dpf_SL-194-DR_512_Nh_merge/200206Fa_2205g_5dpf_SL-194-DR_512_Nh_merge",
        ]
    )

FILES = sorted(
    [
    "/data/WDR12/200117Fa_2150a_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2150a_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2151a_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2151a_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2150b_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2150b_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2151b_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2151b_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2150c_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2150c_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2151c_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2151c_5dpf_SL-195-DR_512_Nh_merge",
    # "/data/WDR12/200117Fa_2150d_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2150d_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2151d_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2151d_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2150e_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2150e_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2151e_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2151e_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2150f_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2150f_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2151f_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2151f_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2150g_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2150g_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2151g_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2151g_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2150h_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2150h_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2151h_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2151h_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2150i_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2150i_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2151i_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2151i_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2150j_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2150j_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2151j_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2151j_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2150k_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2150k_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2151k_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2151k_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2150l_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2150l_5dpf_SL-195-DR_512_Nh_merge",
    "/data/WDR12/200117Fa_2151l_5dpf_SL-195-DR_512_Nh_merge/200117Fa_2151l_5dpf_SL-195-DR_512_Nh_merge",
        ]
    )
PATH_INPUT = "/data/WDR12"
PATH_OUTPUT = "/data/WDR12/"

def generate_superposition(path):
    larva = ReadImage(path + "_C00_larva.mha")
    spacing = larva.GetSpacing()
    larva = GetArrayFromImage(larva)
    white_matter = GetArrayFromImage(
        ReadImage(
            path + "_C00_white_matter_2.mha"
            )
        )
    superposition = larva.copy()
    superposition[white_matter != 0] = 2
    image = sitk.GetImageFromArray(superposition)
    image.SetSpacing(spacing)
    WriteImage(image, path + "_C00_superposition.mha")

CLUSTERS_1 = {
    "2 g/l": {
        "samples" : [
            "2201a",
            "2201b",
            "2201c",
            "2201d",
            "2201e",
            "2201f",
            "2201g",
            "2201h",
            "2201i",
            "2201j",
            "2201k",
            "2201l",
            "2201m",
            "2201n",
            "2201o",
            "2201p",
            "2201q",
            ],
        "size": {
            "larva": [],
            "white": [],
            "ratio": []
            }
        },
    "1 g/l": {
        "samples" : [
            "2202a",
            "2202b",
            "2202c",
            "2202d",
            "2202e",
            "2202f",
            "2202g",
            "2202h",
            "2202i",
            "2202j",
            "2202k",
            "2202l",
            "2202m",
            "2202n",
            "2202o",
            "2202p",
            "2202q",
            ],
        "size": {
            "larva": [],
            "white": [],
            "ratio": []
            }
        },
    "500 mg/l": {
        "samples" : [
            "2203a",
            "2203b",
            "2203c",
            "2203d",
            "2203e",
            "2203f",
            "2203g",
            "2203h",
            "2203i",
            "2203j",
            "2203k",
            "2203l",
            "2203m",
            "2203n",
            "2203o",
            "2203p",
            "2203q",
            ],
        "size": {
            "larva": [],
            "white": [],
            "ratio": [],
            }
        },
    "250 mg/l": {
        "samples" : [
            "2204a",
            "2204b",
            "2204c",
            "2204d",
            "2204e",
            "2204f",
            "2204g",
            "2204h",
            "2204i",
            "2204j",
            "2204k",
            "2204l",
            "2204m",
            "2204n",
            "2204o",
            "2204p",
            "2204q",
            ],
        "size": {
            "larva": [],
            "white": [],
            "ratio": [],
            }
        },
    "125 mg/l": {
        "samples" : [
            "2205a",
            "2205b",
            "2205c",
            "2205d",
            "2205e",
            "2205f",
            "2205g",
            "2205h",
            "2205i",
            "2205j",
            "2205k",
            "2205l",
            "2205m",
            "2205n",
            "2205o",
            "2205p",
            "2205q",
            ],
        "size": {
            "larva": [],
            "white": [],
            "ratio": [],
            }
        },
    "EM": {
        "samples" : [
            "2213a",
            "2213b",
            "2213c",
            "2213d",
            "2213e",
            "2213f",
            "2213g",
            "2213h",
            "2213i",
            "2213j",
            "2213k",
            "2213l",
            "2213m",
            "2213n",
            "2213o",
            "2213p",
            "2213q",
            "2214a",
            "2214b",
            "2214c",
            "2214d",
            "2214e",
            "2214f",
            "2214g",
            "2214h",
            "2214i",
            "2214j",
            "2214k",
            "2214l",
            "2214m",
            "2214n",
            "2214o",
            "2214p",
            "2214q",
            ],
        "size": {
            "larva": [],
            "white": [],
            "ratio": [],
            }
        },
    "DMSO": {
        "samples" : [
            "2215a",
            "2215b",
            "2215c",
            "2215d",
            "2215e",
            "2215f",
            "2215g",
            "2215h",
            "2215i",
            "2215j",
            "2215k",
            "2215l",
            "2215m",
            "2215n",
            "2215o",
            "2215p",
            "2215q",
            "2216a",
            "2216b",
            "2216c",
            "2216d",
            "2216e",
            "2216f",
            "2216g",
            "2216h",
            "2216i",
            "2216j",
            "2216k",
            "2216l",
            "2216m",
            "2216n",
            "2216o",
            "2216p",
            "2216q",
            ],
        "size": {
            "larva": [],
            "white": [],
            "ratio": [],
            }
        }
    }

CLUSTERS_1 = {
    "Siblings": {
        "samples" : [
            "2150a",
            "2150b",
            "2150c",
            "2150d",
            "2150e",
            "2150f",
            "2150g",
            "2150h",
            "2150i",
            "2150j",
            "2150k",
            "2150l",
            "2150m",
            "2150n",
            "2150o",
            "2150p",
            "2150q",
            ],
        "size": {
            "larva": [],
            "white": [],
            "ratio": []
            }
        },
    "Mutants": {
        "samples" : [
            "2151a",
            "2151b",
            "2151c",
            "2151d",
            "2151e",
            "2151f",
            "2151g",
            "2151h",
            "2151i",
            "2151j",
            "2151k",
            "2151l",
            "2151m",
            "2151n",
            "2151o",
            "2151p",
            "2151q",
            ],
        "size": {
            "larva": [],
            "white": [],
            "ratio": []
            }
        },
    }

computes_sizes(
    file_list=FILES,
    path_output=PATH_OUTPUT,
    clusters=CLUSTERS_1
    )
