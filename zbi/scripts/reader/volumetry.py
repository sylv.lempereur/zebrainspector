"""
"""

from numpy import (
	ndarray,
	unique
	)

def volumetry(array):
	"""
	"""

	values, counts = unique(
		array,
		return_counts = True)
	volume_larva = sum(counts[values != 0])
	volume_white = counts[values == 4095]
	print(type(volume_white))
	if isinstance(volume_white, ndarray):
		print(volume_white)
		print(type(volume_white))
		volume_white = volume_white[0]
		print(volume_white)
		print(type(volume_white))
	ratio = volume_white / volume_larva
	return (
		volume_white,
		volume_larva,
		ratio
		)