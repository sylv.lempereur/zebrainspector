from imageio import mimsave

from numpy import count_nonzero, maximum, mean, where

from SimpleITK import ReadImage, GetArrayFromImage

def generate_movies(
        path_input: str,
        path_output: str,
        n_frame: int = 20
        ):
    """
    Generate movies on sagittal views.

    Will generate 3 movies:
    - a Movie containing maximum intensity projections of section of the image
    - a movie containing average intensity projections of section of the image
    - a Movie containing real slice ofthe image

    Parameters
    ----------
    path_input: str
        Path to a 3D image

    path_output: str
        Path uses to store the results

    n_frame: int
        Number of frame in the saved movies
    """
    averages = []
    maximums = []
    raw = []
    array = GetArrayFromImage(ReadImage(path_input))
    shape = array.shape
    thresh = array > 1000
    counts = count_nonzero(thresh, axis=(1, 2))
    start = where(counts)[0][0]
    end = where(counts)[-1][-1]
    for slice in range(start, end, (end-start) // n_frame):
        if slice + (end-start) // n_frame > end:
            end_temp = end
        else:
            end_temp = slice + (end-start) // n_frame
        array_temp = array[slice:end_temp, :, : ]
        array_temp = array_temp.astype('float32') * 255 / 4095
        array_temp = array_temp.astype('uint8')
        print(array_temp.shape)
        maximums.append(array_temp.max(axis = 0))
        averages.append(mean(array_temp, axis = (0)).astype("uint16"))
        raw.append(array_temp[array_temp.shape[0] // 2, : , :])
    mimsave(
        path_output + "_raw.tif",
        raw
        )
    mimsave(
        path_output + "_mip.tif",
        maximums
        )
    mimsave(
        path_output + "_aip.tif",
        averages
        )

files = [
    # "/data/data_for_bio_paper/5Fu/200206Fa_2201a_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2203f_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2201b_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2203g_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2201c_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2203h_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2201d_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2203i_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2201e_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2203j_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2201f_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2203k_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2201g_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2204a_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2202a_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2204b_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2202b_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2204c_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2202c_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2204d_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2202d_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2204e_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2202e_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2204f_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2202f_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2204g_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2202g_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2204h_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2202h_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2205a_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2202i_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2205b_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2202j_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2205c_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2203a_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2205d_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2203b_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2205e_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2203c_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2205f_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2203d_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2205g_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    # "/data/data_for_bio_paper/5Fu/200206Fa_2203e_5dpf_SL-194-DR_512_Nh_merge_C00.mha",
    "/data/data_for_bio_paper/raw/Chosen_ones/200108Fa_2144a_5dpf_SL-193-DR_512_Nh_merge_C00.mha",
    ]

for file in files:
    generate_movies(file,file.replace('.mha',""))