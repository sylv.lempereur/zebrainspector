"""
Contains scripts uses to perform zebrafish analysis
"""

from .reader import volumetry

# Script that perform contrasts corrections procedures.
from .contrast_correction import (
    depth_contrast_correction,
    labeling_contrast_correction
    )
# Script that align sample with the coordinate system.
from .registration import registration
# Scripts performing segmentation of the whole larva and the white matter,
# and returning an array containing both informations.
from .segmentation import segmentation
# Scripts that perform segmentation of the whole larva or the white_matter
# using a given DataHandling instance.
from .segmentations import (
    white_matter,
    whole_larva
    )
