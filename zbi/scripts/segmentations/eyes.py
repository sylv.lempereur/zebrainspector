# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
Perform automatic segmentations of eyes of 5dpf zebrafish
"""

from gc import collect as garbage_collection

import numpy

from SimpleITK import GetImageFromArray, WriteImage

from ... import processing

def eyes(
        array_dye: numpy.ndarray,
        pathout: str = None,
        spacing: tuple = None
        ):
    """
    Segmentation of the eyes ousing Dye channel

    Parameters
    ----------
    array_dye: numpy.ndarray
        Array of the dye channel

    See also
    --------
    whole_larvae_per_watershed
    white_matter
    """
    print('\t Eyes detection')

    array_dye = processing.thresholding.manual(
        array_dye,
        float((2.0**12-1.0)*0.99)
        )

    if pathout:
        image = GetImageFromArray(array_dye.astype('uint8'))
        image.SetSpacing(spacing)
        WriteImage(image, pathout + '_eye1.mha')

    array_dye = processing.labeling.size(array_dye)

    array_dye = processing.thresholding.manual(array_dye, 200)

    if pathout:
        image = GetImageFromArray(array_dye.astype('uint8'))
        image.SetSpacing(spacing)
        WriteImage(image, pathout + '_eye2.mha')

    array_dye = processing.morphology.closing(
        array_dye,
        7,
        'rect'
        )

    if pathout:
        image = GetImageFromArray(array_dye.astype('uint8'))
        image.SetSpacing(spacing)
        WriteImage(image, pathout + '_eye3.mha')

    array_dye = processing.morphology.dilation(
        array_dye,
        7,
        'rect'
        )

    if pathout:
        image = GetImageFromArray(array_dye.astype('uint8'))
        image.SetSpacing(spacing)
        WriteImage(image, pathout + '_eye4.mha')

    garbage_collection()

    return array_dye
