# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
Perform automatic segmentations of white matter of a 5dpf zebrafish
"""

from gc import collect as garbage_collection

from numpy import zeros

from ....handling import DataHandling

from ....processing.basics import inversion
from ....processing.detection import maxima_local
from ....processing.labeling import size
from ....processing.morphology import (
    closing,
    dilation,
    dilation_geodesic,
    erosion,
    gradient,
    opening,
    watershed
    )
from ....processing.thresholding import manual_percent_histo\
    as threshold_percent_histo
from ....processing.thresholding import manual as threshold

# from .eyes import eyes as segmentation_eyes

def white_matter(
        dye_storage: DataHandling
        ):
    """
    Segmentation of the myelin in the brain using DiI channel.

    Parameters
    ----------
    dye_storage: DataHandling
        Store the DiI channel

    See also
    --------
    whole_larvae_per_watershed
    """

    print("Segmentation of brain's myelin")

    opening(
        dye_storage,
        5,
        shape='rect'
        )

    closing(
        dye_storage,
        5,
        shape='rect'
        )

    # Store result of the median filter
    opened = dye_storage.get_current().copy()

    # thresholding
    threshold_percent_histo(
        dye_storage,
        90
        )

    zones = 2 * dye_storage.get_current().copy().astype('uint8')

    # Work on the lowest class
    dye_storage.set_current(opened.copy())
    threshold_percent_histo(
        dye_storage,
        50
        )
    inversion(dye_storage)

    erosion(
        dye_storage,
        15,
        'rect'
        )

    zones[dye_storage.get_current() != 0] = 1

    #Found local maxima
    coordinates = maxima_local(
        opened
        )

    # Set local maximum to their "zone" value.
    markers = zeros(zones.shape)
    markers[coordinates] = zones[coordinates]

    dye_storage.set_current(zones)
    dye_storage.write_step("zones")

    del coordinates, zones

    # Back to the opened filtered image
    dye_storage.set_current(opened.copy())

    # Computation of a morphological gradient
    gradient(
        dye_storage,
        shape='rect'
        )

    #Watersehd
    watershed(
        dye_storage,
        markers
        )

    threshold(
        dye_storage,
        2
        )

    stock = dye_storage.get_current()

    dye_storage.write_step("white_matter")

    size(dye_storage)

    threshold(
        dye_storage,
        dye_storage.get_current().max()
        )

    dilation(
        dye_storage,
        rad=10,
        shape="rect")

    dye_storage.set_current(dye_storage.get_current().copy() * stock)

    dilation_geodesic(dye_storage, stock)

    dye_storage.write_step("white_matter_2")

    garbage_collection()

    return dye_storage.get_current()
