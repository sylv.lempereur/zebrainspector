"""
white_matter
===========

Contains functions to segment white matter.

============ ====================================
white_matter Compute segmentation of white matter
============ ====================================
"""

from .white_matter import white_matter
