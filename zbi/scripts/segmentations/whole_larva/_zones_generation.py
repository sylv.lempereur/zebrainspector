# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
Contains functions to generate zones for segmentation of the whole larva.
"""

from numpy import ndarray, zeros

from ....handling import DataHandling

def _raise_threshold_inside_type():
    """
    Raise an error if provided threshold_inside type is invalid.
    """
    raise TypeError(
        "threshold_inside type is invalid.\n"
        + "Must be float or int"
        )

def _raise_threshold_outside_type():
    """
    Raise an error if provided threshold_outside type is invalid.
    """
    raise TypeError(
        "threshold_outside type is invalid.\n"
        + "Must be float or int"
        )

def _zones_generation_array(
        array: ndarray,
        threshold_inside: (int, float),
        threshold_outside: (int, float)
        ):
    """
    Generate zone array.
    This array is computed as following:
    every pixel higher than threshold_inside are set to 2 where
    pixels lower than threshold_outside are set to 1.

    Parameters
    ----------

    array: numpy.ndarray
        Array used to generate zone array

    threshold_inside: int
        Threshold value for inside pixels.

    threshold_outside: int
        Threshold value for outside elements

    Returns
    -------
    array:
        Zone array
    """

    zones = zeros(array.shape, 'uint8')

    zones[array > threshold_inside] = 2
    zones[array <= threshold_outside] = 1

    return zones

def _zones_generation_datahandling(
        data_handling_instance: DataHandling,
        threshold_inside: (int, float),
        threshold_outside: (int, float)
        ):
    """
    Compute zone array and set it as current_step of the provided DataHandling
    instance.

    Parameters
    ----------

    array: numpy.ndarray
        Array used to generate zone array

    threshold_inside: int
        Threshold value for inside pixels.

    threshold_outside: int
        Threshold value for outside elements

    Returns
    -------
    Bool
        Computation status
    """
    data_handling_instance.add_step("zoneGeneration")
    zones = _zones_generation_array(
        data_handling_instance.get_current().copy(),
        threshold_inside,
        threshold_outside
        )
    data_handling_instance.set_current(zones.copy())
    del zones
    return True

def zones_generation(
        data,
        threshold_inside: (int, float),
        threshold_outside: (int, float)
        ):
    """
    Generation of a zone array.
    data could be a DataHandling isntance or a nmpy.ndarray.

    Parameters
    ----------

    array: numpy.ndarray
        Array used to generate zone array

    threshold_inside: int
        Threshold value for inside pixels.

    threshold_outside: int
        Threshold value for outside elements
    """
    if not isinstance(threshold_outside, (int, float)):
        _raise_threshold_outside_type()
    elif not isinstance(threshold_inside, (int, float)):
        _raise_threshold_inside_type()
    if isinstance(data, ndarray):
        out = _zones_generation_array(
            data,
            threshold_inside,
            threshold_outside
            )
    elif isinstance(data, DataHandling):
        out = _zones_generation_datahandling(
            data,
            threshold_inside,
            threshold_outside
            )
    else:
        raise TypeError(
            "Provided piece of data is not of a valid type."
            + "\n"
            + "Should be a numpy.ndarray or a DataHandling instance."
            )
    return out
