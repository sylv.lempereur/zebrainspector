"""
whole_larva
===========

Contains functions to segment whole larva.

=========== ==================================
whole_larva Compute segmentation of wholelarva
=========== ==================================
"""

from .whole_larva import whole_larva
