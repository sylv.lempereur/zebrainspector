# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
Perform automatic segmentations of whole mounted 5dpf zebrafish
"""

from numpy import (
    nan,
    nanmedian,
    unique
    )

from ....handling import DataHandling

from .... import processing

def remove_black_pixels_on_acquisition_side(
        datahandling: DataHandling,
        threshold: (float, int)=750):
    """
    Compute a depth map into the current step and remove pixels
    with a grey value tinier than provided threshold on the acquisition side.

    Paramaters
    ----------
    datahandling: DataHandling
        Current step of the DataHandling instance must be a boolean array.

    threshold: (float, int)
        Values tinier than this one will be removed on the acquisition side.
    """
    if datahandling.get_current().dtype != 'bool':
        if len(unique(datahandling.get_current())) == 2:
            datahandling.set_current(datahandling.get_current().astype('bool'))
        else:
            raise TypeError(
                "Invalid type for the current step of datahandling."
                )

    larva = datahandling.get_current().copy()

    processing.labeling.depth(datahandling)

    array_for_median = datahandling.get_original().copy().astype('float64')
    array_for_median[datahandling.get_current() == 0] = nan

    medians = []
    for depth in range(1, datahandling.get_current().max() + 1):
        medians.append(
            nanmedian(
                array_for_median[datahandling.get_current() == depth]
                )
            )

    del array_for_median

    acquisition_side = datahandling.get_current() < medians.index(max(medians))
    acquisition_side *= datahandling.get_current() > 0
    thresh = datahandling.get_original().copy() > threshold

    larva[acquisition_side] = thresh[acquisition_side]

    datahandling.set_current(larva.copy())

    del larva
