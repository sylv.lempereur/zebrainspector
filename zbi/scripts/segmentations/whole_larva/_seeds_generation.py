# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
Contains functions to generate seeds for segmentation of the whole larva.
"""

from numpy import ndarray

from ....handling import DataHandling
from ....processing.basics import frame_creation

def _raise_data_type():
    """
    Raise error if the type of the provided piece of data is not available.
    """
    raise TypeError(
        "Provided piece of data have an invalid type."
        + "\n"
        + "Should be a numpy.ndarray or a DataHandling isntance"
        )

def _raise_coordinates_type():
    """
    Raise error if type of coordinates is not a numpy.ndarray
    """
    raise TypeError(
        "Provided coordinates is not a numpy.ndarray."
        )

def _raise_shape():
    """
    Raise error if shape of coordiantes and array are not compatible.
    """
    raise ValueError(
        "coordinates and array does not have the same shape"
        )

def _seeds_generation_array(
        array: ndarray,
        coordinates: ndarray
        ):
    """
    Generation of seed array.
    This seed array is setup as follow:
        Frame of the image is setup to 1, where corrdinates are setup to values
        contained in array

    Parameters
    ----------

    array: numpy.ndarray
        Array used to generate zone array

    coordinates: numpy.ndarray
        Coordinates of local maxima

    Returns
    -------
    array:
        seed array
    """
    if array.shape != coordinates.shape:
        _raise_shape()

    seeds = frame_creation(array).astype("uint8")
    seeds[coordinates] = array[coordinates]

    return seeds

def _seeds_generation_datahandling(
        data_handling_instance: DataHandling,
        coordinates: ndarray
        ):
    """
    Generation of seed array based on the current step
    of a DataHandling isntance.
    This seed array is setup as follow:
        Frame of the image is setup to 1, where corrdinates are setup to values
        contained in array
    Result of this computation will store in the DataHandling instance.

    Parameters
    ----------

    data_handling_instance: DataHandling
        DataHandling instance used to generate zone array

    coordinates: numpy.ndarray
        Coordinates of local maxima

    Returns
    -------
    Bool:
        Computation status
    """
    data_handling_instance.add_step("seedsgeneration")
    seeds = _seeds_generation_array(
        data_handling_instance.get_current().copy(),
        coordinates
        )
    data_handling_instance.set_current(seeds.copy())
    del seeds
    return True

def seeds_generation(
        data,
        coordinates
        ):
    """
    Generation of a seed array.
    data could be a DataHandling isntance or a nmpy.ndarray.

    Parameters
    ----------

    array:
        Piece of data used to generate the seed array

    coordinates: numpy.ndarray
        Position of local maxima
    """
    if not isinstance(coordinates, ndarray):
        _raise_coordinates_type()
    if isinstance(data, ndarray):
        out = _seeds_generation_array(
            data,
            coordinates
            )
    elif isinstance(data, DataHandling):
        out = _seeds_generation_datahandling(
            data,
            coordinates
            )
    else:
        _raise_data_type()
    return out
