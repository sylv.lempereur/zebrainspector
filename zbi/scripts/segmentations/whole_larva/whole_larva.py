# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
Perform automatic segmentations of whole mounted 5dpf zebrafish
"""

from gc import collect as garbage_collection

from ._remove_black_pixels_on_acquisition_side import remove_black_pixels_on_acquisition_side
from ._seeds_generation import seeds_generation
from ._zones_generation import zones_generation

from ....handling import DataHandling
from ....processing.basics import (
    fusion,
    sampling
    )
from ....processing.detection import (
    bounding_box_positions,
    maxima_local
    )
from ....processing.labeling import size
from ....processing.morphology import (
    closing,
    gradient,
    opening,
    watershed
    )
from ....processing.thresholding import manual as threshold


def _watershed_sequence(
        datahandling: DataHandling,
        parameters: dict
        ):
    """
    Procedure to perform an automatic segmentation of the larva per watershed.
    If a "factor" parameter is provided, a segmentation will be performed on
    a downsampled version of the initial array of the DataHandling instance,
    and the result of this computation will be an approximative bounding bxing
    of the initial array.
    Otherwise, the result of the segmentation will be stored
    in the mask attribute of the DataHandling instance and an accurate
    bounding boxing will be applied.

    Parameters
    ----------
    datahandling: DataHandling
        DataHandling instance

    parameters: dict
        Parameters uesed to perform this first segmentation
        and bound the initial image to an approximative bounding box
        of the larva
    """
    #Store initial image
    initial = datahandling.get_current().copy()

    if "factor" in parameters:
        # Downsampling of the initial image
        sampling(
            datahandling,
            parameters["factor"]
            )
        # Store the downsampled image
        downsampling = datahandling.get_current()

    #Closing of downsampled image
    closing(
        datahandling,
        parameters["closing"],
        shape="rect"
        )

    if "opening" in parameters:
        opening(
            datahandling,
            parameters["opening"],
            shape="rect"
            )

    # Store this closed image (array used to compute gradient.)
    closed = datahandling.get_current().copy()

    #Generate zone array
    zones_generation(
        datahandling,
        threshold_inside=parameters['threshold_inside'],
        threshold_outside=parameters['threshold_outside']
        )

    if "factor" in parameters:
        #Generate seeds array
        seeds_generation(
            datahandling,
            maxima_local(downsampling)
            )
    else:
        seeds_generation(
            datahandling,
            maxima_local(initial)
            )

    # Store seeds array
    seeds = datahandling.get_current()

    # Back to the closed image
    datahandling.set_current(closed.copy())

    del closed

    gradient(datahandling)

    watershed(
        datahandling,
        seeds
        )

    del seeds

    threshold(
        datahandling,
        2
        )

    size(datahandling)

    threshold(
        datahandling,
        datahandling.get_current().max()
        )

    segmentation = datahandling.get_current().copy()

    datahandling.set_current(initial.copy())

    if "factor" in parameters:
        bound = bounding_box_positions(
            segmentation,
            tolerance=0
            )

        bound = bound / parameters["factor"]

        bound[:, 0] -= parameters["bounding_tolerance"]

        bound[:, 1] += parameters["bounding_tolerance"]

        for axis in range(len(initial.shape)):
            if bound[axis, 0] < 0:
                bound[axis, 0] = 0
            if bound[axis, 1] >= initial.shape[axis]:
                bound[axis, 1] = initial.shape[axis] - 1
        del initial
        datahandling.set_bounding_box(bound.astype('uint32'))
    else:
        del initial
        datahandling.set_larva(segmentation.copy())
        datahandling.reset_bounding_box()
        del segmentation


def whole_larva(
        dye_storage: DataHandling,
        huc_storage: DataHandling = None
        ):
    """
    Automatic segmentation of the whole larva using a watershed procedure

    Parameters
    ----------
    dye_storage: DataHandling
        Store the dye channel
    huc_storage: DataHandling
        Store the HuC channel

    See also
    --------
    grey_matter
    white_matter
    """
    parameters = {
        "downsampling": {
            "factor": 0.25,
            "closing": 5,
            "threshold_inside": 150,
            "threshold_outside": 0,
            "bounding_tolerance": 15,
            "threshold_removal": 750,
            },
        "raw":{
            "closing": 7,
            "opening": 7,
            "threshold_inside": 200,
            "threshold_outside": 50,
            "opening_2": 7,
            "closing_2": 51,
            "bounding_tolerance": 15,
            "threshold_removal": 500,
            }
        }
    if huc_storage:
        fusion(
            dye_storage,
            huc_storage,
            method="maximum"
            )
    _watershed_sequence(
        dye_storage,
        parameters["downsampling"]
        )

    _watershed_sequence(
        dye_storage,
        parameters["raw"]
        )

    dye_storage.set_current(dye_storage.get_larva().copy())

    remove_black_pixels_on_acquisition_side(
        dye_storage,
        parameters['raw']["threshold_removal"])

    closing(
        dye_storage,
        parameters["raw"]['closing_2'],
        shape="rect"
        )

    dye_storage.write_step("larva")

    # Store this result as larva segmentation for both channel.

    dye_storage.set_larva()
    if huc_storage:
        huc_storage.set_larva(dye_storage.get_larva())

    dye_storage.set_bounding_box(
        bounding_box_positions(
            dye_storage,
            tolerance=parameters["raw"]["bounding_tolerance"]
            )
        )
    if huc_storage:
        huc_storage.set_bounding_box(dye_storage.get_bounding_box())

    dye_storage.set_current(dye_storage.get_original().copy())

    garbage_collection()

    return True
    