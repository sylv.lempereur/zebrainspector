# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
Perform automatix segmentation of zebrafish brain
"""

import sys

# import gc
# import memory_profiler

import numpy
# import scipy.ndimage.measurements

from ... import processing
from ...handling import DataHandling

def grey_matter(
        huc_storage: DataHandling
        ):
    """
    Automatic segmentation of the whole larvae using a watershed procedure

    Parameters
    ----------
    dye_storage: DataHandling
        Store the HuC channel

    See also
    --------
    eye
    myelin
    whole_larvae_per_threshold
    whole_larvae_per_watershed
    """

    def _pretreatment(huc_storage):
        """
        Perform a image treatment to the given image
        to facilitate image analysis.

        Parameters
        ----------
        huc_storage: DataHandling

        Returns
        -------
        bool
            exit status
        """

        processing.morphology.tophat(
            huc_storage,
            rad=5,
            method="black",
            shape='rect'
            )

        # huc_storage.write_step()

        return True

    def _zone_detection(huc_storage):
        """
        Detect internal and external zone of the grey matter

        Parameters
        ----------
        huc_storage: DataHandling

        Returns
        bool
            Exit status
        """

        processing.thresholding.otsu(
            huc_storage,
            4
            )

        # huc_storage.write_step()

        processing.thresholding.manual(
            huc_storage,
            3
            )

        # huc_storage.write_step()

        processing.morphology.closing(
            huc_storage,
            15,
            shape='rect')

        # huc_storage.write_step()

        processing.morphology.opening(
            huc_storage,
            15,
            shape='rect')

        # huc_storage.write_step()

        return True

    print('segmentation of hu')
    local = huc_storage.get_current().copy()

    _pretreatment(huc_storage)

    _zone_detection(huc_storage)

    sys.exit()

    huc_storage.write_step()

    test = huc_storage.get_current().copy()

    huc_storage.set_current(local)

    processing.morphology.closing(
        huc_storage,
        15,
        shape='rect')

    huc_storage.write_step()

    huc_storage.set_current(huc_storage.get_current()-test)

    del test

    huc_storage.write_step('ar')

    processing.thresholding.otsu(
        huc_storage,
        5
        )

    otsu = huc_storage.get_current().copy()

    huc_storage.write_step()

    processing.thresholding.manual(
        huc_storage,
        4
        )

    huc_storage.write_step()

    zones = 2 * huc_storage.get_current().copy()

    huc_storage.set_current(otsu)

    processing.thresholding.manual(
        huc_storage,
        1)

    processing.basics.inversion(huc_storage)

    processing.morphology.erosion(
        huc_storage,
        15,
        shape='rect'
        )

    zones += huc_storage.get_current().copy()

    huc_storage.set_current(zones)
    huc_storage.write_step("zones")
    # huc_storage.set_current(local)

    # # processing.morphology.opening(
    # #     huc_storage,
    # #     5,
    # #     shape='rect'
    # #     )

    # processing.morphology.closing(
    #     huc_storage,
    #     5,
    #     shape='rect'
    #     )


    # huc_storage.write_step()

    # #Get the position of elment of interest
    # processing.thresholding.otsu(
    #     huc_storage,
    #     5
    #     )

    # clusters = huc_storage.get_current()

    # huc_storage.write_step()

    # processing.thresholding.manual(
    #     huc_storage,
    #     3
    #     )

    # # processing.labeling.size(
    # #     huc_storage
    # #     )

    # # processing.thresholding.manual(
    # #     huc_storage,
    # #     huc_storage.get_current().max()
    # #     )

    # processing.morphology.opening(
    #     huc_storage,
    #     11,
    #     shape='rect'
    #     )

    # otsu = huc_storage.get_current().copy()

    # # otsu = processing.morphology.erosion(
    # #     otsu,
    # #     15,
    # #     shape='rect'
    # #     )

    # huc_storage.set_current(clusters)

    # processing.thresholding.manual(huc_storage,1)

    # processing.basics.inversion(huc_storage)

    # processing.morphology.erosion(
    #     huc_storage,
    #     51,
    #     shape='rect'
    #     )

    # zones = huc_storage.get_current() + 2 * otsu.copy()

    # huc_storage.set_current(zones)
    # huc_storage.write_step('zones')

    coordinates = processing.detection.maxima_local(
        huc_storage.get_original().copy()
        )

    markers = numpy.zeros(local.shape)
    markers[coordinates] = 1
    markers *= zones

    huc_storage.set_current(markers)
    huc_storage.write_step('markers')

    markers = markers.astype('uint8')


    huc_storage.set_current(local)

    processing.morphology.opening(
        huc_storage,
        5,
        shape='rect'
        )

    huc_storage.write_step()

    processing.morphology.gradient(
        huc_storage,
        shape='rect'
        )

    huc_storage.write_step()

    processing.morphology.watershed(
        huc_storage,
        markers
        )

    huc_storage.write_step()

    processing.thresholding.manual(
        huc_storage,
        2
        )

    huc_storage.write_step('test')

    # processing.labeling.size(huc_storage)

    # processing.thresholding.manual(
    #     huc_storage,
    #     huc_storage.get_current().max()
    #     )

    # huc_storage.write_step('grey_matter')

    sys.exit()

    return True
