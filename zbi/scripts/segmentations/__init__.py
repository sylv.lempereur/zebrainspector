"""
Segmentations scripts
=====================

================================== =============================================
segmentations
================================================================================
white_matter                       Segment white matter of 5dpf larvae
whole_larva                        Segment whole larvae using watershed
================================== =============================================
"""

from .grey_matter import grey_matter
from .white_matter import white_matter
from .whole_larva import whole_larva
