"""
contains functions used to compute second moment matrix
"""

from numpy import array as nparray
from numpy import sum as npsum
from numpy import (
    arange,
    count_nonzero,
    ndarray,
    zeros
    )

def _compute_second_moments(
        array: ndarray,
        general
        ):
    """
    Compute second moment matrix and translation vector of a provided array.

    Parameters
    ----------
    array: numpy.ndarray

    general: dict
        contains several values use to generate the second moment matrix

    Returns
    -------
    numpy.ndarray
        translation vector

    numpy.ndarray
        Second moment matrix

    """

    # Compute sum for each direction
    sums = {
        "sli": npsum(
            array,
            axis=0
            ),
        "row": npsum(
            array,
            axis=1
            ),
        "col": npsum(
            array,
            axis=2
            ),
        "sli_row": npsum(
            array,
            axis=(0, 1)
            ),
        "sli_col": npsum(
            array,
            axis=(0, 2)
            ),
        "row_col": npsum(
            array,
            axis=(1, 2)
            ),
    }

    # Compute coefficients for each directions
    coefficients = {
        "sli" : npsum(
            sums["row_col"]
            * general["multiplicator"][:, 1, 1]
            ),
        "row" : npsum(
            sums["sli_col"]
            * general["multiplicator"][1, :, 1]
            ),
        "col" : npsum(
            sums["sli_row"]
            * general["multiplicator"][1, 1, :]
            ),
        "sli2": npsum(
            sums["row_col"]
            * general["multiplicator"][:, 1, 1]
            * general["multiplicator"][:, 1, 1]
            ),
        "row2": npsum(
            sums["sli_col"]
            * general["multiplicator"][1, :, 1]
            * general["multiplicator"][1, :, 1]
            ),
        "col2": npsum(
            sums["sli_row"]
            * general["multiplicator"][1, 1, :]
            * general["multiplicator"][1, 1, :]
            ),
        "sli_row": npsum(
            sums["col"]
            * general["multiplicator"][:, :, 1]
            ) / general["mean"],
        "sli_col": npsum(
            sums["row"]
            * general["multiplicator"][:, 1, :]
            ) / general["mean"],
        "row_col": npsum(
            sums["sli"]
            * general["multiplicator"][1, :, :]
            ) / general["mean"],
    }

    # Compute center of mass of the image
    center_z = (
        coefficients["sli"]
        / general["volume"]
        )
    center_x = (
        coefficients["row"]
        / general["volume"]
        )
    center_y = (
        coefficients["col"]
        / general["volume"]
        )

    # Compute second moment coefficients
    u200 = (
        coefficients["sli2"]
        - coefficients["sli"] * center_z
        + general["volume"] * 12
        )
    u020 = (
        coefficients["row2"]
        - coefficients["row"] * center_x
        + general["volume"] * 12
        )
    u002 = (
        coefficients["col2"]
        - coefficients["col"] * center_y
        + general["volume"] * 12
        )

    u110 = (
        coefficients["sli_row"]
        - coefficients["sli"] * center_x
        )

    u101 = (
        coefficients["sli_col"]
        - coefficients["sli"] * center_y
        )

    u011 = (
        coefficients["row_col"]
        - coefficients["row"] * center_y
        )

    # Populate the second moment matrix
    second_moment_matrix = zeros(
        (3, 3),
        'float64'
        )

    second_moment_matrix[0, 0] = u200
    second_moment_matrix[1, 1] = u020
    second_moment_matrix[2, 2] = u002
    second_moment_matrix[1, 0] = second_moment_matrix[0, 1] = u110
    second_moment_matrix[2, 0] = second_moment_matrix[0, 2] = u101
    second_moment_matrix[1, 2] = second_moment_matrix[2, 1] = u011

    print(center_z, center_x, center_y)

    # Creation of the translation vector
    translation = nparray(
        [
            center_z - array.shape[0] / 2,
            center_x - array.shape[1] / 2,
            center_y - array.shape[2] / 2
            ]
        )

    return (
        nparray(([center_z, center_x, center_y])),
        translation,
        second_moment_matrix
        )

def _get_mulitplicator(
        shape: tuple
        ):
    """
    Generate a multiplicator matrix used to compute moments
    for each element in multiplicator matrix, M[x, y, z] = x * y * z

    Paramaters
    ----------
    numpy.ndarray
        Mulitplication matrix
    """
    multiplicator = zeros(
        shape,
        "float32"
        )

    init = arange(0, shape[0]).astype('float32')
    multiplicator[:, 1, 1] = init
    for row in range(2, shape[1]):
        multiplicator[:, row, 1] = row * multiplicator[:, 1, 1]
    for col in range(2, shape[2]):
        multiplicator[:, :, col] = col * multiplicator[:, :, 1]
    return multiplicator

def get_translation_and_moments(array):
    """
    Compute the second moment matrix of an array
    """
    # Create computation values
    general = {
        "volume": npsum(array),
        "mean": npsum(array) / count_nonzero(array),
        "multiplicator": _get_mulitplicator(array.shape)
    }

    #Compute centroids and second moment matrix
    centroids, translation, moments = _compute_second_moments(array, general)

    del general

    return centroids, translation, moments
