"""
script.registration

Perform a registration of the larva using several generated landmarks.
"""

from .registration import registration, get_affine_matrix
