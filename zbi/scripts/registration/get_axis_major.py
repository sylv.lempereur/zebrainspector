"""
Get major axis of the larva.
"""
import sys

from numpy import array as nparray
from numpy import (
    argmax,
    asarray,
    dot,
    ndarray,
    sqrt,
    zeros
    )
from numpy.linalg import eig, norm
from scipy.ndimage.measurements import center_of_mass

from skimage.draw import ellipsoid

from .compute_2nd_moment import get_translation_and_moments
from ...processing.labeling import size
from ...processing.morphology import (
    closing,
    opening
    )
from ...processing.thresholding import (
    manual,
    manual_percent_histo
    )

def get_axis_major(
        array: ndarray
        ):
    """
    Compute main axis of the larva

    Parameters
    ----------
    array:numpy.ndarray

    Returns
    -------
    numpy.ndarray
        Translation vector

    numpy.ndarray
        Major vector of the larva
    """

    def _ellipsoid_equation(
            array,
            eigen_values,
            eigen_vector,
            centroid
        ):

        # r = sqrt(eigen_values)
        # print(r)
        # return ellipsoid(r[0], r[1], r[2])
        ell = zeros(array.shape, "float32")
        for sli in range(0, array.shape[0]):
            for col in range(0, array.shape[1]):
                for row in range(0, array.shape[2]):
                    ellipsoid_value = nparray(
                        [
                            sli - centroid[0],
                            col - centroid[1],
                            row - centroid[2]
                            ]
                        )
                    ell[sli, col, row] = dot(
                        ellipsoid_value,
                        dot(
                            eigen_vector,
                            ellipsoid_value
                            )
                        )
            if sli == 10:
                sys.exit()
        return ell >= 0

    def _verify_orientation(
            center_of_white_matter: ndarray,
            axis_major: ndarray,
            center_of_larva: ndarray
        ):
        """
        Orient the main axis.

        Parameters
        ----------
        center_of_white_matter: numpy.ndarray
            Center of the white matter segmentation

        axis_major: numpy.ndarray
            Eigen vector of the main axis

        center_of_larva: numpy.ndarray
            Center of the larva segmentation
        """
        # Compute vector from center of the white matter
        # to the center of the larva
        vector_to_project = center_of_larva - center_of_white_matter
        # Compute projection of this vector on the axis major.
        vector_projection = dot(
            vector_to_project,
            axis_major
            ) / dot(
                axis_major,
                axis_major
                ) * axis_major

        # Check orientation compatibility
        # to ensure that main axis will point to the left of the image.
        # If the center of the larva is on the left
        # compare to the center of the white matter
        axis_major_sign = (
            axis_major[2]
            / abs(axis_major[2])
            )
        vector_projection_sign = (
            vector_projection[2]
            / abs(vector_projection[2])
            )
        if vector_projection_sign == axis_major_sign:
            # Axis major main orientation should be negative
            axis_major *= -1
        return axis_major

    array = opening(
        array,
        5,
        "rect"
        )

    array = closing(
        array,
        5,
        "rect"
        )

    whole_larva = manual_percent_histo(
        array,
        5
        )

    center_of_mass_whole_larva = asarray(center_of_mass(whole_larva))
    del whole_larva

    # Create a rough segmentation of teh white matter
    array = manual_percent_histo(
        array,
        90
        )

    center_of_mass_white_matter = asarray(center_of_mass(array))

    #Remove small elements
    array = size(
        array
        )

    array = manual(
        array,
        array.max()
        )

    # Get translation matric and covariance matrix
    centroids, translation, moments = get_translation_and_moments(array)
    # del array

    #Compute axis length and axis directions
    values_eigen, vectors_eigen = eig(moments)

    # Get major axis
    index = argmax(values_eigen)
    axis_major = vectors_eigen[:, index]

    del index, values_eigen, vectors_eigen

    axis_major = _verify_orientation(
        center_of_white_matter=center_of_mass_white_matter,
        axis_major=axis_major,
        center_of_larva=center_of_mass_whole_larva
        )
    return centroids, translation, axis_major
