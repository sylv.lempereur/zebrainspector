"""
Generate a rough segmentation of the whole larva
"""

from numpy import ndarray

from ...processing.labeling import size
from ...processing.morphology import (
    closing,
    remove_holes
    )
from ...processing.thresholding import manual

def _rough_larva_detection(
        array: ndarray,
        pigmented_eyes: bool = False
        ):
    """
    Perform a rough segmentation of the provided array
    to get the position of the larva.

    Parameters
    ----------
    array: numpy.ndarray

    Returns
    larva: numpy.ndarray
        Rough segmentation of the larva
    """
    # Low threshold to get the maximum amount of the image
    if pigmented_eyes:
        thresh = 50
    else:
        thresh = 100
    larva = manual(
        array,
        thresh
        )

    if pigmented_eyes:
        larva = closing(
            larva,
            5,
            "rect"
            )

    # Get the size of each segmented object
    larva = size(larva)

    # Keep only the biggest object
    larva = manual(
        larva,
        larva.max() - 1
        )

    if pigmented_eyes:
        rad_closing = 51
    else:
        rad_closing = 25
    # Perfor a closing step to fulfil this segmentation
    larva = closing(
        larva,
        rad_closing,
        'rect'
        )

    larva = remove_holes(larva)
    return larva
    