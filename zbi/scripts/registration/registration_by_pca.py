
from numpy import (
    argmin,
    argmax,
    array as nparray,
    ndarray,
    percentile,
    where,
    zeros
    )

from scipy.ndimage import center_of_mass
from sklearn.decomposition import PCA
    
from zbi.processing.morphology import (
    closing,
    opening, 
    opening_geodesic
    )

def registration_by_pca(array: ndarray):
    """
    perform a registration by computing PCA of a portion of the image.

    Parameters
    ----------
    array: numpy.ndarray

    Returns
    -------
    numpy.ndarray
        Registered array
    """

    def _prepocessing(array: ndarray):
        """
        Perform preprocessing of the data to reduce noise

        Parameters
        ----------.
        array: numpy.ndarray

        Returns
        -------
        numpy.ndarray
            Prepocess array
        """
        array_out = opening(array,10,"rect")
        array_out = array_out > percentile(array_out[array_out!=0], 90)
        array_out = opening_geodesic(array_out, 10, "rect")
        array_temp = array.copy()
        array_temp *= array_out 
        return  array_temp

    def _generate_points(array):
        """
        Generate list of points uses to compute the PCA

        Parameters
        ----------
        array: numpy.ndarray

        Returns
        numpy.ndarray
            Array of points. for each example, points looks like:
            (x,y,z ,gray value of array[x, y, z])
        """
        positions = where(array != 0)
        points = zeros((len(positions[0]), 4), 'float32')
        points[:,0] = positions[0]
        points[:,1] = positions[1]
        points[:,2] = positions[2]
        points[:,3] = (array[positions] - array[positions].min()) / (array[positions].max() - array[positions].min())
        del positions
        return points

    def _compute_PCA(points: ndarray, dimension: int):
        """
        Compute PCA of the given points to reduce dimensions of its

        Parameters
        ----------
        points: numpy.ndarray
            List of points

        dimension: int
            Wanted number of dimension for the PCA

        Returns
        -------
        centroids: numpy.ndarray
            Position of the centroids of the result of the PCA

        eigen_vectors: numpy.ndarray
            Wanted eigen vectors

        eigen_values: numpy.ndarray
            Values of the eigen vector for each wanted dimension
        """
        pca = PCA(n_components = dimension)

        pca.fit(points)
        pca.transform(points)

        eigen_vectors = pca.components_.T[:dimension,:dimension]
        eigen_values = pca.explained_variance_ratio_[:3]
        centroids = pca.mean_[:3]

        del pca

        return centroids, eigen_vectors, eigen_values

    def _generate_rotation_matrix(
            array: ndarray,
            eigen_values: ndarray,
            eigen_vectors: ndarray,
            centroids: ndarray
            ):
        """
        """

        temp = opening(array, 10, "rect")
        temp = temp > percentile(temp[temp!=0], 10)
        temp = closing(temp, 10 , "rect")

        centroids_larva = center_of_mass(temp)

        del temp

        # Check vector's order

        directions = [0, 0, 0]
        directions[0] = argmax(array.shape)
        directions[2] = argmin(array.shape)
        directions[1] = 3 - (directions[0] + directions[2])

        print(directions)

        index = [0, 0, 0]

        index[0] = argmax(eigen_values)
        index[2] = argmin(eigen_values)
        index[1] = 3 - (index[0] + index[2]) 

        print(index)

        # Compute orientation for each direction.

        print(eigen_vectors)

        signes_orientation = zeros((3),"int8")

        for i in range(0,3):
            print(eigen_vectors[directions[i],:])
            print(eigen_vectors[directions[i],index[i]])
            signes_orientation[i] = eigen_vectors[directions[i],[index[i]]] / abs(eigen_vectors[directions[i],[index[i]]]) 
            print(signes_orientation)

        ## Check orientation of main axis. The sample should point to the left of the image.

        vector = centroids - centroids_larva
        print("centroids")
        print(centroids)
        print("centroids larva")
        print(centroids_larva)
        print("vector")
        print(vector)
        print("signes_orientation")
        print(signes_orientation)
        print("eigen_vectors")
        print(eigen_vectors)

        projected_on_main = vector.dot(
            eigen_vectors[:, index[directions[0]]]
            ) / eigen_vectors[:, index[directions[0]]].dot(eigen_vectors[:, index[directions[0]]]) * eigen_vectors[:, index[directions[0]]]

        orientation_maj = projected_on_main[directions[0]] / abs(projected_on_main[directions[0]])
        print(orientation_maj)

        print(eigen_vectors)

        if (orientation_maj == signes_orientation[0]):
            eigen_vectors[:,index[directions[0]]] *= -1
            eigen_vectors[:,index[directions[1]]] *= -1

        projected_on_main = vector.dot(
            eigen_vectors[:, index[directions[2]]]
            ) / eigen_vectors[:, index[directions[2]]].dot(eigen_vectors[:, index[directions[2]]]) * eigen_vectors[:, index[directions[2]]]

        orientation_min = projected_on_main[directions[2]] / abs(projected_on_main[directions[2]])
        print(orientation_maj)

        print(eigen_vectors)

        if (orientation_min == signes_orientation[1]):
            eigen_vectors[:,index[directions[2]]] *= -1
            eigen_vectors[:,index[directions[1]]] *= -1

        print(eigen_vectors)

        rotation_matrix = zeros(
            (3,3),
            "float64"
            )

        rotation_matrix[:3, directions[0]] = eigen_vectors[:, index[0]]
        rotation_matrix[:3, directions[1]] = eigen_vectors[:, index[1]]
        rotation_matrix[:3, directions[2]] = eigen_vectors[:, index[2]]

        direction_maj = directions[0]

        vector = centroids_larva - centroids
        print(vector)
        print(direction_maj)

        del centroids_larva, directions, index

        print(eigen_vectors)

        return rotation_matrix, direction_maj

    def _compute_affine_matrix(
            array: ndarray,
            eigen_vectors: ndarray,
            eigen_values: ndarray,
            centroids: ndarray
            ):
        """
        Compute affine transform uses to perform the rotation of the image

        Parameters
        ----------
        array: numpy.ndarray

        eigen_vectors: numpy.ndarray
            Array of the eigen vectors given by the PCA computation

        eigen_values: numpy.ndarray
            Dimension of each vector in the eigen vector

        Returns
        -------
        numpy.ndarray
            Rotation matrix

        numpy.ndarray
            Translation matrix
        """

        rotation_matrix, direction_maj = _generate_rotation_matrix(
            array,
            eigen_values,
            eigen_vectors,
            centroids
            )

        translation_matrix = nparray(
            [
                array.shape[0] / 2 - centroids[0],
                array.shape[1] / 2 - centroids[1],
                array.shape[2] / 2 - centroids[2],
                ]
            )

        translation_matrix[direction_maj] -= array.shape[direction_maj] / 4

        del direction_maj

        return rotation_matrix, translation_matrix

    array_pre = _prepocessing(array)
    points = _generate_points(array_pre)

    del array_pre

    centroids, eigen_vectors, eigen_values = _compute_PCA(
        points,
        len(array.shape)
        )

    del points
    rotation, _ = _compute_affine_matrix(
        array,
        eigen_vectors,
        eigen_values,
        centroids
        )

    del eigen_values, eigen_vectors
    return rotation, centroids