"""
Project lens to lens vector into main plane and compute third axis.
"""

from numpy import (
    cross,
    ndarray
    )

from numpy.linalg import norm

def project_main_on_lens(
        axis_lens: ndarray,
        axis_main: ndarray
        ):
    """
    Compute projection of axis_lens on the plan define by axis_main
    and get the third axis.

    Parameters
    ----------
    axis_main: numpy.ndarray
        Main vector of the larva

    axis_lens: numpy.ndarray
        Left lens to right lens vector

    Parameters
    ----------
    numpy.ndarray
        Projected lens vector

    numpy.ndarray
        Third vector
    """
    # Projection of the main axis on the plan defined by the lens axis
    cross_lens_main_raw = cross(axis_lens, axis_main)
    axis_main = cross(axis_lens, cross_lens_main_raw)
    axis_main /= norm(axis_main)
    del cross_lens_main_raw

    # Computation of the third axis as the cross product of the two previously defined ones.
    axis_third = cross(axis_main, axis_lens)

    return axis_main, axis_third
