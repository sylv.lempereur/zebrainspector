"""
Get axis gpons from left lens to right lens.
"""

from numpy import array as nparray
from numpy import (
    argmax,
    count_nonzero,
    ndarray,
    pi,
    unique
    )

from numpy.linalg import norm

from scipy.ndimage.measurements import center_of_mass

from skimage.measure import (
    marching_cubes_lewiner,
    mesh_surface_area
    )

from ...processing.basics import (
    frame_creation,
    inversion
    )
from ...processing.labeling import (
    label,
    size
    )
from ...processing.morphology import (
    dilation_geodesic,
    gradient,
    opening,
    opening_geodesic,
    watershed
    )
from ...processing.thresholding import (
    manual,
    manual_percent_histo
    )

from ._rough_larva_detection import _rough_larva_detection

def _lens_detection(
        array: ndarray,
        pigmented_eyes: bool = False
        ):
    """
    Detect lenses of the sample and return the center of mass of the larva.

    Parameters
    ----------
    array: numpy.ndarray

    Returns
    -------
    numpy.ndarray
        Segmentation of lenses

    numpy.ndarray
        Position of the center of mass of the larva
    """

    # Get a rough segmenation of the larva
    larva = _rough_larva_detection(
        array,
        pigmented_eyes
        )

    array[larva == 0] = 0

    # Get every pixel lower than 1% of the histogram into the larva
    seeds_in = manual_percent_histo(array, 1)
    seeds_in = inversion(seeds_in)
    seeds_in *= larva != 0

    if pigmented_eyes:
        print("thresh = 50%")
        seeds = manual_percent_histo(array, 30)
    else:
        print("thresh = 30%")
        seeds = manual_percent_histo(array, 50)

    seeds[larva == 0] = 1
    seeds = opening_geodesic(
        seeds,
        5,
        'rect'
        )
    seeds = seeds.astype('uint8')
    seeds[seeds_in == 1] = 2

    array_gradient = gradient(array)
    # return gradient.astype("uint16"), None

    # Perform a closing to fulfill the shape

    lens = watershed(
        data=array_gradient,
        markers=seeds
        )

    del seeds

    lens = manual(lens, 2)

    # if pigmented_eyes:
    #     lens = opening(
    #         lens,
    #         5
    #         )

    # frame = frame_creation(lens)
    # frame = dilation_geodesic(
    #     frame,
    #     lens
    #     )
    # lens[frame != 0] = 0

    # Remove unspherical elements by computation of sphericity
    lens = lens.astype("bool")
    if pigmented_eyes:
        lens = opening(
            lens,
            5,
            )
    else:
        lens = opening(
            lens,
            5,
            "rect"
            )        
    lens, num_features = label(lens)

    sphericities = lens.copy() * 0

    # For each object in the segmentation, keep only spheric elements
    for label_value in range(1, 1 + num_features):
        array_label = lens == label_value
        sphericity = _compute_sphericity(array_label)
        sphericities[lens == label_value] = 1000 * sphericity
        if sphericity < 0.8:
            lens[lens == label_value] = 0
        else:
            lens[lens == label_value] = 1
    # Remove unspherical elements by computation of sphericity
    # Compute size of remaining elements
    lens = size(lens.astype('bool'))
    sizes = sorted(unique(lens))
    if len(sizes) < 2:
        return False, False

    # keep only the two biggest elements
    lens = manual(
        lens,
        sizes[-2]
        )

    return lens, center_of_mass(larva)

def _compute_sphericity(
        array: ndarray
        ):
    """
    Compute sphericity of non black pixel of an image

    Parameters
    ----------
    array_volume: numpy.ndarray

    array_surface: numpy.ndarray

    Returns
    -------
    float
        Sphericity of the object
    """
    def _compute_surface(array):
        """
        Compute surface of object in the given array.
        This surface is obtained by computing the surface of isosurfaces.
        These isosurfaces are get using a marching cube computation.

        Parameters
        ----------
        array: numpy.ndarray

        Returns
        -------
        float:
            surface of the object


        """
        verts, faces, _, _ = marching_cubes_lewiner(array)
        return mesh_surface_area(verts, faces)

    volume = count_nonzero(array)
    surface = _compute_surface(array)
    sphericity = (
        (pi ** (1 / 3))
        * ((6 * volume) ** (2 / 3))
        ) / surface
    return sphericity

def get_axis_lens(
        array: ndarray,
        pigmented_eyes: bool = False
        ):
    """
    Return vector pointing from left to right eye.

    Parameters
    ----------
    array: numpy.ndarray

    Returns
    -------
    numpy.ndarray
        Vector from left to right eye

    numpy.ndarray
        Center of mass of the larva
    """

    # Get segmentation of lens and center of mass of the larva
    lens, center_of_mass_larva = _lens_detection(
        array,
        pigmented_eyes
        )

    if center_of_mass_larva is None:
        return lens, None, None
    elif lens is False:
        return False, False, False

    # Compute centroids of lens
    centroids_lens = [[], [], []]
    lens, num_features = label(lens)
    for label_value in range(1, 1 + num_features):
        center_lens = (center_of_mass(lens == label_value))
        centroids_lens[0].append(center_lens[0])
        centroids_lens[1].append(center_lens[1])
        centroids_lens[2].append(center_lens[2])

    # Generate axis going from one lens to the other
    axis_lens = nparray([
        centroids_lens[0][1] - centroids_lens[0][0],
        centroids_lens[1][1] - centroids_lens[1][0],
        centroids_lens[2][1] - centroids_lens[2][0]
        ])

    center_of_eyes = nparray([
        (centroids_lens[0][1] + centroids_lens[0][0]) / 2,
        (centroids_lens[1][1] + centroids_lens[1][0]) / 2,
        (centroids_lens[2][1] + centroids_lens[2][0]) / 2
        ])
    print(center_of_eyes)

    #Normalisation of this vector
    axis_lens /= norm(axis_lens)

    # Check if this axis goes from right to left
    # So, highest value of this axis should be positive.
    direction_main = argmax(abs(axis_lens))
    if axis_lens[direction_main] < 0:
        axis_lens *= -1

    return axis_lens, center_of_mass_larva, center_of_eyes
