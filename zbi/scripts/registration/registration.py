"""
Contains functions used to registered image with main axis.
"""

from datetime import datetime
from numpy import (
    argmax,
    asarray,
    eye,
    ndarray,
    rot90,
    transpose,
    zeros
    )
from numpy import array as nparray
from numpy import isclose, where

from SimpleITK import GetImageFromArray, WriteImage

from affine_transform import transform

from ...processing.basics import sampling

from .get_axis_major import get_axis_major

from .get_axis_lens import get_axis_lens

from .project_main_on_lens import project_main_on_lens

from .registration_by_pca import registration_by_pca

def _align_to_main_axis(
        array: ndarray,
        main_axis: int = 2
        ):
    """
    Compute a rotation of the entire array to align it with the main axis

    Parameters
    ----------
    array: numpy.ndarray

    main_axis: int
        Wanted orientation of the main axis
    """
    if main_axis != argmax(array.shape):
        array = rot90(
            m=array,
            axes=(
                main_axis,
                argmax(array.shape)
                )
            )
    return array

def _generate_affine_matrix(
        shape: ndarray,
        translation: ndarray,
        axis_main: ndarray,
        axis_lens: ndarray,
        axis_third: ndarray
        ):
    """
    Compute affine transform matrix

    Parameters
    ----------
    shape: numpy.ndarray
        Shape of the array to transform

    translation: numpy.ndarray
        translation vector

    axis_main: numpy.ndarray
        Main axis of the larva

    axis_len: numpy.ndarray
        Left lens to right lens axis

    axis_third: numpy.ndarray
        Third axis of the rotation matrix

    Returns
    -------
    numpy.ndarray
        Affine transform matrix
    """

    direction_main = argmax(shape)
    if direction_main == 1:
        direction_lens = 2
    else:
        direction_lens = 1
    print(direction_main)
    # Generate affin matrix
    matrix_affine = eye(4, dtype="float32")

    # Populate the rotation part of the affine transform matrix
    matrix_affine[:3, direction_main] = axis_main
    matrix_affine[:3, direction_lens] = axis_lens
    matrix_affine[:3, 0] = axis_third

    # Populate the translation part of the affine transform matrix
    for axis in range(0, 3):
        matrix_affine[axis, 3] = shape[axis] / 2
        matrix_affine[axis, 3] += (
            translation[axis]
            - shape[0] / 2 * matrix_affine[axis, 0]
            - shape[1] / 2 * matrix_affine[axis, 1]
            - shape[2] / 2 * matrix_affine[axis, 2]
            )
    return matrix_affine

def _orient_axis_third(
        array: ndarray,
        axis_main: ndarray,
        axis_third: ndarray,
        translation: ndarray,
        center_of_mass_larva: ndarray
        ):
    """
    Orient the third axis.

    Parameters
    ----------
    array: numpy.ndarray

    axis_third: numpy.ndarray
        Vector to orient

    translation: numpy.ndarray
        Translation vector

    center_of_mass_larva: numpy.ndarray
        Center of mass of the larve in array
    """

    orientation_third_major = argmax(axis_third)

    axis_major = argmax(axis_main)

    if axis_main[axis_major] < 0:
        if axis_third[orientation_third_major] > 0:
            axis_third *= -1
    else:
        if axis_third[orientation_third_major] < 0:
            axis_third *= -1

    center_of_mass_white_matter = zeros(
        3,
        "float32"
        )

    for axis in range(3):
        if axis == axis_major:
            center_of_mass_white_matter[axis] = array.shape[axis] / 2
        else:
            center_of_mass_white_matter[axis] = array.shape[axis] / 2
        center_of_mass_white_matter += translation[axis]

    # Downsample by a factor 4 to get the same scale as array
    center_of_mass_white_matter /= 4

    # Get distance of the center of the larva to the plane generate by the third axis.
    plane_center_of_larva = (
        axis_third[0] * (center_of_mass_larva[0] - center_of_mass_white_matter[0])
        + axis_third[1] * (center_of_mass_larva[1] - center_of_mass_white_matter[1])
        + axis_third[1] * (center_of_mass_larva[1] - center_of_mass_white_matter[1])
        )

    if plane_center_of_larva != 0:
        side_center_of_larva = plane_center_of_larva / abs(plane_center_of_larva)
    else:
        side_center_of_larva = axis_third[orientation_third_major]
        side_center_of_larva /= abs(side_center_of_larva)

    side_third_axis = axis_third[orientation_third_major]
    side_third_axis /= abs(side_third_axis)

    if side_third_axis != side_center_of_larva:
        axis_third *= -1

    return axis_third

def _registration_using_affine_transform(
        array: ndarray,
        matrix_affine: ndarray,
        centroids
        ):
    """
    Compute an affine transform.

    Parameters
    ----------
    array: numpy.ndarray
        Array to transform

    matrix_affine: numpy.ndarray
        Affine transform matrix
    """
    array = array.astype("float32")

    matrix_rotation = transpose(matrix_affine[:3, :3].copy().astype('float64'))

    translation = nparray(
        [
            array.shape[0] / 2 - centroids[0],
            array.shape[1] / 2 - centroids[1],
            array.shape[2] / 2 - centroids[2],
            ]
        )

    direction_main = argmax(array.shape)

    translation[direction_main] -= array.shape[direction_main] / 4

    del direction_main

    # Compute affine transform
    array = transform(
        array,
        matrix_rotation,
        translation,
        origin=centroids
        )
    array[array < 0] = 0
    return array.astype("uint16")

def _debug(
        debug: bool,
        title: str = None
    ):
    """
    If debug is true print given string and time point.

    Parameters
    ----------
    debug: bool
        Debug status.

    title: str
        (Optionnal) Title to write
    """
    if debug:
        print(title + '\n\t' + datetime.now().strftime("%H:%M:%S"))
    return True

def get_affine_matrix(
        array: ndarray,
        RegistrationMethod: int,
        debug: bool = False
        ):
    if (RegistrationMethod != 0):
        pigmented_eyes = RegistrationMethod == 2

        """
        Compute affine matrix use to perform registration

        Parameters
        ----------
        array: numpy.ndarray
            Reference channel use to compute affine matrix

        Returns
        -------
        numpy.ndarray
            Transformation matrix
        """
        # array = _align_to_main_axis(array)

        _debug(
            debug,
            "axis_major"
            )
        centroid, translation, axis_main = get_axis_major(array)

        if axis_main is None:
            return translation

        _debug(
            debug,
            "downsampling"
            )
        # downsampling to reduce computation time
        downsampled = sampling(
            array,
            0.25
            )

        # return downsampled

        _debug(
            debug,
            "lens"
            )
        # Get lens to lens vector and center of mass of the larva
        axis_lens, center_of_mass_larva, center_of_lens = get_axis_lens(
            downsampled,
            pigmented_eyes
            )
        center_of_lens = asarray(center_of_lens)
        if axis_lens is False:
            return "ERROR. Only one lens was detected."
        if center_of_mass_larva is None:
            return axis_lens
        print(center_of_lens)
        center_of_lens *= 4
        print(center_of_lens)

        _debug(
            debug,
            "project"
            )
        # Project main axis on lens to lens plan and get the third axis vector
        axis_main, axis_third = project_main_on_lens(
            axis_lens,
            axis_main
            )

        _debug(
            debug,
            "third"
            )
        # Orientation of the third axis vector.

        axis_third = _orient_axis_third(
            downsampled,
            axis_main,
            axis_third,
            translation,
            center_of_mass_larva
            )

        _debug(
            debug,
            "affine"
            )
        # Generate affine matrix
        matrix_affine = _generate_affine_matrix(
            array.shape,
            translation,
            axis_main,
            axis_lens,
            axis_third
            )
    else:
        matrix_affine, center_of_lens = registration_by_pca(array)

    return matrix_affine, center_of_lens

def registration(
        array: ndarray,
        matrix_affine: ndarray,
        centroid: ndarray,
        debug: bool = False
        ):
    """
    Registration of the provided image using a given affine matrix

    Parameters
    ----------
    array: numpy.ndarray
        array to register
    matrix_affine: numpy.ndarray
        Affine transform matrix
    centroid: numpy.ndarray
        Position of the centroid of the white matter

    debug: bool
        Print debug infos

    Returns
    -------
    numpy.ndarray: registered array
    """
    # array = _align_to_main_axis(array)
    _debug(
        debug,
        "transform"
        )

    # Perform affine transform of the original array
    array_registered = _registration_using_affine_transform(
        array,
        matrix_affine,
        centroid
        )

    _debug(
        debug,
        "Ended"
        )

    return array_registered
