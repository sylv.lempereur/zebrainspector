# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
Perform segmentation-based depth-dependant contract correction
"""

from numpy import unique

from ..handling import DataHandling
from ..processing import contrast_correction
from ..processing.basics import inversion
from ..processing.labeling import depth
from ..processing.morphology import dist

def depth_contrast_correction(
        dye_storage: DataHandling
        ):
    """
    Compute contrast correction of huC and diI channelrs.

    Parameters
    ----------
    dye_storage: DataHandling
        Storage of the DiI channel

    huc_storage: DataHandling
        Storage of the HuC channel
    """
    print('Image improvement')
    ##Detection of the depth of each pixel
    if dye_storage.get_larva() is not None:
        dye_storage.set_current(dye_storage.get_larva())
    elif dye_storage.get_current().dtype != 'bool':
        raise TypeError(
            "At this step, a seglentation of the larva have to be store"
            +" or the current step should be a boolean array"
            )

    depth(dye_storage)

    # Set his mask to the Hu
    # huc_storage.set_current(dye_storage.get_current())

    contrast_correction.segmentation_based_depth_dependant(
        dye_storage
        )

    dye_storage.write_step('contrastCorrected')

    # processing.contrast_correction.segmentation_based_depth_dependant(
    #     huc_storage
    #     )

    # huc_storage.write_step('contrastCorrected')

    return True

def labeling_contrast_correction(
        datahandling: DataHandling
        ):
    """
    In development
    """
    print('Labeling correction')

    print(datahandling.get_larva().dtype)
    print(unique(datahandling.get_larva()))

    if datahandling.get_larva().dtype != "bool":
        print(len(unique(datahandling.get_larva())))
        if len(unique(datahandling.get_larva())) == 2:
            datahandling.set_larva(datahandling.get_larva().copy().astype('bool'))
        else:
            raise TypeError("should be a boolean array")
    if datahandling.get_current().dtype != 'bool':
        datahandling.set_current(datahandling.get_larva().copy())
        inversion(datahandling)

    dist(datahandling)

    datahandling.write_step()

    contrast_correction.segmentation_based_depth_dependant(
        datahandling
        )

    datahandling.write_step("labelCorrection")

    return True

# def labeling_contrast_correction_huC_compensation(
#         dye_storage,
#         huc_storage
#         ):
#     """
#     """
#     print('Labelling correction')

#     processing.labeling.size(dye_storage)

#     dye_storage.write_step()

#     processing.thresholding.manual(
#         dye_storage,
#         100000
#         )

#     brain = dye_storage.get_current().copy()

#     larva = dye_storage.get_larvae().copy()

#     test = flip(brain, 0)

#     test = cumsum(test, 0)

#     test = flip(test, 0)

#     test = processing.thresholding.manual(
#         test,
#         1
#         )

#     test = test * larva

#     test = processing.morphology.closing(
#         test,
#         30,
#         shape='rect'
#         )

#     test = test == 0

#     test_pink = numpy2pink(test.astype('uint8'))
#     test_pink = dist(test_pink, 18)

#     test = pink2numpy(test_pink).copy()

#     processing.contrast_correction.segmentation_based_depth_dependant(
#         huc_storage,
#         deepness_map=test
#         )

#     contrast_corrected = huc_storage.get_current().copy()
#     original = huc_storage.get_original()

#     contrast_corrected[contrast_corrected == 0] = original[contrast_corrected == 0]

#     huc_storage.set_current(contrast_corrected.copy())

#     huc_storage.write_step('label_correction')

#     dye_storage.set_current(test)

#     dye_storage.write_step('test_cumsum')
# """
