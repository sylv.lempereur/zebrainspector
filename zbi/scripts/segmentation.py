"""
Perform segmentation of whole larva and white matter
"""


from numpy import ndarray

from ..handling import DataHandling

from .segmentations import (
    white_matter,
    whole_larva
    )

def segmentation(
        array: ndarray,
        debug: bool = False
        ):
    """
    Compute segmentation of whole larva and white matter on a given array

    Parameters
    ----------
    array: numpy.ndarray

    debug: bool
        (Optionnal) If True, status indication will be written in console

    Returns
    -------
    numpy.ndarray
        Segmentations.
        A grey value of 2 indicates white matter, and 1 indicates rest of the larva
    """

    datahandling_instance = DataHandling(
        array,
        printable=debug
        )

    whole_larva(datahandling_instance)

    white_matter(datahandling_instance)

    datahandling_instance.reset_bounding_box()

    print(datahandling_instance.get_larva().shape)

    print(datahandling_instance.get_current().shape)

    array_to_return = datahandling_instance.get_larva().copy().astype('uint16') * 2000
    array_to_return[datahandling_instance.get_current() != 0] = 4095

    del datahandling_instance

    return array_to_return
