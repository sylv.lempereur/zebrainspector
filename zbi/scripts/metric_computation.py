"""
This function computes segmentations metrics.
"""

from numpy import(
    array,
    ndarray,
    unique,
    where
    )
from numpy import any as npany


from openpyxl import Workbook
from openpyxl.utils  import get_column_letter

# import SimpleITK as sitk
from SimpleITK import (
    GetArrayFromImage,
    ReadImage
    )

import SimpleITK as sitk

from ..metrics.general_balanced import (
    compute_gb,
    compute_dice
    )
from ..metrics.matthew_correlation import compute_mcc

def _create_comparaison(
        path_spec: str,
        path_auto: str
        ):
    """
    Get comparaison between a manual segmetnation and an automatic one.
    Return a bounding boxed array of this comparaison

    Parameters
    ----------

    path_spec: str
        Path to the manual segmentation file

    path_auto: str
        Path to the automatic segmentation

    tolerance: int
        Tolerance for the bounding box

    Returns
    -------

    numpy.ndarray
        Bounding boxed array of the comparison
        between manual and automatic segmentations

    """
    array_spec = GetArrayFromImage(ReadImage(path_spec)).astype('uint8')
    array_auto = GetArrayFromImage(ReadImage(path_auto)).astype('uint8')
    array_comparaison = array_spec + array_auto * 2
    return array_comparaison

def _create_summary(
        book: Workbook,
        segmentations: list,
        modalities: list,
        metrics: list
        ):
    """
    Create the summary sheet of the savec workbook

    Parameters
    ----------

    book: openpyxl.Workbook
        book usde to create the summary spreadsheet

    segmentations: list
        List of performed segmentataions

    modalities: list
        Modalities of manual segmentataions

    metrics: list
        List of desired metrics

    Returns
    -------
    openpyxl.Workbook
        Workbook with the desired summary sheet.
    """

    book.create_sheet("Summary")

    for seg_index, segmentation in enumerate(segmentations):
        column_left = get_column_letter(2 + len(metrics) * seg_index)
        column_right = get_column_letter(1 + len(metrics) * (1 + seg_index))
        cells_to_merge = (
            column_left
            + str(1)
            + ':'
            + column_right
            + str(1)
            )
        book["Summary"][column_left + str(1)].value = segmentation
        book["Summary"].merge_cells(cells_to_merge)
        for metric_index, metric in enumerate(metrics):
            book["Summary"][
                (
                    get_column_letter(
                        2 + len(metrics) * seg_index + metric_index
                        )
                    + str(2)
                    )
                ].value = metric

    for mod_index, modality in enumerate(modalities):
        book["Summary"]["A" + str(mod_index + 3)].value = modality
        for seg_index, segmentation in enumerate(segmentations):
            for metric_index, metric in enumerate(metrics):
                pass

    return book


def _creation_book():
    """
    Creation of the workbook to fulfill.
    """
    segmentations = [
        'Whole larva',
        'Grey matter',
        'White matter'
        ]
    metrics = ['DSC', 'GB2', 'MCC']
    modalities = [
        '1st specialist without contrast correction',
        '1st specialist with contrast correction',
        '2nd specialist'
        ]
    book = Workbook()
    book.active.title = "Whole fish"
    book.active["A1"].value = 'Sample'
    book.active.merge_cells("A1:A2")

    for mod_index, modality in enumerate(modalities):
        column_left = get_column_letter(2 + len(metrics) * mod_index)
        column_right = get_column_letter(1 + len(metrics) * (1 + mod_index))
        cells_to_merge = (
            column_left
            + str(1)
            + ':'
            + column_right
            + str(1)
            )
        book.active[column_left + str(1)].value = modality
        book.active.merge_cells(cells_to_merge)
        for met_index, metric in enumerate(metrics):
            book.active[
                get_column_letter(
                    2 + met_index + len(metrics) * mod_index
                    ) + str(2)
                ].value = metric

    for name in segmentations[1:]:
        copy = book.copy_worksheet(book.active)
        copy.title = name

    book = _create_summary(
        book,
        segmentations,
        modalities,
        metrics)

    return book


def get_segmentation_metrics(
        list_files,
        folder_specs,
        folder_auto,
        path_output):
    """
    Write a xlsx file to store segmentation metric computations.

    Parameters
    ----------
    list_files: list
        List of file names

    folder_auto: str
        Path to folder containing automatic segmentations

    folder_spec_1: str
        Path to folder containing segmentations of the first neuroanatomist

    folder_spec_2: str
        Path to folder containing segmentations of the second neuroanatomist

    path_output: str
        Path used to save resutls of the computation

    Returns
    -------
    bool:
        Exit status
    """
    book = _creation_book()
    count = 1
    for file in list_files:
        print(file)
        book["Whole fish"]["A" + str(count + 2)].value = file
        book["Grey matter"]["A" + str(count + 2)].value = file
        book["White matter"]["A" + str(count + 2)].value = file
        metrics_ms, metrics_sl = compute_coefficients_whole_larva(
            file,
            folder_specs,
            folder_auto
            )
        column_index = 2
        for metric_result in metrics_ms:
            book['Whole fish'][
                get_column_letter(column_index)+ str(count +2)
                ].value = metric_result
            column_index += 1
        for metric_result in metrics_sl:
            book['Whole fish'][
                get_column_letter(column_index)+ str(count +2)
                ].value = metric_result
            column_index += 1

        metrics = compute_coefficients_white_matter(
            file,
            folder_specs,
            folder_auto
            )

        column_index = 2
        for metric_result in metrics[0]:
            book['White matter'][
                get_column_letter(column_index)+ str(count +2)
                ].value = metric_result
            column_index += 1
        for metric_result in metrics[2]:
            book['White matter'][
                get_column_letter(column_index)+ str(count +2)
                ].value = metric_result
            column_index += 1

        count += 1

    book['Summary']['B3'] = (
        "= Median('Whole fish'!B3:B"
        + str(count + 1)
        + ")"
        )

    book['Summary']['C3'] = (
        "= Median('Whole fish'!C3:C"
        + str(count + 1)
        + ")"
        )

    book['Summary']['D3'] = (
        "= Median('Whole fish'!D3:D"
        + str(count + 1)
        + ")"
        )

    book['Summary']['B4'] = (
        "= Median('Whole fish'!E3:E"
        + str(count + 1)
        + ")"
        )

    book['Summary']['C4'] = (
        "= Median('Whole fish'!F3:F"
        + str(count + 1)
        + ")"
        )

    book['Summary']['D4'] = (
        "= Median('Whole fish'!G3:G"
        + str(count + 1)
        + ")"
        )

    book['Summary']['B5'] = (
        "= Median('Whole fish'!H3:J"
        + str(count + 1)
        + ")"
        )

    book['Summary']['C5'] = (
        "= Median('Whole fish'!I3:J"
        + str(count + 1)
        + ")"
        )

    book['Summary']['D5'] = (
        "= Median('Whole fish'!J3:J"
        + str(count + 1)
        + ")"
        )

    #Grey matter

    book['Summary']['E3'] = (
        "= Median('Grey matter'!B3:B"
        + str(count + 1)
        + ")"
        )

    book['Summary']['F3'] = (
        "= Median('Grey matter'!C3:C"
        + str(count + 1)
        + ")"
        )

    book['Summary']['G3'] = (
        "= Median('Grey matter'!D3:D"
        + str(count + 1)
        + ")"
        )

    book['Summary']['E4'] = (
        "= Median('Grey matter'!E3:E"
        + str(count + 1)
        + ")"
        )

    book['Summary']['F4'] = (
        "= Median('Grey matter'!F3:F"
        + str(count + 1)
        + ")"
        )

    book['Summary']['G4'] = (
        "= Median('Grey matter'!G3:G"
        + str(count + 1)
        + ")"
        )

    book['Summary']['E5'] = (
        "= Median('Grey matter'!H3:J"
        + str(count + 1)
        + ")"
        )

    book['Summary']['F5'] = (
        "= Median('Grey matter'!I3:J"
        + str(count + 1)
        + ")"
        )

    book['Summary']['G5'] = (
        "= Median('Grey matter'!J3:J"
        + str(count + 1)
        + ")"
        )

    #White matter

    book['Summary']['H3'] = (
        "= Median('White matter'!B3:B"
        + str(count + 1)
        + ")"
        )

    book['Summary']['I3'] = (
        "= Median('White matter'!C3:C"
        + str(count + 1)
        + ")"
        )

    book['Summary']['J3'] = (
        "= Median('White matter'!D3:D"
        + str(count + 1)
        + ")"
        )

    book['Summary']['H4'] = (
        "= Median('White matter'!E3:E"
        + str(count + 1)
        + ")"
        )

    book['Summary']['I4'] = (
        "= Median('White matter'!F3:F"
        + str(count + 1)
        + ")"
        )

    book['Summary']['J4'] = (
        "= Median('White matter'!G3:G"
        + str(count + 1)
        + ")"
        )

    book['Summary']['H5'] = (
        "= Median('White matter'!H3:J"
        + str(count + 1)
        + ")"
        )

    book['Summary']['I5'] = (
        "= Median('White matter'!I3:J"
        + str(count + 1)
        + ")"
        )

    book['Summary']['J5'] = (
        "= Median('White matter'!J3:J"
        + str(count + 1)
        + ")"
        )

    book.save(path_output)
    return True

def bounding_box(
        array_unboxed,
        tolerance
        ):
    """
    Compute bounding box with a tolerance of objects in an array and

    Parameters
    ----------

    array: ndarray
        Array to reduce. This array must not be empty.

    tolerance: int
        Number of pixels to add arounf the computed bounding box.
        if addition of the tolerance give higher values than array size,
        the returned array will be limited to array edges.

    Returns
    -------
    ndarray
        Reduced array
    """
    bounding_box_position = []
    for axis in range(len(array_unboxed.shape)):
        axis_projection = list(range(len(array_unboxed.shape)))
        axis_projection.pop(axis)
        projection = [0, 0]
        projection[0] = where(
            npany(
                array_unboxed, tuple(axis_projection)
                )
            )[0][0]
        projection[1] = where(
            npany(
                array_unboxed, tuple(axis_projection)
                )
            )[0][-1]
        projection[0] -= tolerance
        if projection[0] < 0:
            projection[0] = 0
        projection[1] += tolerance
        if projection[1] > array_unboxed.shape[axis] -1:
            projection[1] = array_unboxed.shape[axis] -1
        bounding_box_position.append(slice(projection[0], projection[1]))
    bounding_box_position = tuple(bounding_box_position)
    return array_unboxed[bounding_box_position].copy()

def compute_coefficients(
        array_comparaison: ndarray
        ):
    """
    Compute following segmentation metrics:
    - Dice-Sorenson coefficient
    - General balanced coefficient with delta = 2
    - Matthew correlation coefficient

    Parameters
    ----------
    array_comparaison: numpy.ndarray
        Array containing the comparaison
        between 1 ground truth and 1 automatic segmentation.

    Returns
    -------
    list:
        asked metrics in this order:
            - DSC
            - GB2
            - MCC
    """
    metrics = []
    values, pixel_set = unique(
        array_comparaison,
        return_counts=True
        )
    if len(values) < 4:
        for value in range(0, 4):
            if not value in values:
                list_values = values.tolist()
                list_pixel_set = pixel_set.tolist()
                list_values.insert(value, value)
                list_pixel_set.insert(value, 0)
                values = array(list_values)
                pixel_set = array(list_pixel_set)
    metrics.append(compute_dice(pixel_set=pixel_set))
    metrics.append(compute_gb(pixel_set=pixel_set, delta=2))
    metrics.append(compute_mcc(pixel_set=pixel_set))
    return metrics


def compute_coefficients_white_matter(
        file,
        folder_specs,
        folder_auto
        ):
    """
    Compute segmentation metrics for white matter segmentation

    Parameters
    ----------
    file: str
        Name of the file

    folder_spec_1: str
        Path to the folder that contains manual segmentation
        of the first specialist

    folder_spec_2: str
        Path to the folder that contains manual segmentation
        of the second specialist

    folder_auto: str
        Path to the folder that contains
        automatic segmentation of the whole larva

    returns
    -------
    list:
        Metrics against manual segmentations for the first specialist

    list:
        Metrics against manual segmentations for the second specialist
    """
    paths = {
        "path auto": (
            folder_auto
            + file
            + "/"
            + file
            + '_C00_white_matter_2.mha'
            ),
        "path spec 1 before": (
            folder_specs[0]
            + file
            + '_C00.labels.mha'
            ),
        "path spec 1 after": (
            folder_specs[0]
            + file
            + '_C00.labels.mha'
            ),
        "path spec 2": (
            folder_specs[1]
            + file
            + '_C00.labels.mha'
            )
        }

    # 1st specialist comparison

    print("1st specialist WM")

    print("Before")

    array_comparaison_bounded_boxed = _create_comparaison(
        paths["path spec 1 before"],
        paths["path auto"]
        )
    image = sitk.GetImageFromArray(array_comparaison_bounded_boxed)
    image.SetSpacing(sitk.ReadImage(paths["path spec 1 before"]).GetSpacing())
    sitk.WriteImage(image, folder_auto + file + '/' + file +'compSegs.mha')


    metrics_ms_bf = compute_coefficients(array_comparaison_bounded_boxed)

    print("After")
    array_comparaison_bounded_boxed = _create_comparaison(
        paths["path spec 1 after"],
        paths["path auto"]
        )
    metrics_ms_af = compute_coefficients(array_comparaison_bounded_boxed)

    # 2nd specialist comparison

    print("2nd specialist WM")

    array_comparaison_bounded_boxed = _create_comparaison(
        paths["path spec 2"],
        paths["path auto"]
        )
    metrics_sl = compute_coefficients(array_comparaison_bounded_boxed)

    return metrics_ms_bf, metrics_ms_af, metrics_sl

def compute_coefficients_whole_larva(
        file,
        folder_specs,
        folder_auto
        ):
    """
    Compute segmentation metrics for the segmentation of the whole larva.

    Parameters
    ----------
    file: str
        Name of the file

    folder_spec_1: str
        Path to the folder that contains manual segmentation
        of the first specialist

    folder_spec_2: str
        Path to the folder that contains manual segmentation
        of the second specialist

    folder_auto: str
        Path to the folder that contains
        automatic segmentation of the whole larva

    returns
    -------
    list:
        Metrics against manual segmentations for the first specialist

    list:
        Metrics against manual segmentations for the second specialist
    """
    print(file)
    paths = {
        "path auto": (
            folder_auto
            + file
            + '/'
            + file
            + '_C00_larva.mha'
            ),
        "path spec 1": (
            folder_specs[0]
            + file
            + '_larva.labels.mha'
            ),
        "path spec 2": (
            folder_specs[1]
            + file
            + "_larva.labels.mha"
            )
        }

    # 1st specialist comparison

    print("1st specialist larva")
    array_comparaison_bounded_boxed = _create_comparaison(
        paths["path spec 1"],
        paths["path auto"]
        )
    metrics_ms = compute_coefficients(array_comparaison_bounded_boxed)

    # 2nd specialist comparison

    print("2nd specialist larva")

    array_comparaison_bounded_boxed = _create_comparaison(
        paths["path spec 2"],
        paths["path auto"]
        )
    metrics_sl = compute_coefficients(array_comparaison_bounded_boxed)

    return metrics_ms, metrics_sl
