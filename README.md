# ZeBraInspector

ZeBraInspector is a free tool for automatic analysis of 5 days-post-fertilisation (dpf) zebrafish larvae allowing:
* realignement of the samples for a better comparison between them following screenshot captures;
* whole larva and white matter segmentation requesting a lipophilic dye staining;
* automatic creation of an excel sheet including results of the volumetric analysis.

Citation: [ZeBraInspector, a whole organism screening platform enabling volumetric analysis of zebrafish brain white matter](https://www.biorxiv.org/content/10.1101/2020.10.26.353656v1?rss=1)
Lempereur S, Machado E, Licata F, Buzer L, Robineau I, Hémon J, Banerjee P, De Crozé N, Léonard M, Affaticati P, Jenett A, Talbot H, Joly JS.
doi: https://doi.org/10.1101/2020.10.26.353656 

License: GNU General Public License
Question and/or inquiries: zbi@tefor.net

## Installation
### Using cpecomputed binaries

A Precomputed binary file is available on the [tefor website](https://tefor.net/portfolio/zebrainspector).
This binary file allows to performed every analysis avaialble using the ZeBraInspector software.
However, installation of a C++ compiler is mandatory.
For windows users, links to compliant Microsoft Visual C++ versions are available.

### Using source
Following cloning of this repository, the software could be installed using pip and the following command lines

``` 
clone git@gitlab.com:sylv.lempereur/zebrainspector.git
cd zebrainspector
pip install .
``` 

The software could be launch using the following command line:

```
python ZeBraInspector.py
```

## User guidelines

A user guideline is available on [tefor website](https://tefor.net/portfolio/zebrainspector#guidelines).

This websire also contains [test datasets ](https://tefor.net/portfolio/zebrainspector#packages).
