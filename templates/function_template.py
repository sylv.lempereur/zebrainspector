# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file is a template to write function in the core folder.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from X*.handling import DataHandling # change X*. to the correct amount of .

def _function_array(
        array: ndarray
        ):
    """
    Description of this function on an numpy.ndarray.
    This funciton will be the computation function when this function is called.

    Parameters
    ----------
    array: numpy.ndarray
        A binary array

    Returns
    -------
    numpy.ndarray
        Result of the computation of the distance map
    """

    #Traitement
    return array

def _function_data_handling(
        data_handling_instance: DataHandling
        ):
    """
    Extratction of the current array in a DataHandling instance
    to compute the function.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class
    """
    data_handling_instance.add_step("distanceMap")
    array = _function_array(data_handling_instance.get_current())
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def function(data):
    """
    Call of the function. Will return the computed array if an array id provided, or a True status if a DataHandling instance is provided.

    Parameters
    ----------
    data:
        Data use to perform the closing.
        could be a numpy.ndarray or a DataHandling instance
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _function_array(data)
    elif isinstance(data,
                    DataHandling
                    ):
        out = _function_data_handling(data)
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
