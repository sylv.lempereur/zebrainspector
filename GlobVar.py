import os


class GlobVar:


    #########################################################################

    LThickness = [4, 8, 12, 24, 40]     # donne le nombre de couches à cumuler



    VueDorsale       = "VueDorsale"
    VueTransverse    = "VueTransverse"
    VueSagittale     = "VueSagittale"

    VueShortName     = { VueDorsale : "H", VueTransverse : "T", VueSagittale : "S"}


    TailleFenetreDemarrage = 0.8

    CurrentDir = os.getcwd()

    LicenceInfo = "This software was developed by Lilian Buzer and Sylvain Lempereur\nfollowing requests from the TEFOR Paris-Saclay team."

    MaxChannels = 3

    MaxCol = 3

    MaxRow = 6

    MaxFishPerPage = MaxCol * MaxRow

    MaxFishes     = 99

    SegFolder = ""

    AppIcon = None

    TitleApp = "ZeBraInspector v1.3.7"

    ChannelShortName = "CHA"

    



