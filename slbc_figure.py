from zbi import processing
from SimpleITK import (
	ReadImage,
	GetArrayFromImage,
	GetImageFromArray,
	WriteImage
	)
from numpy import unique

PATH_REF = r"D:\work\SL-Data-Test\WT\raw\Chosen_ones\200108Fa_2144a_5dpf_SL-193-DR_512_Nh_merge_C01.mha"
PATH_SEG = r"H:\work\data\AB-DiO-HuC\AB-DiO-HuC-1_C000_segmentation.mha"

print("Load HuC image")

image = ReadImage(PATH_REF)
spacing = image.GetSpacing()
array_ref = GetArrayFromImage(image)

del image

print("Get whole larva segmentation")

image = ReadImage(PATH_SEG)
spacing = image.GetSpacing()
array_larva = GetArrayFromImage(image)
array_larva = array_larva == 4095

array_larva = processing.morphology.closing(
	array_larva,
	150,
	"rect"
	)
bounding_box = processing.detection.bounding_box_positions(array_larva, tolerance = 20)

print(bounding_box)

array_ref_bounded = array_ref[
	bounding_box[0][0] : bounding_box[0][1],
	bounding_box[1][0] : bounding_box[1][1],
	bounding_box[2][0] : bounding_box[2][1],
	].copy()
array_larva_bounded = array_larva[
	bounding_box[0][0] : bounding_box[0][1],
	bounding_box[1][0] : bounding_box[1][1],
	bounding_box[2][0] : bounding_box[2][1],
	].copy()

print(array_ref.shape)
print(array_larva.shape)
print(array_ref_bounded.shape)
print(array_larva_bounded.shape)

print("Compute level ligne")

image = GetImageFromArray(array_ref_bounded)
image.SetSpacing(spacing)
WriteImage(image, r"H:\work\data\AB-DiO-HuC\AB-DiO-HuC-1_C001_bounded.mha")

image = GetImageFromArray(array_larva_bounded.astype('uint8'))
image.SetSpacing(spacing)
WriteImage(image, r"H:\work\data\AB-DiO-HuC\AB-DiO-HuC-1_C001_larva_bounded.mha")

# array_level = processing.morphology.dist(
# 	processing.basics.inversion(array_larva_bounded)
# 	)
# print(unique(array_level))

# image = GetImageFromArray(array_level)
# image.SetSpacing(spacing)
# WriteImage(image, r"H:\work\data\AB-DiO-HuC\AB-DiO-HuC-1_C001_dist_bounded.mha")

# print("Correction")

# array = processing.contrast_correction.segmentation_based_depth_dependant(
# 	array_ref_bounded,
# 	array_level.astype('uint32')
# 	)

# image = GetImageFromArray(array)
# image.SetSpacing(spacing)
# WriteImage(image, r"H:\work\data\AB-DiO-HuC\AB-DiO-HuC-1_C001_corrected_bounded.mha")